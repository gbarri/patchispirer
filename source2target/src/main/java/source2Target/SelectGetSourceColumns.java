package source2Target;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jsqlparser.expression.Alias;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.expression.operators.relational.IsNullExpression;
import net.sf.jsqlparser.expression.operators.relational.ItemsList;
import net.sf.jsqlparser.expression.operators.relational.ItemsListVisitor;
import net.sf.jsqlparser.expression.operators.relational.MultiExpressionList;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.AllColumns;
import net.sf.jsqlparser.statement.select.AllTableColumns;
import net.sf.jsqlparser.statement.select.FromItem;
import net.sf.jsqlparser.statement.select.FromItemVisitor;
import net.sf.jsqlparser.statement.select.Join;
import net.sf.jsqlparser.statement.select.LateralSubSelect;
import net.sf.jsqlparser.statement.select.ParenthesisFromItem;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.SelectBody;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;
import net.sf.jsqlparser.statement.select.SelectItemVisitor;
import net.sf.jsqlparser.statement.select.SelectVisitorAdapter;
import net.sf.jsqlparser.statement.select.SetOperationList;
import net.sf.jsqlparser.statement.select.SubJoin;
import net.sf.jsqlparser.statement.select.SubSelect;
import net.sf.jsqlparser.statement.select.TableFunction;
import net.sf.jsqlparser.statement.select.ValuesList;
import net.sf.jsqlparser.statement.select.WithItem;

/**
 * classe pensata per il popolamento di una SourceTargetRelationship, per 1 singolo statement di INSERT o UPDATE
 * @author g.barri
 *
 */
public class SelectGetSourceColumns extends SelectVisitorAdapter implements FromItemVisitor, ItemsListVisitor, SelectItemVisitor{
	
	static Logger logger=LoggerFactory.getLogger(SelectGetSourceColumns.class);
	
	public SourceTargetRelationship sourTarg;
	private int deepnes;
	private String currentSubSelectAlias=null;
	
	public SelectGetSourceColumns(int deepnes) {
		sourTarg=new SourceTargetRelationship();
		this.deepnes=deepnes;
	}
	
	public SelectGetSourceColumns() {
		this(0);
	}
	
	public SourceTargetRelationship getSourTarg() {
		return sourTarg;
	}
	
//	/**
//	 * deve capire se le due stringhe stanno indicando la stessa sorgente tra una select e una subselect
//	 * @param currentSource
//	 * @param newSourceItem
//	 * @return
//	 */
//	private boolean areLinked(String currentSource, String newSourceItem) {
//		return currentSource.equals(newSourceItem) || currentSource.equals(currentSubSelectAlias+"."+newSourceItem);
//	}

	
	@Override
    public void visit(PlainSelect plainSelect) {
		deepnes++;
		List<SelectItem> itemList = plainSelect.getSelectItems();
		for(int i=0; i<itemList.size(); i++) {
			itemList.get(i).accept(this); //popola i campi reference e colNamesList
		}
		plainSelect.getFromItem().accept(this);
		deepnes--;
    }
	
    @Override
    public void visit(SetOperationList setOpList) {
    	List<SelectBody> selBdList =setOpList.getSelects();
    	for(SelectBody selItem : selBdList) {
    		selItem.accept(this);
    	}
    }

    @Override
    public void visit(WithItem withItem) {
    	logger.error(" found a WithItem statement while looking for a select, "+withItem);
    	throw new UnsupportedOperationException();
    }

    @Override
    public void visit(Table table) {
	 	logger.trace("visiting: "+table);
	 	sourTarg.listSourceName.add(table.getFullyQualifiedName());
    }

    @Override
    public void visit(SubSelect subSelect) {
    	logger.trace("visiting: "+subSelect);
    	if(subSelect.getAlias()!=null) {
    		currentSubSelectAlias=subSelect.getAlias().getName();
    	}else {
    		currentSubSelectAlias=null;
    	}
    	subSelect.getSelectBody().accept(this);
    	currentSubSelectAlias=null;
    }

    @Override
    public void visit(SubJoin subjoin) {
    	logger.trace("visiting: "+subjoin);
    	List<Join> joinlist = subjoin.getJoinList();
    	for(Join jitem:joinlist) {
    		jitem.getRightItem().accept(this);
    	}
    }

    @Override
    public void visit(LateralSubSelect lateralSubSelect) {
    	logger.trace("visiting: "+lateralSubSelect);
    	lateralSubSelect.getSubSelect().accept((FromItemVisitor)this);
    }

    @Override
    public void visit(ValuesList valuesList) {
    	logger.trace("visiting: "+valuesList);
    	throw new UnsupportedOperationException("Encountered a valueList when I wish I only had to work with from *table*");
    }

    @Override
    public void visit(TableFunction valuesList) {
    	logger.trace("visiting: "+valuesList);
    	//forse aggiungere direttamente la stringa nell'elenco e bon.
    	throw new UnsupportedOperationException("Encountered a valueList when I wish I only had to work with from *table*");
    }

    @Override
    public void visit(ParenthesisFromItem aThis) {
    	logger.trace("visiting: "+aThis);
        aThis.accept(this);
    }

    /**
     * retrieve the names of the sources used
     * @return
     */
	public List<String> getSources() {
		return sourTarg.getListSourceName();
	}

	public void addTargetColumns(List<String> targetColNames) {
		for(String colName: targetColNames) {
	 		sourTarg.addTargetColumn(colName);
	 	}
	}

	/**
	 * dovrebbe essere usato quando trovo dei values
	 */
	@Override
	public void visit(ExpressionList expressionList) {
		List<Expression> expressions=expressionList.getExpressions();
		for(int indexpre=0; indexpre<expressions.size(); indexpre++) {
			List<String>sourceColNames=new ArrayList<>();
			Expression expr=expressions.get(indexpre);
			if(!(expr instanceof Column)) {
				//expression non semplice
				//TODO da flaggare
			}
//			ColumnsNamesFinder extractor=new ColumnsNamesFinder();
			List<Column> tmpColList=new ColumnsNamesFinder().getColumnsList(expr);
			if(tmpColList.size()==0) {
				sourceColNames.add("BGEXPR<<<"+expr.toString());
			}else {
				for(Column col:tmpColList) {
					sourceColNames.add(col.getFullyQualifiedName());
				}
			}
			sourTarg.addNewMapping(sourceColNames);
		}
	}

	@Override
	public void visit(MultiExpressionList multiExprList) {
		List<ExpressionList> tmpList=multiExprList.getExprList();
		for(ExpressionList exprLst:tmpList) {
			exprLst.accept(this);
		}	
	}
	////////////////////////////
	//ex parte di selctItemgetSourceColumns
	
	@Override
    public void visit(SelectExpressionItem item) {
		Expression expression=item.getExpression();
		if(!(expression instanceof Column)) {
			//questa espressione NON E' una espressione semplice
			//TODO in futuro andrà gestita la cosa
		}
		ColumnsNamesFinder cnf=new ColumnsNamesFinder();
		List<Column> listColumn=cnf.getColumnsList(expression);
		List<String> listColNames=new ArrayList<>();
		//poco preciso
		if(listColumn.size()==0) {
			listColNames.add("BGEXPR<<<"+expression.toString());
		}else {
			for(Column col:listColumn) {
				listColNames.add(col.getFullyQualifiedName());
			}
		}
		Alias alias=item.getAlias();
		String reference=null;
		if(alias!=null) {
			reference=alias.getName();
		}else if(expression instanceof Column){
			reference=((Column) expression).getFullyQualifiedName();
		}else {
			IllegalArgumentException e=new IllegalArgumentException("Sembra che in "+item+" vi sia una espressione che non è una colonna semplice senza un alias esplcito");
			logger.error("Intercepted exception in visit(SelectExpressionItem)", e);
			throw e;
		}
		if(deepnes==1) {
			//siamo alla prima select, quindi nel mapping NON ho ancora nessuna source. 
			sourTarg.addNewMapping(listColNames);
		}else {
			//non è la prima select, quindi i valori che leggo devono essere già stati chiesti come output tramite il loro nome o l'alias, altrimenti sono scartati
			sourTarg.updateMapping(reference, listColNames, currentSubSelectAlias);
		}
    }
	
	 @Override
	 public void visit(AllColumns columns) {
		 logger.error("we ended up in a AllColumns statement!! "+columns);
		 throw new UnsupportedOperationException();
	 }

	 @Override
	 public void visit(AllTableColumns columns) {
		 logger.error("we ended up in a AllTableColumns statement!! "+columns);
		 throw new UnsupportedOperationException();
     }
	 
}
