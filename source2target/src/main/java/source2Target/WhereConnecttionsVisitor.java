package source2Target;

import java.util.ArrayList;
import java.util.List;

import org.tinylog.Logger;

import net.sf.jsqlparser.expression.Alias;
import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.ExpressionVisitorAdapter;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.Between;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.GreaterThanEquals;
import net.sf.jsqlparser.expression.operators.relational.InExpression;
import net.sf.jsqlparser.expression.operators.relational.IsNullExpression;
import net.sf.jsqlparser.expression.operators.relational.LikeExpression;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;
import net.sf.jsqlparser.expression.operators.relational.MultiExpressionList;
import net.sf.jsqlparser.expression.operators.relational.NotEqualsTo;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;

public class WhereConnecttionsVisitor extends ExpressionVisitorAdapter{
	 
	 public ExposedWithSources exposed;
	 
	 protected void visitBinaryExpression(BinaryExpression expr) {
	    expr.getLeftExpression().accept(this);
	    expr.getRightExpression().accept(this);
	 }
	 
	 
	 public WhereConnecttionsVisitor() {
		super();
	 	this.exposed = new ExposedWithSources();
	 }
	 
	 public WhereConnecttionsVisitor(ExposedWithSources exposed) {
			super();
		 	this.exposed = exposed;
     }



	@Override
	 public void visit(EqualsTo expr) {
		 String coLeft=null;
		 String colRight=null;
		 boolean leftAlright=true, rightAlright=true;
		 ColumnsNamesFinder finder=new ColumnsNamesFinder();
		 List<Column> coLeftList=finder.getColumnsList(expr.getLeftExpression());
		 if(coLeftList.size()!=1) {
			 Logger.error("Failed to obtain the left columns of "+expr);
			 leftAlright=false;
		 }else {
			 coLeft=coLeftList.get(0).getFullyQualifiedName();
		 }
		 finder=new ColumnsNamesFinder();
		 List<Column> colRightList=finder.getColumnsList(expr.getRightExpression());
		 if(colRightList.size()!=1) {
			 Logger.error("Failed to obtain the right columns of "+expr);
			 rightAlright=false;
		 }else {
			 colRight=colRightList.get(0).getFullyQualifiedName();
		 }
		 if(leftAlright && rightAlright) {
			 List<String> colRightNames=new ArrayList<>();
			 colRightNames.add(colRight);
			 exposed.addNewMapping(coLeft, colRightNames);
		 }
	 }

	 @Override
	 public void visit(NotEqualsTo expr) {
		 String coLeft=null;
		 String colRight=null;
		 boolean leftAlright=true, rightAlright=true;
		 ColumnsNamesFinder finder=new ColumnsNamesFinder();
		 List<Column> coLeftList=finder.getColumnsList(expr.getLeftExpression());
		 if(coLeftList.size()!=1) {
			 Logger.error("Failed to obtain the left columns of "+expr);
			 leftAlright=false;
		 }else {
			 coLeft=coLeftList.get(0).getFullyQualifiedName();
		 }
		 finder=new ColumnsNamesFinder();
		 List<Column> colRightList=finder.getColumnsList(expr.getRightExpression());
		 if(colRightList.size()!=1) {
			 Logger.error("Failed to obtain the right columns of "+expr);
			 rightAlright=false;
		 }else {
			 colRight=colRightList.get(0).getFullyQualifiedName();
		 }
		 if(leftAlright && rightAlright) {
			 List<String> colRightNames=new ArrayList<>();
			 colRightNames.add(colRight);
			 exposed.addNewMapping(coLeft, colRightNames);
		 }
	 }

}
