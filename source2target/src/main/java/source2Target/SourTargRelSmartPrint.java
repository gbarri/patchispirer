package source2Target;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.tinylog.Logger;

import utilities.StringUtilities;

public class SourTargRelSmartPrint extends SourceTargetRelationship {
	@Override
	public String toTabString() {
		return toTabString("");
	}
	
	@Override
	public String toTabString(String prefix) {
		String textDescription="";
		for(String key:super.getTargetCol2sourceCol().keySet()) {
			List<String> colSourceList=super.getTargetCol2sourceCol().get(key);
			List<String> sourcesList=super.getListSourceName();
			List<String> sourceColumnsName=new ArrayList<>();
			for(String columName:colSourceList) {
				for(String sourceName:sourcesList) {
					if(StringUtils.startsWithIgnoreCase(columName, StringUtilities.detectNameInObjectName(sourceName)+".")) {
						columName=StringUtils.replaceIgnoreCase(columName, StringUtilities.detectNameInObjectName(sourceName)+".", "");//  columName.replaceFirst(regex, replacement)(sourceName+".", "");
						break;
					}
				}
				sourceColumnsName.add(columName);
			}
			String sourcesColumnsString="";
			if(sourceColumnsName.size()>0) {
				String name=sourceColumnsName.get(0);
				if(name.contains(".") && !name.startsWith("BGEXPR<<<")) {
					int lastPoint=name.lastIndexOf('.');
					name=name.substring(lastPoint+1);
				}
				sourcesColumnsString+=name;
			}
			for(int i=1; i<sourceColumnsName.size(); i++) {
				String name=sourceColumnsName.get(i);
				if(name.contains(".") && !name.startsWith("BGEXPR<<<")) {
					int lastPoint=name.lastIndexOf('.');
					name=name.substring(lastPoint+1);
				}
				sourcesColumnsString+=","+name;
			}
			String targetValues=";";
			String schemaTargetTable=StringUtilities.detectSchemaInObjectName(getTargetName()).toLowerCase();
			String nameTargetTable=StringUtilities.detectNameInObjectName(getTargetName());
			if(schemaTargetTable!=null && nameTargetTable!=null) {
				targetValues=schemaTargetTable.toLowerCase()+";"+nameTargetTable.toLowerCase();
			}
			String sourceValues=";";
			String schemaSource=null, nameSource=null;
			if(this.getListSourceName().size()==1){
				schemaSource=StringUtilities.detectSchemaInObjectName(getSourcesNamesToString());
				nameSource=StringUtilities.detectNameInObjectName(getSourcesNamesToString());
			}else if(this.getListSourceName().size()>1) {
				boolean multipleSchemas=false;
				List<String> tmpListSources=this.getListSourceName();
				schemaSource=StringUtilities.detectSchemaInObjectName(tmpListSources.get(0));
				nameSource=StringUtilities.detectNameInObjectName(tmpListSources.get(0));
				for(int i=1; i<tmpListSources.size(); i++) {
					String tmpSchema=StringUtilities.detectSchemaInObjectName(tmpListSources.get(i));
					nameSource+=", "+StringUtilities.detectNameInObjectName(tmpListSources.get(i));
					if(!StringUtils.equalsIgnoreCase(schemaSource, tmpSchema)) {
						multipleSchemas=true;
						break;
					}
				}
				if(multipleSchemas) {
					schemaSource="";
					nameSource=tmpListSources.get(0);
					for(int i=1; i<tmpListSources.size(); i++) {		
						nameSource+=", "+tmpListSources.get(i);
					}
				}
			}
			if(schemaSource!=null && nameSource!=null) {
				sourceValues=schemaSource.toLowerCase()+";"+nameSource.toLowerCase();
			}else {
				;
			}
			if(key.contains(".")) {
				int latPoint=key.lastIndexOf('.');
				key=key.substring(latPoint+1);
			}
			textDescription+=prefix.replaceAll("\t",";")+";"+sourceValues+";"+sourcesColumnsString+";"+targetValues+";"+key+"\r\n";
		}
		return textDescription;
	}
}
