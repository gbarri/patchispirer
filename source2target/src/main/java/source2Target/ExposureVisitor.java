package source2Target;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Alias;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.expression.operators.relational.IsNullExpression;
import net.sf.jsqlparser.expression.operators.relational.ItemsList;
import net.sf.jsqlparser.expression.operators.relational.ItemsListVisitor;
import net.sf.jsqlparser.expression.operators.relational.MultiExpressionList;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.AllColumns;
import net.sf.jsqlparser.statement.select.AllTableColumns;
import net.sf.jsqlparser.statement.select.ExceptOp;
import net.sf.jsqlparser.statement.select.FromItem;
import net.sf.jsqlparser.statement.select.FromItemVisitor;
import net.sf.jsqlparser.statement.select.IntersectOp;
import net.sf.jsqlparser.statement.select.Join;
import net.sf.jsqlparser.statement.select.LateralSubSelect;
import net.sf.jsqlparser.statement.select.MinusOp;
import net.sf.jsqlparser.statement.select.ParenthesisFromItem;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.SelectBody;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;
import net.sf.jsqlparser.statement.select.SelectItemVisitor;
import net.sf.jsqlparser.statement.select.SelectVisitorAdapter;
import net.sf.jsqlparser.statement.select.SetOperation;
import net.sf.jsqlparser.statement.select.SetOperationList;
import net.sf.jsqlparser.statement.select.SubJoin;
import net.sf.jsqlparser.statement.select.SubSelect;
import net.sf.jsqlparser.statement.select.TableFunction;
import net.sf.jsqlparser.statement.select.UnionOp;
import net.sf.jsqlparser.statement.select.ValuesList;
import net.sf.jsqlparser.statement.select.WithItem;
import utilities.Initializer;
import utilities.RepoSearcher;
import utilities.SQLSearcher;
import utilities.TableUtilities;
import utilities.ViewUtilities;

/**
 * classe pensata per il popolamento di una SourceTargetRelationship, per 1 singolo statement di INSERT o UPDATE
 * @author g.barri
 *
 */
public class ExposureVisitor extends SelectVisitorAdapter implements FromItemVisitor, ItemsListVisitor, SelectItemVisitor{
	
	static Logger logger=LoggerFactory.getLogger(ExposureVisitor.class);
	
	public ExposedWithSources expSou;
	public ExposedWithSources oldExpSou;
	protected String currentSubSelectAlias=null;
	protected File fileUnderStudy;
	
	
	public ExposureVisitor(int deepnes) {
		expSou=new ExposedWithSources();
		this.oldExpSou=new ExposedWithSources();
	}
	
	public ExposureVisitor(File file) {
		this(0);
		this.fileUnderStudy=file;
	}
	
	private ExposureVisitor(ExposureVisitor expVisitor) {
		this(expVisitor.fileUnderStudy);
		
	}
	
	public ExposedWithSources getExpSou() {
		return expSou;
	}
	
	@Override
    public void visit(PlainSelect plainSelect) {
//		deepnes++;
		logger.debug("Plainselect: "+plainSelect);
		ExposureVisitor expVisitor=new ExposureVisitor(this);
		FromItem fromItem=plainSelect.getFromItem();
		if(fromItem==null) {
			logger.warn("PlainSelect\t"+plainSelect+"\tdoes not have a from expression: probable loop");
			//TODO verificare cosa posso fare per gestire questo
		}else {
			fromItem.accept(expVisitor);
		}
		List<Join> joins=plainSelect.getJoins();
		if(joins!=null) {
			for(Join join:joins) {
				FromItem rightItem=join.getRightItem();
				rightItem.accept(expVisitor);
			}
		}
		oldExpSou=expVisitor.getExpSou();
		logger.debug("in plainselect "+plainSelect+" exposed from FROM clause: "+oldExpSou);
		List<SelectItem> itemList = plainSelect.getSelectItems();
		for(int i=0; i<itemList.size(); i++) {
			itemList.get(i).accept(this); //popola i campi reference e colNamesList
		}
		expSou.addListSourceName(oldExpSou.getListSourceName());
		oldExpSou=expSou;
//		deepnes--;
    }
	
    @Override
    public void visit(SetOperationList setOpList) {
    	List<SetOperation> operations = setOpList.getOperations();
    	List<SelectBody> selBdList =setOpList.getSelects();
    	if(selBdList.size()>0) {
    		SelectBody selItem = selBdList.get(0);
    		selItem.accept(this);
    	}
    	for(int i=1; i<selBdList.size(); i++) {
    		SetOperation setOp=operations.get(i-1);
    		if(setOp instanceof ExceptOp) {
    			
    		}else if(setOp instanceof IntersectOp) {
    			
       		}else if(setOp instanceof MinusOp) {
       			
       		}else if(setOp instanceof UnionOp) {
        		SelectBody selItem = selBdList.get(i);
        		ExposureVisitor tmpVisitor=new ExposureVisitor(this);
        		selItem.accept(tmpVisitor);
        		ExposedWithSources expSources= tmpVisitor.getExpSou();
        		if(expSources.getExposedColumns().size()!=this.getExpSou().getExposedColumns().size()) {
        			logger.error("We found an union with a different numbers of exposed columns!! file "+this.fileUnderStudy.getAbsolutePath());
        		}else {
        			List<String> firstQueryExposedColumns=this.getExpSou().getExposedColumns();
        			List<String> secondQueryExposedColumns=tmpVisitor.getExpSou().getExposedColumns();
        			for(int indColumn=0; indColumn<firstQueryExposedColumns.size(); indColumn++) {
        				this.expSou.addNewMapping(firstQueryExposedColumns.get(indColumn), tmpVisitor.getExpSou().getExposed2sources().get(secondQueryExposedColumns.get(indColumn)));
        			}
        		}
       		}
    	}
    	logger.debug("After set of operations, exposed links are: "+this.expSou);
    }

    @Override
    public void visit(WithItem withItem) {
    	logger.error(" found a WithItem statement while looking for a select, "+withItem);
    	throw new UnsupportedOperationException();
    }

    private String fullyQualifyTableName(Table table) {
    	String tableName=table.getName().toUpperCase();		
 		//search for the source columns
 		String tableSchema=table.getSchemaName();
 		if(tableSchema==null) {
 			tableSchema=SQLSearcher.detectSchema(this.fileUnderStudy);
 		}
 		tableSchema=tableSchema.toLowerCase();
 		String fullyQualifiedTableName=tableSchema+"."+tableName;
 		return fullyQualifiedTableName;
    }
    
    @Override
    public void visit(Table table) {
	 	logger.trace("visiting: "+table);
	 	if(false) {
	 		//TODO add gestione dei with
	 	}else {
	 		String tableName=table.getName().toUpperCase();
	 		String tableRef;
	 		if(table.getAlias()!=null){
	 			tableRef=table.getAlias().getName();
	 		}else {
	 			tableRef=tableName;
	 		} 		
	 		String fullyQualifiedTableName=fullyQualifyTableName(table);
//	 		RepoSearcher rps=RepoSearcher.getRepoSearcher();
//	 		List<String> tmpList=rps.quickSearchFor(fullyQualifiedTableName);
	 		List<String> tmpList=new ArrayList<>();
	 		List<String> tableColumns=null;
	 		if(tmpList==null || tmpList.size()==0) {
	 			logger.error("unable to detect any file containing DDL for "+fullyQualifiedTableName);
	 			tableColumns=new ArrayList<>();
	 		}else{
	 			if(tmpList.size()>1) {
	 				logger.warn("Multiple files in "+Initializer.getProperty("localRepositoryDir")+" for object "+fullyQualifiedTableName);
	 			}
	 			try {
	 				tableColumns=TableUtilities.getColumnNames(new File(tmpList.get(0)));
	 			}catch (Exception e) {
 					logger.error("unable to detect columns for supposed table "+fullyQualifiedTableName+". Intercepted exception "+e.getClass()+" : "+e.getMessage());
 					e.printStackTrace();
				}
	 			if(tableColumns==null) {
	 				try {
	 					tableColumns=ViewUtilities.getColumnNames(new File(tmpList.get(0)));
	 				}catch (Exception e) {
	 					logger.error("unable to detect columns for supposed view "+fullyQualifiedTableName+". Intercepted exception "+e.getClass()+" : "+e.getMessage());
	 					e.printStackTrace();
					}
	 				if(tableColumns==null) {
	 					tableColumns=new ArrayList<>();
	 				}
	 			}
	 		}
	 		for(String col:tableColumns) {
	 			List<String> sources=new ArrayList<>();
		 		sources.add(tableName+"."+col.toUpperCase());
	 			expSou.addNewMapping(tableRef+"."+col.toUpperCase(), sources);
	 		}
	 		List<String> newListSourceName=expSou.getListSourceName();
	 		newListSourceName.add(fullyQualifiedTableName);
	 		expSou.setListSourceName(newListSourceName);
	 	}
    }

    @Override
    public void visit(SubSelect subSelect) {
    	logger.trace("visiting: "+subSelect);
    	if(subSelect.getAlias()!=null) {
    		currentSubSelectAlias=subSelect.getAlias().getName();
    	}else {
    		currentSubSelectAlias=null;
    	}
    	subSelect.getSelectBody().accept(this);
    	currentSubSelectAlias=null;
    }

    @Override
    public void visit(SubJoin subjoin) {
    	logger.trace("visiting: "+subjoin);
    	List<Join> joinlist = subjoin.getJoinList();
    	for(Join jitem:joinlist) {
    		jitem.getRightItem().accept(this);
    	}
    }

    @Override
    public void visit(LateralSubSelect lateralSubSelect) {
    	logger.trace("visiting: "+lateralSubSelect);
    	lateralSubSelect.getSubSelect().accept((FromItemVisitor)this);
    }

    @Override
    public void visit(ValuesList valuesList) {
    	logger.trace("visiting: "+valuesList);
    	throw new UnsupportedOperationException("Encountered a valueList when I wish I only had to work with from *table*");
    }

    @Override
    public void visit(TableFunction valuesList) {
    	logger.trace("visiting: "+valuesList);
    	//forse aggiungere direttamente la stringa nell'elenco e bon.
    	throw new UnsupportedOperationException("Encountered a valueList when I wish I only had to work with from *table*");
    }

    @Override
    public void visit(ParenthesisFromItem aThis) {
    	logger.trace("visiting: "+aThis);
        aThis.accept(this);
    }

    /**
     * retrieve the names of the sources used
     * @return
     */
	public List<String> getSources() {
		return expSou.getListSourceName();
	}

	/**
	 * dovrebbe essere usato quando trovo dei values
	 */
	@Override
	public void visit(ExpressionList expressionList) {
		List<Expression> expressions=expressionList.getExpressions();
		oldExpSou=null; //TODO verrà sistemato, es: posso sempre avere un link a una with
		for(int indexpre=0; indexpre<expressions.size(); indexpre++) {
			List<String>sourceColNames=new ArrayList<>();
			Expression expr=expressions.get(indexpre);
			if(!(expr instanceof Column)) {
				//expression non semplice
				//TODO da flaggare
			}
//			ColumnsNamesFinder extractor=new ColumnsNamesFinder();
			List<Column> tmpColList=new ColumnsNamesFinder().getColumnsList(expr);
			if(tmpColList.size()==0) {
				sourceColNames.add("BGEXPR<<<"+expr.toString());
			}else {
				for(Column col:tmpColList) {
					sourceColNames.add(col.getFullyQualifiedName());
				}
			}
			expSou.addNewMapping(expr.toString(), sourceColNames);
		}
	}

	@Override
	public void visit(MultiExpressionList multiExprList) {
		List<ExpressionList> tmpList=multiExprList.getExprList();
		for(ExpressionList exprLst:tmpList) {
			exprLst.accept(this);
		}	
	}
	////////////////////////////
	//ex parte di selctItemgetSourceColumns
	
	@Override
    public void visit(SelectExpressionItem item) {
		Expression expression=item.getExpression();
		if(!(expression instanceof Column)) {
			//questa espressione NON E' una espressione semplice
			//TODO in futuro andrà gestita la cosa
		}
		ColumnsNamesFinder cnf=new ColumnsNamesFinder();
		List<Column> listColumn=cnf.getColumnsList(expression);
		List<String> listColNames=new ArrayList<>();
		//in case we fail to get a column name we put the whole expression there
		if(listColumn.size()==0) {
			listColNames.add("BGEXPR<<<"+expression.toString());
		}else {
			for(Column col:listColumn) {
				listColNames.add(col.getFullyQualifiedName());
			}
		}
		Alias alias=item.getAlias();
		String reference=null;
		if(alias!=null) {
			reference=alias.getName();
		}else if(expression instanceof Column){
//			reference=((Column) expression).getFullyQualifiedName();
			reference=((Column) expression).getColumnName();
		}else {
			reference=expression.toString();
		}
		reference=currentSubSelectAlias==null?reference:currentSubSelectAlias+"."+reference;
		List<String> newSourcesOfFiled=oldExpSou.updateHistory(listColNames);
		
		expSou.addNewMapping(reference, newSourcesOfFiled );
    }
	
	 @Override
	 public void visit(AllColumns columns) {
		 //aggiungo tutto quanto viene esposto
		 for(String exposedColumn:oldExpSou.getExposedColumns()) {
			//TODO diverrà un link
			expSou.addNewMapping(exposedColumn, oldExpSou.getExposed2sources().get(exposedColumn));
		}
	 }

	 @Override
	 public void visit(AllTableColumns columns) {
		 logger.error("we ended up in a AllTableColumns statement!! "+columns);
		 throw new UnsupportedOperationException();
     }
	 
}
