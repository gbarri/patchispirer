package
source2Target;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SourceTargetRelationship {
	
	private static final Logger logger = LoggerFactory.getLogger(SourceTargetRelationship.class);
	/**
	 * nome della tabella target
	 */
	public String targetName;
	/**
	 * nomi degli oggetti sorgenti
	 */
	public List<String> listSourceName;
	/**
	 * lista delle colonne di target nell'ordine in cui sono state inserite
	 */
	public List<String> targetColumns;
	/**
	 * associazione colonna target-colonne sorgenti da cui deriva, o l'espressione che la genera
	 */
	private Map<String, List<String>> targetCol2sourceCol;
	
	public SourceTargetRelationship() {
		targetName=null;
		listSourceName=new ArrayList<>();
		targetColumns=new ArrayList<>();
		targetCol2sourceCol=new HashMap<>();
	}
	
	public SourceTargetRelationship(SourceTargetRelationship strel) {
		this();
		this.targetName=strel.getTargetName();
		this.listSourceName=new ArrayList<>(strel.getListSourceName());
		this.targetColumns=new ArrayList<>(strel.getTargetColumns());
		this.targetCol2sourceCol=new HashMap<>(strel.getTargetCol2sourceCol());
	}

	public List<String> getTargetColumns() {
		return targetColumns;
	}

	public void setTargetColumns(List<String> targetColumns) {
		this.targetColumns = targetColumns;
	}

	public void setListSourceName(List<String> listSourceName) {
		this.listSourceName = listSourceName;
	}

	public String getTargetName() {
		return targetName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

	public List<String> getListSourceName() {
		return listSourceName;
	}
	
	public void setTargetCol2sourceCol(Map<String, List<String>> targetCol2sourceCol) {
		this.targetCol2sourceCol = targetCol2sourceCol;
	}

	protected String getSourcesNamesToString() {
		String out="";
		if(listSourceName.size()>0) {
			out+=listSourceName.get(0);
			for(int i=1; i<listSourceName.size(); i++) {
				out+=", "+listSourceName.get(i);
			}
		}
		return out;
	}

	public Map<String, List<String>> getTargetCol2sourceCol() {
		return targetCol2sourceCol;
	}
	
	@Override
	public String toString() {
		String textDescription="Mapping from the columns of\t"+getTargetName()+"\ttoward\t"+getListSourceName()+"\t:[";
		for(String key:targetCol2sourceCol.keySet()) {
			textDescription+=key+","+targetCol2sourceCol.get(key)+"|";
		}
		textDescription+="]";
		return textDescription;
	}
	
	public String toTabString() {
		String textDescription="";
		for(String key:targetCol2sourceCol.keySet()) {
			textDescription+=getTargetName().toUpperCase()+"\t"+key+"\t"+getSourcesNamesToString()+"\t"+targetCol2sourceCol.get(key)+"\r\n";
		}
		return textDescription;
	}
	
	/**
	 * aggiunge ad ogni riga del print delle colonne iniziali, uguali per tutte le righe
	 * @param prefix
	 * @return
	 */
	public String toTabString(String prefix) {
		String textDescription="";
		for(String key:targetCol2sourceCol.keySet()) {
			textDescription+=prefix+"\t"+getTargetName().toUpperCase()+"\t"+key+"\t"+getSourcesNamesToString()+"\t"+targetCol2sourceCol.get(key)+"\r\n";
		}
		return textDescription;
	}

	/**
	 * restituisce la i-esima entrata delle colonne target, secondo l'ordine in cui sono state dichiarate
	 * @param i
	 * @return
	 */
	public String getTargetColumn(int i) {
		return targetColumns.get(i);
	}

	public void addTargetColumn(String colName) {
		targetColumns.add(colName);
		targetCol2sourceCol.put(colName, null);
	}
	
	public void addTargetColumn(List<String> colNames) {
		for(String colName:colNames) {
			addTargetColumn(colName);
		}
	}
	
	/**
	 * aggiunge una nuova relazione di mapping partendo da una coppia targetColumn-lista_di_sourceColumns.
	 * Be careful not to mix inserting new mapping with this method and the one with just one argument!!
	 * @param targetColumn
	 * @param colNamesList
	 */
	public void addNewMapping(String targetColumn, List<String> colNamesList) {
		if(!targetCol2sourceCol.containsKey(targetColumn)){
			IllegalArgumentException e= new IllegalArgumentException("You tried to add a new mapping for the target column "+targetColumn+" that wasn't registered before");
			logger.error("thrown exception ", e);
			throw e;
		}else if(targetCol2sourceCol.get(targetColumn)!=null) {
			IllegalArgumentException e= new IllegalArgumentException("You tried to add a new mapping for the target column "+targetColumn+" but it has been already mapped");
			logger.error("thrown exception ", e);
			throw e;
		}else {
			targetCol2sourceCol.put(targetColumn, colNamesList);
		}
	}
	
	///trick brutto, da rivedere
	//MEGLIO sarebbe che lavoriamo con oggetti di tipo tabella, colonna etc per cui posso fare un equal 
	/**
	 * deve capire se le due stringhe stanno indicando la stessa sorgente tra una select e una subselect
	 * @param currentSource
	 * @param newSourceItem
	 * @return
	 */
	private boolean areLinked(String currentSource, String newSourceItem, String currentSubSelectAlias) {
		return currentSource.equals(newSourceItem) || currentSource.equals(currentSubSelectAlias+"."+newSourceItem);
	}
	///

	/**
	 * avendo una nuova referenza (alias o nome colonna), aggiorna le dipendenze esistenti.
	 * Ovvero cerca dipendenze dalle colonne target ad reference, e sostituisce tale dipendenza con le sue dipendenze, colNamesList 
	 * @param reference
	 * @param colNamesList
	 */
	public void updateMapping(String reference, List<String> colNamesList, String subSelectAlias) {
		boolean updated=false;
		for(String key:targetCol2sourceCol.keySet()) {
			List<String> currentSources=targetCol2sourceCol.get(key);
			if(currentSources != null) {
				List<String> copy=new ArrayList<>(currentSources);
				for(int i=0; i<copy.size(); i++) {
					String currentSource=copy.get(i);
					if(areLinked(currentSource, reference, subSelectAlias)) {
						currentSources.remove(i);
						currentSources.addAll(colNamesList);
						updated=true;
						break;
					}
				}
			}
		}
		if(!updated) {
			logger.warn("E' stato cercato di aggiornare le dipendenze del campo "+reference+", ma non sembra essere stato usato (possibile chiave). Sources' columns: "+getTargetCol2sourceCol());
			String stack="";
			for(StackTraceElement element:Thread.currentThread().getStackTrace()) {
				stack+=element+"\n";
			}
			logger.warn(stack);
		}
	}
	
	/**
	 * metodo di updateMapping che presuppone che la referenza sia esatta, senza bisogno di considerare l'implicita aggiunta di un alias per i risultati
	 * @param reference
	 * @param list
	 */
	public void updateMapping(String reference, List<String> list) {
		updateMapping(reference, list, "");
	}
	
	/**
	 * aggiunge una nuova relazione di mapping, prendendo la prima relazione non ancora popolata.
	 * @param listColNames
	 */
	public void addNewMapping(List<String> listColNames) {
		boolean added=false;
		for(String tc:targetColumns) {
			if(!targetCol2sourceCol.containsKey(tc)) {
				throw new IllegalStateException("Stato non coerente in SourceTargetRelationship per disallineamento tra target colmunms e mapping di relazione col source!");
			}
			if(targetCol2sourceCol.get(tc)==null) {
				addNewMapping(tc, listColNames);
				added=true;
				break;
			}
		}
		if(!added) {
			IllegalArgumentException e= new IllegalArgumentException("We did not find any empty target column to add a new mapping!");
			logger.error("throw exception ",e);
			throw e;
		}
	}
	
}
