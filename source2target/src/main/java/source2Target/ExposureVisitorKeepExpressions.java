package source2Target;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Alias;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.expression.operators.relational.IsNullExpression;
import net.sf.jsqlparser.expression.operators.relational.ItemsList;
import net.sf.jsqlparser.expression.operators.relational.ItemsListVisitor;
import net.sf.jsqlparser.expression.operators.relational.MultiExpressionList;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.AllColumns;
import net.sf.jsqlparser.statement.select.AllTableColumns;
import net.sf.jsqlparser.statement.select.ExceptOp;
import net.sf.jsqlparser.statement.select.FromItem;
import net.sf.jsqlparser.statement.select.FromItemVisitor;
import net.sf.jsqlparser.statement.select.IntersectOp;
import net.sf.jsqlparser.statement.select.Join;
import net.sf.jsqlparser.statement.select.LateralSubSelect;
import net.sf.jsqlparser.statement.select.MinusOp;
import net.sf.jsqlparser.statement.select.ParenthesisFromItem;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.SelectBody;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;
import net.sf.jsqlparser.statement.select.SelectItemVisitor;
import net.sf.jsqlparser.statement.select.SelectVisitorAdapter;
import net.sf.jsqlparser.statement.select.SetOperation;
import net.sf.jsqlparser.statement.select.SetOperationList;
import net.sf.jsqlparser.statement.select.SubJoin;
import net.sf.jsqlparser.statement.select.SubSelect;
import net.sf.jsqlparser.statement.select.TableFunction;
import net.sf.jsqlparser.statement.select.UnionOp;
import net.sf.jsqlparser.statement.select.ValuesList;
import net.sf.jsqlparser.statement.select.WithItem;
import utilities.Initializer;
import utilities.RepoSearcher;
import utilities.SQLSearcher;
import utilities.TableUtilities;
import utilities.ViewUtilities;

/**
 * classe pensata per il popolamento di una SourceTargetRelationship, per 1 singolo statement di INSERT o UPDATE
 * @author g.barri
 *
 */
public class ExposureVisitorKeepExpressions extends ExposureVisitor{
	
	static Logger logger=LoggerFactory.getLogger(ExposureVisitorKeepExpressions.class);
	
	public ExposureVisitorKeepExpressions(File file) {
		super(file);
	}	
	
	@Override
    public void visit(SelectExpressionItem item) {
		Expression expression=item.getExpression();
		ColumnsNamesFinder cnf=new ColumnsNamesFinder();
		List<Column> listColumn=cnf.getColumnsList(expression);
		List<String> listColNames=new ArrayList<>();
		if(!(expression instanceof Column) || listColumn.size()==0) {
			listColNames.add("BGEXPR<<<"+expression.toString());
		}else {
			for(Column col:listColumn) {
				listColNames.add(col.getFullyQualifiedName());
			}
		}
		Alias alias=item.getAlias();
		String reference=null;
		if(alias!=null) {
			reference=alias.getName();
		}else if(expression instanceof Column){
//			reference=((Column) expression).getFullyQualifiedName();
			reference=((Column) expression).getColumnName();
		}else {
			reference=expression.toString();
		}
		reference=currentSubSelectAlias==null?reference:currentSubSelectAlias+"."+reference;
		List<String> newSourcesOfFiled=oldExpSou.updateHistory(listColNames);
		
		expSou.addNewMapping(reference, newSourcesOfFiled );
    }
	
}
