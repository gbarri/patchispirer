package
source2Target;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ExposedWithSources {
	
	private static final Logger logger = LoggerFactory.getLogger(ExposedWithSources.class);

	/**
	 * nomi degli oggetti sorgenti
	 */
	public List<String> listSourcesNames;
	/**
	 * lista delle colonne di target nell'ordine in cui sono state inserite
	 */
	public List<String> exposedColumns;
	/**
	 * associazione colonna target-colonne sorgenti da cui deriva, o l'espressione che la genera
	 */
	private Map<String, List<String>> exposed2sources;
	
	public ExposedWithSources() {
		listSourcesNames=new ArrayList<>();
		exposedColumns=new ArrayList<>();
		exposed2sources=new HashMap<>();
	}
	
	public ExposedWithSources(ExposedWithSources strel) {
		this();
		this.listSourcesNames=new ArrayList<>(strel.getListSourceName());
		this.exposedColumns=new ArrayList<>(strel.getExposedColumns());
		this.exposed2sources=new HashMap<>(strel.getExposed2sources());
	}

	public List<String> getExposedColumns() {
		return exposedColumns;
	}

	public void setExposedColumns(List<String> exposedColumns) {
		this.exposedColumns = exposedColumns;
	}

	public void setListSourceName(List<String> listSourceName) {
		this.listSourcesNames = listSourceName;
	}
	
	public void addListSourceName(List<String> listSourceName) {
		if(this.listSourcesNames==null) {
			this.listSourcesNames=new ArrayList<>();
		}
		for(String sourceName: listSourceName) {
			boolean alreadyPresent=false;
			for(String alreadyPresentSourceName:this.listSourcesNames) {
				if(StringUtils.equalsIgnoreCase(alreadyPresentSourceName, sourceName)){
					alreadyPresent=true;
					break;
				}
			}
			if(!alreadyPresent) {
				this.listSourcesNames.add(sourceName);
			}
		}
	}
	
	public void addSourceName(String sourceName) {
		List<String> listSourceName=new ArrayList<>();
		listSourceName.add(sourceName);
		addListSourceName(listSourceName);
	}

	public List<String> getListSourceName() {
		return listSourcesNames;
	}
	
	public void setExposed2sources(Map<String, List<String>> exposed2sources) {
		this.exposed2sources = exposed2sources;
	}

	public Map<String, List<String>> getExposed2sources() {
		return exposed2sources;
	}
	
	@Override
	public String toString() {
		String textDescription="Exposing:[";
		for(String key:exposed2sources.keySet()) {
			textDescription+=key+","+exposed2sources.get(key)+"|";
		}
		textDescription+="]";
		return textDescription;
	}

	/**
	 * restituisce la i-esima entrata delle colonne target, secondo l'ordine in cui sono state dichiarate
	 * @param i
	 * @return
	 */
	public String getTargetColumn(int i) {
		return exposedColumns.get(i);
	}

	public void addTargetColumn(String colName) {
		exposedColumns.add(colName);
		exposed2sources.put(colName, null);
	}
	
	/**
	 * aggiunge una nuova relazione di esposizione partendo da una coppia campo esposto -lista_di_sourceColumns.
	 * @param targetColumn
	 * @param colNamesList
	 */
	public void addNewMapping(String exposedColumn, List<String> sources) {
		logger.debug("Adding link between "+exposedColumn+" and "+sources);
		if(exposed2sources.get(exposedColumn)!=null) {
			logger.warn("You tried to add a new mapping for the target column "+exposedColumn+" but it has been already mapped. Mabye a UNION? We merge the sources");
			List<String> tmp=exposed2sources.get(exposedColumn);
			tmp.addAll(sources);
			exposed2sources.put(exposedColumn, tmp);
		}else {
			exposedColumns.add(exposedColumn);
			exposed2sources.put(exposedColumn, sources);
		}
	}

	/**
	 * UPDATE DELLA HISTORY DI UN SINGOLO CAMPO!!
	 * For each string in listColNames(which must be just 1!!! Are you sure?), if the string is actually a field currently exposed, it substitute the string with the referenced sources for that field
	 * @param listColNames the names of the sources' columns from which a current column depends.
	 * @return the updated list of the name of sources' columns
	 */
	public List<String> updateHistory(List<String> listColNames) {
		if(listColNames.size()!=1) {
			throw new UnsupportedOperationException("Written only for updating one field at the same time");
		}
		List<String> sourcesOfField=new ArrayList<>(listColNames);
		List<Integer> toRemove=new ArrayList<>();
		for(int i=0; i<listColNames.size(); i++) {
			String usedCol=listColNames.get(i);
			for(String exposedColumn:this.getExposedColumns()) {
				if(sameObjectName(exposedColumn,usedCol)) {
					//bug: non è obbligatorio che i nomi delle colonne richiamate abbiano il prefisso corretto
					//TODO attenzione alle ripetizioni!!
					sourcesOfField.addAll(this.getExposed2sources().get(exposedColumn));
					toRemove.add(i);
					break;
				}
			}			
		}
		for(int j=toRemove.size()-1; j>=0; j--){
			int index=toRemove.get(j);
			sourcesOfField.remove(index);
		}
		return sourcesOfField;
	}

	/**
	 * controlla se sto richiamando un field esposto, con o senza l'alias della subquery davanti
	 * @param exposedColumn
	 * @param usedCol
	 * @return
	 */
	private boolean sameObjectName(String exposedColumn, String usedCol) {
		boolean same=StringUtils.equalsIgnoreCase(exposedColumn, usedCol);
		if(!same) {
			same=StringUtils.endsWithIgnoreCase(exposedColumn,"."+usedCol );
		}
		return same;
	}
	
}
