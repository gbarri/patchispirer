package source2Target;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tinylog.Logger;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.relational.ItemsList;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.util.TablesNamesFinder;

public class ColumnsNamesFinder extends TablesNamesFinder {
	private List<Column> columns;
	public static Map<String,String> fakeColumnName;
	static {
		//NB: ONLY LOWERCASE NAME ALLOWED!
		fakeColumnName=new HashMap<>();
		fakeColumnName.put("localtimestamp".toLowerCase(), "Key word often used to valorize time field");
	}
	
	public ColumnsNamesFinder() {
		setColumns(new ArrayList<>());
	}
	
	private void setColumns(List<Column> arrayList) {
		this.columns=arrayList;
	}
	
	@Override
    public void visit(Column tableColumn) {
		super.visit(tableColumn);
		if(!fakeColumnName.containsKey(tableColumn.getColumnName().toLowerCase())) {
			this.columns.add(tableColumn);
		}
		//
//		for(Column col:columns) {
//			if(col.getFullyQualifiedName().contains(".")) {
//				Logger.debug(col.getFullyQualifiedName()+" vs "+col.getColumnName());
//			}
//		}
		//
    }

	public List<Column> getColumns() {
		return columns;
	}

	/**
     * Main entry for this Tool class. A list of found columns is returned.
     *
     * @param update
     * @return
     */
    public List<Column> getColumnsList(Expression expr) {
        init(true);
        expr.accept(this);
        return getColumns();
    }
    
    public List<Column> getColumnsList(ItemsList expr) {
        init(true);
        expr.accept(this);
        return getColumns();
    }

	
}
