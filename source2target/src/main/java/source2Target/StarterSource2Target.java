package source2Target;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.nio.file.Files;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.codec.language.bm.Rule.RPattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.statement.Statement;
import utilities.Initializer;
import utilities.SQLSearcher;
import utilities.SQLSearcher.SQLTypes;

public class StarterSource2Target {
	static Logger logger=LoggerFactory.getLogger(StarterSource2Target.class);
	public String printToFilePath;
	
	/**
	 * metodo per stabilire se occorre procedere per un dato file o meno.
	 * Utile per stabilire la logica con cui si accede ad un file
	 * @param path
	 * @return se procedere
	 */
	private static boolean checkFile(File file) {
		logger.trace("Checking file "+file.getName());
		boolean cond=true;
		cond=cond && file.getName().endsWith(".sql");
		cond=cond && !file.getName().startsWith(".");
		if(cond) {
			SQLTypes type=SQLSearcher.detectType(file);
			cond=type!=null;
			if(!cond) {
				logger.debug("file "+file.getName()+", "+file.getAbsolutePath()+" è stato scartato perchè non riconosciuto come def. di un oggetto SQL.");
			}else {
				cond=cond && type==SQLTypes.FUNCTION;
				//cond=cond && SQLSearcher.detectName(file).contains("MP_") || SQLSearcher.detectName(file).contains("mp_");
				if(!cond) {
					logger.debug("file "+file.getName()+" è stato scartato perchè non è una function. "+file.getAbsolutePath());
				}
			}
		}else {
			logger.trace("file "+file.getName()+" has been discarded");
		}
		return cond;
	}
	
	/**
	 * metodo per stabilire se occorre procedere per una data cartella o meno.
	 * Utile per stabilire la logica con cui si accede ad una cartella, o escludere certe cartelle.
	 * @param file
	 * @return
	 */
	private static boolean checkFolder(File folder) {
		logger.trace("checking folder "+folder.getName());
		boolean cond=true;
		cond=cond && !folder.getName().equals("_Ispirer");
		if(!cond) logger.trace("folder "+folder.getName()+" to be skipped");
		return cond;
	}
	
	/**
	 * carica i files di una directory. Per ora in un array di file. Possibili miglioramenti
	 * @param folder
	 * @return File array
	 */
	public static File[] loadFiles(File folder) {
		File[] listOfFiles = folder.listFiles();
		return listOfFiles;
	}
	
//	public void printToFile(SourceTargetRelationship rel, String filepath) {
//		try(FileWriter fw=new FileWriter(filepath, true);
//				BufferedWriter bw = new BufferedWriter(fw);
//				PrintWriter print = new PrintWriter(bw)){
//				print.print(rel.toTabString());
//			}catch (IOException e) {
//				throw new RuntimeException("Exception raised while writing "+filepath, e);
//			}
//	}
	
	public void printToFile(SourceTargetRelationship rel, String filepath, String prefix) {
		try(FileWriter fw=new FileWriter(filepath, true);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter print = new PrintWriter(bw)){
				print.print(rel.toTabString(prefix));
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while writing "+filepath, e);
			}
	}
	
	/**
	 * metodo che deve scorrere i files da analizzare e richiama i vari metodi necessari
	 * @param path
	 */
	private void goThrough(String path) {
		File folder = new File(path);
		if(!folder.isDirectory()) {
			if(folder.isFile() && checkFile(folder)) {
				File file=folder;
				logger.trace("Starting checking file "+path);
				folder=null;
				Source2TargetMapping s2t=new Source2TargetMapping(file);
				List<String> texts=s2t.scanForOperation(file,"INSERT");
				texts.addAll(s2t.scanForOperation(file, "insert"));
				if(texts.size()>0) {
					for(String text:texts) {
						Statement st=null;
						logger.debug("Preparing to parse statement:\n"+text);
						try(StringReader strReader=new StringReader(text)) {
							st = new CCJSqlParserManager().parse(strReader);
							st.accept(s2t);
							logger.trace(s2t.getRelationship().toString());
							String prefix=SQLSearcher.detectName(file)+"\t"+"INSERT STATEMENT";
							printToFile(s2t.getRelationship(), printToFilePath, prefix );
						} catch (JSQLParserException e) {
							logger.error("Intercepted exception "+e.getClass()+": "+e.getMessage());
						} catch (UnsupportedOperationException e) {
							logger.error("file "+file.getName()+" has been skipped until future developments.");
						} catch (NullPointerException e) {
							logger.error("true error while checking inserts of file "+file.getAbsolutePath());
						}
					}
				}
				texts=s2t.scanForOperation(file, "UPDATE");
				texts.addAll(s2t.scanForOperation(file, "update"));
				for(String text:texts){
					Statement st=null;
					try(StringReader strReader=new StringReader(text)){
						logger.debug("Preparing to parse statement:\n"+text);
						st = new CCJSqlParserManager().parse(strReader);
						st.accept(s2t);
						logger.debug(s2t.getRelationship().toString());
					String prefix=SQLSearcher.detectName(file)+"\t"+"UPDATE STATEMENT";
						printToFile(s2t.getRelationship(), printToFilePath, prefix );
					} catch (JSQLParserException e) {
						logger.error("Intercepted exception "+e.getClass()+": "+e.getMessage());
					} catch (UnsupportedOperationException e) {
						logger.error("file "+file.getName()+" has been skipped until future developments.");
					} catch (NullPointerException e) {
						logger.error("true error while checking updates of file "+file.getAbsolutePath());
					}
				}
			}
		}else if(folder.isDirectory() && checkFolder(folder)){
			File[] listOfFiles=loadFiles(folder);
			for (File file : listOfFiles) {
				goThrough(file.getAbsolutePath());
			}
		} 
	}
	
	public static void main(String[] args) throws IOException {
		/*real repository*/
//		String sql_path="C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\EDWP2";

		/*test folder outside repository*/
		String sql_path="C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\pl_colors\\functions\\pl_colors_mvn_mp_ft_immat_mm_grup.sql";
//		String sql_path=Initializer.getProperty("workingDir");
		
		String results="C:\\Users\\g.barri\\Desktop\\sourceMapping\\results_pl_colors_mvn_mp_ft_immat_mm_grup.csv";
		if(new File(results).exists()) {
			Files.delete(new File(results).toPath());
		}
		try(FileWriter fw=new FileWriter(results);
			 BufferedWriter bw = new BufferedWriter(fw);
			 PrintWriter print = new PrintWriter(bw)){
			print.println("Mapping;INSERT/UPDATE;Source_schema;Source_name;Source_column;Target_schema;Target_name;Target_column");
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while writing "+results, e);
		}
		logger.trace("Starting main of "+StarterSource2Target.class);
		StarterSource2Target start=new StarterSource2Target();
		start.printToFilePath=results;
		logger.trace("Going through files in "+sql_path);
		start.goThrough(sql_path);
		logger.trace("printing results to file: "+results);
		logger.trace("Ending main of "+StarterSource2Target.class);
	}
}
