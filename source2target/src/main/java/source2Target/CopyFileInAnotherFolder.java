package source2Target;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import loaders.Loader;
import loaders.LoaderByColumnIndex;
import loaders.LoaderByColumnIndexOrName;
import utilities.Initializer;
import utilities.RepoSearcher;
import utilities.StringUtilities;

public class CopyFileInAnotherFolder {

	public static void main(String[] args) throws EncryptedDocumentException, InvalidFormatException, IOException {
//		Loader loaderBuoniRomagnoli=new LoaderByColumnIndex(new File("C:\\Users\\g.barri\\Downloads\\Mapping sicuri G. Romagnolo.xlsx"), "Loader.useColumnsAt=1,2\r\n" + 
//																															"Loader.useSheetAt=0\r\n" + 																						"Loader.usePKAt=0");
//		Map<String,String[]> goodMappingRomagnoli=loaderBuoniRomagnoli.loadMap();
//		List<String> names=new ArrayList<>();
//		Iterator<String> iter=goodMappingRomagnoli.keySet().iterator();
//		while(iter.hasNext()) {
//			String[] tmp=goodMappingRomagnoli.get(iter.next());
//			names.add(StringUtilities.formatObjectName(tmp[0]+"."+tmp[1]));
//		}
//		Loader loaderBuoniLuca=new LoaderByColumnIndex(new File("C:\\Users\\g.barri\\Downloads\\Lista mapping.xlsx"), "Loader.useColumnsAt=1,3\r\n" + 
//																														"Loader.useSheetAt=0\r\n" + 
//																														"Loader.usePKAt=0");
//		Map<String,String[]> goodMappingLuca=loaderBuoniLuca.loadMap();
//		iter=goodMappingLuca.keySet().iterator();
//		while(iter.hasNext()) {
//			String[] tmp=goodMappingLuca.get(iter.next());
//			names.add(StringUtilities.formatObjectName(tmp[0]+"."+tmp[1]));
//		}
//		Loader loaderBadLuca=new LoaderByColumnIndex(new File("C:\\Users\\g.barri\\Downloads\\Lista mapping.xlsx"), "Loader.useColumnsAt=1,3\r\n" + 
//				"Loader.useSheetAt=1\r\n" + 
//				"Loader.usePKAt=0");
//		Map<String,String[]> unsafeMappingLuca=loaderBadLuca.loadMap();
//		iter=unsafeMappingLuca.keySet().iterator();
//		while(iter.hasNext()) {
//			String[] tmp=unsafeMappingLuca.get(iter.next());
//			names.add(StringUtilities.formatObjectName(tmp[0]+"."+tmp[1]));
//		}
		
		//
		
		
//		String excelFile=Initializer.getProperty("excelDocument");
		String excelFile="C:\\Users\\g.barri\\Downloads\\LB0 - DM Marketing (3).xlsx";
		Loader loaderAllFromTableMappingConstraintYes=new LoaderByColumnIndexOrName(new File(excelFile),
//				"Loader.useColumnsAt=3,7,8\r\n" +
				"Loader.useColumnsAt=Target Schema,Mapping,Constraint Check\r\n" +
				"Loader.useSheetAt=Table Mapping\r\n" + 
				"Loader.usePKAt=0,1,3,4,7");
			
		Map<String,String[]> all=loaderAllFromTableMappingConstraintYes.loadMap();
		List<String> namesAll=new ArrayList<>();
		Iterator<String> iterAll=all.keySet().iterator();
		while(iterAll.hasNext()) {
			String[] tmp=all.get(iterAll.next());
			if(tmp[2].equals("YES")){
				namesAll.add(StringUtilities.formatObjectName(tmp[0]+"."+tmp[1]));
			};
		}
		
		excelFile="C:\\Users\\g.barri\\Downloads\\Q50 - DM PO (4).xlsx";
		loaderAllFromTableMappingConstraintYes=new LoaderByColumnIndexOrName(new File(excelFile),
//				"Loader.useColumnsAt=3,7,8\r\n" +
				"Loader.useColumnsAt=Target Schema,Mapping,Constraint Check\r\n" +
				"Loader.useSheetAt=Table Mapping\r\n" + 
				"Loader.usePKAt=0,1,3,4,7");
			
		all=loaderAllFromTableMappingConstraintYes.loadMap();
		iterAll=all.keySet().iterator();
		while(iterAll.hasNext()) {
			String[] tmp=all.get(iterAll.next());
			if(tmp[2].equals("YES")){
				namesAll.add(StringUtilities.formatObjectName(tmp[0]+"."+tmp[1]));
			};
		}
		
		excelFile="C:\\Users\\g.barri\\Downloads\\DE3 - DM Campaign (7).xlsx";
		loaderAllFromTableMappingConstraintYes=new LoaderByColumnIndexOrName(new File(excelFile),
//				"Loader.useColumnsAt=3,7,8\r\n" +
				"Loader.useColumnsAt=Target Schema,Mapping,Constraint Check\r\n" +
				"Loader.useSheetAt=Table Mapping\r\n" + 
				"Loader.usePKAt=0,1,3,4,7");
			
		all=loaderAllFromTableMappingConstraintYes.loadMap();
		iterAll=all.keySet().iterator();
		while(iterAll.hasNext()) {
			String[] tmp=all.get(iterAll.next());
			if(tmp[2].equals("YES")){
				namesAll.add(StringUtilities.formatObjectName(tmp[0]+"."+tmp[1]));
			};
		}
		
		//print result
		System.out.println("total files:");
		for(String file:namesAll) {
			System.out.println("FILE: "+file);
		}
		System.out.println("total count: "+namesAll.size());
		
		
//		String[] namesAll= {
//				"tl_afs.WPV_MP_TAB_SA_AGGREG_FMTA",
//				"tl_afs.WPV_MP_TAB_SA_CODICE_SPESA_M",
//				"tl_afs.WPV_MP_TAB_SA_DESC_AGGREG_FMTA",
//				"tl_qlt.WPV_MP_TAB_SA_FORZATURA",
//				"tl_qlt.WPV_MP_TAB_SA_ANAG_FMTA",
//				"tl_qlt.WPV_MP_TAB_SA_ANOMALIA",
//				"tl_qlt.WPV_MP_TAB_SA_COD_OPERAZIONE",
//				"tl_qlt.WPV_MP_TAB_SA_CODICE_SPESA",
//				"tl_qlt.WPV_MP_TAB_SA_COMPL_INPUT",
//				"tl_qlt.WPV_MP_TAB_SA_DES_MERCATO",
//				"tl_qlt.WPV_MP_TAB_SA_DESC_OPERATIVE",
//				"tl_qlt.WPV_MP_TAB_SA_DESC_PEZZO_FUNZ",
//				"tl_qlt.WPV_MP_TAB_SA_DESC_PEZZO_INPUT",
//				"tl_qlt.WPV_MP_TAB_SA_DESC_PEZZO_STRUT",
//				"tl_qlt.WPV_MP_TAB_SA_DESC_RICAMBIO",
//				"tl_qlt.WPV_MP_TAB_SA_GRUPPO_FUNZ",
//				"tl_qlt.WPV_MP_TAB_SA_INSIEME_FUNZ",
//				"tl_qlt.WPV_MP_TAB_SA_MOT_RETT",
//				"tl_qlt.WPV_MP_TAB_SA_OPERATIVE",
//				"tl_qlt.WPV_MP_TAB_SA_PEZZO_FUNZIONALE",
//				"tl_qlt.WPV_MP_TAB_SA_PEZZO_STRUTT",
//				"tl_qlt.WPV_MP_TAB_SA_POSIZIONI",
//				"tl_qlt.WPV_MP_TAB_SA_SEZIONE_FUNZ"
//			};
		//
		RepoSearcher rps=RepoSearcher.getRepoSearcher();
		for(String name:namesAll) {
			name=StringUtilities.formatObjectName(name);
			List<String> files=rps.quickSearchFor(name);
			if(files.size()>0) {
//				System.out.println("copy "+files.get(0)+" C:\\Users\\g.barri\\Desktop\\sourceMapping\\listaFiles\\"+new File(files.get(0)).getName());
				File file=new File(files.get(0));
				try {
//					Files.copy(file.toPath(), new File("C:\\Users\\g.barri\\Desktop\\sourceMapping\\listaFiles_TMP\\"+new File(files.get(0)).getName()).toPath(), StandardCopyOption.REPLACE_EXISTING);
					System.out.println("cp \""+file.toPath().toString()+"\" \"C:\\Users\\g.barri\\Desktop\\tmp\\addTAG\\"+file.getName()+"\"");
				} catch (Exception e) {
					System.out.println("WARNING! "+name);
				}
			}else {
				System.out.println("Object "+name+" not found");
			}
		}
	}

}
