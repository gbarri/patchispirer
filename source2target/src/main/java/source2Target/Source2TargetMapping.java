package source2Target;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.event.ListSelectionEvent;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Alias;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.relational.ItemsList;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.parser.TokenMgrException;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.StatementVisitorAdapter;
import net.sf.jsqlparser.statement.insert.Insert;
import net.sf.jsqlparser.statement.select.FromItem;
import net.sf.jsqlparser.statement.select.Join;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectItem;
import net.sf.jsqlparser.statement.update.Update;
import net.sf.jsqlparser.util.TablesNamesFinder;
import utilities.CompleteDecommenter;
import utilities.Decommenter;
import utilities.Initializer;
import utilities.RepoSearcher;
import utilities.SQLSearcher;
import utilities.StringUtilities;
import utilities.TableUtilities;
import utilities.ViewUtilities;

public class Source2TargetMapping extends StatementVisitorAdapter{
	
	static Logger logger=LoggerFactory.getLogger(Source2TargetMapping.class);
	SourceTargetRelationship relationship;
	private File fileInAnalisi;
	
	public Source2TargetMapping(File file) {
		this.fileInAnalisi=file;
	}
	
	public SourceTargetRelationship getRelationship() {
		return relationship;
	}
	
//	private static boolean checkLine(String line) {
//		boolean cond=true;
//		//escludo righe commentate MA non se contengono la prima parte del TAG
//		cond=cond && (!line.trim().startsWith("--")||line.trim().contains("<|"));
//		return cond;
//	}
	
	/**
	 * metodo per cercare tutte le instanze di INSERT, insert, UPDATE e update
	 * @param file
	 * @return
	 */
	public List<String> scanForOperation(File file, String keyword) {
		logger.trace("Searching for "+keyword+" in "+file.getName()+" ("+file.getAbsolutePath());
		List<String> found=new ArrayList<>();
		
		try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
				Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
			CompleteDecommenter decom=new CompleteDecommenter(scann);
			while(decom.hasNextLine()) {
				decom.clearMemory();
				String nextLine=decom.nextLine();
//				logger.debug(nextLine);
//				System.out.println(nextLine);
				Boolean checkForOccurrences=null;
				///
				try {
					List<Integer> occurrences;
					occurrences = StringUtilities.findExactly(nextLine, keyword);
					checkForOccurrences=occurrences!=null && occurrences.size()>0;
				} catch (TokenMgrException  e) {
					//workaround un poco schifido
					checkForOccurrences=nextLine.contains(keyword);
				} catch (Exception e) {
					checkForOccurrences=nextLine.contains(keyword);
				}
				if(checkForOccurrences) {
//						String tmpLine=nextLine+"\n";
					while(nextLine.indexOf(';')==-1 && decom.hasNextLine()) {
						//evito il controllo hasNextLine() dato che di certo deve concludere
//							nextLine=scann.nextLine();
						nextLine=decom.nextLine();
//						logger.debug(nextLine);
//							tmpLine+=nextLine+"\n";
					}
					nextLine=nextLine.trim();
					int indexOfKeyWord=nextLine.indexOf(keyword);
					int indexOfLastSemicolon=nextLine.lastIndexOf(';');
					nextLine=nextLine.substring(indexOfKeyWord, indexOfLastSemicolon+1);
					found.add(nextLine);
				}
			}
			if(found.size()==0) {
				logger.warn("File "+file.getName()+" does not contain a "+keyword+" statement ("+file.getAbsolutePath()+")");
			}
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while reading for \n\t"+file.getAbsolutePath(), e);
		}
		return found;
	}
	
	@Override
	    public void visit(Insert insert) {
			//retrieving target table information
			Table target=insert.getTable();
			if(target==null) {
				logger.error("table in "+insert+" not found");
				throw new IllegalArgumentException("table in "+insert+" not found");
			}
			//retrieve the columns of the target table influenced by this function
		 	List<Column> targetColumns=insert.getColumns();
		 	List<String> targetColNames=new ArrayList<>();
		 	if(targetColumns==null) {
		 		logger.trace("No columns esplicitally indicated in Insert statement "+insert+".\tRetrieve from table name");
		 		String tableSchema=target.getSchemaName();
		 		if(tableSchema==null) {
		 			tableSchema=SQLSearcher.detectSchema(this.fileInAnalisi);
		 		}
		 		tableSchema=tableSchema.toLowerCase();
		 		String tableName=target.getName().toUpperCase();
		 		String fullyQualifiedTableName=tableSchema+"."+tableName;
		 		RepoSearcher rps=RepoSearcher.getRepoSearcher();
		 		List<String> tmpList=rps.quickSearchFor(fullyQualifiedTableName);
		 		if(tmpList==null || tmpList.size()==0) {
		 			logger.error("unable to detect any file containing DDL for "+fullyQualifiedTableName);
		 			relationship=new SourceTargetRelationship();
		 			return;
		 		}else if(tmpList.size()>1) {
		 			logger.warn("Multiple files in "+Initializer.getProperty("localRepositoryDir")+" for object "+fullyQualifiedTableName);
		 		}
		 		targetColNames=TableUtilities.getColumnNames(new File(tmpList.get(0)));
		 	}else {
		 		//retrieving their names
		 		for(Column colItem: targetColumns) {
		 			targetColNames.add(colItem.getColumnName());
		 		}
		 	}
//		 	ExposureVisitor exposureVisitor=new ExposureVisitor(fileInAnalisi);
		 	ExposureVisitor exposureVisitor=new ExposureVisitorKeepExpressions(fileInAnalisi);
		 	Select selSource=insert.getSelect();
		 	if(selSource!=null) {
		 		//retrieve source information
		 		selSource.getSelectBody().accept(exposureVisitor);
		 	}else {
		 		ItemsList values=insert.getItemsList();
		 		if(values!= null) {
		 			//retrieve source information
		 			values.accept(exposureVisitor);
		 		}else {
		 			logger.error("La istanza di Insert "+insert+" non contiene nè una select nè un values(...)");
		 			throw new IllegalArgumentException("La istanza di Insert "+insert+" non contiene nè una select nè un values(...)");
		 		}
		 	}
		 	ExposedWithSources ews=exposureVisitor.getExpSou();
		 	relationship=new SourTargRelSmartPrint();
		 	relationship.setTargetName(target.getFullyQualifiedName());
		 	relationship.addTargetColumn(targetColNames);
		 	List<String> tmpSourceList=new ArrayList<>();
	 		for(String source:exposureVisitor.getSources()) {
	 			if(!StringUtils.equalsIgnoreCase(source,target.getFullyQualifiedName())) {
	 				tmpSourceList.add(source);
	 			}else {
	 				logger.debug("SOURCE EQUAL TO TARGET IN INSERT FILTERED AWAY");
	 			}
	 		}
		 	relationship.setListSourceName(tmpSourceList);
		 	for(int i=0; i<ews.getExposedColumns().size(); i++) {
		 		String targetColumn=targetColNames.get(i);
		 		String exposedColumn=ews.getExposedColumns().get(i);
		 		List<String> sources=ews.getExposed2sources().get(exposedColumn);
		 		relationship.addNewMapping(targetColumn, sources);
		 	}
	    }
	
	@Override
    public void visit(Update update) {
		relationship=new SourTargRelSmartPrint();
		//retrieving target table information
		List<Table> targets=update.getTables();
		if(targets==null || targets.size()==0) {
			logger.error("table in "+update+" not found");
			throw new IllegalArgumentException("table in "+update+" not found");
		}if(targets.size()!=1) {
			logger.error("more than one table in "+update+" has been found! "+targets);
			throw new IllegalArgumentException("multiple tables found in update statement");
		}
		Table target=targets.get(0);
		//check table name
		String tableSchema=target.getSchemaName();
 		if(tableSchema==null) {
 			tableSchema=SQLSearcher.detectSchema(this.fileInAnalisi);
 		}
 		tableSchema=tableSchema.toLowerCase();
 		String fullyQualifiedTableName=tableSchema+"."+target.getName().toUpperCase();
 		relationship.setTargetName(fullyQualifiedTableName);
		//retrieve the columns of the target table influenced by this function
	 	List<Column> targetColumns=update.getColumns();
	 	if(targetColumns==null || targetColumns.size()==0) {
	 		logger.warn("No columns are updated?! "+update);
	 		targetColumns=new ArrayList<>();
	 	}
	 	//retrieving their names
	 	for(int i=0; i<targetColumns.size(); i++) {
	 		Column colItem=targetColumns.get(i);
	 		relationship.addTargetColumn(colItem.getFullyQualifiedName());
	 	}
	 	//////////////
	 	//retrieving list of expressions
	 	List<Expression> listExpress=update.getExpressions();
//	 	List<String> usedColumNames=new ArrayList<>();
	 	for(int i=0; i<listExpress.size(); i++) {
	 		Expression exItem=listExpress.get(i);
	 		ColumnsNamesFinder tbfind=new ColumnsNamesFinder();
	 		List<Column> columns=tbfind.getColumnsList(exItem);
	 		List<String> listColNames=new ArrayList<>();
	 		if(columns!= null && columns.size()>0) {
		 		for(Column column:columns) {
		 			listColNames.add(column.getFullyQualifiedName());
		 		}
	 		}else {
	 			listColNames.add("BGEXPR<<<"+exItem.toString());
	 		}
	 		relationship.addNewMapping(relationship.getTargetColumns().get(i), listColNames);
	 	}
	 	//////////////
	 	//visitor to retrieve source's information.
//	 	ExposureVisitor exposureVisitor=new ExposureVisitor(fileInAnalisi);
	 	ExposureVisitor exposureVisitor=new ExposureVisitorKeepExpressions(fileInAnalisi);
	 	//ho settato artificiosamente la relazione perchè consideri come target columns le colonne usate nei set
		FromItem selSource=update.getFromItem();
	 	if(selSource==null) {
	 		logger.warn("La istanza di update "+update+" non contiene un from");
	 	}else {
	 		//retrieve source information
	 		selSource.accept(exposureVisitor);
	 		ExposedWithSources exp=exposureVisitor.getExpSou();
		 	//exp dovrebbe essere stato corretto
	 		List<String> tmpSourceList=new ArrayList<>();
	 		for(String source:exp.getListSourceName()) {
	 			if(!StringUtils.equalsIgnoreCase(source, fullyQualifiedTableName)) {
	 				tmpSourceList.add(source);
	 			}else {
	 				logger.debug("SOURCE EQUAL TO TARGET IN UPDATE FILTERED AWAY");
	 			}
	 		}
	 		relationship.setListSourceName(tmpSourceList);
	 		for(String exposedCol:exp.getExposedColumns()) {
	 			relationship.updateMapping(exposedCol, exp.getExposed2sources().get(exposedCol));
	 		}
	 	} 
// 		logger.debug("Exposed&sources pre where: "+exp.toString());
 		//must add links between fields in join
	 	Expression where=update.getWhere();
	 	WhereConnecttionsVisitor whereVisitor=new WhereConnecttionsVisitor();
	 	where.accept(whereVisitor);
	 	ExposedWithSources farlocco=whereVisitor.exposed;
	 	List<String> targetFileds=farlocco.exposedColumns;
	 	Map<String, List<String>> links=farlocco.getExposed2sources();
	 	//
	 	//must decide if the left-expression or right expression is the one associated with the primary target table
	 	//the reference of field of target table must have either tablename/alias or nothing, if it has nothing it must be him. Reference to source table must use the proper alias
	 	//
	 	String refUpdateTable=target.getAlias()==null?target.getFullyQualifiedName():target.getAlias().getName();
	 	for(String field:targetFileds) {
	 		if(!links.containsKey(field)) {
	 			throw new IllegalStateException("manca un target field nalla mappatura del where");
	 		}else {
	 			boolean alreadyPresent=false;
	 			boolean isTargetTableField=false;
	 			String updatedTableField;
	 			List<String> sourceTableField;
	 			if(StringUtils.containsIgnoreCase(field, refUpdateTable+".")) {
	 				isTargetTableField=true;
	 			}else if(StringUtils.containsIgnoreCase(field, target.getName()+".")) {
	 				//we check this in case there is no alias and there is no explicit schema indicated
	 				isTargetTableField=true;
	 			}else {
	 				if(!field.contains(".")) {
	 					isTargetTableField=true;
	 				}//TODO in questo caso si potrebbe mettere un check per verificare che contenga effettivamente l'alias della sorgente indicata nel from
	 			}
	 			if(!isTargetTableField) {
	 				updatedTableField=links.get(field).get(0);
	 				ArrayList<String> tmp=new ArrayList<>();
	 				tmp.add(field);
	 				sourceTableField=tmp;
	 			}else {
	 				updatedTableField=field;
	 				sourceTableField=links.get(field);
	 			}
	 			//assumere che sia sempre a sx è errato
	 			String fieldClenaed=null;
	 			if(StringUtils.containsIgnoreCase(updatedTableField, refUpdateTable+".")) {
	 				fieldClenaed=StringUtils.removeIgnoreCase(updatedTableField, refUpdateTable+".");
	 				
	 			}else if(StringUtils.containsIgnoreCase(updatedTableField, target.getName()+".")) {
	 				//we check this in case there is no alias and there is no explicit schema indicated
	 				fieldClenaed=StringUtils.removeIgnoreCase(updatedTableField, target.getName()+".");
	 			}else {
	 				fieldClenaed=updatedTableField;
	 			}
	 			for(String col:relationship.targetColumns) {
	 				if(StringUtils.equalsIgnoreCase(col,fieldClenaed)) {
	 					logger.debug("column "+col+" plan to add sources/link from where condition "+sourceTableField+" even if it was arleady present");
	 					alreadyPresent=true;
	 					break;
	 				}
	 			}
	 			if(!alreadyPresent) {
	 				List<String> updatedSource=exposureVisitor.getExpSou().updateHistory(sourceTableField);
	 				relationship.addTargetColumn(fieldClenaed);
	 				relationship.addNewMapping(fieldClenaed,updatedSource);
	 			}else {
	 				List<String> updatedSource=exposureVisitor.getExpSou().updateHistory(sourceTableField);
	 				relationship.updateMapping(fieldClenaed, updatedSource);
	 			}
	 		}
	 	}
// 		logger.debug("Exposed&sources post where: "+exp.toString());

	}
	 	
}

