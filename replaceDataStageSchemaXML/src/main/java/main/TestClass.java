package main;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import loaders.*;
import schemaMapping.LoaderSchemaMap;
import utilities.Initializer;

public class TestClass {

	static Logger logger = LoggerFactory.getLogger(TestClass.class);

	public int tabNum;

	public Document parse(File file) throws DocumentException {
		SAXReader reader = new SAXReader();
		Document document = reader.read(file);
		return document;
	}

	public void print(Element el) {
		for (int i = 0; i < this.tabNum; i++) {
			System.out.print("\t");
		}
		System.out.println(el.getName());
	}

	public void substituteIntext(File file, String newFileName) {
		File newcopy = new File(file.getParent() + "\\" + newFileName + ".copy");
		logger.trace("Printing the new xml at\t"+newcopy.getAbsolutePath());
		if (newcopy.exists()) {
			newcopy.delete();
		}
		int count = 0;
		try (Scanner scann = new Scanner(new FileReader(file)); FileWriter writer = new FileWriter(newcopy, true)) {
			while (scann.hasNextLine()) {
				String nextLine = scann.nextLine();
				for (int i = 0; i < nextLine.length(); i++) {
					char c = nextLine.charAt(i);
					switch (c) {
					case ('\"'):
						if (count != 0) {
							writer.append(c);
						} else {
							writer.append("&quot;");
						}
						break;
					case ('\''):
						if (count != 0) {
							writer.append(c);
						} else {
							writer.append("&apos;");
						}
						break;
					case ('<'):
						count++;
						writer.append(c);
						break;
					case ('>'):
						count--;
						writer.append(c);
						break;
					default:
						writer.append(c);
						break;
					}
				}
				writer.append("\r\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
//		try {
//			Files.move(newcopy.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

	}

	public static List<Node> selectNodesFrom(Node node, String relXPath, String textVal) {
		List<Node> listNodes = node.selectNodes(relXPath);
		List<Node> out = new ArrayList<>();
		for (Node nodItem : listNodes) {
			if (nodItem.getText().trim().equals(textVal)) {
				out.add(nodItem);
			}
		}
		return out;
	}
	static Map<String, String> replaceMap = null;

	private static void updateMappingInXMLFor(Node document, String textIdentifier) {
		logger.trace("Replacing schema for "+textIdentifier);
		String XPath = "//Collection/SubRecord/Property[@Name=\"Name\"]";
		String parameterSet=Initializer.getProperty("parameterSet");
		List<Node> packageNodes = selectNodesFrom(document, XPath, textIdentifier.replace("\"", ""));
		for (Node node : packageNodes) {
			Element SubRecord = node.getParent();
			Node packageNameNode = SubRecord.selectSingleNode("Property[@Name=\"Description\"]");
			if (packageNameNode != null) {
				String packageName = packageNameNode.getText().replace("\"", "");
				// recupera nome da excell
				if (replaceMap == null) {
					LoaderSchemaMap loader = new LoaderSchemaMap();
					Map<String, String> dirtyMap = new HashMap<>();
					Map<String, String> replacementMap = loader.loadMap();
					for (String key : replacementMap.keySet()) {
						String value = replacementMap.get(key);
						String trimmedkey = null;
						if (key.contains(".")) {
							trimmedkey = key.substring(key.indexOf(".") + 1);
						} else {
							throw new IllegalArgumentException(
									"Controllare le chiavi: una chiave originale è senza punto!");
						}
						String newValue;
						if (dirtyMap.containsKey(trimmedkey)) {
							//la chiave trimmata non è univoca
							String previuosValue = dirtyMap.get(trimmedkey);
							if(previuosValue.trim().equals(value.trim())) {
								//salvi perchè coincidenti
								newValue=previuosValue;
							}else {
								//aggiungo entrambi i valori diversi e metto un tag CONFLICT:
								newValue = previuosValue.startsWith("CONFLICT:") ? previuosValue + "|" + value : "CONFLICT:" + previuosValue + "|" + value;
							}
						} else {
							//nuova chiave, vecchio valore
							newValue=value;
						}
						//popolo la mappa
						dirtyMap.put(trimmedkey, newValue);
					}
					replaceMap = dirtyMap;
				}
				String newSchemaObject = replaceMap.get(packageName.toUpperCase().trim());
				if (newSchemaObject != null) {
					String newSchema = newSchemaObject.substring(0, newSchemaObject.indexOf(".")).toUpperCase();
					List<Node> listSiblings = SubRecord.selectNodes("preceding-sibling::* | following-sibling::*");
					for (Node sib : listSiblings) {
						List<Node> tmp = selectNodesFrom(sib, "Property[@Name=\"Name\"]", parameterSet);
						if (tmp.size() >= 1) {
							List<Node> propertyNodes = tmp.get(0).selectNodes(
									"preceding-sibling::*[@Name=\"Description\"] | preceding-sibling::*[@Name=\"DisplayValue\"] | following-sibling::*[@Name=\"Description\"] | following-sibling::*[@Name=\"DisplayValue\"]");
							for (Node nodeItem : propertyNodes) {
								if(newSchema.startsWith("CONFLICT:")) {
									logger.error("The schema substitution for node\t"+nodeItem.getUniquePath()+"\tis not univoque");
								}
								nodeItem.setText("\"" + newSchema + "\"");
							}
						}
					}
				}else {
					logger.warn("Package name "+packageName.toUpperCase().trim()+" not found in mapping. See node "+packageNameNode.getUniquePath());
				}
			}
		}
	}

	public static void main(String[] args) throws URISyntaxException, DocumentException, IOException {
		logger.info("Parte il main di testClass");
		TestClass test = new TestClass();
		String filePosition = Initializer.getProperty("DSXMLExportPath");
		logger.trace("Replacing schema in file "+filePosition);
		File xmlExportFile = new File(filePosition);
		// String filePosition="C:\\Users\\g.barri\\Desktop\\email 21 02 19 p calegari\\SUBMAIN.xml";
		// String filePosition="C:\\Users\\g.barri\\Desktop\\email 21 02 19 p calegari\\test.xml";
		// Parse
		Document document = test.parse(xmlExportFile);
		// modify values
		//// first choice: recover info over owner
//		updateMappingInXMLFor(document, "PACKAGE", "ABA21");
//		updateMappingInXMLFor(document, "ET_TABLE", "DBA21");
//		updateMappingInXMLFor(document, "ET_TABLE", "ABA21");
		// second choice: dirty replacement
		updateMappingInXMLFor(document, "PACKAGE");
		updateMappingInXMLFor(document, "ET_TABLE");
		// print result
		Path folder = Initializer.getRunningFolder();
		File copyFile = new File(folder.toString() + "\\ST_" + xmlExportFile.getName());
		FileWriter out = new FileWriter(copyFile);
		OutputFormat format = OutputFormat.createPrettyPrint();
		format.setIndentSize(0);
		format.setNewlines(false);
		format.setTrimText(false);
		format.setPadText(false);
		XMLWriter writer = new XMLWriter(out, format);
		writer.write(document);
		writer.close();
		test.substituteIntext(copyFile, xmlExportFile.getName());
		copyFile.delete();
		logger.info("Termina il main di testClass");
	}
}
