package mkcsv;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utilities.Initializer;

public class MkCsv{
	static Logger logger=LoggerFactory.getLogger(MkCsv.class);
	
	public void spoolExcel(File excelDocument, String output){
		String[] sheetsName= {"Table","Sequence","View","Procedure","DbLink Object","External Table"};
//		Map<String, String> formatMap=new HashMap<>();		
//		formatMap.put(sheetsName[0], "0;1;2; ;3");
//		formatMap.put(sheetsName[1], "0;1;2; ;3");
//		formatMap.put(sheetsName[2], "0;1;2; ;3");
//		formatMap.put(sheetsName[3], "0;1;2; ;3");
//		formatMap.put(sheetsName[4], "0;2;5;9;6");
//		formatMap.put(sheetsName[5], "0;1;4;11;5");
		try(Workbook workbook = WorkbookFactory.create(excelDocument);
				FileWriter writer=new FileWriter(new File(output))){
			for(String sheetName : sheetsName) {
				logger.trace("Start sheet "+sheetName);
				Sheet sheet = workbook.getSheet(sheetName);	
				String format=Initializer.getProperty("format."+sheetName.replace(" ", "_"));
				String[] indexes=format.split(";");
				DataFormatter dataFormatter = new DataFormatter();				
				for (Row row: sheet) {
					if(row.getRowNum()==0) {
						for(int i=0; i<indexes.length; i++) {
							String colName=null;
							try {
								int index=Integer.parseInt(indexes[i].trim());
								colName=dataFormatter.formatCellValue(row.getCell(index));
								logger.info("Using column "+index+": "+colName);
							}catch (NumberFormatException e) {
							}
						}
					}else {
						writer.write(sheetName.toUpperCase());
						for(int i=0; i<indexes.length; i++) {
							String key="";
							try {
								int index=Integer.parseInt(indexes[i].trim());
								key=dataFormatter.formatCellValue(row.getCell(index));
							}catch (NumberFormatException e) {}
							writer.write(";"+key);
						}
						writer.write("\r\n");
					}
				}
				logger.trace("Sheet "+sheetName+" finished");
	        }
			
		}catch (InvalidFormatException e) {
			logger.error("Intecepted error:",e);
		}		
		catch (EncryptedDocumentException | IOException e) {
			logger.error("Intecepted error:",e);
		}
	}
	
	public static void main(String[] args) throws Exception {
		logger.trace("Starting creation csv");
		MkCsv loader=new MkCsv();
		String excel=Initializer.getProperty("excel");
		String output=Initializer.getProperty("output");
		logger.warn("REMEMBER: CHECK IF ONE OF THE FIELDS CONTAINS FORMULAS! FORMULAS ARE COPIED LITTERALY, NOT BY VALUE!");
		logger.trace("excel file: "+excel);
		logger.trace("output    : "+output);
		loader.spoolExcel(new File(excel), output);
		logger.trace("Ending creation csv");
	}
	
}