package prgAnalyzeTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import functionUtilities.AbstractFunctionCall;
import functionUtilities.CallAlteration;

public class GenericPrgAnalyzeTable extends AbstractFunctionCall implements ToPR_Analyze_GreenplumCapable{
	
	static Logger logger=LoggerFactory.getLogger(GenericPrgAnalyzeTable.class);
	private static Map<String, List<Integer>> mapSchemaNumargs=new HashMap<>();
	static {
		ArrayList<Integer> tmp=new ArrayList<>();
		tmp.add(1);
		mapSchemaNumargs.put("DBA25",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		mapSchemaNumargs.put("DBA28",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		mapSchemaNumargs.put("DBA24",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		tmp.add(2);
		mapSchemaNumargs.put("DBA22",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		mapSchemaNumargs.put("DBA27",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		mapSchemaNumargs.put("DBA29",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		mapSchemaNumargs.put("DBA40",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		mapSchemaNumargs.put("DBA41",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		tmp.add(2);
		mapSchemaNumargs.put("DBA21",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		mapSchemaNumargs.put("DBA26",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		mapSchemaNumargs.put("DBA23",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		mapSchemaNumargs.put("DBA30",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		tmp.add(2);
		mapSchemaNumargs.put("DBA42",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		tmp.add(2);
		mapSchemaNumargs.put("DBA60",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		mapSchemaNumargs.put("DBA70",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		tmp.add(2);
		mapSchemaNumargs.put("TBA25",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		tmp.add(2);
		mapSchemaNumargs.put("TAS31",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		tmp.add(2);
		mapSchemaNumargs.put("DAS31",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		tmp.add(2);
		mapSchemaNumargs.put("DAS32",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		tmp.add(2);
		mapSchemaNumargs.put("TAS32",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		mapSchemaNumargs.put("DAS33",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		mapSchemaNumargs.put("TAS33",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		tmp.add(2);
		mapSchemaNumargs.put("DAS36",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		tmp.add(2);
		mapSchemaNumargs.put("TAS36",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		tmp.add(2);
		mapSchemaNumargs.put("REPORT118",tmp);
		tmp=new ArrayList<>();
		tmp.add(1);
		tmp.add(2);
		mapSchemaNumargs.put("DQ501",tmp);
	}

	@Override
	public String getName() {
		return "PRG_ANALYZE_TABLE";
	}

	@Override
	public String getSchema() {
		logger.trace("asked schema at generic function");
		return null;
	}

	@Override
	public int getNumArgs() {
		// TODO Auto-generated method stub
		return -1;
	}
	
	@Override
	public void accept(CallAlteration alterator) {
		alterator.visit(this);
	}
	
	@Override
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean schemaCheck,numArgsCheck, nameCheck=parsedCall[1]!=null && parsedCall[1].trim().toUpperCase().equals(getName().toUpperCase());
		if(parsedCall[0]==null || !GenericPrgAnalyzeTable.mapSchemaNumargs.containsKey(parsedCall[0])) {
			schemaCheck=false;
			numArgsCheck=false;
		}else {
			schemaCheck=true;
			numArgsCheck=false;
			for(Integer integer:mapSchemaNumargs.get(parsedCall[0])) {
				if(integer.equals(parsedCall.length-2)) {
					numArgsCheck=true;
					break;
				}
			}
		}
		return numArgsCheck && nameCheck && schemaCheck;
	}
	
}
