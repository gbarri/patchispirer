package prgAnalyzeTable;

/**
 * classe che rappresenta una invocazione alla classe dg_utilities.PR_ANALYZE sotto greenplum
 * @author g.barri
 *
 */
public class PR_ANALYZE_Greenplum {
	private String schema;
	private String tabella;
	private static String mySchema="dg_utilities";
	private static String myName="PR_ANALYZE";
	
	public PR_ANALYZE_Greenplum(String schema, String tabella) {
		super();
		this.schema = schema;
		this.tabella = tabella;
	}
	public String getSchema() {
		return schema;
	}
	public void setSchema(String schema) {
		this.schema = schema;
	}
	public String getTabella() {
		return tabella;
	}
	public void setTabella(String tabella) {
		this.tabella = tabella;
	}
	
	/**
	 * method that return the string representation of the calling to PR_ANALYZE with the arguments given to its constructor
	 * @return
	 */
	public String getCallAt() {
		String callToPR_ANALYZE=mySchema+"."+myName+"( '"+this.schema.toLowerCase()+"' , '"+this.tabella.toLowerCase()+"' )";
		return callToPR_ANALYZE;
	}
	

}
