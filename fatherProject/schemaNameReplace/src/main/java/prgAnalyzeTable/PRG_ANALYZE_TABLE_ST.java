package prgAnalyzeTable;

import java.io.File;

import org.tinylog.Logger;

import functionUtilities.AbstractFunctionCall;
import functionUtilities.CallAlteration;
import schemaMapping.LoaderSchemaMap;
import utilities.SQLSearcher;

public class PRG_ANALYZE_TABLE_ST extends AbstractFunctionCall implements ToPR_Analyze_GreenplumCapable{
	private static String myName="PRG_ANALYZE_TABLE_ST";
	private static String oracleOwner="DBA21";

	@Override
	public String getName() {
		return myName;
	}

	@Override
	public String getSchema() {
		return oracleOwner;
	}

	@Override
	public int getNumArgs() {
		return 2;
	}

//	@Override
//	protected String getArgumentFunctionName(String call) {
//		String[] argsFromParse=super.parse(call);
//		return argsFromParse[1+2];
//	}
	
	@Override
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean numArgsCheck=parsedCall.length==4 || parsedCall.length==3;
		boolean nameCheck=parsedCall[1]!=null && parsedCall[1].trim().toUpperCase().equals(getName().toUpperCase());
		boolean schemaCheck=parsedCall[0]!=null && parsedCall[0].trim().toUpperCase().equals(getSchema().toUpperCase());
		return numArgsCheck && nameCheck && schemaCheck;
	}
	
	@Override
	public void accept(CallAlteration alterator) {
		alterator.visit(this);
		
	}

}
