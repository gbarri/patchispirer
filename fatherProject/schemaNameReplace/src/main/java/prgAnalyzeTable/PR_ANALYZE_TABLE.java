package prgAnalyzeTable;

import java.io.File;

import org.tinylog.Logger;

import functionUtilities.AbstractFunctionCall;
import functionUtilities.CallAlteration;
import schemaMapping.LoaderSchemaMap;
import utilities.SQLSearcher;

public class PR_ANALYZE_TABLE extends AbstractFunctionCall implements ToPR_Analyze_GreenplumCapable{
	private static String oracleOwner="DBA25";
	private String tname;

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "PR_ANALYZE_TABLE";
	}

	@Override
	public String getSchema() {
		// TODO Auto-generated method stub
		return oracleOwner;
	}

	@Override
	public int getNumArgs() {
		// TODO Auto-generated method stub
		return 2;
	}
	
	@Override
	public void accept(CallAlteration alterator) {
		alterator.visit(this);
		
	}
	
}
