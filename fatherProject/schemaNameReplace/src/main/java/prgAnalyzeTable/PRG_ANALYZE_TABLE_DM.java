package prgAnalyzeTable;

import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.tinylog.Logger;

import functionUtilities.AbstractFunctionCall;
import functionUtilities.CallAlteration;
import schemaMapping.LoaderSchemaMap;
import utilities.SQLSearcher;

public class PRG_ANALYZE_TABLE_DM extends AbstractFunctionCall implements ToPR_Analyze_GreenplumCapable{
	private static String oracleOwner="TAS31";

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "PRG_ANALYZE_TABLE_DM";
	}

	@Override
	public String getSchema() {
		// TODO Auto-generated method stub
		return oracleOwner;
	}

	@Override
	public int getNumArgs() {
		// TODO Auto-generated method stub
		return 1; // 1 or 2 arguments, the first one is the table_name argument
	}
	
	@Override
	public void accept(CallAlteration alterator) {
		alterator.visit(this);
		
	}
	
	@Override
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean numArgsCheck=parsedCall.length>=3 && parsedCall.length<=4;
		boolean nameCheck=parsedCall[1]!=null && parsedCall[1].trim().toUpperCase().equals(getName().toUpperCase());
		boolean schemaCheck=parsedCall[0]!=null && StringUtils.equalsIgnoreCase(parsedCall[0].trim(), getSchema());
		return numArgsCheck && nameCheck && schemaCheck;
	}
	
}
