package prgTruncateTable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import functionUtilities.AbstractFunctionCall;
import functionUtilities.CallAlteration;
import functionUtilities.FunCallAlterer;
import functionUtilities.FunctionCall;
import loaders.Loader;
import loaders.LoaderByColumnIndexesDBExcel;
import schemaMapping.LoaderSchemaMap;
import utilities.StringUtilities;

public class ReplaceWithPrtruncate implements CallAlteration{

	static Logger logger=LoggerFactory.getLogger(ReplaceWithPrtruncate.class);
	ToPR_Truncate_GreenplumCapable functionCall;
	private Integer indexArgumentTableName=null;
	private Map<String, String[]> tableMap=null;
	
	public ReplaceWithPrtruncate(ToPR_Truncate_GreenplumCapable functionCall) {
		this.functionCall=functionCall;
	} 
	
	@Override
	public String alterCall(String str, int[] extrema, FunCallAlterer funCallAlterer) {
		String[] args=((AbstractFunctionCall) functionCall).parse(str.substring(extrema[0], extrema[1]+1));
		functionCall.accept(this);
		String argumentTableName=args[indexArgumentTableName+2].replace("'", "");
		LoaderSchemaMap loader=new LoaderSchemaMap();
		String schema=functionCall.getSchema();
		if(schema==null) {
			//c'è di certo perchè ha superato il check
			schema=args[0];
		}
		//in some cases the qualified table name is used: if it works no need to search for a default schema
		String newSchemaObject=StringUtilities.formatObjectName(loader.loadMap().get(argumentTableName));
		if(newSchemaObject==null) {
			newSchemaObject=StringUtilities.formatObjectName(loader.loadMap().get(schema+"."+argumentTableName.toUpperCase()));
			if(newSchemaObject==null) {
				if(this.tableMap==null) {
					Loader loaderTable=new LoaderByColumnIndexesDBExcel("Loader.useColumnsAt=2,1,3\r\n" +
															"Loader.useSheetAt=5\r\n" + 
															"Loader.usePKAt=0,1");
					try {
						this.tableMap=loaderTable.loadMap();
					} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
						logger.error("problems with excel files", e);
						e.printStackTrace();
					}
				}
				ArrayList<String> tmpCount=new ArrayList<>();
				Iterator<String> iter=this.tableMap.keySet().iterator();
				while(iter.hasNext()) {
					String nextKey=iter.next();
					if(StringUtils.equalsIgnoreCase(nextKey.replaceFirst(".*\\.", ""), argumentTableName)) {
						String newSchema=this.tableMap.get(nextKey)[0];
						if(tmpCount.size()==0) {
							tmpCount.add(newSchema);
						}else {
							for(String addedSchemas:tmpCount) {
								if(!addedSchemas.toLowerCase().equalsIgnoreCase(newSchema)) {
									tmpCount.add(newSchema);
									break;
								}
							}
						}
					}
				}
				if(tmpCount.size()==1) {
					newSchemaObject=tmpCount.get(0).toLowerCase()+"."+argumentTableName.toUpperCase();
				}
				if(newSchemaObject==null) {
					logger.error("object recalled in "+str.substring(extrema[0], extrema[1]+1)+" not found in schema replace mapping");
					return str;
				}
			}
		}
		String analSchema=StringUtilities.detectSchemaInObjectName(newSchemaObject);
		String analTabName=StringUtilities.detectNameInObjectName(newSchemaObject);
		PR_TRUNCATE_Greenplum truncate= new PR_TRUNCATE_Greenplum(analSchema, analTabName);
		String alteredCall=truncate.getCallAt();
		if(alteredCall.equals(str.substring(extrema[0], extrema[1]+1))) {
			return str;
		}else {
			int lastBefore=str.substring(0, extrema[0]).lastIndexOf("\n");
			String pre="";
			if(lastBefore==-1) {
				pre="--";
				lastBefore=0;
			}
			int firstAfter=str.indexOf("\r\n",extrema[1]);
			String after="";
			if(firstAfter==-1) {
				firstAfter=str.indexOf("\n",extrema[1]);
				if(firstAfter==-1) {
					after="\r\n";
					firstAfter=str.length();
				}
			}
			String firstUntouched=str.substring(0,lastBefore);
			String modifiedLines=str.substring(lastBefore, firstAfter);
			String afterUntouched=str.substring(firstAfter);
			String newStr=firstUntouched+
					(firstUntouched.endsWith("\n")?"":"\r\n")+
					pre+modifiedLines.replace("\n", "\n--")+"\r\n"
					+modifiedLines.replace(str.substring(extrema[0], extrema[1]+1), alteredCall)+after					
					+afterUntouched;
//			String newStr=str.substring(0,extrema[0])+"\r\n--"+str.substring(extrema[0],extrema[1]+1)+"\r\n"+alteredCall+(extrema[1]+1<str.length()-1?str.substring(extrema[1]+1):"");
			logger.trace("replaced\n"+str.substring(extrema[0], extrema[1]+1)+"\nwith\n"+alteredCall);
			return newStr;
		}
	}

	@Override
	public void visit(FunctionCall functionCall) {
		indexArgumentTableName=0;
	}

}
