package schemaMapping;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utilities.SQLSearcher;
import utilities.StringUtilities;

public class SchemaNamesUpdateByConfig implements SchemaNamesUpdate{

	private String configFile;
	public Map<String, String> replaceMap; 
	private static String delimiter="::";
	private static Logger logger=LoggerFactory.getLogger(SchemaNamesUpdateByConfig.class);
	
	public String config_path;
	
	public SchemaNamesUpdateByConfig(String  config_path, String configFile)  {
//		this.configFile=configFile;
//		this.config_path=config_path;
//		replaceMap=new HashMap<>();
//		loadMap();
	}
	
	public SchemaNamesUpdateByConfig(String  config_path) {
		//TODO da aggiornare: sql_path NON E' necessaria
		this(config_path,"config.txt");
	}
	
	
	@Override
	public void updateNames(File file) {
		String fileName=file.getAbsolutePath();
		String fullText="";
		boolean found=false;
		boolean toBeMoved=false;
		try(FileInputStream fileStream=new FileInputStream(fileName);
		    Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
			Set<String> keys=replaceMap.keySet();
			while(scann.hasNextLine()) {
				String nextLine=scann.nextLine();
//				logger.debug(nextLine);
				if(!nextLine.trim().startsWith("--")) {
					for(String keyItem:keys) {
						if(nextLine.contains(keyItem)) {
							if(!found) {
								found=true;
								logger.trace("un change in "+file.getName());
							}
							String newLine=nextLine.replaceAll(keyItem,replaceMap.get(keyItem));
							//NB: in replace exactly viene rifatto il check della presenza o meno della chiave...
							//TODO: verificare se chiamare sempre il replaceExactly � po meno pi� veloce di un check iniziale con contains sulla stringa 
							//String newLine=StringUtilities.replaceExactly(nextLine, keyItem,replaceMap.get(keyItem));
//							System.out.println(newLine.equals(nextLine));
//							System.out.println(toBeMoved);
//							System.out.println(!(toBeMoved || newLine.equals(nextLine)) );
							if( !(toBeMoved || newLine.equals(nextLine)) ) {
								logger.info("\n\tIl file "+file.getName()+" � stato modificato\n");
//								toBeMoved=true;
								try {
									if(SQLSearcher.isFunctionDef(nextLine)) {
										toBeMoved=true;
										String alter="ALTER FUNCTION "+keyItem+" SET SCHEMA "+replaceMap.get(keyItem).replaceAll("\\..*","")+";";
										annotatiLaFunction(file, replaceMap.get(keyItem).replaceAll("\\..*","")+"\t"+alter);
									}else if(SQLSearcher.isTableDef(nextLine)) {
										toBeMoved=true;
										String alter="ALTER TABLE "+keyItem+" SET SCHEMA "+replaceMap.get(keyItem).replaceAll("\\..*","")+";";
										annotatiLaTable(file, replaceMap.get(keyItem).replaceAll("\\..*","")+"\t"+alter);
									}else if(SQLSearcher.isViewDef(nextLine)) {
										toBeMoved=true;
										String alter="ALTER VIEW "+keyItem+" SET SCHEMA "+replaceMap.get(keyItem).replaceAll("\\..*","")+";";
										annotatiLaView(file, replaceMap.get(keyItem).replaceAll("\\..*","")+"\t"+alter);
									}
								}catch(Exception e) {
									logger.error("Exception intercepted by insertCaller.updateNames",e);
								}
							}
							nextLine=newLine;
						}
					}
				}
				fullText+=nextLine+"\n";
			}
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while reading/searching for \n\t"+fileName, e);
		}
		if(found) {
			try(FileOutputStream fileOutStream=new FileOutputStream(fileName); 
					PrintWriter print=new PrintWriter(new BufferedOutputStream(fileOutStream))){
				print.print(fullText);
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while writing/searching for \n\t"+fileName, e);
			}
		}
	}
	
	/**
	 * function  che deve leggere da configFile e caricare la mappa delle sostituzioni
	 * 
	 */
	private void loadMap() {
		try(FileInputStream fileStream=new FileInputStream(config_path+"\\"+configFile);
			Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
			while(scann.hasNextLine()) {
				String nextLine=scann.nextLine();
				StringTokenizer st=new StringTokenizer(nextLine, delimiter);
				if(st.countTokens()!=2) {
					throw new IllegalArgumentException("Config. file not in desired form");
				}
				replaceMap.put(st.nextToken(), st.nextToken());
			}
		}catch (FileNotFoundException e) {
			throw new RuntimeException("Config file missing");
		}
		catch (IOException e) {
			//TODO pheraps in case something goes wrong, simply the mapp reverts to identity?
			throw new RuntimeException("Exception raised while reading/searching for a config. file", e);
		}
	}
	
	
	private void annotatiLaFunction(File file, String string) {
		annotatiLaSmth(file, "FUNCTION", string);
	}
	
	private void annotatiLaTable(File file, String string) {
		annotatiLaSmth(file, "TABLE", string);
	}
	
	private void annotatiLaView(File file, String string) {
		annotatiLaSmth(file, "VIEW", string);
	}
	
	private void annotatiLaSmth(File file, String typeDef, String note) {
		logger.debug("annoto into file: "+config_path+"\\MOVED_"+typeDef+".sql");
		try(FileWriter print=new FileWriter(config_path+"\\MOVED_"+typeDef+".sql", true)) {
			print.append(file.getAbsolutePath()+"\t"+note+"\n");
		}catch (IOException e) {
			throw new RuntimeException("2Exception raised while writing to \n\t"+config_path+"\\MOVED_"+typeDef+".sql", e);
		}
	}

}
