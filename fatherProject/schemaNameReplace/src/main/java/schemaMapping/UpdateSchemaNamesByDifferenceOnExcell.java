package schemaMapping;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jsqlparser.parser.ParseException;
import net.sf.jsqlparser.parser.TokenMgrException;
import utilities.SQLSearcher;
import utilities.StringUtilities;

public class UpdateSchemaNamesByDifferenceOnExcell extends AbstractSchemaNamesUpdate {
	
	private Map<String, String> replaceMap;
	Logger logger = LoggerFactory.getLogger(this.getClass());
	public String excellName;
	public String excelPath;
	
	public String loaderUpdateMapConfigPath="C:\\Users\\g.barri\\git\\repository\\patchIspirer\\patchIspirer\\src\\main\\resources\\LoaderUpdateMap.properties";
	
	public UpdateSchemaNamesByDifferenceOnExcell(String  excelName, String excelPath) throws InvalidFormatException, IOException  {
		this.excellName=excelName;
		this.excelPath=excelPath;
		replaceMap=new HashMap<>();
		//LoaderSchemaMap loader=new LoaderSchemaMap(this.excellName, this.excelPath);
		LoaderUpdateMap loader=new LoaderUpdateMap(this.excellName, this.excelPath, "", loaderUpdateMapConfigPath);
		try {
			Map<String, List<String>> newMap=loader.loadMap();
			for(String item: newMap.keySet()) {
				replaceMap.put(item, newMap.get(item).get(0));
			}
//			replaceMap=loader.loadMap();
		} catch (EncryptedDocumentException e) {
			System.out.println("Failed to load the replacement map (is it open in Excell?).");
			e.printStackTrace();
			throw e;
		} catch (InvalidFormatException e) {
			System.out.println("The excell is not in the xlsx format.");
			e.printStackTrace();
			throw e;
		} catch (IOException e) {
			System.out.println("Failed to load the replacement map.");
			e.printStackTrace();
			throw e;
		}
	}
	
	@Override
	public void updateNames(File file) {
		String fileName=file.getAbsolutePath();
		String fullText="";
		boolean found=false;
		boolean toBeMoved=false;
		try(FileInputStream fileStream=new FileInputStream(fileName);
		    Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
			Set<String> keys=replaceMap.keySet();
			while(scann.hasNextLine()) {
				String nextLine=scann.nextLine();
				if(!nextLine.trim().startsWith("--")) {
					for(String keyItem:keys) {
						if(nextLine.contains(keyItem)) {
							if(!found) {
								found=true;
								//logger.trace("");
							}
							//nextLine=nextLine.replaceAll(keyItem,replaceMap.get(keyItem));
							//NB: in replace exactly viene rifatto il check della presenza o meno della chiave...
							//TODO: verificare se chiamare sempre il replaceExactly � po meno pi� veloce di un check iniziale con contains sulla stringa 
							String newLine;
							try {
								newLine = StringUtilities.replaceExactly(nextLine, keyItem,replaceMap.get(keyItem));
							} catch (TokenMgrException | ParseException e1) {
								newLine = nextLine.replaceAll(keyItem,replaceMap.get(keyItem));
							}
							if( !(toBeMoved || newLine.equals(nextLine)) ) {
								logger.info("\n\tIl file "+file.getName()+" deve essere spostato (variazione del suo schema attuale)\n");
								toBeMoved=true;
								try {
									if(SQLSearcher.isFunctionDef(nextLine)) {
										annotatiLaFunction(file, replaceMap.get(keyItem).replaceAll("\\..*",""));
									}else if(SQLSearcher.isTableDef(nextLine)) {
										annotatiLaTable(file, replaceMap.get(keyItem).replaceAll("\\..*",""));
									}else if(SQLSearcher.isViewDef(nextLine)) {
										annotatiLaView(file, replaceMap.get(keyItem).replaceAll("\\..*",""));
									}
								}catch(Exception e) {
									logger.error("Exception intercepted by insertCaller.updateNames",e);
								}
							}
							nextLine=newLine;
						}
					}
				}
				fullText+=nextLine+"\n";
			}
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while reading/searching for \n\t"+fileName, e);
		}
		if(found) {
			try(FileOutputStream fileOutStream=new FileOutputStream(fileName); 
					PrintWriter print=new PrintWriter(new BufferedOutputStream(fileOutStream))){
				print.print(fullText);
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while writing/searching for \n\t"+fileName, e);
			}
		}
	}
	
	protected void annotatiLaFunction(File file, String string) {
		annotatiLaSmth(file, "FUNCTION", string);
	}
	
	protected void annotatiLaTable(File file, String string) {
		annotatiLaSmth(file, "TABLE", string);
	}
	
	protected void annotatiLaView(File file, String string) {
		annotatiLaSmth(file, "VIEW", string);
	}
	
	private void annotatiLaSmth(File file, String typeDef, String note) {
		try(FileWriter print=new FileWriter(excelPath+"\\MOVED_"+typeDef+".sql", true)) {
			print.append(file.getAbsolutePath()+"\t"+note+"\n");
		}catch (IOException e) {
			throw new RuntimeException("1Exception raised while writing to \n\t"+excelPath+"\\MOVED_"+typeDef+".sql", e);
		}
	}

	@Override
	public String getExcelPath() {
		return excelPath;
	}
}
