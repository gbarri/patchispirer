package schemaMapping;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.poi.EncryptedDocumentException;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jsqlparser.parser.TokenMgrException;
import utilities.SQLSearcher;

/**
 * this class is supposed for checking if an object is used without its proper schema first
 * @author g.barri
 *
 */
public class CheckForSchemaPresence extends SimpleFileVisitor<Path>{

	static Logger logger=LoggerFactory.getLogger(CheckForSchemaPresence.class); 
	private List<File> XMLfile;
	private String packageName;
	public String XMLfiles="C:\\Users\\g.barri\\Documents\\FCA_technology\\archive output ispirer\\XMLS";
	private Map<String, String> replaceMap=null;
	private Map<String, List<String>> inverseReplaceMap=null;
	
	public CheckForSchemaPresence() {
		super();
		this.XMLfile = new ArrayList<>();
		this.packageName = null;
	}
	
	public Document parse(File file) throws DocumentException {
	        SAXReader reader = new SAXReader();
	        Document document = reader.read(file);
	        return document;
	}
	 
	public List<Node> detectNodes(Document document){
		 String Xpath="//Table | //View | //Synonym | //Package";
		 return document.selectNodes(Xpath);
	}
	
	public List<Node> detectNodes(File file){
		try {
			Document document=this.parse(file);
			List<Node> nodes=this.detectNodes(document);
			return nodes;
		} catch (DocumentException e1) {
			e1.printStackTrace();
			logger.error("Intercepted exception "+e1.getClass()+": "+e1.getMessage());
			return new ArrayList<Node>();
		}
	}
	
	@Override
	public FileVisitResult visitFile(Path filepath, BasicFileAttributes attrs)   throws IOException{
		try {
			Document doc=this.parse(filepath.toFile());
			List<Node> nodes=doc.selectNodes("//Package[normalize-space(@name)='"+packageName+"']");
			if(nodes.size()>0) {
				XMLfile.add(filepath.toFile());
			}
		} catch (DocumentException e) {
//			e.printStackTrace();
		}
		return FileVisitResult.CONTINUE; 
    }
	
	@Override
	public FileVisitResult visitFileFailed(Path file,IOException exc) throws IOException{
		logger.error("intercepted IOexception "+exc.getMessage());
		return FileVisitResult.CONTINUE;
		
	}
	
	public File checkFor(String packageName, File rootFolder) throws IOException {
		this.packageName=packageName;
		this.XMLfile=new ArrayList<>();
		Files.walkFileTree(rootFolder.toPath(), this);
		if(XMLfile.size()==1) {
			logger.trace("found 1 xml file containing package "+packageName+": "+XMLfile.get(0).getAbsolutePath());
			return XMLfile.get(0);
		}else if(XMLfile.size()>1) {
			logger.trace("found "+XMLfile.size()+" xml files containing package "+packageName);
			return XMLfile.get(0);
		}else {
			logger.warn("found 0 XML files related to "+packageName);
			return null;
		}
	}
	
	public File checkFor(String packageName, String rootFolderPath) throws IOException {
		return this.checkFor(packageName, new File(rootFolderPath));
	}
		
	private String searchForAppropriateSchema(String objectName, File file) {
		try {
			if(this.replaceMap==null) {
				logger.trace("loading replace map from Domenica's document");
				this.replaceMap=new LoaderSchemaMap().loadMap();
			}
			//we search without the schema, and keep the result only if there is just one correspondence
			List<String> replaces=new ArrayList<>();
			for(String keyItem:replaceMap.keySet()) {
				if(keyItem.replaceAll(".*\\.", "").trim().equals(objectName)){
					replaces.add(replaceMap.get(keyItem));
				}
			}
			if(replaces.size()==1) {
				String schema=replaces.get(0).replaceAll("\\..*", "");
				logger.trace("The schema to be added to "+objectName+" is "+schema);
				return schema;
			}else if(replaces.size()==0){
				logger.warn("No object with the name "+objectName+" has been found. No schema can be provided.");
				throw new IllegalArgumentException("No object with the name "+objectName+" has been found. No schema can be provided.");
			}else {
				//in caso di schema multipli, per prima cosa cerchiamo di individuare quello corretto.
				//in caso di schema multipli, se uno di essi è lo schema greenplum della function chiamante, si preferisce e si usa quello
				String mainObjectSchema=SQLSearcher.detectSchema(file);
				if(mainObjectSchema!=null) {
					for(String schemaItem:replaces) {
						if(schemaItem.equals(mainObjectSchema)) {
							logger.trace("The schema to be added to "+objectName+" is ASSUMED to be "+schemaItem);
							return schemaItem;
						}
					}
				}
				logger.warn("There are more than one choice for the schema of "+objectName+", used in "+file.getName()+". Pls check manually which one is correct ("+file.getAbsolutePath());
				throw new IllegalArgumentException("There are more than one choice for the schema of "+objectName+", pls check manually which one is correct.");
			}
			
		}catch(EncryptedDocumentException e) {
			logger.error("intercepted exception "+e.getMessage());
			throw new IllegalArgumentException("bleah");
		}
	}
	
	public void checkForAbsentSchema(File file) {
//		file=new File("C:\\Users\\g.barri\\Desktop\\wrk\\tl_analytics_mp_dw_classe_merc.sql");
		//devo cercare l'XML corretto
		File XML=null;
		try {
			XML = this.checkFor(SQLSearcher.detectName(file).replaceAll(".*\\.", ""), XMLfiles);
		} catch (NullPointerException | IOException e1) {
			logger.error("Intercepted exception "+e1.getClass()+": "+e1.getMessage());
			XML=null;
		}
		if(XML==null) {
			return;
		}
		List<Node> nodes=detectNodes(XML);
		
		try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
				Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
			while(scann.hasNextLine()) {
				String nextLine=scann.nextLine();
				if(checkLine(nextLine)) {
					for(Node nodeItem : nodes) {
						String objectName=nodeItem.valueOf("@name");
						String keySpace1=" "+objectName+" "+objectName;
						String keySpace2=" "+objectName+"  "+objectName;
						String keyAS=" "+objectName+" AS "+objectName;
						try {
//							boolean found=StringUtilities.findExactly(nextLine,keySpace).size()>0;
							boolean found=nextLine.contains(keySpace1) || nextLine.contains(keySpace2) ;
							if(!found) {
//								found=StringUtilities.findExactly(nextLine,keyAS).size()>0;
								found=nextLine.contains(keyAS);
							}
							if(found) {
								logger.warn("detected presumed variable synonim for "+objectName+" in line "+nextLine+" ("+file.getAbsolutePath());
//								searchForAppropriateSchema(objectName);
							}
						} catch (TokenMgrException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while reading for \n\t"+file.getAbsolutePath(), e);
		}
	}
	
	
	public void InsertAbsentSchema(File file) {
		
		//devo cercare l'XML corretto
		File XML=null;
		try {
			XML = this.checkFor(SQLSearcher.detectName(file).replaceAll(".*\\.", ""), XMLfiles);
		} catch (NullPointerException | IOException e1) {
			logger.error("Intercepted exception "+e1.getClass()+": "+e1.getMessage());
			XML=null;
		}
		if(XML==null) {
			return;
		}
		List<Node> nodes=detectNodes(XML);
		String text="";
		try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
				Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
			while(scann.hasNextLine()) {
				String nextLine=scann.nextLine();
				if(checkLine(nextLine)) {
					for(Node nodeItem : nodes) {
						String objectName=nodeItem.valueOf("@name");
						//dato che contains non ammette regex dobbiamo provare più combinazioni...
						String keySpace1=" "+objectName+" "+objectName;
						String keySpace2=" "+objectName+"  "+objectName;
						String keyAS=" "+objectName+" AS "+objectName;
						String[] tries= {keySpace1, keySpace2, keyAS};
						for(String keyItem : tries) {
							try {
								//la mancanza di una function affidabile per la ricerca di espressioni regolari sta divenendo sempre più un problema...
								boolean found=nextLine.contains(keySpace1) || nextLine.contains(keySpace2) || nextLine.contains(keyAS);
								if(found) {
									logger.warn("detected presumed variable synonim for "+objectName+" in line "+nextLine+" ("+file.getAbsolutePath());
									try {
										String schema=searchForAppropriateSchema(objectName, file);
//										String schema=searchForAppropriateSchema(objectName);													
//										if(schema!=schema2) {
//											logger.error("The new function searchForAppropriateSchema(objectName, file); is not coherent with the old one");
//											throw new IllegalArgumentException("controllami meglio");
//										}
										nextLine=nextLine.replaceFirst(keyItem, keyItem.replaceFirst(objectName, schema+"."+objectName));
									}catch(IllegalArgumentException e) {
										//TODO remove this one
//										e.printStackTrace();
									}
								}
							} catch (TokenMgrException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}
				text+=nextLine+"\r\n";
//				System.out.println(text);
			}
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while reading for\t"+file.getAbsolutePath(), e);
		}
		
		try(FileOutputStream fileOutStream=new FileOutputStream(file.getAbsolutePath()); 
				PrintWriter print=new PrintWriter(new BufferedOutputStream(fileOutStream))){
			print.print(text);
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while writing\n\t"+file.getAbsolutePath(), e);
		}
	}
	
	
	/**
	 * 
	 * @param nextLine
	 * @return
	 */
	private boolean checkLine(String nextLine) {
		return !nextLine.trim().startsWith("--");
	}

//	public static void main(String[] args) throws DocumentException, IOException {
//		String path="C:\\Users\\g.barri\\Desktop\\MAT_5626\\MAT_5626\\MAT_20190108\\XXSCHAPP_MP_DM_GIACENZE.xml";
//		CheckForSchemaPresence check=new CheckForSchemaPresence();
////		check.checkFor("MP_DW_CLASSE_MERC", new File("C:\\Users\\g.barri\\Desktop\\MAT_5626\\MAT_5626\\MAT_20190108"));
//		check.checkForAbsentSchema(null);
//		
//	}
	
}
