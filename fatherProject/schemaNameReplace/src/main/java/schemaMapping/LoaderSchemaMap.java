package schemaMapping;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hpsf.Property;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.util.StringUtil;
import org.slf4j.LoggerFactory;
import org.tinylog.Logger;

import loaders.LoaderMultipleSheetsDBExcel;
import utilities.Initializer;

public class LoaderSchemaMap {
	
	static org.slf4j.Logger logger=LoggerFactory.getLogger(LoaderSchemaMap.class);
	
/* parametri legati ad usare un file di configurazione fatto a mano */
	private static String configFile="replace.map";
	//public Map<String, String> replaceMap; 
	private static String delimiter="::";
	public String config_path;
	private static Map<String, String> replaceMap=null;

/* parametri di lettura dal file di excel */
	public String excelName;
	public String excelPath;
	
	public LoaderSchemaMap() {
		String DBexcelDOcument=Initializer.getProperty("excelDocument");
		this.excelPath=DBexcelDOcument.substring(0,DBexcelDOcument.lastIndexOf("\\"));
		this.excelName=DBexcelDOcument.substring(DBexcelDOcument.lastIndexOf("\\")+1);
		if(this.excelPath==null) {
			throw new IllegalArgumentException("Parameter excelPath has not been found. Check the file "+Initializer.configFile);
		}
		if(this.excelName==null) {
			throw new IllegalArgumentException("Parameter excelName has not been found. Check the file "+Initializer.configFile);
		}
	}
	
	@Deprecated
	public LoaderSchemaMap(String excelName, String excelPath) {
		this.excelName=excelName;
		this.excelPath=excelPath;	
	}
	
	protected String formReplace(String schema, String obj, Sheet sheet) {
		if(sheet.getSheetName().trim().equalsIgnoreCase("External Table")) {
			return schema+"."+(obj.replaceFirst("\\bEXT_", "ET_"));
		}else {
			return schema+"."+obj;
		}
	}
	
	protected String formKey(String oldSchema, String obj, Sheet sheet) {
		return oldSchema+"."+obj;
	}
	
	protected Map<String, List<String>> popolateMap(Sheet sheet, Map<String, List<String>> outMap) {
		   String propertyFile=Initializer.getProperty("LoaderSchemaMapPropertyFile");
		   Properties properties=new Properties();
		   if(!new File(propertyFile).exists()) {
			   throw new IllegalArgumentException("Missing property file "+propertyFile+" indicated by LoaderSchemaMapPropertyFile in the configuration file");
		   }
		   try {
			properties.load(new FileReader(propertyFile));
		} catch (IOException e1) {
			logger.error("Intercepted exception "+e1.getClass()+". "+e1.getMessage(), e1);
			throw new IllegalArgumentException(e1);
		}
	       DataFormatter dataFormatter = new DataFormatter();
	       logger.trace("Retrivieng "+sheet.getSheetName());
	       try {
	    	   int indColTargetSchema=Integer.parseInt(properties.getProperty((sheet.getSheetName().trim().replace(" ", "_")+".targetSchema").trim()));
	    	   int indColOldSchema=Integer.parseInt(properties.getProperty((sheet.getSheetName().trim().replace(" ", "_")+".oldSchema").trim()));
	    	   int indColObjName=Integer.parseInt(properties.getProperty((sheet.getSheetName().trim().replace(" ", "_")+".targetObject").trim()));
	    	   int indColObjMigr=Integer.parseInt(properties.getProperty((sheet.getSheetName().trim().replace(" ", "_")+".migration").trim()));
	    	   for (Row row: sheet) {
	    		   if(row.getRowNum()==0) {
	    			   continue;
	    		   }
	    		   String migration= dataFormatter.formatCellValue(row.getCell(indColObjMigr)).trim();
	    		   if(!migration.isEmpty() && migration.equalsIgnoreCase("YES")) {
	    			   String obj=dataFormatter.formatCellValue(row.getCell(indColObjName));
	    			   obj=obj.replaceFirst("\\bEXT_", "ET_"); //replace EXT_ with ET_
	    			   String targetSchema= dataFormatter.formatCellValue(row.getCell(indColTargetSchema));
	    			   String oldSchema = dataFormatter.formatCellValue(row.getCell(indColOldSchema));
	    			   String key=formKey(oldSchema, obj, sheet);
	    			   String replace=formReplace(targetSchema, obj, sheet);
	    			   if(!targetSchema.equals(oldSchema)) {
	    				   if(!outMap.containsKey(key)) {
	    					   outMap.put(key, new ArrayList<>());
	    				   }
	    			       outMap.get(key).add(replace);
	    			   }
	    		   }
	           	}
	       }catch(NumberFormatException e) {
	    	   logger.error("malformed parameter in configuration file "+propertyFile,e);
	    	   throw e;
	       }
	       logger.trace("--Completed "+sheet.getSheetName());
	       return outMap;
		}
	
	
	protected void printMappingToFile(Map<String, String> outMap) {
		
		try(FileOutputStream fileOutStream=new FileOutputStream(excelPath+"\\"+configFile); 
			PrintWriter print=new PrintWriter(new BufferedOutputStream(fileOutStream))){
			for(String keyItem: outMap.keySet()) {
					print.println(keyItem+"::"+outMap.get(keyItem));
			}
			logger.trace("Schema replacement map as config file printed at "+excelPath+"\\"+configFile);
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while writing the replacement map", e);
		}
	}
	
	protected void printMappingToFile2(Map<String, List<String>> outMap) {
		
		try(FileOutputStream fileOutStream=new FileOutputStream(excelPath+"\\"+configFile+".inverse"); 
			PrintWriter print=new PrintWriter(new BufferedOutputStream(fileOutStream))){
			for(String keyItem: outMap.keySet()) {
					print.print(keyItem+"::[");
					for(String s:outMap.get(keyItem)) {
						print.print(s+",");
					}
					print.println("]");
			}
			logger.trace("Inverse Schema replacement map as config file printed at "+excelPath+"\\"+configFile+"inverse");
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while writing the replacement map", e);
		}
	}
	
	public Map<String, String> loadMap_OLD(){
		if(replaceMap!=null) {
			return replaceMap;
		}else {
			// Creating a Workbook from an Excel file (.xls or .xlsx)
			String xlsxFile=excelPath+"\\"+excelName;
			try(Workbook workbook = WorkbookFactory.create(new File(xlsxFile));) {
				Map<String, List<String>> outMap=new HashMap<>();
				// Getting the Sheets from the excell file
				Sheet sheet = workbook.getSheet("Table");
				outMap=this.popolateMap(sheet, outMap);
				sheet = workbook.getSheet("External Table");
				outMap=this.popolateMap(sheet, outMap);
				sheet = workbook.getSheet("View");
				outMap=this.popolateMap(sheet, outMap);
				sheet = workbook.getSheet("Procedure");
				outMap=this.popolateMap(sheet, outMap);
				sheet = workbook.getSheet("Sequence");
				outMap=this.popolateMap(sheet, outMap);
				sheet = workbook.getSheet("DbLink Object");
				outMap=this.popolateMap(sheet, outMap);
				logger.trace("--Checking if multiple istances are relevated");
				Map<String, String> outSS=new HashMap<>();
		        for(String key:outMap.keySet()) {
		    	   List<String> tmp=outMap.get(key);
		    	   if(tmp.size()>1) {
		    		   logger.error("the replace map for the schema of\t"+key+"\tin file "+Initializer.getProperty("excelDocument")+" is not unique! We keep the first one found\t"+tmp+"\t");
		    	   }
		    	   outSS.put(key, tmp.get(0));
		        }
				replaceMap=new HashMap<>(outSS);
				printMappingToFile(outSS);
	        
			} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
				logger.error("Loading of the schema replace map failed! "+e.getMessage());
				return new HashMap<>();
			}
			return replaceMap;
		}
	}
	
	public Map<String, String> loadMap(){
		if(replaceMap!=null) {
			return replaceMap;
		}else {
			LoaderMultipleSheetsDBExcel loaderMS=new LoaderMultipleSheetsDBExcel(new File(Initializer.getProperty("NEWLoaderSchemaMapPropertyFile")));
			Map<String, String[]> outMap;
			try {
				outMap=loaderMS.loadMap();
			} catch (EncryptedDocumentException | InvalidFormatException | IOException e1) {
				outMap=null;
				e1.printStackTrace();
				RuntimeException e=new RuntimeException("Impossibile accedere al file "+Initializer.getProperty("excelDocument")+": chiudi il file excel!");
				logger.error("Exception raised: ",e);
				throw e;
				//TODO usare ashii art per ingrandire i caratteri;P
			}
			Map<String, String> outSS=new HashMap<>();
	        for(String key:outMap.keySet()) {
	    	   String[] tmp=outMap.get(key);
	    	   String replace=null;
	    	   if(tmp.length==3) {
	    		   if(!tmp[2].trim().equals("")) {
	    			   replace=tmp[0].toLowerCase()+"."+tmp[2].toUpperCase();
	    		   }else {
	    			   replace=tmp[0].toLowerCase()+"."+tmp[1].toUpperCase();
	    		   }
	    	   }else {
	    		   replace=tmp[0].toLowerCase()+"."+tmp[1].toUpperCase();
	    	   }
	    	   outSS.put(key.toUpperCase(), replace);
	        }
			replaceMap=new HashMap<>(outSS);
			printMappingToFile(outSS);

			return replaceMap;
		}
	}
	
	
	public Map<String, List<String>> loadInverseMap(){
		Map<String, String> repMap=this.loadMap();
		Map<String, List<String>> inverseMap=new HashMap<>();
		for(String key: repMap.keySet()) {
			String value=repMap.get(key);
			if(!inverseMap.containsKey(value)) {
				inverseMap.put(value, new ArrayList<>());
			}
			inverseMap.get(value).add(key);
		}
		printMappingToFile2(inverseMap);
		return inverseMap;
	}

}
