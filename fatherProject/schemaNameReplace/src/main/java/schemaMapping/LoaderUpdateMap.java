package schemaMapping;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoaderUpdateMap {

	private static Logger logger=LoggerFactory.getLogger(LoaderUpdateMap.class);
	
/* parametri legati ad usare un file di configurazione */
	public String configFile="replace.map";
	public String delimiter="::";
	public String outConfigPath;

/* parametri di lettura dal file di excel */
	public String excelName;
	public String excelPath;

/* properties */
	public String propertyFile;
	Properties properties=new Properties();
	
	public LoaderUpdateMap(String excelName, String excelPath, String outConfigPath, String propertyFile) {
		this.excelName=excelName;
		this.excelPath=excelPath;
		this.outConfigPath=outConfigPath;//cosa serviva?
		this.propertyFile=propertyFile;
		try(InputStream input=new FileInputStream(propertyFile)) {
			properties.load(input);
		} catch (IOException e) {
			logger.error("Unable to find the configuration file of the properties for LoaderUpadteMap",e);
		}
	}
	
	protected String formReplace(String schema, String obj, Sheet sheet) {
		if(sheet.getSheetName().trim().equalsIgnoreCase("External Table")) {
			return schema+"."+(obj.replaceFirst("\\bEXT_", "ET_"));
		}else {
			return schema+"."+obj;
		}
	}
	
	protected String formKey(String oldSchema, String obj, Sheet sheet) {
		return oldSchema+"."+obj;
	}
	
	private Map<String, List<String>> popolateMap(Sheet sheet, Map<String, List<String>> outMap) {
       DataFormatter dataFormatter = new DataFormatter();
       logger.trace("Retrivieng "+sheet.getSheetName());
       try {
    	   int indColTargetSchema=Integer.parseInt(properties.getProperty((sheet.getSheetName()+".targetSchema").trim()));
    	   int indColOldSchema=Integer.parseInt(properties.getProperty((sheet.getSheetName()+".oldSchema").trim()));
    	   int indColObjName=Integer.parseInt(properties.getProperty((sheet.getSheetName()+".targetObject").trim()));
    	   int indColObjMigr=Integer.parseInt(properties.getProperty((sheet.getSheetName()+".migration").trim()));
    	   for (Row row: sheet) {
    		   String migration= dataFormatter.formatCellValue(row.getCell(indColObjMigr)).trim();
    		   if(!(migration.isEmpty() || migration.equalsIgnoreCase("NO"))) {
    			   String obj=dataFormatter.formatCellValue(row.getCell(indColObjName));
    			   
    			   String targetSchema= dataFormatter.formatCellValue(row.getCell(indColTargetSchema));
    			   String oldSchema = dataFormatter.formatCellValue(row.getCell(indColOldSchema));
    			   String key=formKey(oldSchema, obj, sheet);
    			   String replace=formReplace(targetSchema, obj, sheet);
    			   if(!targetSchema.equals(oldSchema)) {
    				   if(!outMap.containsKey(key)) {
    					   outMap.put(key, new ArrayList<>());
    				   }
    			       outMap.get(key).add(replace);
    			   }
    		   }
           	}
       }catch(NumberFormatException e) {
    	   logger.error("malformed parameter in configuration file "+propertyFile,e);
    	   throw e;
       }
       logger.trace("--Completed "+sheet.getSheetName());
       return outMap;
	}
	
	private void printMappingToFile(Map<String, List<String>> outMap) {
		
		try(FileOutputStream fileOutStream=new FileOutputStream(excelPath+"\\"+configFile); 
			PrintWriter print=new PrintWriter(new BufferedOutputStream(fileOutStream))){
			for(String keyItem: outMap.keySet()) {
				if(outMap.get(keyItem).size()>1) {
					logger.error("E' stato rilevato un replace multiplo per la chiave "+keyItem+" (presente "+outMap.get(keyItem).size()+" volte)");
					throw new IllegalArgumentException("multiple instances of the same replace");
				}
//				print.println(keyItem+"::"+outMap.get(keyItem)); //TODO
				print.println(keyItem+"::"+outMap.get(keyItem).get(0));
			}
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while writing "+excelPath+"\\"+configFile, e);
		}
	}
	
	public Map<String, List<String>> loadMap() throws EncryptedDocumentException, InvalidFormatException, IOException{
		Map<String, List<String>> outMap=new HashMap<>();
		String xlsxFile=excelPath+"\\"+excelName;
		try(Workbook workbook = WorkbookFactory.create(new File(xlsxFile));) {
			String[] sheetsNames= {"Table","View","Procedure"};
			for(String sheetNameItem:sheetsNames) {
				outMap=popolateMap(workbook.getSheet(sheetNameItem), outMap);
			}
	        printMappingToFile(outMap);
	        
		} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
			//TODO da sistemare
			throw e;
		}
		
		return outMap;
	}
	
}

