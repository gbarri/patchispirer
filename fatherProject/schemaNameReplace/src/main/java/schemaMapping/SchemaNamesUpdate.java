package schemaMapping;

import java.io.File;

/**
 * Interface meant for the update of the names of the schemas in the new greenplum DB.
 * The mapping of the schema names is recovered from a config text
 * @author g.barri
 *
 */
public interface SchemaNamesUpdate {
	/**
	 * Metodo per l'update dei nomi degli schema presenti nel file.
	 * @param file il file da modificare
	 */
	void updateNames(File file);
}
