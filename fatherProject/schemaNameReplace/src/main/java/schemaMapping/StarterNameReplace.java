package schemaMapping;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utilities.Initializer;
import utilities.SQLSearcher;


public class StarterNameReplace {
	
	static Logger logger=LoggerFactory.getLogger(StarterNameReplace.class);
	
	public static File[] loadFiles(File folder) {
		File[] listOfFiles = folder.listFiles();
		return listOfFiles;
	}
	
	private static boolean checkFolder(File folder) {
		boolean cond=!folder.getName().startsWith("_") && !folder.getName().equals("oracompat");
		return cond;
	}
	
	private static boolean checkFile(File file) {
		return file.getName().endsWith(".sql");
	}
		
	private static void updateFileIn(SchemaNamesUpdate schup, String sql_dir) {
		File folder = new File(sql_dir);
		if(!folder.isDirectory()) {
			//System.out.println(">>>>NOTE:\n\t"+sql_dir+"\n is not a directory.");
			if(folder.isFile() && checkFile(folder)) {
				File file=folder;//only to help the readability of the code
				schup.updateNames(file);
			}
		}else if(checkFolder(folder)){
			File[] listOfFiles=loadFiles(folder);
			for (File file : listOfFiles) {
				//System.out.println(file.getName());
				String sqlFileAbsPath=sql_dir+"\\"+file.getName();
				//System.out.println(sqlFileAbsPath);
				if (file.isFile() && file.getName().endsWith("sql")) {
					schup.updateNames(file);
				}else if(file.isDirectory()) {
					updateFileIn(schup, sqlFileAbsPath);
				}
			}
		}
	}
	
	private static void checkOlderSchema(String sql_dir) {
		File folder = new File(sql_dir);
		if(!folder.isDirectory()) {
			if(folder.isFile() && folder.getName().endsWith(".sql")) {
				File file=folder;//only to help the readability of the code
				folder=null;
				SQLSearcher.checkActiveOracleSchema(file);
			}
		}else if(!folder.getName().startsWith("_") && !folder.getName().equals("oracompat")){
			//logger.trace("controlling folder "+folder.getAbsolutePath());
			File[] listOfFiles=loadFiles(folder);
			for (File file : listOfFiles) {
				checkOlderSchema(file.getAbsolutePath());
			}
		}
	}
	
	public static void main(String[] args) throws InvalidFormatException, IOException {
		logger.trace("Start");
		/*all*/
//		String sql_path="C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\EDWP2";
		/*single target schema*/
//		String sql_path="C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\EDWP2\\bl_mfg";
//		String sql_path="C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\EDWP2\\bl_scm_ind";
//		String sql_path="C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\EDWP2\\dg_utilities";
//		String sql_path="C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\EDWP2\\lg_mfg";
//		String sql_path="C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\EDWP2\\tl_analytics";
//		String sql_path="C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\EDWP2\\tl_mfg";
//		String sql_path="C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\EDWP2\\tl_pur";
//		String sql_path="C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\EDWP2\\tl_scm_ind\\views\\dw";
		/*test folder outside repository*/
//		String sql_path="C:\\Users\\g.barri\\Desktop\\wrk2";
//		String sql_path="C:\\Users\\g.barri\\Desktop\\wrk\\XXDWHAPP_MP_ST_CAUSALI_MDR";
		String sql_path=Initializer.getProperty("workingDir");
//		String sql_path="C:\\Users\\g.barri\\Desktop\\ALL_PROC_5626\\ALL_PROC_5626\\XXDWHAPP_PR_POST_XXDWHAPP_DW_CUST_REQ\\tl_scm_ind_pr_post_xxdwhapp_dw_cust_req.sql";
		logger.info("Directory considered: "+sql_path);
		
		String configPath=Initializer.getProperty("StarterNameReplace.configPath");
		String configFile=Initializer.getProperty("StarterNameReplace.configFile");
		
//		String DBexcelDocument=Initializer.getProperty("excelDocument");
//		String excelName=DBexcelDocument.substring(DBexcelDocument.lastIndexOf("\\"+1));//"EDWP - SupplyChain & Manufacturing (43).xlsx";
//		String excellPath=DBexcelDocument.substring(0, DBexcelDocument.lastIndexOf("\\"));//"C:\\Users\\g.barri\\Downloads";
//		
//		logger.info("Reading metadata from : "+excellPath+"\\"+excelName);
		
		/* reading from .map*/
		//SchemaNamesUpdate schup=new SchemaNamesUpdateByConfig(configPath, configFile);
		/* reading from excell*/		
		SchemaNamesUpdate schup=new SchemaNamesUpdateByExcell();
		
		logger.trace("Starting updating schema names");
		updateFileIn(schup, sql_path);
		
		//verifica se esistono ancora istanze degli schema Oracle ancora in funzione
		logger.trace("Starting checking for active obsolete schema names");
		logger.trace("Checking for active obsolete schema names SKIPPED until update");
//		checkOlderSchema(sql_path);
		
	}

}
