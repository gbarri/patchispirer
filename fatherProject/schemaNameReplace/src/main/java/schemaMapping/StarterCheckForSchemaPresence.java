package schemaMapping;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import utilities.SQLSearcher;
import utilities.SQLSearcher.SQLTypes;

public class StarterCheckForSchemaPresence extends SimpleFileVisitor<Path>{

	private CheckForSchemaPresence check;
	private File rootFile;
	
	public StarterCheckForSchemaPresence(CheckForSchemaPresence check, File rootFile) {
		super();
		this.check = check;
		this.rootFile = rootFile;
	}

	
	@Override
	public FileVisitResult visitFile(Path filepath, BasicFileAttributes attrs)   throws IOException{
		if(checkFile(filepath.toFile())) {
//			this.check.checkForAbsentSchema(filepath.toFile());
			this.check.InsertAbsentSchema(filepath.toFile());
		}
		return FileVisitResult.CONTINUE;
	}
	
	private boolean checkFile(File file) {
		boolean cond=file.isFile();
		cond=cond && file.getName().endsWith(".sql");
		return cond;
	}


	public static void main(String[] args) throws IOException {
		File rootFile=new File("C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\EDWP2");
		StarterCheckForSchemaPresence starter=new StarterCheckForSchemaPresence(new CheckForSchemaPresence(), rootFile);
		Files.walkFileTree(rootFile.toPath(), starter);
	}

}
