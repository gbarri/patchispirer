package schemaMapping;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jsqlparser.parser.ParseException;
import net.sf.jsqlparser.parser.TokenMgrException;
import utilities.Initializer;
import utilities.SQLSearcher;
import utilities.StringUtilities;

public abstract class AbstractSchemaNamesUpdate implements SchemaNamesUpdate {

	private Map<String, String> replaceMap;
	private Logger logger = LoggerFactory.getLogger(this.getClass());
		
	@Override
	public void updateNames(File file) {
		String fileName=file.getAbsolutePath();
		String fullText="";
		boolean found=false;
		boolean toBeMoved=false;
		try(FileInputStream fileStream=new FileInputStream(fileName);
		    Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
			Set<String> keys=replaceMap.keySet();
			while(scann.hasNextLine()) {
				String nextLine=scann.nextLine();
				if(!nextLine.trim().startsWith("--")) {
					for(String keyItem:keys) {
						if(nextLine.contains(keyItem)) {
							if(!found) {
								found=true;
								//logger.trace("");
							}
							//nextLine=nextLine.replaceAll(keyItem,replaceMap.get(keyItem));
							//NB: in replace exactly viene rifatto il check della presenza o meno della chiave...
							//TODO: verificare se chiamare sempre il replaceExactly � po meno pi� veloce di un check iniziale con contains sulla stringa 
							String newLine;
							try {
								newLine = StringUtilities.replaceExactly(nextLine, keyItem,replaceMap.get(keyItem));
							} catch (TokenMgrException | ParseException e1) {
								newLine = nextLine.replaceAll(keyItem,replaceMap.get(keyItem));
							}
							if( !(toBeMoved || newLine.equals(nextLine)) ) {
								logger.info("\n\tIl file "+file.getName()+" deve essere spostato (variazione del suo schema attuale)\n");
								toBeMoved=true;
								try {
									if(SQLSearcher.isFunctionDef(nextLine)) {
										String alter="ALTER FUNCTION "+keyItem+" SET SCHEMA "+replaceMap.get(keyItem).replaceAll("\\..*","")+";";
										annotatiLaFunction(file, replaceMap.get(keyItem).replaceAll("\\..*","")+"\t"+alter);
									}else if(SQLSearcher.isTableDef(nextLine)) {
										String alter="ALTER TABLE "+keyItem+" SET SCHEMA "+replaceMap.get(keyItem).replaceAll("\\..*","")+";";
										annotatiLaTable(file, replaceMap.get(keyItem).replaceAll("\\..*","")+"\t"+alter);
									}else if(SQLSearcher.isViewDef(nextLine)) {
										String alter="ALTER VIEW "+keyItem+" SET SCHEMA "+replaceMap.get(keyItem).replaceAll("\\..*","")+";";
										annotatiLaView(file, replaceMap.get(keyItem).replaceAll("\\..*","")+"\t"+alter);
									}
								}catch(Exception e) {
									logger.error("Exception intercepted by insertCaller.updateNames",e);
								}
							}
							nextLine=newLine;
						}
					}
				}
				fullText+=nextLine+"\n";
			}
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while reading/searching for \n\t"+fileName, e);
		}
		if(found) {
			try(FileOutputStream fileOutStream=new FileOutputStream(fileName); 
					PrintWriter print=new PrintWriter(new BufferedOutputStream(fileOutStream))){
				print.print(fullText);
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while writing/searching for \n\t"+fileName, e);
			}
		}
	}
	
	protected void annotatiLaFunction(File file, String string) {
		annotatiLaSmth(file, "FUNCTION", string);
	}
	
	protected void annotatiLaTable(File file, String string) {
		annotatiLaSmth(file, "TABLE", string);
	}
	
	protected void annotatiLaView(File file, String string) {
		annotatiLaSmth(file, "VIEW", string);
	}
	
	private void annotatiLaSmth(File file, String typeDef, String note) {
		try(FileWriter print=new FileWriter(Initializer.getRunningFolder()+"\\MOVED_"+typeDef+".sql", true)) {
			print.append(file.getAbsolutePath()+"\t"+note+"\n");
		}catch (IOException e) {
			throw new RuntimeException("3:Exception raised while writing to \n\t"+getExcelPath()+"\\MOVED_"+typeDef+".sql", e);
		} catch (URISyntaxException e1) {
			logger.error("failed to properly locate running folder.");
		}
	}

	public abstract String getExcelPath();


}
