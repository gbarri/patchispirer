package utilities;

import java.util.HashMap;
import java.util.Stack;

public class Decommenter {
	
	private Stack<Integer> commentIndex;
	private Stack<String>  commentText;
	
	public Decommenter() {
		this.commentIndex=new Stack<>();
		this.commentText=new Stack<>();
	}
	
	public String initLine(String nextLine, Boolean skipStartLineComment) {
		if(this.commentIndex==null) {
			this.commentIndex=new Stack<>();
		}
		if(this.commentText==null) {
			this.commentText=new Stack<>();
		}
		//remove --comments
		if(!skipStartLineComment) {
			nextLine=removeLineComments(nextLine);
		}
		//remove \\/ * * \\/ comments
		nextLine=removeBlockComments(nextLine);
		return nextLine;
	}
	
	public String initLine(String nextLine) {
		return initLine(nextLine, false);
	}
	
	
	
	public String deinitLine(String text) {
		int[] tmp={};
		return deinitLine(text, tmp);
	}
	
	public String deinitLine(String text, int[] extrema) {
		while(!commentIndex.isEmpty()) {
			Integer index=commentIndex.pop();
			String comment=commentText.pop();
			for(int i=0; i<extrema.length; i++) {
				if(extrema[i]>=index) {
					extrema[i]=extrema[i]+comment.length();
				}
			}
			String firstPart=text.substring(0, index);
			String secondPart=text.substring(index);
			text=firstPart+comment+secondPart;
		}
		return text;
	}

	protected String removeLineComments(String nextLine) {
		while(nextLine.indexOf("--")!=-1) {
			int ind=nextLine.indexOf("--");
			int ind2=nextLine.indexOf("\r\n", ind+1);
			if(ind2==-1) {
				ind2=nextLine.indexOf("\n", ind+1);
			}
			if(ind2==-1) {
				ind2=nextLine.length();
			}
			commentIndex.push(ind);
			commentText.push(nextLine.substring(ind,ind2));
//			nextLine=nextLine.replaceFirst("--.*", "");
			nextLine=nextLine.substring(0,ind)+(ind2<=nextLine.length()-1?nextLine.substring(ind2):"");
		}
		return nextLine;
	}
	
	protected String removeBlockComments(String nextLine) {
		while(nextLine.indexOf("/*")!=-1) {
			int ind=nextLine.indexOf("/*");
			int ind2=nextLine.indexOf("*/", ind+2);
			if(ind2==-1) {
				throw new IllegalStateException("There is a mistake in the string passed to the decommenter. The /* comment is never closed. String: "+nextLine);
			}
			commentIndex.push(ind);
			commentText.push(nextLine.substring(ind,ind2+2));
			nextLine=nextLine.substring(0,ind)+""+nextLine.substring(ind2+2);
		}
		return nextLine;
	}
}
