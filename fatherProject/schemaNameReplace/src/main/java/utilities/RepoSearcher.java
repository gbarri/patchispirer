package utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.poi.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utilities.SQLSearcher.SQLTypes;

/**
 * Classe pensata per svolgere il controllo sulla presenza di oggetti duplicati sul DB.
 * @author g.barri
 *
 */
public class RepoSearcher extends SimpleFileVisitor<Path>{
	
	static Logger logger=LoggerFactory.getLogger(RepoSearcher.class);
	private String searchedObject;
	private List<String> copies;
	//private static String propertyFile="C:\\Users\\g.barri\\git\\repository\\schemaNameReplace\\src\\main\\resources\\RepoSearcher.properties";
	private Map<String, List<String>> object2positions;
	private static RepoSearcher rps;
	
	private RepoSearcher(String searchedObject) {
		this.searchedObject=searchedObject;
		this.copies=new ArrayList<>();
		object2positions=null;
	}
	
	private RepoSearcher() {
		this.searchedObject=null;
		this.copies=new ArrayList<>();
		object2positions=null;
	}
	
	public static RepoSearcher getRepoSearcher() {
		if(rps==null) {
			logger.debug("initializing reposearcher");
			rps=new RepoSearcher();
		}
		return rps;
	}
	
	protected static boolean checkFile(File file) {
		boolean cond=file.getName().endsWith(".sql");
		if(cond) {
			SQLTypes type=SQLSearcher.detectType(file);
			if(null==type) {
//				logger.trace("File "+file.getName()+" type not recognised. File skipped. ("+file.getAbsolutePath());
				cond=false;
			}
		}
		return cond;
	}
	
	public FileVisitResult visitFile(Path path,   BasicFileAttributes attrs) throws IOException{
		File file=path.toFile();
		if(checkFile(file)) {
			String objectName=SQLSearcher.detectName(file);
			if(StringUtilities.equals(objectName, this.searchedObject)) {
				logger.trace("Found declaration of\t"+searchedObject+"\tat\t"+file.getAbsolutePath());
				this.copies.add(file.getAbsolutePath());
			}
		}
		return FileVisitResult.CONTINUE;	
	}

	public String getSearchedObject() {
		return searchedObject;
	}

	public List<String> getCopies() {
		return copies;
	}
	
	private static String getDefaultRoot() {
		return Initializer.getProperty("localRepositoryDir");
	}
	
	/**
	 * returns the absolute paths of DDLs under dirRoot which define objectName 
	 * @param objectName
	 * @param dirRoot
	 * @return
	 */
	public static List<String> searchFor(String objectName, String dirRoot){
		RepoSearcher rps=new RepoSearcher(objectName);
		try {
			Files.walkFileTree(new File(dirRoot).toPath(), rps);
		} catch (IOException e) {
			logger.error("FAILed to search for "+objectName+".", e);
		}
		return rps.getCopies();
	}
	
	/**
	 * returns the absolute paths of DDLs under default repository which define objectName 
	 * @param objectName
	 * @return
	 */
	public static List<String> searchFor(String objectName){
		String root=getDefaultRoot();
		return searchFor(objectName, root);
	}
	
	/**
	 * returns the absolute paths of DDLs under dirRoot which define the same object of file, minus the absolute path of file itself if it is under diRoot.
	 * In case file does not define any recognised object it returns an empty list.
	 * @param objectName
	 * @param dirRoot
	 * @return
	 */
	public static List<String> searchFor(File file, String dirRoot){
		String objectName=SQLSearcher.detectName(file);
		if(objectName==null) {
			logger.error("File "+file.getName()+" is not a recognised DDL. ("+file.getAbsolutePath());
			return new ArrayList<String>();
		}
		List<String> tmp=searchFor(objectName, dirRoot);
		List<String> out=new ArrayList<>();
		for(String str: tmp) {
			if(!file.getAbsolutePath().equals(str)) {
				out.add(str);
			}
		}
		return out;
	}
	
	/**
	 * returns the absolute paths of DDLs under default repository which define the same object of file, minus the absolute path of file itself if it is under default root.
	 * In case file does not define any recognised object it returns an empty list.
	 * @param objectName
	 * @return
	 */
	public static List<String> searchFor(File file){
		String objectName=SQLSearcher.detectName(file);
		if(objectName==null) {
			logger.error("File "+file.getName()+" is not a recognised DDL. ("+file.getAbsolutePath());
			return new ArrayList<String>();
		}
		List<String> tmp=searchFor(objectName);
		List<String> out=new ArrayList<>();
		for(String str: tmp) {
			if(!file.getAbsolutePath().equals(str)) {
				out.add(str);
			}
		}
		return out;
	}
	
	public void prepare(String root) throws IOException {
		logger.trace("Starting to prepare quick search in "+root);
		object2positions=new HashMap<>();
		MapperRootDirectory mapper=new MapperRootDirectory(this);
		Files.walkFileTree(new File(root).toPath(), mapper);
		logger.trace("Ended preparation");
	}
	
	public List<String> quickSearchFor(String objectName){
		if(object2positions==null) {
			String root=getDefaultRoot();
			try {
				this.prepare(root);
			} catch (IOException e) {
				logger.error("Failed to prepare the search.", e);
			}
		}
		List <String> out=object2positions.get(objectName);
		if(out==null) {
			return new ArrayList<String>();
		}else {
			return out;
		}
	}
	
	public List<String> quickSearchFor(File file){
		String objectName=SQLSearcher.detectName(file);
		if(objectName==null) {
			logger.error("Failed to detect object name in file "+file+". ("+file.getAbsolutePath());
			return new ArrayList<String>();
		}else {
			List<String> out=quickSearchFor(objectName);
			if(out==null) {
				logger.warn("No files defining the object "+objectName+" has been found.");
				return new ArrayList<String>();
			}else {
				ArrayList<String> filteredList=new ArrayList<>(out.size());
				for(String foundPath:out) {
					if(!foundPath.equals(file.getAbsolutePath())) {
						filteredList.add(foundPath);
					}
				}
				logger.trace("the following files defining the same object "+objectName+" has been found: "+filteredList);
				return filteredList;
			}
		}
	}
	
	public static void main(String[] args) throws IOException {
//		String repository="C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\EDWP2";
//		String root=repository;
//		List<String> list=RepoSearcher.searchFor("tl_mfg.VS_DW_ANAGRAFICA_DISEGNI", root);
//		for(String str:list) {
//			System.out.println(str+": "+SQLSearcher.detectName(new File(str)));
//		}
//		System.out.println("----");
//		List<String> list2=RepoSearcher.searchFor(new File("C:\\Users\\g.barri\\Desktop\\wrk\\XXDWHAPP_MP_DW_ANAGRAFICA_DISEGNI\\tl_mfg_vs_dw_anagrafica_disegni.sql"), root);
//		for(String str:list2) {
//			System.out.println(str+": "+SQLSearcher.detectName(new File(str)));
//		}
//		System.out.println("----");
//		List<String> list3=RepoSearcher.searchFor(new File("C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\EDWP2\\tl_mfg\\views\\dw\\xxdwhapp_vs_dw_anagrafica_disegni.sql"), root);
//		for(String str:list3) {
//			System.out.println(str+": "+SQLSearcher.detectName(new File(str)));
//		}
		
		RepoSearcher rps=new RepoSearcher();
		rps.prepare(getDefaultRoot());
		List<String> list=rps.quickSearchFor("tl_mfg.VS_DW_ANAGRAFICA_DISEGNI");
		for(String str:list) {
			System.out.println(str+": "+SQLSearcher.detectName(new File(str)));
		}
		
	}
	private class MapperRootDirectory extends SimpleFileVisitor<Path>{
		
		private RepoSearcher rps;
		
		private MapperRootDirectory(RepoSearcher rps) {
			this.rps=rps;
		}
		
		public FileVisitResult visitFile(Path path,   BasicFileAttributes attrs) throws IOException{
			File file=path.toFile();
			if(checkFile(file)) {
				//logger.debug("mapping file "+file.getName());
				String objectName=SQLSearcher.detectName(file);
				if(objectName!=null) {
					if(!rps.object2positions.containsKey(objectName)) {
						rps.object2positions.put(objectName, new ArrayList<>());
					}
					rps.object2positions.get(objectName).add(file.getAbsolutePath());
				}
			}
			return FileVisitResult.CONTINUE;	
		}
		
	}
	
}
