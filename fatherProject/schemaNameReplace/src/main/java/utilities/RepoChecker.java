package utilities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utilities.SQLSearcher.SQLTypes;

public class RepoChecker extends SimpleFileVisitor<Path>{

	static Logger logger=LoggerFactory.getLogger(RepoSearcher.class);
	//private static String propertyFile="C:\\Users\\g.barri\\git\\repository\\schemaNameReplace\\src\\main\\resources\\RepoChecker.properties";
	private Map<String,List<String>> multipleObjectDDL;
	private RepoSearcher rps;
	
	public RepoChecker(String rootDirectory) {
		this.rps=RepoSearcher.getRepoSearcher();
		try {
			rps.prepare(rootDirectory);
		} catch (IOException e) {
			logger.error("Faild to prepare the search path "+rootDirectory, e);
			throw new IllegalArgumentException("Unexpected filure in reading the directory "+rootDirectory, e);
		}
		this.multipleObjectDDL=new HashMap<>();
	}
	
	public RepoChecker() {
		this(getDefaultRoot());
	}
	
	protected static boolean checkFile(File file) {
		boolean cond=file.getName().endsWith(".sql");
		if(cond) {
			SQLTypes type=SQLSearcher.detectType(file);
			if(null==type) {
				cond=false;
			}
		}
		return cond;
	}
	
	public FileVisitResult visitFile(Path path,   BasicFileAttributes attrs) throws IOException{
		File file=path.toFile();
		if(checkFile(file)) {
			String objectName=SQLSearcher.detectName(file);
			List<String> tmp=this.rps.quickSearchFor(objectName);
			if(tmp.size()>1 && !multipleObjectDDL.containsKey(objectName)){
				logger.trace("Multiple DDL for "+objectName);
				multipleObjectDDL.put(objectName, tmp);
			}
		}
		return FileVisitResult.CONTINUE;	
	}
	
	private static String getDefaultRoot() {
	/*	Properties properties=new Properties();
		try {
			properties.load(new FileReader(propertyFile));
		} catch (FileNotFoundException e) {
			logger.warn("Properties file not found in the default directory");
			e.printStackTrace();
		} catch (IOException e) {
			logger.warn("Unknown error occured while reading property file.", e);
			e.printStackTrace();
		}
		String root=properties.getProperty("RepoChecker.repository");
		return root;
		*/
		return Initializer.getProperty("localRepositoryDir");
	}
	
//	public static String getPropertyFile() {
//		return propertyFile;
//	}

//	public static void setPropertyFile(String propertyFile) {
//		RepoChecker.propertyFile = propertyFile;
//	}

	public Map<String, List<String>> getMultipleObjectDDL() {
		return multipleObjectDDL;
	}

	public void printMultipleDDLToFile(String printFile) {
		try(FileWriter fw=new FileWriter(printFile, true);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter print = new PrintWriter(bw)){
				for(String keyObj: multipleObjectDDL.keySet()) {
					print.print(keyObj+"\tdefined in");
					for(String pathDDL: multipleObjectDDL.get(keyObj)) {
						print.print("\t"+pathDDL);
					}
					print.println("");
				}
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while writing "+printFile, e);
			}
	}
	
	public static void main(String[] args) throws IOException, URISyntaxException {		
		logger.trace("Starting definitions of RepoCheker");
		String root=getDefaultRoot();
//		String root="C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\EDWP2";
		RepoChecker rpcheck=new RepoChecker(root);
		logger.trace("Starting to check for multiple definitions of the same object");
		Files.walkFileTree(Paths.get(root), rpcheck);		
		logger.trace("Ending check of mupltuiple DDL for the same object");
		LocalDate date = LocalDate.now();
		String dateText = date.format(DateTimeFormatter.ofPattern("YYYY_MM_DD"));
		String printTo="C:\\Users\\g.barri\\Desktop\\test\\multipleDDlcheck_"+dateText+".txt";
		logger.trace("printing to file "+printTo);
		rpcheck.printMultipleDDLToFile(printTo);
	}
	
}
