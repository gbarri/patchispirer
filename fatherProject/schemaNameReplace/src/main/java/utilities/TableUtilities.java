package utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.poi.ss.formula.functions.Columns;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import utilities.SQLSearcher.SQLTypes;

public class TableUtilities {
	
	static Logger logger=LoggerFactory.getLogger(TableUtilities.class);
	
	/**
	 * permette di leggere solo token costituiti da parole. Tutto il resto viene rimosso
	 * @param scann
	 * @return
	 */
	public static String getNextToken(Scanner scann) {
		scann.useDelimiter("[^\\w\\$\\@\\_\\.]");
		return scann.next();
	}
	
//	public static String detectTableDefinition(Scanner scann, int post) {
//		String outDDLText=null, objectFun="CREATE", keywordObjcet="TABLE";
//		CompleteDecommenter decomm=new CompleteDecommenter(scann);
//		while(decomm.hasNextLine()) {
//			String nextLine=decomm.nextLine();
//			int start=StringUtils.indexOfIgnoreCase(nextLine, objectFun, post);
//			if(start!=-1 && isWholeWord(nextLine, start, start+objectFun.length())) {
//				int indexOfTableKeyWord=StringUtils.indexOfIgnoreCase(nextLine, keywordObjcet,start+objectFun.length());
//				while(indexOfTableKeyWord==-1 || isWholeWord(nextLine, indexOfTableKeyWord, keywordObjcet.length())) {
//					//... da completare
//					.decomm..
//					//nope, perchè TABLE va bene anche in functions.
//				}
//			}
//		}
//		
//		return outDDLText;
//	}
	
	/**
	 * controls if the words starting at start for length characters is a whole word, i.e. not a substring of other names
	 * @param nextLine
	 * @param start
	 * @param length
	 * @return
	 */
	private static boolean isWholeWord(String nextLine, int start, int length) {
		boolean isWhole=true;
		if(start>0) {
			Character pre=nextLine.charAt(start-1);
			isWhole=!(Character.isLetterOrDigit(pre) || pre.equals('_') || pre.equals('$')|| pre.equals('.') || pre.equals('@'));
		}
		if(start+length+1<nextLine.length()) {
			Character post=nextLine.charAt(start+length+1);
			isWhole=isWhole &&  !(Character.isLetterOrDigit(post) || post.equals('_') || post.equals('$')|| post.equals('.') || post.equals('@'));
		}
		return isWhole;
	}

	public static List<String> getColumnNames(File file){
		List<String> out=null;
		if(file==null || SQLSearcher.detectType(file)!=SQLTypes.TABLE) {
			logger.warn("file "+file.getName()+" has been not detected as a table definition ("+file.getAbsolutePath()+")");
			throw new IllegalArgumentException("file "+file.getName()+" has been not detected as a table definition");
		}else {
			String textDDL=isolateDDL(file);
			try {
//				Statement st = new CCJSqlParserManager().parse(new FileReader(file));
				Statement st = new CCJSqlParserManager().parse(new StringReader(textDDL));
				CreateTable crTb=(CreateTable) st;
				List<ColumnDefinition> coList=crTb.getColumnDefinitions();
				out=new ArrayList<>();
				for(ColumnDefinition colItem : coList) {
					out.add(colItem.getColumnName());
				}
			} catch (JSQLParserException e) {
				logger.error("Intercepted parser exception "+e.getClass()+": "+e.getMessage()+"\tfile "+file.getAbsolutePath());
			} catch (ClassCastException e) {
				logger.error("file "+file.getName()+" once parsed is not a CreateTable");
			}
			return out;
		}
	}
	
	public static String isolateDDL(File file) {
		String out="";
		try(Scanner scann=new Scanner(new FileReader(file))){
			CompleteDecommenter decomm=new CompleteDecommenter(scann);
			decomm.clearMemory();
			while(decomm.hasNextLine()) {
				String nextLine=decomm.nextLine();
				if(StringUtils.containsIgnoreCase(nextLine, "CREATE ")) {
					while(!nextLine.contains(";")) {
						nextLine=decomm.nextLine();
					}
					int start=StringUtils.indexOfIgnoreCase(nextLine, "CREATE ");
					int end=nextLine.indexOf(";");
					out=nextLine.substring(start, end+1);
					break;
				}else {
					decomm.clearMemory();
				}
			}
		}catch (Exception e) {
			logger.error("Failed to analyze file "+file.getAbsolutePath()+" in search of its columns");
		}
		return out;
	}
	
	
	public static void main(String[] args) {
//		File file=new File("C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_sal\\views\\tl_sal_vs_ass_eventi_ord_imm_upd.sql");
		String test="ALTER TABLE CREATE blabla; cscvj/()  . ";
		Scanner scann=new Scanner(new StringReader(test));
		while(scann.hasNext()) {
			System.out.println(getNextToken(scann));
		}
//		System.out.println(isolateDDL(file));
	}
}
