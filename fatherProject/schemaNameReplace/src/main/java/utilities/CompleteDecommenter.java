package utilities;

import java.util.Scanner;
import java.util.Stack;

import javax.sound.sampled.LineUnavailableException;

import org.tinylog.Logger;

public class CompleteDecommenter extends Decommenter {
	Scanner scanner;
	String text;
	
	public CompleteDecommenter(Scanner scanner) {
		super();
		this.scanner = scanner;
		this.text="";
	}
	
	public void clearMemory() {
		this.text="";
	}
	
	public String nextLine() {
		boolean flagBreak=false, needToEndBlockComment=false;
		do {
			if(hasNextLine()) {
				String newLine=scanner.nextLine()+"\r\n";
				try {
					this.text=super.initLine(this.text+=newLine, needToEndBlockComment);
					flagBreak=true;
					if(needToEndBlockComment) {
						needToEndBlockComment=false;
					}
				}catch(IllegalStateException e) {
					needToEndBlockComment=true;
				}
			}else {
				throw new IllegalArgumentException("File ended");
			}
		}while(!flagBreak);
		return this.text;
	}
	
	public boolean hasNextLine() {
//		Logger.debug("contains "+this.text.replaceAll("\r\n", "\\\\r\\\\n\r\n"));
		return scanner.hasNextLine();
	}
	
}
