package utilities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import loaders.Loader;
import loaders.LoaderMultipleSheetsDBExcel;
import schemaMapping.LoaderSchemaMap;

/*
 * class that starting from an Oracle object or greenplum object we check if it should be migrated, if it's file is present in the repository datalake
 */
public class CheckAlreadyMigrated {

	List<String> objectsNames;
	private Map<String, String[]> migrated=null;
	static Logger logger=LoggerFactory.getLogger(CheckAlreadyMigrated.class);
	Map<String, String> schemaReplaceMap;
	Map<String,List<String>> inverseSchemaReplaceMap;
	
	
	public CheckAlreadyMigrated(String[] names) {
		ArrayList<String> tmp=new ArrayList<>();
		for(String name:names) {
			tmp.add(name);
		}
		this.objectsNames=tmp;
		LoaderSchemaMap loaderSchemaMap=new LoaderSchemaMap();
		this.schemaReplaceMap=loaderSchemaMap.loadMap();
		this.inverseSchemaReplaceMap=loaderSchemaMap.loadInverseMap();
	}
	
	/**
	 * questions the excel document to understand if the object is recorded and marked to be migrated
	 * @param objectName
	 * @return
	 */
	public boolean detectMigrated(String objectName) {
		if(this.migrated==null) {
			Loader loader=new LoaderMultipleSheetsDBExcel(new File(Initializer.getProperty("LoadProperties_move")));
//			Map<String, String[]> migrated;
			try {
				this.migrated=loader.loadMap();
				Map<String, String[]> tmp=new HashMap<>();
				Iterator<String> iter=this.migrated.keySet().iterator();
				while(iter.hasNext()) {
					String PKey=iter.next();
					String[] val=this.migrated.get(PKey);
					if(val.length>2 && !val[2].trim().equals("")) {
						tmp.put(StringUtilities.formatObjectName(val[0]+"."+val[2]),new String[0]);
					}else {
						tmp.put(StringUtilities.formatObjectName(val[0]+"."+val[1]),new String[0]);
					}
				}
				this.migrated=tmp;
			} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
				logger.error("Intercepted exception while trying to read the excel document. Loaded empty map instead", e);
				e.printStackTrace();
				this.migrated=new HashMap<>();
			}
		}
		int indexPoint=objectName.indexOf(".");
		String checkedObjectName;
		if(indexPoint==-1) {
			logger.warn("object "+objectName+" is not fully qualyfied, we do not modify it. High probability of failure");
			checkedObjectName=objectName;
		}else {
			String schema=objectName.substring(0,indexPoint).toLowerCase();
			String object=objectName.substring(indexPoint+1).toUpperCase();
			checkedObjectName=schema+"."+object;
		}
		return this.migrated.containsKey(checkedObjectName);
	}
	
	public static void main(String[] args) {
//		CheckAlreadyMigrated check=new CheckAlreadyMigrated(args);
//		RepoSearcher rps=RepoSearcher.getRepoSearcher();
		for(String arg:args) {
			String objectName=StringUtilities.formatObjectName(arg);
//			String objectName=arg;
			LoaderSchemaMap loaderSchemaMap=new LoaderSchemaMap();
			Map<String, List<String>> inverseSchemaReplaceMap=loaderSchemaMap.loadInverseMap();
			objectName=StringUtilities.formatObjectName(arg);
			Iterator<String> iter=inverseSchemaReplaceMap.keySet().iterator();
			while(iter.hasNext()) {
				String key=iter.next();
				if(StringUtils.equalsIgnoreCase(objectName, key)) {
					System.out.println(inverseSchemaReplaceMap.get(key).get(0));
					break;
				}
			}
		}
	}
	
}
