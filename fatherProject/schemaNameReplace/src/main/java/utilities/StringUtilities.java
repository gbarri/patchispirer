package utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jsqlparser.parser.CCJSqlParser;
import net.sf.jsqlparser.parser.ParseException;
import net.sf.jsqlparser.parser.Token;
import net.sf.jsqlparser.parser.TokenMgrException;
import prgAnalyzeTable.PR_ANALYZE_Greenplum;

public class StringUtilities {
	
	static Logger logger=LoggerFactory.getLogger(StringUtilities.class);
	/**
	 * conta le occorrenze di una sottstringa sub dentro la stringa str
	 * DEPRECATED: non basandosi sui token della stringa, � soggetta ad errori!!
	 * molto meglio sfruttare findExactly e contare la lunghezza dell'array risultante.
	 * @param str
	 * @param sub
	 * @return il numero delle occorrenze
	 */
	 public static int countMatches(String str, String sub) {
		    int count = 0;
		    if(!str.isEmpty() && !sub.isEmpty()) {
		        for (int i = 0; (i = str.indexOf(sub, i)) != -1; i += sub.length()) {
		            count++;
		        }
		    }
		    return count;
	 }
	 
	 public static String[] toStringArray(String str) {		
		try {
			CCJSqlParser parser = new CCJSqlParser(str);
			List<String> tmpList=new ArrayList<>();
			Token tkn=parser.getNextToken();
			while(!tkn.image.equals("")) {
//			while(parser.jj_nt!=null) {
				tmpList.add(tkn.image);
				tkn=parser.getNextToken();
			}
			String[] out=new String[tmpList.size()];
			out=tmpList.toArray(out);
			return out;
			 
		} catch (TokenMgrException | ParseException e) {
			// TODO Auto-generated catch block
            logger.error("Exception caught while parsing: "+str);
//			e.printStackTrace();
			return null;
		}
		
	 }
	 
	 public static Token[] toTokenArray(String str) {		
			try {
				CCJSqlParser parser = new CCJSqlParser(str);
				List<Token> tmpList=new ArrayList<>();
				Token tkn=parser.getNextToken();
				while(!tkn.image.equals("")) {
//				while(parser.jj_nt!=null) {
					tmpList.add(tkn);
					tkn=parser.getNextToken();
				}
				Token[] out=new Token[tmpList.size()];
				out=tmpList.toArray(out);
				return out;
				 
			} catch (TokenMgrException | ParseException e) {
				// TODO Auto-generated catch block
				logger.error("Exception caught while parsing: "+str);
//				e.printStackTrace();
				return null;
			}
			
		 }
	 
	public static Map<String, List<Integer>> toStringMap(String str) throws ParseException, TokenMgrException  {
//		 logger.debug(str);
			try {
				CCJSqlParser parser = new CCJSqlParser(str);
				/*map token-position of the token in the string, 0-based (0-> it's the first tokem, 1-> it's the second token and so on */
				Map<String, List<Integer>> tmpList=new TreeMap<>();
				int pos=0;
				Token tkn=parser.getNextToken();
				while(!tkn.image.equals("")) {
					if(!tmpList.containsKey(tkn.image)) {
						tmpList.put(tkn.image, new ArrayList<>());
					}
					tmpList.get(tkn.image).add(pos++);
					tkn=parser.getNextToken();
				}
//				while(parser.jj_nt!=null) {
//					Token tkn=parser.getNextToken();
//					if(!tmpList.containsKey(tkn.image)) {
//						tmpList.put(tkn.image, new ArrayList<>());
//					}
//					tmpList.get(tkn.image).add(pos++);
//				}
				return tmpList;
				 
			} catch (TokenMgrException e) {
				logger.error("Exception caught while parsing: "+str);
//				e.printStackTrace();
				throw e;
			}
			
		 }
	 /**
	  * a fixed version of the contains function of collections 
	  * @param group
	  * @param it
	  * @return
	  */
	 //TODO: rendere generico questo metodo
	 public static boolean contains(List<Integer> group, Integer it) {
		 if(group==null || it==null) {
			 return false;
		 }
		 boolean found=false;
		 for(Integer item: group) {
			 if(item.equals(it)) {
				 found=true;
				 break;
			 }
		 }
		 return found;
	 }
	 
	 /**
	  * verifica se una certa sottostringa � presente all'interno di un campo text come parola isolata.
	  * Ad es: non deve rivelare "ora" all'interno della parola "orario".
	  * Per farlo ci appoggiamo ad un parser per SQL CC.
	  * NB: questo metodo � sconsigliato qualora vi siano presenze sovrapposte della stessa chiave. ES: se la chiave fosse
	  * "ora ora" e il testo � "ora ora ora" verranno segnalate delle keys che partono sia dal token 0, che dal token 1.
	  * Questo pu� NON ESSERE l'effetto voluto durante la fase di replace.
	  * @param text
	  * @param key
	  * @return indice del token di partenza della key � presente in text, 0 based
	 * @throws ParseException 
	 * @throws TokenMgrException 
	  */
	 public static List<Integer> findExactly(String text, String key) throws TokenMgrException, ParseException {
		 try {
			String[] keyArray=StringUtilities.toStringArray(key);
			Map<String, List<Integer>> textMap=toStringMap(text);
			List<Integer> firstKeyTknOccurrences;
			if(keyArray.length>0 && textMap.containsKey(keyArray[0])) {
				firstKeyTknOccurrences=textMap.get(keyArray[0]);
			}else {
				firstKeyTknOccurrences=new ArrayList<>();
			}
//			List<Integer> keep=new ArrayList<>(firstKeyTknOccurrences);
			List<Integer> remove=new ArrayList<>();
			for(int i=0; i<firstKeyTknOccurrences.size(); i++) {
				for(int j=1; j<keyArray.length; j++) {
					try {
						if(!contains(textMap.get(keyArray[j]),firstKeyTknOccurrences.get(i)+j)) {
//							remove.add(i);
							firstKeyTknOccurrences.remove(i--);
							break;
						}
					}catch (NullPointerException e) {
						logger.debug("Caught exception while searching exactly for "+key+" in\n"+text, e);
						return new ArrayList<>();
					}
				}
			}
			
			return firstKeyTknOccurrences;
		} catch(IndexOutOfBoundsException e) {
			logger.debug("Caught exception while searching exactly for "+key+" in\n"+text, e);
			return new ArrayList<>();
		}
		 
	 }
	 
	 /**
	  * Function per il replace all'interno di stringhe SQL o PLSQL.
	  * Preferibile rispetto al semplice replaceAll testuale.
	  * Pensato per replace della forma "schema.TABELLA" >>> "newSchema.newTABELLA"
	  * @param text
	  * @param key
	  * @param replace
	  * @return la stringa con le dovute sostituzioni
	 * @throws ParseException 
	 * @throws TokenMgrException 
	  */
	 public static String replaceExactly(String text, String key, String replace) throws TokenMgrException, ParseException {
		 Token[] textTknArray=toTokenArray(text);
		 Token[] keyTknArray=toTokenArray(key);
		 Token[] replaceTknArray=toTokenArray(replace);
		 List<Integer> list=findExactly(text,key);
		 if(replaceTknArray.length!=keyTknArray.length) {
			 throw new IllegalArgumentException("the format of the former key ("+key+") and its replacement ("+replace+") is supposed to be the same."); 
		 }
		 String newText="";
		 int pos=0;
		 for(Integer posItem:list) {
			 for(int i=0; i<keyTknArray.length; i++) {
				 newText+=text.substring(pos,textTknArray[posItem+i].beginColumn-1);
				 newText+=replaceTknArray[i].image;
				 pos=textTknArray[posItem+i].endColumn; 
			 }
		 }
		 newText+=text.substring(pos);
		 return newText;
	 }

	 /**
	  * safe equals. If null return false
	  * @param str1
	  * @param str2
	  * @return
	  */
	 public static boolean equals(String str1, String str2) {
		 if(str1==null || str2==null) {
			 return false;
		 }
		 return str1.equals(str2);
	 }

	 /**
	  * safe equals, ignore white spaces at begins and ends
	  * @param str1
	  * @param str2
	  * @return
	  */
	 public static boolean equalsTrim(String str1, String str2) {
		 if(str1==null || str2==null) {
			 return false;
		 }
		 return str1.trim().equals(str2.trim());
	 }	  
	 
	 public static String formatObjectName(String objName) {
		 if(objName==null) {
			 logger.error("tried to arrange a null name");
			 return null;
		 }else {
			int indexPoint=objName.indexOf(".");
			if(indexPoint==-1) {
				logger.warn("the object name "+objName+" does not contains any schema (no \".\")");
				return objName;
			}else {
				String analTabSchema=objName.substring(0, objName.indexOf(".")).toLowerCase();
				String analTabName=objName.substring(objName.indexOf(".")+1).toUpperCase();
				return  analTabSchema+"."+analTabName;
			}
		 }	
	 }
	 
	 public static String detectSchemaInObjectName(String objName) {
		 if(objName==null) {
			 logger.error("tried to work on a null name");
			 return null;
		 }else {
			int indexPoint=objName.indexOf(".");
			if(indexPoint==-1) {
				logger.warn("the object name "+objName+" does not contains any schema (no \".\")");
				return null;
			}else {
				String analTabSchema=objName.substring(0, objName.indexOf(".")).toLowerCase();
				return  analTabSchema;
			}
		 }	
	 }
	 
	 public static String detectNameInObjectName(String objName) {
		 if(objName==null) {
			 logger.error("tried to work on a null name");
			 return null;
		 }else {
			int indexPoint=objName.indexOf(".");
			if(indexPoint==-1) {
				logger.warn("the object name "+objName+" does not contains any schema (no \".\")");
				return objName;
			}else {
				String analTabName=objName.substring(objName.indexOf(".")+1).toUpperCase();
				return analTabName;
			}
		 }	
	 }
	 
	 
	 
}
