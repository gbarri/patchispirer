package utilities;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jsqlparser.parser.ParseException;
import net.sf.jsqlparser.parser.TokenMgrException;
import schemaMapping.LoaderSchemaMap;

public class SQLSearcher {
	
	public enum SQLTypes{
		VIEW, TABLE, FUNCTION, SEQUENCE, INDEX, CONSTRAINT
	}
	
	public static Logger logger=LoggerFactory.getLogger(SQLSearcher.class);
	
	public static List<String> scanForFunctionDefName(File file) {
//		return scanFor(file, "CREATE", "FUNCTION");
		List<String> names= scanFor(file, "CREATE", "FUNCTION");
		if(names.size()==0) {
			names= scanFor(file, "ALTER", "FUNCTION");
		}
		return names;
	}
	
	public static List<String> scanForSequenceDefName(File file) {
		List<String> names= scanFor(file, "CREATE", "SEQUENCE");
		return names;
	}
	
	public static List<String> scanForTableDefName(File file) {
		//to avoid confusion with contraints, we ignore the alter table statements here
		List<String> names= scanFor(file, "CREATE", "TABLE");
		return names;
	}
	
	public static List<String> scanForViewDefName(File file) {
		//to avoid confusion with contraints, we ignore the alter statements here
		List<String> names= scanFor(file, "CREATE", "VIEW");
		return names;
	}
	
	/**
	 * dovrebbe restituire i nomi degli oggetti di tipo objType che � stato
	 * @param file
	 * @param keyWord
	 * @param objType
	 * @return
	 */
	public static List<String> scanFor(File file, String keyWord, String objType){    
		List<String> out=new ArrayList<>();
		try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
				Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
			while(scann.hasNextLine()) {
				String nextLine=scann.nextLine();
				nextLine=nextLine.replaceAll(keyWord.toLowerCase()+"\\s", " "+keyWord.toUpperCase()+" ");
				nextLine=nextLine.replaceAll("\\s"+objType.toLowerCase()+"\\s", " "+objType.toUpperCase()+" ");
				nextLine=nextLine.replaceAll("\\/\\*.*\\*\\/", ""); //rimuovo eventuali commenti
				if(Pattern.matches(keyWord+"\\s+\\w*\\s?\\w*\\s?"+objType+"\\s+\\w+.*", nextLine.trim())) {
					out.add((nextLine.replaceAll(".*"+objType+"\\s+", "")).replaceAll("\\(.*", "").trim().replaceAll("\\s.*", ""));
					break;
				}
			}
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while reading/searching for \n\t"+file.getAbsolutePath(), e);
		}
		return out;
	}
	
	public static boolean isFunctionDef(File file) {
		return scanForFunctionDefName(file).size()>0;
	}
	
	public static boolean isSequenceDef(File file) {
		return scanForSequenceDefName(file).size()>0;
	}
	
	public static boolean isTableDef(File file) {
		return scanForTableDefName(file).size()>0;
	}
	
	public static boolean isViewDef(File file) {
		return scanForViewDefName(file).size()>0;
	}
	
	public static boolean isIndexDef(File file) {
		//return file.getName().endsWith("_idx.sql");
		//return scanFor(file, "CREATE", "INDEX").size()>0;
		return file.getName().endsWith("_idx.sql");
	}
	
	public static boolean isConstraintDef(File file) {
		//return file.getName().endsWith("_cns.sql");
		return scanFor(file, "ALTER", "TABLE").size()>0;
	}
	
	public static boolean isFunctionDef(String file) {
		return iSmthngDef(file, "CREATE", "FUNCTION");
	}
	
	public static boolean isSequenceDef(String file) {
		return iSmthngDef(file,"CREATE", "SEQUENCE");
	}
	
	public static boolean isTableDef(String file) {
		return iSmthngDef(file, "CREATE", "TABLE");
	}
	
	public static boolean isViewDef(String file) {
		return iSmthngDef(file, "CREATE", "VIEW");
	}
	
	public static boolean isIndexDef(String file) {
		return file.matches(".*CREATE.*INDEX.*ON.*") || file.matches(".*create.*index.*on.*");
	}
	
	private static boolean iSmthngDef(String file, String keyWord, String objType) {
		return Pattern.matches(keyWord+"\\s+\\w*\\s?\\w*\\s?"+objType+"\\s+\\w+.*", file.trim()) || isIndexDef(file);
	}

	public static SQLTypes detectType(File file) {
		if (isFunctionDef(file)) {
			return SQLTypes.FUNCTION;
		}
		if(isTableDef(file)) {
			return SQLTypes.TABLE;
		}
		if(isViewDef(file)) {
			return SQLTypes.VIEW;
		}
		if(isIndexDef(file)) {
			return SQLTypes.INDEX;
		}
		if(isConstraintDef(file)) {
			return SQLTypes.CONSTRAINT;
		}
		if(isSequenceDef(file)) {
			return SQLTypes.SEQUENCE;
		}
		return null;
	}
	

	@Deprecated
	private static boolean isConstraintDef(String text) {
		//TODO: usare un match è estremamente time consuming per file lunghi.
		return text.matches(".*ALTER.*TABLE.*ADD.*CONSTRAINT");
		
	}

	/**
	 * try to identify the name of the SQL object decleared in the file. To do so, it first try to identify the type of declaration (table, view, function and so on)
	 * Then it extract the name of the object, complete of its schema if present. (should be always present).
	 * In case something went wrong it returns null.
	 * @param file
	 * @return
	 */
	public static String detectName(File file) {
		SQLTypes type=detectType(file);
		String name=null;
		try {
			switch (type) {
			case FUNCTION :
				name=scanForFunctionDefName(file).get(0);
				break;
			case TABLE :
				name=scanForTableDefName(file).get(0);
				break;
			case VIEW :
				name=scanForViewDefName(file).get(0);
				break;
			case CONSTRAINT:
				name=scanForConstraintDefName(file);
				break;
			case INDEX:
				name=scanForIndexDefName(file);
				break;
			case SEQUENCE:
				name=scanForSequenceDefName(file).get(0);
				break;
			default:
				throw new IllegalArgumentException("File "+file+" not recognoised.");
			}
			if(name.indexOf('.')!=-1) {
				int point=name.indexOf('.');
				name=name.substring(0, point).toLowerCase()+"."+name.substring(point+1).toUpperCase();
			}
			return name;
		}catch (NullPointerException | IllegalArgumentException e) {
			logger.error("il file "+file+" posto in "+file.getAbsolutePath()+" non � una ddl nota.");
			return null;
		}
	}
	
	public static String scanForIndexDefName(File file) {
		String text="";
		try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
				Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
			while(scann.hasNextLine()) {
				String nextLine=scann.nextLine();
				if(nextLine.trim()!="" && !nextLine.trim().startsWith("--")) {
					text+=nextLine.replaceAll("--.*", "")+"\n";
				}
			}
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while reading from \n\t"+file.getAbsolutePath(), e);
		}
		text=text.replaceAll("--.*"," ").replaceAll("\n"," ").replaceFirst(".*CREATE.*INDEX\\s*","");
		String name=text.replaceFirst("\\s.*", "");
		return name;
	}

	public static String scanForConstraintDefName(File file) {
	String text="";
		try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
				Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
			while(scann.hasNextLine()) {
				String nextLine=scann.nextLine();
				if(nextLine.trim()!="" && !nextLine.trim().startsWith("--")) {
					text+=nextLine+"\n";
				}
			}
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while reading from \n\t"+file.getAbsolutePath(), e);
		}
		text=text.replaceAll("--.*"," ").replaceAll("\n"," ").replaceFirst(".*ADD.*CONSTRAINT[\\s\n]*","");
		String name=text.replaceFirst("\\s.*", "");
		return name;
	}

	public static String detectSchema(File file) {
		String schema=null;
		SQLTypes type=SQLSearcher.detectType(file);
		if(type==null) {
			return schema;
		}
		String completeName=null;
		switch(type) {
		case TABLE:
		case VIEW:
		case FUNCTION:
		case SEQUENCE:
			completeName=SQLSearcher.detectName(file);
			break;
		case CONSTRAINT:
			List<String> tmp=SQLSearcher.scanFor(file, "ALTER", "TABLE");
			if(tmp!=null && tmp.size()>0) {
				completeName=tmp.get(0);
			}else {
				completeName=null;
			}
			break;
		case INDEX:
			String text="";
			try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
					Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
				while(scann.hasNextLine()) {
					String nextLine=scann.nextLine();
					if(nextLine.trim()!="" && !nextLine.trim().startsWith("--")) {
						text+=nextLine.replaceAll("--.*", "")+"\n";
					}
				}
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while reading from \n\t"+file.getAbsolutePath(), e);
			}
			text=text.replaceAll("--.*"," ").replaceAll("\n"," ").replaceFirst(".*INDEX\\s*","");
			text=text.replaceFirst("\\w+\\s+ON\\s*", "");
			completeName=text.replaceFirst("\\s.*", "");
			break;
		}
		if(completeName==null) {
			completeName="";
		}
		try {
			if(completeName.contains(".")) {
				schema=completeName.substring(0, completeName.indexOf(".")).trim();
				if(schema.isEmpty()) {
					logger.warn("Nella DDL "+file.getName()+" non � stato rilevato uno schema di riferimento.");
					throw new IllegalArgumentException("Nella DDL \"+observedFile.getName()+\" non � stato rilevato uno schema di riferimento.");
				}
			}else {
				logger.warn("Nella DDL "+file.getName()+" non � stato rilevato uno schema di riferimento.");
				throw new IllegalArgumentException("Nella DDL \"+observedFile.getName()+\" non � stato rilevato uno schema di riferimento.");
			}
		}catch(IllegalArgumentException e) {
			//qui decido cosa fare quando non sono riuscito ad individuare lo schema di riferimento
			logger.info("schema non individuato in "+file.getName());
			schema=null;
		}
		return schema;
	}
	
	/**
	 * tries to recover the oracle schema of the object defined in the file, based on the mapping in domenica's excell
	 * @param file
	 * @return
	 */
	public static String detectFormerOracleSchema(File file) {
		String name=SQLSearcher.detectName(file);
		if(name!=null) {
			return detectFormerOracleSchema(name);
		}else {
			logger.error("We couldn't find the object name in file "+file.getName()+". ("+file.getAbsolutePath());
			return null;
		}
	}
	
	/**
	 * tries to recover the oracle schema of the object indicated, based on the mapping in domenica's excell
	 * @param name of the object, complete with its greenplum schema
	 * @return
	 */
	public static String detectFormerOracleSchema(String objectName) {
		LoaderSchemaMap load=new LoaderSchemaMap();
		Map<String, List<String>> inverseMap=load.loadInverseMap();
		List<String> invObjects=inverseMap.get(objectName);
		if(invObjects!=null && invObjects.size()==1) {
			return invObjects.get(0);
		}else if(invObjects==null || invObjects.size()==0) {
			logger.warn("the object "+objectName+" does not have a valid inverse.");
			return null;
		}else {
			logger.warn("the object "+objectName+" does not have a unique valid inverse: "+invObjects.toString());
			return null;
		}		
	}
		
	/**
	 * Controlla se esistono delle chiamate a degli oggetti sotto schemi con nomi Oracle, cosa che non dovrebbe mai accadere.
	 * Restituisce true se ne trova ed effettua un log dei file incriminati. 
	 * @return numero di occorrenze di tali schema
	 */
	public static boolean checkActiveOracleSchema(File file, boolean useParser) {
		boolean foundAtleastOne=false;
		List<String> oracleSchemas=new ArrayList<>();
		oracleSchemas.add("XXDESCH");
		oracleSchemas.add("XXADM");
		oracleSchemas.add("XXDEPM");
		oracleSchemas.add("XXDESCOR");
		oracleSchemas.add("XXDESCHAPP");
		oracleSchemas.add("XXPMAPP");
		oracleSchemas.add("XXSCHAPP");
		oracleSchemas.add("XXDWHAPP");
		oracleSchemas.add("PUBLIC");
		oracleSchemas.add("XXDWH");
		oracleSchemas.add("XXDWHSCORAPP");
		oracleSchemas.add("XXSCH");
		oracleSchemas.add("XXSCORAPP");
		oracleSchemas.add("DSETL");
		oracleSchemas.add("XXPM");
		oracleSchemas.add("XXSCOR");
		oracleSchemas.add("XXDWHSCOR");
		try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
				Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
			//logger.trace("Searching file "+file.getName()+ "for unreplaced schemas. ("+file.getAbsolutePath()+")");
			while(scann.hasNextLine()) {
				String nextLine=scann.nextLine();
				for(String schemaItem: oracleSchemas) {
					boolean found=false;
					if(useParser) {
						try {
							found=StringUtilities.findExactly(nextLine, schemaItem+".").size()>0;
						} catch (Exception e) {
							logger.info("\tFind exactly raised an error. Switching to searching the string in a more standard way (no token analysis");
							found=nextLine.contains(schemaItem+".");
						}
					}else {
						found=nextLine.contains(schemaItem);
					}
					if(found) {
						if(!foundAtleastOne) {
							foundAtleastOne=true;
						}
						try{
							String objName=SQLSearcher.detectName(file);
							logger.warn(file.getAbsolutePath()+"\tObject"+objName+"\t:schema "+schemaItem+" has been found!!\t"+nextLine);
						}catch(Exception e) {
							logger.warn(file.getAbsolutePath()+"\t\t:schema "+schemaItem+" has been found!!\t"+nextLine);
						}
					}
				}
			}
		}catch (IOException e) {
			throw new RuntimeException("\tException raised while reading/searching for \n\t"+file.getAbsolutePath(), e);
		}
		return foundAtleastOne;
	}
	
	public static boolean checkActiveOracleSchema(File file) {
		return checkActiveOracleSchema(file, true);
	}
	
}
