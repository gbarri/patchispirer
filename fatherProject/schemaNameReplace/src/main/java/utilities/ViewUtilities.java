package utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.create.view.CreateView;
import utilities.SQLSearcher.SQLTypes;

public class ViewUtilities {
	
	static Logger logger=LoggerFactory.getLogger(ViewUtilities.class);
	
	public static List<String> getColumnNames(File file){
		List<String> out=null;
		if(file==null || SQLSearcher.detectType(file)!=SQLTypes.VIEW) {
			logger.warn("file "+file.getName()+" has been not detected as a view definition ("+file.getAbsolutePath()+")");
			throw new IllegalArgumentException("file "+file.getName()+" has been not detected as a view definition");
		}else {
			String textDDL=TableUtilities.isolateDDL(file);
			try {
//				Statement st = new CCJSqlParserManager().parse(new FileReader(file));
				Statement st = new CCJSqlParserManager().parse(new StringReader(textDDL));
				CreateView crTb=(CreateView) st;
				out=crTb.getColumnNames();
			} catch (JSQLParserException e) {
				logger.error("Intercepted parser exception "+e.getClass()+": "+e.getMessage()+"\tfile "+file.getAbsolutePath());
			} catch (ClassCastException e) {
				logger.error("file "+file.getName()+" once parsed is not a Create-View");
			}
			return out;
		}
	}
	
	public static void main(String[] args) {
		File file=new File("C:\\Users\\g.barri\\Desktop\\tmp\\test.sql");
		getColumnNames(file);
	}
}
