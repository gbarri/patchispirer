package utilities;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Initializer {
	static Logger logger=LoggerFactory.getLogger(Initializer.class);
	private static Properties properties;
	
	public static String configFile="patchIspirer.config";
	private static final String specifiContextConfigFile = "additional.config";
	
	public static Path getRunningFolder() throws URISyntaxException {
		return new File(Initializer.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParentFile().toPath();
	}
	
	private Initializer() throws URISyntaxException, IOException {
		init();
	}
		
	private static String preprocessingPropertiesFile(File file) {
		String txt="";
		try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
				Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
			while(scann.hasNextLine()) {
				txt+=scann.nextLine()+"\n";
			}
		}catch (IOException e) {
			RuntimeException exc= new RuntimeException("Exception raised while reading from\t"+file.getAbsolutePath(), e);
			logger.error("Config file failure!", exc);
			throw exc;
		}
		return preprocessingPropertiesString(txt);
	}
	
	private static String preprocessingPropertiesString(String properties) {
		return properties.replace("\\","\\\\");
	}
	
//	/**
//	 * recover the folder in which we are working and check if there is a configuration file named patchIspirer.config
//	 * @throws URISyntaxException 
//	 * @throws IOException 
//	 */
//	private static void init() {
//		properties=new Properties();
//		try {
//			File wrkDir=getRunningFolder().toFile();
//			if(wrkDir.isDirectory()) {
//				File file=new File(wrkDir.getAbsolutePath()+"\\"+configFile);
//				if(file.exists()) {
//					//read from existing properties
////					properties.load(new FileReader(file));
//					properties.load(new StringReader(preprocessingPropertiesFile(file)));
//				}else {
//					//
//					properties.load(new StringReader(preprocessingPropertiesFile(new File("C:\\Users\\g.barri\\Desktop\\patchIspirer\\patchIspirer.config"))));
//				}
//			}
//		} catch (URISyntaxException e) {
//			e.printStackTrace();
//			throw new IllegalArgumentException(e);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//			throw new IllegalArgumentException(e);
//		} catch (IOException e) {
//			e.printStackTrace();
//			throw new IllegalArgumentException(e);
//		}
//			
//	}
	
	/**
	 * recover the folder in which we are working and check if there is a configuration file named patchIspirer.config
	 * @throws URISyntaxException 
	 * @throws IOException 
	 */
	private static void init() {
		String userHome=System.getProperty("user.home");
		File wrkDir=new File(userHome.replace("\\", "\\\\")+"\\patchIspirer.setup");
		if(!wrkDir.exists()) {
			try {
				Files.createDirectory(wrkDir.toPath());
				File patchIspirerConfigBase=new File(wrkDir.getAbsolutePath()+"\\"+configFile);
				try(FileWriter fw=new FileWriter(patchIspirerConfigBase.getAbsolutePath());
						BufferedWriter bw = new BufferedWriter(fw);
						PrintWriter print = new PrintWriter(bw);)
				{
					String text="##Complete with the path of the desired position for in your particular case\r\n"
							+   "##Or simply DELete the following line and put in this position your patchIspirer.config file\r\n" + 
								"REDIRECT_TO="+getRunningFolder()+"\\"+configFile;
					print.print(text);
				}catch (IOException e) {
					throw new RuntimeException("Exception raised while writing to "+patchIspirerConfigBase.getAbsolutePath(), e);
				} catch (URISyntaxException e) {
					try(FileWriter fw=new FileWriter(patchIspirerConfigBase.getAbsolutePath());
							BufferedWriter bw = new BufferedWriter(fw);
							PrintWriter print = new PrintWriter(bw);)
					{
						String text="##Complete with the path of the desired position for in your particular case\r\n"
								+   "##Or simply DELete the following line and put in this position your patchIspirer.config file\r\n" + 
									"REDIRECT_TO=I/must_be/altered/check:patchIspirer.setup";
						print.print(text);
					}catch (IOException exc) {
						throw new RuntimeException("Exception raised while writing to "+patchIspirerConfigBase.getAbsolutePath(), exc);
					}
				}
			} catch (IOException e) {
				logger.error("You are supposed to have a folder "+wrkDir.getAbsolutePath()+". It seems to not be present and somwthing stops me from creating it. Please create the folder with patchIspirer.config in it");
				e.printStackTrace();
				throw new RuntimeException("Folder ${HOME}\\patchIspirer.setup not found",e); 
			}
		}
		init(wrkDir);
	}
	
	/**
	 * Using the given path, if it is a folder checks if there is a configuration file named patchIspirer.config, if it is file it reads it to recover configurations 
	 * @throws URISyntaxException 
	 * @throws IOException 
	 */
	private static void init(File directory) {
		boolean redirect=false;
		do {
//			properties=new Properties();
			if(directory.isDirectory()) {
				logger.info("The alternative path for configuration variables is a directory, therefore we suppose default file "+configFile+" is present in that directory.");
				directory=new File(directory.getAbsolutePath()+"\\"+configFile);
			}
			if(!directory.exists()) {
				logger.warn("The file indicated for configuration of patchIspirer "+directory+" does not exists. We search for the standard file ("+configFile+") in the running location of this jar");
				try {
					directory=new File(getRunningFolder().toFile().getAbsolutePath()+"\\"+configFile);
					if(directory.isDirectory()) {
						directory=new File(directory.getAbsolutePath()+"\\"+configFile);
					}
				} catch (URISyntaxException e) {
					RuntimeException exc=new IllegalArgumentException("failed to locate properly the current working directory", e);
					e.printStackTrace();
					throw exc;
				}
			}
			if(directory.exists() && directory.isFile()) {
				loadProperties(directory);
			}else {
				RuntimeException e=new IllegalArgumentException("There has been a problem with the configuration file "+directory+". It is not a file, or does not exists");
				logger.error("Config file failure!", e);
				throw e;
			}
			redirect=properties.getProperty("REDIRECT_TO")!=null && (!properties.getProperty("REDIRECT_TO").trim().equals(directory.getAbsolutePath()) && !properties.getProperty("REDIRECT_TO").trim().equals(directory.getAbsolutePath()+"\\"+configFile));
			if(redirect) {
				directory=new File(properties.getProperty("REDIRECT_TO"));
			}
		}while(redirect);
	}
	
	private static void loadProperties(File file) {
		if(file.exists()) {
			try {				
				properties=new Properties();
				properties.load(new StringReader(preprocessingPropertiesFile(file)));
				if(properties.getProperty("REDIRECT_TO")==null) {
					String context=properties.getProperty("Context");
					if(context==null || context.trim().equals("")) {
						logger.error("The Context configuration parameter is mandatory");
						throw new IllegalArgumentException("Context parameter missing");
					}else {
						String baseFolder;
						try {
							Path baseFolderPath=getRunningFolder();
							baseFolder=baseFolderPath.toFile().getAbsolutePath();
						} catch (URISyntaxException e) {
							RuntimeException exc=new IllegalArgumentException("failed to locate properly the current working directory", e);
							e.printStackTrace();
							throw exc;
						}
						File contextFolder=new File(baseFolder+"\\"+context);
						if(!contextFolder.exists()) {
							String GreenplumLocation=properties.getProperty("localRepositoryDir");
							if(GreenplumLocation!=null) {
								File datalakeLocDir=new File(GreenplumLocation);
								String jarDefaultDir=datalakeLocDir.getParentFile().getAbsolutePath()+"\\tools\\javaPostProcessing\\patchIspirer";
								contextFolder=new File(jarDefaultDir+"\\"+context);
								if(!contextFolder.exists()) {
									throw new IllegalArgumentException("The folder "+context+" indicated in Context parameter does not exists in the same directory as the jar, nor under "+jarDefaultDir+" folder (path calculated from variable localRepositoryDir)");
								}
							}else {
								throw new IllegalArgumentException("The folder "+context+" indicated in Context parameter does not exists in default location (same directory as jar)");
							}
						}
						String contextFolderString=contextFolder.getAbsolutePath();
						Properties additionalConfigFiles=new Properties();
						additionalConfigFiles.load(new StringReader(preprocessingPropertiesFile(new File(contextFolderString+"\\"+specifiContextConfigFile))));
						for(String property : additionalConfigFiles.stringPropertyNames()) {
							String propValue=additionalConfigFiles.getProperty(property);
							properties.setProperty(property, contextFolderString+"\\"+propValue);
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
				logger.error("Config file failure!", e);
				throw new IllegalArgumentException(e);
			}
		}
		else {
			throw new IllegalArgumentException("Configuration file "+file.getAbsolutePath()+" not found!");
		}
	}
	
	public static String getProperty(String key, boolean suppressError) {
		if(properties==null) {
			Initializer.init();
		}
		String value= properties.getProperty(key);
		if(!suppressError && value==null) {
			logger.error("field "+key+" missing in "+configFile);
		}
		return value;
	}
	
	public static String getProperty(String key) {
		return getProperty(key, false);
	}
}
