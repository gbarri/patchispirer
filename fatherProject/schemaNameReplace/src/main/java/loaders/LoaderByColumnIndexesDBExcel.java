package loaders;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Sheet;

import schemaMapping.LoaderSchemaMap;
import utilities.Initializer;

/**
 * uguale a LoadByColumnIndex ma aggiunge uno strato di logica a come sono utilizzati i campi estratti
 * @author g.barri
 *
 */
public class LoaderByColumnIndexesDBExcel extends LoaderByColumnIndexOrName {

	public LoaderByColumnIndexesDBExcel(File configFile) {
		super(Initializer.getProperty("excelDocument"), configFile);
	}
	
	public LoaderByColumnIndexesDBExcel(String configOptions) {
		super(new File(Initializer.getProperty("excelDocument")), configOptions);
	}

	/**
	 * Suppone che la chiave primaria sia schema.object oppure che sia object e il primo argomento a seguire sia lo schema.
	 * L'ultimo argomento deve indicare se è o meno da migrare. Restituisce tutti gli argomenti a parte l'ultimo
	 */
	@Override
	protected Map<String, String[]> popolateMap(Sheet sheet, Map<String, String[]> outMap) {
		Map<String, String[]> tmpMap=super.popolateMap(sheet, outMap);
		outMap=new HashMap<String, String[]>();
		for(String PK:tmpMap.keySet()){
			String[] args=tmpMap.get(PK);
			if("YES".equals(args[args.length-1])) {
				String[] newArgs=new String[args.length-1];
				for(int i=0; i<newArgs.length; i++) {
					newArgs[i]=args[i];
				}
				String schema="";
				String object="";
				if(PK.contains(".")) {
					schema=PK.substring(0, PK.indexOf(".")).toLowerCase();
					object=PK.substring(PK.indexOf(".")+1).toUpperCase();
				}else {
					schema=newArgs[0].toLowerCase();
					object=PK.toUpperCase();
				}
//				System.out.println(schema+"."+object+" , "+newArgs);
				outMap.put(schema+"."+object, newArgs);
			}
		}
		return outMap;
	}

}
