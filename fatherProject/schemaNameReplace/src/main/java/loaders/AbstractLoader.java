package loaders;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.LoggerFactory;
import org.tinylog.Logger;

public abstract class AbstractLoader implements Loader {
	public String excelFile=null;
	static org.slf4j.Logger logger=LoggerFactory.getLogger(AbstractLoader.class);
	public AbstractLoader() {
		
	}
	
	public AbstractLoader(String excelFile) {
		this.excelFile=excelFile;
	}
	
	@Override
	public String toString() {
		return this.getClass()+": reading from "+excelFile;
	}
	
	protected Map<String, String[]> popolateMap(Sheet sheet, Map<String, String[]> outMap) {
		// Create a DataFormatter to format and get each cell's value as String
        DataFormatter dataFormatter = new DataFormatter();
        // You can use a for-each loop to iterate over the rows and columns
        Integer[] columnIndexes=getColumnIndxes(sheet);
        Integer[] PKindexes=getPKIndexes(sheet);
        
        for (Row row: sheet) {
        	if(row.getRowNum()==0) {
            	for(int ind=0; ind<PKindexes.length; ind++) {
            		logger.info("PK made of column: "+dataFormatter.formatCellValue(row.getCell(PKindexes[ind])));
            	}
            	for(int ind=0; ind<columnIndexes.length; ind++) {
            		logger.info("Exporting column: "+dataFormatter.formatCellValue(row.getCell(columnIndexes[ind])));
            	}
        		continue;
        	}
        	String PK=dataFormatter.formatCellValue(row.getCell(PKindexes[0])).trim();
        	for(int ind=1; ind<PKindexes.length; ind++) {
        		PK+="."+dataFormatter.formatCellValue(row.getCell(PKindexes[ind])).trim();
        	}
        	String[] args=new String[columnIndexes.length];
        	for(int ind=0; ind<columnIndexes.length; ind++) {
        		args[ind]=dataFormatter.formatCellValue(row.getCell(columnIndexes[ind])).trim();
        	}
        	outMap.put(PK,args);
        	
        }
//        System.out.println("---Completed "+sheet.getSheetName());
        return outMap;
	}

	@Override
	public Map<String, String[]> loadMap() throws EncryptedDocumentException, InvalidFormatException, IOException{
		 Map<String, String[]> outMap=new HashMap<>();
		 // Creating a Workbook from an Excel file (.xls or .xlsx)
		String xlsxFile=excelFile;
		try(Workbook workbook = WorkbookFactory.create(new File(xlsxFile));) {
			// Getting the Sheets from the excell file
			Sheet sheet = getSheet(workbook);
			logger.info("Loading data from sheet "+sheet.getSheetName());
			outMap=this.popolateMap(sheet, outMap);
			//System.out.println(outMap);
			
		} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
			//TODO da sistemare
			throw e;
		}
		
		return outMap;
	}
	
	@Override
	public Map<String, String[]> loadMap(Workbook workbook) throws EncryptedDocumentException, InvalidFormatException, IOException{
		 Map<String, String[]> outMap=new HashMap<>();
		// Getting the Sheets from the workbook file
		Sheet sheet = getSheet(workbook);
		outMap=this.popolateMap(sheet, outMap);
		//System.out.println(outMap);		
		return outMap;
	}

	/**
	 * 
	 * @param workbook
	 * @return
	 */
	protected abstract Sheet getSheet(Workbook workbook);
	
	/**
	 * indica le colonne del file excell dove recuperare i dati.
	 * I dati sono interpretati come stringhe di testo.
	 * @param sheet
	 * @return
	 */
	protected abstract Integer[] getColumnIndxes(Sheet sheet);
	/**
	 * indica le colonne del file excell dove recuperare il valore della PK.
	 * I dati sono interpretati come stringhe di testo.
	 * @param sheet
	 * @return
	 */
	protected abstract Integer[] getPKIndexes(Sheet sheet);


}
