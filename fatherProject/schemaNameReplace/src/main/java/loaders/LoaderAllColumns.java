package loaders;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

@Deprecated
public abstract class LoaderAllColumns extends AbstractLoader {

	public LoaderAllColumns(String excelFile) {
		super(excelFile);
	}
	
	@Override
	public String toString() {
		return this.getClass()+": reading from "+excelFile;
	}
	
	@Override
	protected Integer[] getColumnIndxes(Sheet sheet) {
		Row row=sheet.getRow(0);
        int startNum=row.getFirstCellNum();
		int endNum=row.getLastCellNum(); 
        List<Integer> tmpList=new ArrayList<>();
        tmpList.add(startNum); //PK
        //adding the other non-empty, non-null column
        for(int indCell=startNum+1; indCell<endNum; indCell++) {
        	Cell cl=row.getCell(indCell);
        	if(!(cl==null)) {
        		//la cella non � vuota
        		tmpList.add(indCell);
        	}
        }
        Integer[] out=new Integer[tmpList.size()];
   		out= tmpList.toArray(out);
 
		return out;
	}

	@Override
	protected Sheet getSheet(Workbook workbook) {
		return new LoaderByColumnIndex((String)null, null).getSheet(workbook);
	}

	@Override
	protected Integer[] getPKIndexes(Sheet sheet) {
		// TODO Auto-generated method stub
		return null;
	}

}
