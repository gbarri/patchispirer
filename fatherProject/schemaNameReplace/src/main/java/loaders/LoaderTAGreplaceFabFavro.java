package loaders;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import schemaMapping.LoaderSchemaMap;

public class LoaderTAGreplaceFabFavro extends LoaderByColumnIndex {
	
	static Logger logger = LoggerFactory.getLogger(LoaderTAGreplaceFabFavro.class);
	String DBexcellName=null;
	String DBExcellPath=null;
	
	public LoaderTAGreplaceFabFavro(String excelFile, File configFile, String excelpath2, String excelname2) {
		super(excelFile,configFile);
		DBexcellName=excelname2;
		DBExcellPath=excelpath2;
//		DBexcellName="EDWP - SupplyChain & Manufacturing (27).xlsx";
//		DBExcellPath="C:\\Users\\g.barri\\Desktop\\files_favro_tmp";
	}
	
	@Override
	protected Map<String, String[]> popolateMap(Sheet sheet, Map<String, String[]> outMap) {
		// Create a DataFormatter to format and get each cell's value as String
        DataFormatter dataFormatter = new DataFormatter();
        Integer[] columnIndexes=getColumnIndxes(sheet);
        /////
        //since the file excel does not contain the complete pckg name as used in tag replacement, we must build it starting from the object name.
        //this means reading from the excel by DB
        Map<String, String> replaceMap=new HashMap<>();
		LoaderSchemaMap loader=new LoaderSchemaMap(DBexcellName, DBExcellPath);

		try {
			replaceMap=loader.loadMap();
		} catch (EncryptedDocumentException e) {
			logger.error("Failed to load the replacement map (is it open in Excell?).");
			e.printStackTrace();
			throw e;
		}
		//Il mapping di Favro e pieno di problemi. Rimuoviamo la parte di schema dalla PK, e ricerchiamo solo tramite il nome dello oggetto
		Map<String, String> newMap=new HashMap<>();
		for(String keyItem: replaceMap.keySet()) {
			String newKey=keyItem.substring(keyItem.indexOf(".")+1);
			if(newMap.containsKey(newKey)) {
				logger.warn("BE CAREFUL: using only the object name "+newKey+" instead of schema.object as PK led to a duplicate key!!!\n"
						+ "\tExisting mapping: ("+newKey+","+newMap.get(newKey)+"), adding: ("+newKey+","+replaceMap.get(keyItem)+")");
			}
			newMap.put(newKey, replaceMap.get(keyItem));
		}
		replaceMap=newMap;
        ///////
        for (Row row: sheet) {
//        	System.out.println(sheet.getSheetName()+": "+row.getRowNum());
        	if(row.getRowNum()==0) {
        		continue;
        	}
//        	String PK_oldSchema=dataFormatter.formatCellValue(row.getCell(columnIndexes[0]));a
        	String objectName=dataFormatter.formatCellValue(row.getCell(columnIndexes[1]));
        	String PK=replaceMap.get(objectName.toUpperCase());
        	if(PK==null) {
        		logger.warn("the object "+objectName.toUpperCase()+" has not been found.\nSheet:"+sheet.getSheetName()+", rowNum:"+row.getRowNum());
        		continue;
        	}
          	String[] args=new String[columnIndexes.length-2];
        	for(int ind=2; ind<columnIndexes.length; ind++) {
        		args[ind-2]=dataFormatter.formatCellValue(row.getCell(columnIndexes[ind]));
        	}
        	//TODO: aggiungere check sui vincoli delle chiavi??
        	outMap.put(PK,args);
        }
//        System.out.println("---Completed "+sheet.getSheetName());
        return outMap;
	}
}
