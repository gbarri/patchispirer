package loaders;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utilities.Initializer;

public class LoaderMultipleSheetsDBExcel extends LoaderMultipleSheets {

	static Logger logger=LoggerFactory.getLogger(LoaderMultipleSheetsDBExcel.class);
	
	public LoaderMultipleSheetsDBExcel(File configFile) {
		super(new File(Initializer.getProperty("excelDocument")), configFile);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public Map<String, String[]> loadMap() throws EncryptedDocumentException, InvalidFormatException, IOException{
		 Map<String, String[]> outMap=new HashMap<>();
		 // Creating a Workbook from an Excel file (.xls or .xlsx)
		String xlsxFile=excelFile;
		try(Workbook workbook = WorkbookFactory.create(new File(xlsxFile));) {
			// Getting the Sheets from the excell file
			String sheetsNames=super.configOptionsMap.get("Loader.useSheets");
			String[] names=sheetsNames.split(",");
		for(String name:names) {
			int sheetIndex=workbook.getSheetIndex(name.trim());
			String useColumnsAt=super.configOptionsMap.get("Loader."+name.replace(" ","_").trim()+".useColumnsAt");
			String usePKAt=super.configOptionsMap.get("Loader."+name.replace(" ","_").trim()+".usePKAt");
			String configOptions="Loader.useColumnsAt="+useColumnsAt+"\r\n"
							   + "Loader.useSheetAt="+sheetIndex+"\r\n"
							   + "Loader.usePKAt="+usePKAt;
			
			Loader loader=new LoaderByColumnIndexesDBExcel(configOptions);
			Map<String, String[]> tmpMap=loader.loadMap(workbook);
			Iterator<String> iter=tmpMap.keySet().iterator();
			while(iter.hasNext()) {
				String key=iter.next();
				if(outMap.containsKey(key)) {
					String[] valOutMap=outMap.get(key);
					String[] valTmpMap=tmpMap.get(key);
					boolean equivalent=valTmpMap.length==valOutMap.length;
					int i=0;
					while(equivalent && i<valTmpMap.length) {
						equivalent=valOutMap[i].equals(valTmpMap[i]);
					}
					if(!equivalent) {
						String valOutMapArgs="[";
						if(valOutMap.length>0) {
							valOutMapArgs+=valOutMap[0];
						}
						for(int j=1; j<valOutMap.length; j++) {
							valOutMapArgs+=","+valOutMap[j];
						}
						valOutMapArgs+="]";
						String valTmpMapArgs="[";
						if(valTmpMap.length>0) {
							valTmpMapArgs+=valTmpMap[0];
						}
						for(int j=1; j<valTmpMap.length; j++) {
							valTmpMapArgs+=","+valTmpMap[j];
						}
						valTmpMapArgs+="]";
						logger.error("The PK "+key+" used for mapping from "+xlsxFile+" is not unique!\t"+valOutMapArgs+"\treplaced by\t"+valTmpMapArgs);
					}
				}
				outMap.put(key, tmpMap.get(key));
			}
			//outMap.putAll(tmpMap);
		}
			
		} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
			//TODO da sistemare
			throw e;
		}
		
		return outMap;
	}

}
