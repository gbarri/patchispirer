package loaders;

import java.io.IOException;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;

public interface Loader {
	public Map<String, String[]> loadMap() throws EncryptedDocumentException, InvalidFormatException, IOException;
	public Map<String, String[]> loadMap(Workbook workbook) throws EncryptedDocumentException, InvalidFormatException, IOException;
}
