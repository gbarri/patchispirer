package loaders;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringBufferInputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;


import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoaderByColumnIndex extends AbstractLoader{
	public final static int defaultSheetIndex=0;
	public String configFile=null;
	protected Map<String, String> configOptionsMap;
	private static Logger logger=LoggerFactory.getLogger(LoaderByColumnIndex.class);
	
	public LoaderByColumnIndex(String excelFile, File configFile) {
		super(excelFile);
		this.configFile=configFile.getAbsolutePath();
		try {
			configOptionsMap=loadConfigParameters(configFile);
		}catch (FileNotFoundException e) {
			logger.info("Configuration file "+configFile+" is missing or misplaced.\n"+e.getMessage()+"\n\tEmpty map loaded to avoid stops.");
			configOptionsMap=new HashMap<>();
			//TODO: ha senso un default, ad es: leggere tutte le colonne?
			//	se mi serve avere un controllo sulle colonne mi faccio il file di config. opportuno.
			//  se non lo faccio ma chiamo questa funz. per errore, potrei essere rediretto su LoaderAllColumns...
		}
		
	}
	
	public LoaderByColumnIndex(File excelFile, File configFile) {
		this(excelFile.getAbsolutePath().toString(), configFile);
	}
	
	/**
	 * In case you want to avoid creating several configuration file or something like that, for now.
	 * @param excelFile
	 * @param configOptions the content that would be in the configuration file
	 */
	public LoaderByColumnIndex(File excelFile, String configOptions) {
		super(excelFile.getAbsolutePath());
		try {
			configOptionsMap=loadConfigParameters(new StringReader(configOptions), "=");
		}catch (RuntimeException e) {
			logger.info("Failed to load configuration options. Empty map loaded to avoid stops.", e);
			configOptionsMap=new HashMap<>();
		}
	}

	public String getConfigFile() {
		return configFile;
	}
	
	public static Map<String, String> loadConfigParameters(InputStream input, String delimiter){
		Map<String,String> configParaMap=new HashMap<>();
		
		try(Scanner scann=new Scanner(new BufferedInputStream(input))){
			while(scann.hasNextLine()) {
				String nextLine=scann.nextLine();
				StringTokenizer st=new StringTokenizer(nextLine, delimiter);
				if(st.countTokens()==2) {
					configParaMap.put(st.nextToken(), st.nextToken());
				}
			}
		}
		return configParaMap;
	}
	
	public static Map<String, String> loadConfigParameters(Readable input, String delimiter) {
		Map<String,String> configParaMap=new HashMap<>();
		
		try(Scanner scann=new Scanner(input)){
			while(scann.hasNextLine()) {
				String nextLine=scann.nextLine();
				StringTokenizer st=new StringTokenizer(nextLine, delimiter);
				if(st.countTokens()==2) {
					configParaMap.put(st.nextToken(), st.nextToken());
				}
			}
		}
		return configParaMap;
	}
	
	public static Map<String, String> loadConfigParameters(File configFile) throws FileNotFoundException {
		InputStream input=new FileInputStream(configFile);
		return loadConfigParameters(input, "=");
	}
	
	public static Map<String, String> loadConfigParameters(InputStream input) throws FileNotFoundException {
		return loadConfigParameters(input, "=");
	}
	
	@Override
	protected Integer[] getColumnIndxes(Sheet sheet) {
		// TODO read from config. file for options and info about columns 
		//put it inside an array, return it
		if(configOptionsMap.containsKey("Loader.useColumnsAt")) {
			StringTokenizer strTkn=new StringTokenizer(configOptionsMap.get("Loader.useColumnsAt"), ",");
			List<Integer> tmpList=new ArrayList<>();
			while(strTkn.hasMoreTokens()) {
				String nextToken=null;
				try {
					nextToken=strTkn.nextToken().trim();
					Integer tmp=Integer.parseInt(nextToken.trim());
					tmpList.add(tmp);
				}catch(NumberFormatException e) {
					logger.info("argument "+nextToken+" passed to \"Loader.useColumnsAt\" is not an integer. This value is skipped");
				}				
			}
			Integer[] colIndexes=new Integer[tmpList.size()];
			colIndexes=tmpList.toArray(colIndexes);
			return colIndexes;
		}else {
			logger.info("Parameter \"Loader.useColumnsAt\" not found among saved parameters"); 
			return null;
		}
		
	}
	
	@Override
	protected Integer[] getPKIndexes(Sheet sheet) {
		// TODO read from config. file for options and info about columns 
		//put it inside an array, return it
		if(configOptionsMap.containsKey("Loader.usePKAt")) {
			StringTokenizer strTkn=new StringTokenizer(configOptionsMap.get("Loader.usePKAt"), ",");
			List<Integer> tmpList=new ArrayList<>();
			while(strTkn.hasMoreTokens()) {
				String nextToken=null;
				try {
					nextToken=strTkn.nextToken().trim();
					Integer tmp=Integer.parseInt(nextToken.trim());
					tmpList.add(tmp);
				}catch(NumberFormatException e) {
					logger.info("argument "+nextToken+" passed to \"Loader.usePKAt\" is not an integer. This value is skipped");
				}				
			}
			Integer[] colIndexes=new Integer[tmpList.size()];
			colIndexes=tmpList.toArray(colIndexes);
			return colIndexes;
		}else {
			logger.error("Parameter \"Loader.usePKAt\" not found among saved parameters"); 
			return null;
		}
		
	}


	@Override
	public Sheet getSheet(Workbook workbook) {
		//read from config. file for appropriate parameter, null if absent.
		if(configOptionsMap.containsKey("Loader.useSheetAt")) {
			Integer sheetIndex=null;
			try {
				sheetIndex=Integer.parseInt(configOptionsMap.get("Loader.useSheetAt").trim());
				return workbook.getSheetAt(sheetIndex);
			}catch (NumberFormatException e) {
				logger.info("argument "+configOptionsMap.get("Loader.useSheetAt")+" passed to \"Loader.useSheetAt\" is not an integer. Switching to the default sheet index 0");
				return workbook.getSheetAt(0);
			}catch(IllegalArgumentException e) {
				logger.info("There is no sheet corresponding to index "+sheetIndex+". \n Switching to the default index 0.");
				return workbook.getSheetAt(0);
			}
		}else {
			logger.info("Parameter \"Loader.useSheetAt\" not found among saved parameters.\n Switching to the default sheet index 0"); 
			return workbook.getSheetAt(0);
		}

	}
	
		
}
