package loaders;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.tinylog.Logger;

import utilities.Initializer;

public class LoaderMultipleSheets extends LoaderByColumnIndexOrName{
	
	public LoaderMultipleSheets(File excelFile, File configFile) {
		super(excelFile, configFile);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public Map<String, String[]> loadMap() throws EncryptedDocumentException, InvalidFormatException, IOException{
		 Map<String, String[]> outMap=new HashMap<>();
		 // Creating a Workbook from an Excel file (.xls or .xlsx)
		String xlsxFile=excelFile;
		try(Workbook workbook = WorkbookFactory.create(new File(xlsxFile));) {
			// Getting the Sheets from the excell file
			String sheetsNames=super.configOptionsMap.get("Loader.useSheets");
			String[] names=sheetsNames.split(",");
		for(String name:names) {
			int sheetIndex=workbook.getSheetIndex(name.trim());
			String useColumnsAt=super.configOptionsMap.get("Loader."+name.replace(" ","_").trim()+".useColumnsAt");
			String usePKAt=super.configOptionsMap.get("Loader."+name.replace(" ","_").trim()+".usePKAt");
			String configOptions="Loader.useColumnsAt="+useColumnsAt+"\r\n"
							   + "Loader.useSheetAt="+sheetIndex+"\r\n"
							   + "Loader.usePKAt="+usePKAt;
			
//			Loader loader=new LoaderByColumnIndex(new File(excelFile), configOptions);
			Loader loader=new LoaderByColumnIndexOrName(new File(excelFile), configOptions);
			outMap.putAll(loader.loadMap(workbook));
		}
			
		} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
			//TODO da sistemare
			throw e;
		}
		
		return outMap;
	}
	
}
