package loaders;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoaderByColumnIndexOrName extends LoaderByColumnIndex {
	static Logger logger=LoggerFactory.getLogger(LoaderByColumnIndexOrName.class);

	public LoaderByColumnIndexOrName(String excelFile, File configFile) {
		super(excelFile, configFile);
		// TODO Auto-generated constructor stub
	}

	public LoaderByColumnIndexOrName(File excelFile, File configFile) {
		super(excelFile, configFile);
		// TODO Auto-generated constructor stub
	}

	public LoaderByColumnIndexOrName(File excelFile, String configOptions) {
		super(excelFile, configOptions);
		// TODO Auto-generated constructor stub
	}
	
	private Integer[] parserOption(Sheet sheet, String optionParameter) { 
		//put it inside an array, return it
		if(configOptionsMap.containsKey(optionParameter)) {
			StringTokenizer strTkn=new StringTokenizer(configOptionsMap.get(optionParameter), ",");
			List<Integer> tmpList=new ArrayList<>();
			while(strTkn.hasMoreTokens()) {
				String nextToken=null;
				try {
					nextToken=strTkn.nextToken().trim();
					Integer tmp=Integer.parseInt(nextToken.trim());
					tmpList.add(tmp);
				}catch(NumberFormatException e) {
					logger.trace("argument "+nextToken+" passed to \""+optionParameter+"\" is not an integer. Interpreting as name");
					Row row=sheet.getRow(sheet.getFirstRowNum());
					if(row==null) {
						throw new IllegalArgumentException("Sheet "+sheet.getSheetName()+" seems to be empty");
					}else {
						DataFormatter dataFormatter = new DataFormatter();
						Iterator<Cell> iter=row.cellIterator();
						boolean nameFound=false;
						while(iter.hasNext()) {
							Cell cell=iter.next();
							String cellContent=dataFormatter.formatCellValue(cell).trim();
							if(StringUtils.equalsIgnoreCase(cellContent, nextToken)) {
								logger.info("Found column "+cell.getColumnIndex()+" with name "+cellContent);
								tmpList.add(cell.getColumnIndex());
								nameFound=true;
								break;
							}
						}
						if(!nameFound) {
							throw new IllegalArgumentException("In the first row of sheet "+sheet.getSheetName()+" we did not find any column named "+nextToken); 
						}
					}
				}				
			}
			Integer[] colIndexes=new Integer[tmpList.size()];
			colIndexes=tmpList.toArray(colIndexes);
			return colIndexes;
		}else {
			logger.error("Parameter \"Loader.usePKAt\" not found among saved parameters"); 
			return null;
		}	
	}
	
//	@Override
//	protected Integer[] getColumnIndxes(Sheet sheet) { 
//		//put it inside an array, return it
//		if(configOptionsMap.containsKey("Loader.useColumnsAt")) {
//			StringTokenizer strTkn=new StringTokenizer(configOptionsMap.get("Loader.useColumnsAt"), ",");
//			List<Integer> tmpList=new ArrayList<>();
//			while(strTkn.hasMoreTokens()) {
//				String nextToken=null;
//				try {
//					nextToken=strTkn.nextToken().trim();
//					Integer tmp=Integer.parseInt(nextToken.trim());
//					tmpList.add(tmp);
//				}catch(NumberFormatException e) {
//					logger.trace("argument "+nextToken+" passed to \"Loader.useColumnsAt\" is not an integer. Interpreting as name");
//					Row row=sheet.getRow(sheet.getFirstRowNum());
//					if(row==null) {
//						throw new IllegalArgumentException("Sheet "+sheet.getSheetName()+" seems to be empty");
//					}else {
//						DataFormatter dataFormatter = new DataFormatter();
//						Iterator<Cell> iter=row.cellIterator();
//						while(iter.hasNext()) {
//							Cell cell=iter.next();
//							String cellContent=dataFormatter.formatCellValue(cell).trim();
//							if(StringUtils.equalsIgnoreCase(cellContent, nextToken)) {
//								logger.info("Found column "+cell.getColumnIndex()+" with name "+cellContent);
//								tmpList.add(cell.getColumnIndex());
//								break;
//							}
//						}
//					}
//				}				
//			}
//			Integer[] colIndexes=new Integer[tmpList.size()];
//			colIndexes=tmpList.toArray(colIndexes);
//			return colIndexes;
//		}else {
//			logger.info("Parameter \"Loader.useColumnsAt\" not found among saved parameters"); 
//			return null;
//		}
//		
//	}
//	
//	@Override
//	protected Integer[] getPKIndexes(Sheet sheet) {
//		// TODO read from config. file for options and info about columns 
//		//put it inside an array, return it
//		if(configOptionsMap.containsKey("Loader.usePKAt")) {
//			StringTokenizer strTkn=new StringTokenizer(configOptionsMap.get("Loader.usePKAt"), ",");
//			List<Integer> tmpList=new ArrayList<>();
//			while(strTkn.hasMoreTokens()) {
//				String nextToken=null;
//				try {
//					nextToken=strTkn.nextToken().trim();
//					Integer tmp=Integer.parseInt(nextToken.trim());
//					tmpList.add(tmp);
//				}catch(NumberFormatException e) {
//					logger.trace("argument "+nextToken+" passed to \"Loader.useColumnsAt\" is not an integer. Interpreting as name");
//					Row row=sheet.getRow(sheet.getFirstRowNum());
//					if(row==null) {
//						throw new IllegalArgumentException("Sheet "+sheet.getSheetName()+" seems to be empty");
//					}else {
//						DataFormatter dataFormatter = new DataFormatter();
//						Iterator<Cell> iter=row.cellIterator();
//						while(iter.hasNext()) {
//							Cell cell=iter.next();
//							String cellContent=dataFormatter.formatCellValue(cell).trim();
//							if(StringUtils.equalsIgnoreCase(cellContent, nextToken)) {
//								logger.info("Found column "+cell.getColumnIndex()+" with name "+cellContent);
//								tmpList.add(cell.getColumnIndex());
//								break;
//							}
//						}
//					}
//				}				
//			}
//			Integer[] colIndexes=new Integer[tmpList.size()];
//			colIndexes=tmpList.toArray(colIndexes);
//			return colIndexes;
//		}else {
//			logger.error("Parameter \"Loader.usePKAt\" not found among saved parameters"); 
//			return null;
//		}
//		
//	}
	@Override
	protected Integer[] getPKIndexes(Sheet sheet) {
		return parserOption(sheet, "Loader.usePKAt");
	}
	
	@Override
	protected Integer[] getColumnIndxes(Sheet sheet) { 
		return parserOption(sheet, "Loader.useColumnsAt");
	}
	
	@Override
	public Sheet getSheet(Workbook workbook) {
		//read from config. file for appropriate parameter, null if absent.
		if(configOptionsMap.containsKey("Loader.useSheetAt")) {
			Integer sheetIndex=null;
			String sheet;
			try {
				sheet=configOptionsMap.get("Loader.useSheetAt").trim();
				sheetIndex=Integer.parseInt(sheet);
				return workbook.getSheetAt(sheetIndex);
			}catch (NumberFormatException e) {
				logger.info("argument "+configOptionsMap.get("Loader.useSheetAt")+" passed to \"Loader.useSheetAt\" is not an integer. Interpreting it as a name");
				sheet=configOptionsMap.get("Loader.useSheetAt").trim();
				Sheet foundSheet=workbook.getSheet(sheet);
				if(foundSheet==null) {
					logger.info("There is no sheet corresponding to name \""+sheet+"\". \n Switching to the default index 0.");
					foundSheet=workbook.getSheetAt(0);
				}
				return foundSheet;
			}catch(IllegalArgumentException e) {
				logger.info("There is no sheet corresponding to index "+sheetIndex+". \n Switching to the default index 0.");
				return workbook.getSheetAt(0);
			}
		}else {
			logger.info("Parameter \"Loader.useSheetAt\" not found among saved parameters.\n Switching to the default sheet index 0"); 
			return workbook.getSheetAt(0);
		}

	}

}
