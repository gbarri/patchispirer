package fix_PRG_ANALYZE_TABLE;

import java.io.File;

import org.tinylog.Logger;

import schemaMapping.LoaderSchemaMap;

public class DBA22_PRG_ANALYZE_TABLE_DBMS_STAT extends Abstract_PRG_ANALYZE{
	private String tname;
	private String towner;
	private String tpercent;
	
	protected DBA22_PRG_ANALYZE_TABLE_DBMS_STAT(String tname, String towner, String tpercent) {
		super(3, "PRG_ANALYZE_TABLE_DBMS_STAT","dg_utilities");
		if(tname!=null) {
			this.tname=tname.replace("'", "").trim();
		}
		if(towner!=null) {
			this.towner=towner.replace("'", "").trim();
		}
		if(tpercent!=null) {
			this.tpercent=tpercent.replace("'", "").trim();
		}
	}

	public DBA22_PRG_ANALYZE_TABLE_DBMS_STAT() {
		this(null,null,null);
	}

	@Override
	public PR_ANALYZE_Greenplum toNewFunction(File file) {
		LoaderSchemaMap loader=new LoaderSchemaMap();
		String newSchemaObject=loader.loadMap().get(towner.toUpperCase()+"."+tname.toUpperCase());
		String schema, objectName;
		if(newSchemaObject==null){
			Logger.error("object "+towner.toUpperCase()+"."+tname.toUpperCase()+" not found in schema replace mapping");
			schema=this.towner;
			objectName=this.tname;
		}else {
			schema=newSchemaObject.substring(0, newSchemaObject.indexOf(".")).toLowerCase();
			objectName=newSchemaObject.substring(newSchemaObject.indexOf(".")+1).toUpperCase();
		}
		return new PR_ANALYZE_Greenplum(schema, objectName);
	}


	@Override
	public Abstract_PRG_ANALYZE getIstance(String call) throws IllegalArgumentException {
		String[] parsedCall=super.parse(call);
		DBA22_PRG_ANALYZE_TABLE_DBMS_STAT newObject=new DBA22_PRG_ANALYZE_TABLE_DBMS_STAT(parsedCall[2],parsedCall[3],parsedCall[4]);
		return newObject;
	}

}
