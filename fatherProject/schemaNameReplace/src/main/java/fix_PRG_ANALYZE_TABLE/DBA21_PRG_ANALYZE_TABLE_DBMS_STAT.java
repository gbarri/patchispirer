package fix_PRG_ANALYZE_TABLE;

import java.io.File;

import org.tinylog.Logger;

import schemaMapping.LoaderSchemaMap;
import utilities.SQLSearcher;


public class DBA21_PRG_ANALYZE_TABLE_DBMS_STAT extends Abstract_PRG_ANALYZE {
	private static String oracleOwner="DBA21";
	private String tname;	
	
	protected DBA21_PRG_ANALYZE_TABLE_DBMS_STAT(String tname) {
		super(1, "PRG_ANALYZE_TABLE_DBMS_STAT", "dg_utilities");
		if(tname!=null) {
			this.tname=tname.replace("'", "").trim();
		}
	}

	public DBA21_PRG_ANALYZE_TABLE_DBMS_STAT() {
		this(null);
	}

	@Override
	public PR_ANALYZE_Greenplum toNewFunction(File file) {
		LoaderSchemaMap loader=new LoaderSchemaMap();
		String schema, objectName;
		String newSchemaObject=loader.loadMap().get(oracleOwner+"."+this.tname.toUpperCase());
		if(newSchemaObject==null) {
			Logger.error("object "+oracleOwner+"."+tname.toUpperCase()+" not found in schema replace mapping");
			schema=SQLSearcher.detectSchema(file);
			objectName=this.tname;
		}else {
			schema=newSchemaObject.substring(0, newSchemaObject.indexOf(".")).toLowerCase();
			objectName=newSchemaObject.substring(newSchemaObject.indexOf(".")+1).toUpperCase();
		}
		return new PR_ANALYZE_Greenplum(schema, objectName);
	}

	@Override
	public Abstract_PRG_ANALYZE getIstance(String call) throws IllegalArgumentException {
		String[] parsedCall=super.parse(call);
		DBA21_PRG_ANALYZE_TABLE_DBMS_STAT newObject=new DBA21_PRG_ANALYZE_TABLE_DBMS_STAT(parsedCall[2]);
		return newObject;
	}

}
