package fix_PRG_ANALYZE_TABLE;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utilities.Initializer;
import utilities.SQLSearcher;
import utilities.StringUtilities;

public class Fix_PRG_ANALYZE_TABLE extends SimpleFileVisitor<Path>{

		static Logger logger=LoggerFactory.getLogger(Fix_PRG_ANALYZE_TABLE.class);
		private List<Integer> ignore;
		private Scanner scann; //scanner che legge text
		private String text;
		private Stack<Integer> commentIndex;
		private Stack<String>  commentText;
		private Abstract_PRG_ANALYZE functionType;
		private File file;
		private static String TAGPre="--<|DEV_COMMENT_PR%_ANALYZE%|_pre>";
		private static String TAGPost="--<|DEV_COMMENT_PR%_ANALYZE%|_post>";
		private int newPost;
		
		public Fix_PRG_ANALYZE_TABLE(Abstract_PRG_ANALYZE function) {
			this.functionType=function;
		}
		
		/**
		 * aggiunge una riga di analisi su richiesta
		 * @return
		 */
		private boolean askForNewLine() {
			if(scann.hasNextLine()) {
				text=initLine(text+=scann.nextLine()+"\n");
				return true;
			}else {
				return false;
			}
		}
		
		/**
		 * only modify files for which checkFile is true
		 * @param file
		 * @return
		 */
		private boolean checkFile(File file) {
			boolean cond=file.getName().endsWith(".sql");
			if(cond) {
				String objectName=SQLSearcher.detectName(file);
				if(objectName==null || (objectName.toLowerCase().equals((functionType.getMySchema()+"."+functionType.getName()).toLowerCase()))) {
					cond=false;
				}
			}
			
			return cond;
		}
		
		/**
		 * only modify lines for which checkLine is true
		 * @param line
		 * @return
		 */
		private boolean checkLine(String line) {
			return !line.trim().startsWith("--");
		}
		
		
		
		public String alterCall(String line, int[] extrema, Abstract_PRG_ANALYZE function) {
			String alteredCall=alterFunctionCall(function, line.substring(extrema[0], extrema[1]+1));
			if(alteredCall.equals(line.substring(extrema[0], extrema[1]+1))) {
				newPost=extrema[0]+"dg_utilities.PR_ANALYZE".length();
				return line;
			}else {
				String oldLine="--"+line.replace("\n", "\n--");
				if(oldLine.endsWith("--")) {
					oldLine=oldLine.substring(0, oldLine.lastIndexOf("--"));
				}
				if(oldLine.endsWith("\r\n")) {
					oldLine=oldLine.substring(0, oldLine.lastIndexOf("\r\n"));
				}
				String endLine=line.substring(extrema[1]+1);
				newPost=(TAGPre+"\r\n"+oldLine+"\r\n"+line.substring(0,extrema[0])).length();
				String newLine=TAGPre+"\r\n"+oldLine+"\r\n"+line.substring(0,extrema[0])+alteredCall+endLine+(endLine.endsWith("\n")?"":"\r\n")+TAGPost+"\r\n";
				logger.trace("replacing:\n"+line+"\n"+newLine);
				return newLine;
			}
			/*List<Integer> updateIgnore=new ArrayList<>();
			for(Integer i:this.ignore) {
				int newI=i;
				if(i>extrema[0]) {
					newI+="NULLIF(".length();
				}
				if(i>extrema[1]) {
					newI+=",'')".length();
				}
				updateIgnore.add(newI);
			}
			this.ignoreTRIM=updateIgnore;
			Stack<Integer> newCommentIndex=new Stack<>();
			for(int i=0; i<commentIndex.size(); i++) {
				Integer el=commentIndex.get(i);
				if(el>extrema[0]) {
					el+="NULLIF(".length();
				}
				if(el>extrema[1]) {
					el+=",'')".length();
				}
				newCommentIndex.add(newCommentIndex.size(), el);
			}
			this.commentIndex=newCommentIndex;*/
		}

		
		/**
		 * selezione esattamente il blocco della prima chiamata alla function objectFun dopo il carattere numero post
		 * va in errore se non contiene l'oggetto objectFun
		 * @return
		 */
		private int[] detectFunctionCallSubString(Abstract_PRG_ANALYZE function, int post) {
			int[] outExtrema=new int[2];
			String objectFun=function.getName();
			int start=text.indexOf(objectFun, post);
			if(start==-1) {
				//no objectFunction found
				return null;
			}
			//search for the end
			int end=text.indexOf("(",start+objectFun.length());
			int count=end==-1?0:1;
			while(count==0) {
				if(askForNewLine()) {
					end=text.indexOf("(",start+objectFun.length());
					count=end==-1?0:1;
				}else {
					break;
				}
			}
			if(count==0) {
				//there is a mistake
				return null;
			}
			if(start+objectFun.length()<end && !Pattern.matches("\\s*", text.substring(start+objectFun.length(), end))) {
				//it is a mismatch, not a call to trim function! Redo starting from after this point
				return detectFunctionCallSubString(function, start+objectFun.length());
			}
			if(text.charAt(start-1)=='.') {
				int startSchema=start;
				while(startSchema>=1) {
					if(Character.isWhitespace(text.charAt(startSchema-1))) {
						start=startSchema;
						break;
					}
					startSchema--;
				}
			}
			while(count!=0 ) {
				int val=1;
				if(end<text.length()-1){
					Character c=text.charAt(++end);
					if(c=='\'') {
						//assuming val assumes either 0 or 1 values, when encountering \' switch val between 0 and 1.
						val=val==1?0:1;
					}
					count+=(c==')'?-val:(c=='('?val:0));
				}else if(!askForNewLine()) {
					break;
				}
			}if(count!=0) {
				//there is a mistake
				return null;
			}
			outExtrema[0]=start;
			outExtrema[1]=end;
			return outExtrema;
		}
		
		private String initLine(String nextLine) {
			/*if(this.commentIndex==null) {
				this.commentIndex=new Stack<>();
			}
			if(this.commentText==null) {
				this.commentText=new Stack<>();
			}
			//remove --comments
			while(nextLine.indexOf("\\-\\-")!=-1) {
				int ind=nextLine.indexOf("\\-\\-");
				int ind2=nextLine.indexOf("\r\n", ind+2);
				commentIndex.push(ind);
				commentText.push(nextLine.substring(ind,ind2+1));
				nextLine=nextLine.replaceFirst("\\-\\-.*", "\r\n");
			}*/
			//remove \\/ * * \\/ comments
//			while(nextLine.indexOf("/*")!=-1) {
//				int ind=nextLine.indexOf("/*");
//				int ind2=nextLine.indexOf("*/", ind+2);
//				while(ind2==-1) {
//					nextLine+=scann.nextLine()+"\r\n";
//					ind2=nextLine.indexOf("*/", ind+2);
//				}
//				commentIndex.push(ind);
//				commentText.push(nextLine.substring(ind,ind2+2));
////				String f=nextLine.substring(0,ind);
////				String s=nextLine.substring(ind2+2);
//				nextLine=nextLine.substring(0,ind)+" "+nextLine.substring(ind2+2);
//			}
			return nextLine;
		}
		
		private String deinitLine(String text) {
//			while(!commentIndex.isEmpty()) {
//				Integer index=commentIndex.pop();
//				String comment=commentText.pop();
//				String firstPart=text.substring(0, index);
//				String secondPart=text.substring(index+1);
//				text=firstPart+comment+secondPart;
//			}
			return text;
		}
	
	private String replaceFunctionCall(String nextLine, Abstract_PRG_ANALYZE function) {
		this.text=null;
		int indFunction=0;
		this.ignore=new ArrayList<>();
		this.commentIndex=new Stack<>();
		this.commentText=new Stack<>();
		//initialize text
		this.text=initLine(nextLine);
		while(indFunction<this.text.length()) {
			int[] extrema=detectFunctionCallSubString(function, indFunction);
			if(extrema==null) {
				//did not found any reference to function
				indFunction=this.text.length();
			}else {
				//found a call to function
				//modify
				this.text=alterCall(this.text, extrema, function);
				//advance index
				//TODO da sistemare
				indFunction=newPost+"PR_ANALYZE".length()+"dg_utilities".length(); //TODO ?? una cosa così?
			}
		}
		//text is now completely replaced
		this.text=deinitLine(this.text);
		//System.out.println(this.text);
		nextLine=this.text;
		return nextLine;
	}
	
	/**
	 * il metodo deve prendere una sottostringa ritenuta a tutti gli effetti una chiamata a function
	 * parsarla, costruire un oggetto della classe origine, da questa ottenere un oggetto della classe nuova,
	 * inserire una chiamata alla classe nuova nel codice
	 * @param function
	 * @param extrema
	 * @return
	 */
	private String alterFunctionCall(Abstract_PRG_ANALYZE function, String callToFunction) {
		String newCall=null;
		if(function.checkCall(callToFunction)) {
			Abstract_PRG_ANALYZE oldFunction=function.getIstance(callToFunction);
			newCall=oldFunction.toNewFunction(this.file).getCallAt();
		}else {
			logger.error("the substring "+callToFunction+" is not a valid call to function "+function.getClass());
			newCall=callToFunction;
		}
		return newCall;
	}

		@Override
		public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException{
			logger.trace("visiting "+path.toString());
			this.file=path.toFile();
			if(checkFile(file)) {
				String textFile="";
				try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
					Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
					this.scann=scann;
					while(scann.hasNextLine()) {
						String nextLine=scann.nextLine()+"\r\n";
						if(checkLine(nextLine)) {
							nextLine=replaceFunctionCall(nextLine, this.functionType);
						}
						textFile+=nextLine;					
					}
				}catch (IOException e) {
					throw new RuntimeException("Exception raised while reading for\t"+file.getAbsolutePath(), e);
				}
				
				try(FileOutputStream fileOutStream=new FileOutputStream(file.getAbsolutePath()); 
						PrintWriter print=new PrintWriter(new BufferedOutputStream(fileOutStream))){
					print.print(textFile);
				}catch (IOException e) {
					throw new RuntimeException("Exception raised while writing\n\t"+file.getAbsolutePath(), e);
				}
			}
			return FileVisitResult.CONTINUE;
		}
		

		public static void main(String[] args) throws IOException {
			Path path=new File(Initializer.getProperty("workingDir")).toPath();
			Abstract_PRG_ANALYZE[] functions= {new PR_ANALYZE_TABLE(), new PRG_ANALYZE_TABLE(), new PRG_ANALYZE_TABLE_ST(),
											   new DBA21_PRG_ANALYZE_TABLE_DBMS_STAT(), new DBA22_PRG_ANALYZE_TABLE_DBMS_STAT()};
			for(Abstract_PRG_ANALYZE function: functions) {
				logger.trace("Starting replacing "+function.getClass()+" calls");
				Fix_PRG_ANALYZE_TABLE fix= new Fix_PRG_ANALYZE_TABLE(function);
				Files.walkFileTree(path,fix);
				logger.trace("Ended replacing "+function.getClass()+" calls");
			}
		}

}
