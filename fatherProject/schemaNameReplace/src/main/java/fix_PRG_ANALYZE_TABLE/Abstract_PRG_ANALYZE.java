package fix_PRG_ANALYZE_TABLE;

import java.io.File;

public abstract class Abstract_PRG_ANALYZE {
	public int numArguments;
	public String name;
	public String mySchema;
	protected Abstract_PRG_ANALYZE(int numArguments, String name, String mySchema) {
		this.numArguments = numArguments;
		this.name = name.trim();
		this.mySchema=mySchema.trim();
	}
	public int getNumArguments() {
		return numArguments;
	}
	public void setNumArguments(int numArguments) {
		this.numArguments = numArguments;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMySchema() {
		return mySchema;
	}
	public void setMySchema(String mySchema) {
		this.mySchema = mySchema;
	}
	protected String[] parse(String call) {
		call=call.trim();
		String schema;
		String nameObject;
		String[] args;
		if(call!= null && call.indexOf(".")!=-1) {
			schema=call.substring(0, call.indexOf("."));
		}else {
			schema=null;
		}
		if(call!= null && call.indexOf("(")!=-1) {
			nameObject=call.substring(schema==null?0:call.indexOf(".")+1, call.indexOf("(")).trim();
		}else {
			throw new IllegalArgumentException("problemi con il nome oggetto. call: "+call);
		}
		if(call!= null && call.indexOf(")")!=-1) {
			String argument=call.substring(call.indexOf("(")+1,call.indexOf(")"));
			args=argument.split(",");
		}else {
			throw new IllegalArgumentException("problemi con il nome oggetto. call: "+call);
		}
		String[] out=new String[2+args.length];
		out[0]=schema;
		out[1]=nameObject;
		for(int i=2; i<out.length; i++) {
			out[i]=args[i-2];
		}
		return out;
	}
	
	public abstract PR_ANALYZE_Greenplum toNewFunction(File file);
	
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean numArgsCheck=parsedCall.length==this.numArguments+2;
		boolean nameCheck=parsedCall[1]!=null && parsedCall[1].trim().toUpperCase().equals(getName());
		boolean schemaCheck=parsedCall[0]!=null && parsedCall[0].trim().toUpperCase().equals(getMySchema().toUpperCase());
		return numArgsCheck && nameCheck && schemaCheck;
	}
	
	public abstract Abstract_PRG_ANALYZE getIstance(String call) throws IllegalArgumentException;
	
}
