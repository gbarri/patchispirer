package fix_PRG_ANALYZE_TABLE;

import java.io.File;

import org.tinylog.Logger;

import schemaMapping.LoaderSchemaMap;
import utilities.SQLSearcher;

public class PR_ANALYZE_TABLE extends Abstract_PRG_ANALYZE{
	private static String oracleOwner="DBA25";
	private String tname;
	private String method;

	protected PR_ANALYZE_TABLE(String tname, String method) {
		super(2, "PR_ANALYZE_TABLE","dg_utilities");
		if(tname!=null) {
			this.tname=tname.replace("'", "").trim();
		}
		if(method!=null) {
			this.method=method.replace("'", "").trim();
		}
	}

	public PR_ANALYZE_TABLE() {
		this(null,null);
	}

	@Override
	public PR_ANALYZE_Greenplum toNewFunction(File file) {
		LoaderSchemaMap loader=new LoaderSchemaMap();
		String schema, objectName;
		String newSchemaObject=loader.loadMap().get(oracleOwner+"."+this.tname.toUpperCase());
		if(newSchemaObject==null) {
			Logger.error("object "+oracleOwner+"."+tname.toUpperCase()+" not found in schema replace mapping");
			schema=SQLSearcher.detectSchema(file);
			objectName=this.tname;
		}else {
			schema=newSchemaObject.substring(0, newSchemaObject.indexOf(".")).toLowerCase();
			objectName=newSchemaObject.substring(newSchemaObject.indexOf(".")+1).toUpperCase();
		}
		return new PR_ANALYZE_Greenplum(schema, objectName);
	}

	@Override
	public Abstract_PRG_ANALYZE getIstance(String call) throws IllegalArgumentException {
		String[] parsedCall=super.parse(call);
		PR_ANALYZE_TABLE newObject=new PR_ANALYZE_TABLE(parsedCall[2],parsedCall[3]);
		return newObject;
	}

}
