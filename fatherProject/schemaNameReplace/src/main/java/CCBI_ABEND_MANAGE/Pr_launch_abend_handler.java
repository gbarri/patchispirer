package CCBI_ABEND_MANAGE;

import org.apache.commons.lang3.StringUtils;

import functionUtilities.AbstractFunctionCall;
import functionUtilities.CallAlteration;

public class Pr_launch_abend_handler extends AbstractFunctionCall {


	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "PR_LAUNCH_ABEND_HANDLER";
	}

	@Override
	public String getSchema() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getNumArgs() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public void accept(CallAlteration alterator) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean nameCheck=parsedCall[1]!=null && StringUtils.equalsIgnoreCase(parsedCall[1].trim(), getName());
		boolean numArgsCheck=parsedCall.length==getNumArgs()+2;
		return nameCheck && numArgsCheck;
	}

}
