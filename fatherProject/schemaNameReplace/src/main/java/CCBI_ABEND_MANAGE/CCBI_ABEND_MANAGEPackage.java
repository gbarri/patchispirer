package CCBI_ABEND_MANAGE;

import org.apache.commons.lang3.StringUtils;

import functionUtilities.*;

/**
 * former package oracle CCBI_ABEND_MANAGE, moved under dg_maintenance
 * @author g.barri
 *
 */
public class CCBI_ABEND_MANAGEPackage extends AbstractFunctionCall {

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "CCBI_ABEND_MANAGE";
	}

	@Override
	public String getSchema() {
		// TODO Auto-generated method stub
		return "dg_maintenance";
	}

	@Override
	public int getNumArgs() {
		// TODO Auto-generated method stub
		return -1;
	}

	@Override
	public void accept(CallAlteration alterator) {
		alterator.visit(this);
	}
	
	@Override
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean nameCheck=parsedCall[1]!=null && StringUtils.equalsIgnoreCase(parsedCall[1].trim(), getName());
		boolean schemaCheck=getSchema()==null || (parsedCall[0]!=null && StringUtils.equalsIgnoreCase(parsedCall[0].trim(), getSchema()));
		return nameCheck && schemaCheck;
	}

}
