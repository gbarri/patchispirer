package functionUtilities;

import org.apache.commons.lang3.StringUtils;

public class NVL2Coalesce extends SimplestNameReplace{

	public NVL2Coalesce(FunctionCall fun1, FunctionCall fun2) {
		super(fun1, fun2);
	}
	
	@Override
	public String alterCall(String str, int[] extrema, FunCallAlterer funCallAlterer) {
		FunctionCall fun1=super.getFun1();
		FunctionCall fun2=super.getFun2();
		int ind1=StringUtils.indexOfIgnoreCase(str, fun1.getName(), extrema[0]);
		int startSchema=extrema[0];
		if(startSchema-1>=0 && str.charAt(startSchema-1)=='.') {
			//vi è uno schema
			while(startSchema>=1) {
				if(!Character.isAlphabetic(str.charAt(startSchema-1)) && str.charAt(startSchema-1)!='_' && str.charAt(startSchema-1)!='$' && str.charAt(startSchema-1)!='@') {
					break;
				}
				startSchema--;
			}
		}
		String newStr=str.substring(0,  startSchema)+fun2.getName()+str.substring(ind1+fun1.getName().length());
		return newStr;
	}

}
