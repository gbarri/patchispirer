package functionUtilities;

import org.tinylog.Logger;

public class CommentWithTags extends WrapInComment {

	public String tag;
	
	public CommentWithTags(String tag) {
		this.tag=tag;
	}
	
	private String getPreTag() {
		return "--<|DEV_COMMENT_"+tag+"-pre|>";
	}
	
	private String getPostTag() {
		return "--<|DEV_COMMENT_"+tag+"-post|>";
	}
	
	@Override
	public String alterCall(String str, int[] extrema, FunCallAlterer funCallAlterer) {
		int lastBefore=str.substring(0, extrema[0]).lastIndexOf("\n");
		String old=str;
		if(lastBefore==-1) {
			str=getPreTag()+"\r\n"+str;
		}else {
			//funziona perchè una chiamata ad una funzione non parte/termina con un a capo
			str=str.substring(0,lastBefore+1)+getPreTag()+"\r\n"+str.substring(lastBefore+1);
		}
		extrema[0]=extrema[0]+(getPreTag()+"\r\n").length();
		extrema[1]=extrema[1]+(getPreTag()+"\r\n").length();
		lastBefore=str.substring(0, extrema[0]).lastIndexOf("\n");
		int firstAfter=str.indexOf("\n",extrema[1]+1);
		if(firstAfter==-1) {
			str=str+"\r\n"+getPostTag();
			firstAfter=str.indexOf("\n",extrema[1]+1);
		}else {
			str=str.substring(0,firstAfter+1)+getPostTag()+"\r\n"+str.substring(firstAfter+1);
		}
		String newStr=str.substring(0,lastBefore)+str.substring(lastBefore, extrema[1]+1).replace("\n", "\n--")
				+(extrema[1]+1<str.length()-1?str.substring(extrema[1]+1):"");
		Logger.trace("replaced\n"+old.replace("\r\n", "\\r\\n\r\n")+"\nwith\n"+newStr.replace("\r\n", "\\r\\n\r\n")+"\nEND");
		return newStr;
	}

//	public static void main(String[] args) {
//		String test="2blabla\r\nblabla\r\nblubla3";
//		CommentWithTags comm=new CommentWithTags("CCI_ABEND");
//		int[] extrema= {test.indexOf("2"), test.indexOf("3")};
//		System.out.println(comm.alterCall(test,extrema, null));
//	}
	
}
