package functionUtilities;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.StringUtil;

public class FunCallAlterer {
	public FunctionCall function;
	public CallAlteration alter;
	public Scanner scann;
	private Stack<Integer> commentIndex;
	private Stack<String>  commentText;
	private String text;
	//public String TAGinit;
	//public String TAGend;
	public FunCallAlterer(FunctionCall function, CallAlteration alter, Scanner scann) {
		super();
		this.function = function;
		this.alter = alter;
		this.scann = scann;
		this.text=null;
	}
	
	public FunCallAlterer(FunctionCall function, CallAlteration alter) {
		this(function, alter, new Scanner(new StringReader("")));		
	}
	
	public FunctionCall getFunction() {
		return function;
	}
	public void setFunction(FunctionCall function) {
		this.function = function;
	}
	public CallAlteration getAlter() {
		return alter;
	}
	public void setAlter(CallAlteration alter) {
		this.alter = alter;
	}
	public Scanner getScann() {
		return scann;
	}
	public void setScann(Scanner scann) {
		this.scann = scann;
	}
	
	/**
	 * aggiunge una riga di analisi su richiesta
	 * @return
	 */
	private boolean askForNewLine() {
		if(this.text==null) {
			this.text="";
		}
		if(scann.hasNextLine()) {
			text=initLine(text+scann.nextLine()+"\r\n");
			return true;
		}else {
			return false;
		}
	}
		
	private String initLine(String nextLine) {
		if(this.commentIndex==null) {
			this.commentIndex=new Stack<>();
		}
		if(this.commentText==null) {
			this.commentText=new Stack<>();
		}
		int indexOfLineComment=0, indexOfBlockComment=0;
		indexOfLineComment=nextLine.indexOf("--");
		indexOfBlockComment=nextLine.indexOf("/*");
		while(indexOfLineComment!=-1 || indexOfBlockComment!=-1) {
			if(indexOfLineComment==-1) {
				indexOfLineComment=nextLine.length()+1;
			}
			if(indexOfBlockComment==-1) {
				indexOfBlockComment=nextLine.length()+1;
			}
			if(indexOfLineComment<indexOfBlockComment) {
				int ind2=nextLine.indexOf("\r\n", indexOfLineComment+1);
				if(ind2==-1) {
//					System.out.println(nextLine.replace("\r\n", "\\r\\n\r\n"));
					ind2=nextLine.indexOf("\n", indexOfLineComment+1);
				}
				if(ind2==-1) {
					ind2=nextLine.length();
				}
//				System.out.println(ind2);
				commentIndex.push(indexOfLineComment);
				commentText.push(nextLine.substring(indexOfLineComment,ind2));
				nextLine=nextLine.replaceFirst("--.*", "");
			}else if(indexOfBlockComment<indexOfLineComment) {
				int ind2=nextLine.indexOf("*/", indexOfBlockComment+2);
				while(ind2==-1) {
					nextLine+=scann.nextLine()+"\n";
					ind2=nextLine.indexOf("*/", indexOfBlockComment+2);
				}
				commentIndex.push(indexOfBlockComment);
				commentText.push(nextLine.substring(indexOfBlockComment,ind2+2));
				nextLine=nextLine.substring(0,indexOfBlockComment)+""+nextLine.substring(ind2+2);
			}
			indexOfLineComment=nextLine.indexOf("--");
			indexOfBlockComment=nextLine.indexOf("/*");
		}
//		//remove --comments
//		while(nextLine.indexOf("--")!=-1) {
//			int ind=nextLine.indexOf("--");
////			System.out.println(ind);
//			int ind2=nextLine.indexOf("\r\n", ind+1);
//			if(ind2==-1) {
////				System.out.println(nextLine.replace("\r\n", "\\r\\n\r\n"));
//				ind2=nextLine.indexOf("\n", ind+1);
//			}
//			if(ind2==-1) {
//				ind2=nextLine.length();
//			}
////			System.out.println(ind2);
//			commentIndex.push(ind);
//			commentText.push(nextLine.substring(ind,ind2));
//			nextLine=nextLine.replaceFirst("--.*", "");
//		}
//		//remove \\/ * * \\/ comments
//		while(nextLine.indexOf("/*")!=-1) {
//			int ind=nextLine.indexOf("/*");
//			int ind2=nextLine.indexOf("*/", ind+2);
//			while(ind2==-1) {
//				nextLine+=scann.nextLine()+"\n";
//				ind2=nextLine.indexOf("*/", ind+2);
//			}
//			commentIndex.push(ind);
//			commentText.push(nextLine.substring(ind,ind2+2));
//			nextLine=nextLine.substring(0,ind)+""+nextLine.substring(ind2+2);
//		}
		return nextLine;
	}
	
	private String deinitLine(String text) {
		int[] tmp={};
		return deinitLine(text, tmp);
	}
	
	private String deinitLine(String text, int[] extrema) {
		while(!commentIndex.isEmpty()) {
			Integer index=commentIndex.pop();
			String comment=commentText.pop();
			for(int i=0; i<extrema.length; i++) {
				if(extrema[i]>=index) {
					extrema[i]=extrema[i]+comment.length();
				}
			}
			String firstPart=text.substring(0, index);
			String secondPart=text.substring(index);
			text=firstPart+comment+secondPart;
		}
		return text;
	}
	
	private int firstDiff(String text2, String alteredText) {
		int i;
		for(i=0; i<Math.min(text2.length(), alteredText.length()); i++) {
			if(text2.charAt(i)!=alteredText.charAt(i)) {
				break;
			}
		}
		return i; 
	}
	
	/**
	 * seleziona in text le invocazioni di una function mediante la ricerca del suo nome, contando le parentesi (, )
	 * Se la funzione è invocata con uno schema davanti, è incluso nell'invocazione individuata.
	 * @param post
	 * @return
	 */
	int[] detectFunctionCallSubString(int post) {
		int[] outExtrema=new int[2];
		String objectFun=function.getName();
		int start= StringUtils.indexOfIgnoreCase(text, objectFun, post);
		if(start==-1) {
			//no objectFunction found
			return null;
		}
		//search for the end
		int end=text.indexOf("(",start+objectFun.length());
		int count=end==-1?0:1;
		while(count==0) {
			if(askForNewLine()) {
				end=text.indexOf("(",start+objectFun.length());
				count=end==-1?0:1;
			}else {
				break;
			}
		}
		if(count==0) {
			//there is a mistake
			return null;
		}
		if(start+objectFun.length()<end && !Pattern.matches("\\s*", text.substring(start+objectFun.length(), end))) {
			//it is a mismatch, not a call to trim function! Redo starting from after this point
			return detectFunctionCallSubString(start+objectFun.length());
		}
		
//		if(start-1>=0 && text.charAt(start-1)=='.') {
//			int startSchema=start;
//			while(startSchema>=1) {
//				//if(Character.isWhitespace(text.charAt(startSchema-1))) {
//				if(!Character.isLetterOrDigit(text.charAt(startSchema-1)) && text.charAt(startSchema-1)!='_' && text.charAt(startSchema-1)!='$' && text.charAt(startSchema-1)!='@' && text.charAt(startSchema-1)!='.') {
//					start=startSchema;
//					break;
//				}
//				startSchema--;
//			}
//			start=startSchema;
//		}
		if(start-1>=0) {
			if(start-1>=0 && text.charAt(start-1)=='.') {
				int startSchema=start;
				while(startSchema>=1) {
					//if(Character.isWhitespace(text.charAt(startSchema-1))) {
					if(!Character.isLetterOrDigit(text.charAt(startSchema-1)) && text.charAt(startSchema-1)!='_' && text.charAt(startSchema-1)!='$' && text.charAt(startSchema-1)!='@' && text.charAt(startSchema-1)!='.') {
						start=startSchema;
						break;
					}
					startSchema--;
				}
				start=startSchema;
			}else {
				if(Character.isLetterOrDigit(text.charAt(start-1)) || text.charAt(start-1)=='_' || text.charAt(start-1)=='$' || text.charAt(start-1)=='@') {
					//the found instance of the name is in reality just the ending of another name.
					//ignored
					return null;
				}
			}
		}

		while(count!=0 ) {
			int val=1;
			if(end<text.length()-1){
				Character c=text.charAt(++end);
				if(c=='\'') {
					//assuming val assumes either 0 or 1 values, when encountering \' switch val between 0 and 1.
					val=val==1?0:1;
				}
				count+=(c==')'?-val:(c=='('?val:0));
			}else if(!askForNewLine()) {
				break;
			}
		}if(count!=0) {
			//there is a mistake
			return null;
		}
		outExtrema[0]=start;
		outExtrema[1]=end;
		return outExtrema;
	}
	
	private String alterLine() {
		int indFunction=0;
		//initialize text
		while(indFunction<this.text.length()) {
			int[] extrema=detectFunctionCallSubString(indFunction);
			if(extrema==null) {
				//did not found any reference to function
				indFunction=this.text.length();
			}else {
				//found a call to function
				if(function.checkCall(text.substring(extrema[0],extrema[1]+1))) {	
					//restore comment & update extrema indexes
					String tmpStr=deinitLine(this.text, extrema);
					//modify text
					tmpStr=this.alter.alterCall(tmpStr, extrema, this);
					//remove comments
					this.commentIndex=null;
					this.commentText=null;
					tmpStr=initLine(tmpStr);
					//compare 
					indFunction=firstDiff(this.text, tmpStr);
					//update this.text
					this.text=tmpStr;
				}else {
					int indexParenthesis=text.indexOf("(",extrema[0]);
					indFunction=indexParenthesis;
				}
			}
		}
		//text is now completely replaced
		//System.out.println(this.text);
		String nextLine=deinitLine(this.text);
		return nextLine;
	}
	
	public String alterFunctionCalls() {
		String fullText="";
		this.text=null;
		while(askForNewLine()) {
			if(checkLine(this.text)) {
				this.text=alterLine();
			}
			fullText+=text;
			this.text=null;
		}
		return fullText;
	}
	
	private boolean checkLine(String text2) {
		return !text2.trim().startsWith("--");
	}
	
//	public static void main(String[] args) throws FileNotFoundException {
////		Scanner scann=new Scanner(new FileReader(new File("C:\\Users\\g.barri\\Desktop\\tmp\\tl_sal_wvn_mp_wt_immat_ita.sql")));
////		CallAlteration callAlter=new WrapInComment();
////		FunctionCall funCall=new Pr_check_err_logFunctionCall();
////		FunCallAlterer alter=new FunCallAlterer(funCall, callAlter, scann);
////		String newText=alter.alterFunctionCalls();
////		System.out.println(newText);
////		scann.close();
////		scann=new Scanner(new FileReader(new File("C:\\Users\\g.barri\\Desktop\\tmp\\tl_sal_wvn_mp_wt_immat_ita.sql")));
////		FunctionCall nvl=new NVL();
////		FunctionCall coalesce=new COALESCE();
////		CallAlteration callAlter2=new NVL2Coalesce(nvl, coalesce);
////		FunCallAlterer alter2=new FunCallAlterer(nvl, callAlter2, scann);
////		String newText2=alter2.alterFunctionCalls();
////		System.out.println(newText2);
////		scann.close();
//	}
	
}
