package functionUtilities;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import CCBI_ABEND_MANAGE.CCBI_ABEND_MANAGEPackage;
import CCBI_ABEND_MANAGE.PR_ABEND_HANDLER;
import CCBI_ABEND_MANAGE.Pr_launch_abend_handler;
import prgAnalyzeTable.GenericPrgAnalyzeTable;
import prgAnalyzeTable.GenericPrgAnalyzeTableDbmsStat;
import prgAnalyzeTable.PRG_ANALYZE_TABLE_DM;
import prgAnalyzeTable.PRG_ANALYZE_TABLE_ST;
import prgAnalyzeTable.PRG_ANALYZE_TABLE_WITH_IDX;
import prgAnalyzeTable.PR_ANALYZE_TABLE;
import prgAnalyzeTable.ReplaceWithPrAnalyze;
import prgTruncateTable.GenericPrgTruncateTable;
import prgTruncateTable.ReplaceWithPrtruncate;
import utilities.Initializer;
import utilities.SQLSearcher;

public class FilesCyclerFunCallAlterer extends SimpleFileVisitor<Path>{
	static Logger logger=LoggerFactory.getLogger(FilesCyclerFunCallAlterer.class);
	List<FunCallAlterer> alterers;
	
	public FilesCyclerFunCallAlterer(List<FunCallAlterer> alterer) {
		super();
		this.alterers = alterer;
	}

	@Override
	public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException{
		logger.trace("Visiting file "+path.toAbsolutePath().toString());
		File file=path.toFile();
		if(checkFile(file)) {
			String textFile=null;
			Scanner scann=null;
			try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
				Scanner scannFromFile=new Scanner(new BufferedInputStream(fileStream))){
				scann=scannFromFile;
				for(FunCallAlterer funCurrent: this.alterers) {
					funCurrent.setScann(scann);
					textFile=funCurrent.alterFunctionCalls();
					scann=new Scanner(new StringReader(textFile));
				}
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while reading for\t"+file.getAbsolutePath(), e);
			}
			scann.close();
			
			try(FileOutputStream fileOutStream=new FileOutputStream(file.getAbsolutePath()); 
					PrintWriter print=new PrintWriter(new BufferedOutputStream(fileOutStream))){
				print.print(textFile);
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while writing\n\t"+file.getAbsolutePath(), e);
			}
		}
		return FileVisitResult.CONTINUE;
	}

	private boolean checkFile(File file) {
		boolean cond=file.getName().endsWith(".sql");
		if(cond) {
			String objectName=SQLSearcher.detectName(file);
			if(objectName==null) {
				cond=false;
			}
		}
		return cond;
	}
	
	public static void main(String[] args) throws IOException {
		logger.trace("Starting FunCallAltererCycle");
		CallAlteration comment=new WrapInComment();
		FunctionCall pr_check_err_log=new Pr_check_err_logFunctionCall();
		FunCallAlterer commentPr_check_err_log=new FunCallAlterer(pr_check_err_log, comment);
		//
		CallAlteration comment2=new WrapInComment();
		FunctionCall PRG_CHANGE_OPTIMIZER=new PRG_CHANGE_OPTIMIZER();
		FunCallAlterer commentPRG_CHANGE_OPTIMIZER=new FunCallAlterer(PRG_CHANGE_OPTIMIZER, comment2);
		//
		FunctionCall pr_check_err_log_default=new PR_check_err_log3ParamFunctionCall();
		FunCallAlterer commentPr_check_err_log_default=new FunCallAlterer(pr_check_err_log_default, comment);
		//
		FunctionCall nvl=new NVL();
		FunctionCall coalesce=new COALESCE();
		CallAlteration nvl2coalesce=new NVL2Coalesce(nvl, coalesce);
		FunCallAlterer replaceNVLwithCOAELESCE=new FunCallAlterer(nvl, nvl2coalesce);
		//
		GenericPrgTruncateTable genTrunc1=new GenericPrgTruncateTable();
		ReplaceWithPrtruncate repTrunc=new ReplaceWithPrtruncate(genTrunc1);
		FunCallAlterer replaceTruncateTable=new FunCallAlterer(genTrunc1, repTrunc);
		//
		GenericPrgAnalyzeTable genAnalyzeTab=new GenericPrgAnalyzeTable();
		ReplaceWithPrAnalyze repAnalyze=new ReplaceWithPrAnalyze(genAnalyzeTab);
		FunCallAlterer replaceAnalyze1=new FunCallAlterer(genAnalyzeTab, repAnalyze);
		GenericPrgAnalyzeTableDbmsStat genAnalyzeDbmsTab=new GenericPrgAnalyzeTableDbmsStat();
		ReplaceWithPrAnalyze repAnalyze2=new ReplaceWithPrAnalyze(genAnalyzeDbmsTab);
		FunCallAlterer replaceAnalyze2=new FunCallAlterer(genAnalyzeDbmsTab, repAnalyze2);
		PR_ANALYZE_TABLE prAnalyzeTable=new PR_ANALYZE_TABLE();
		ReplaceWithPrAnalyze repAnalyze3=new ReplaceWithPrAnalyze(prAnalyzeTable);
		FunCallAlterer replaceAnalyze3=new FunCallAlterer(prAnalyzeTable, repAnalyze3);
		PRG_ANALYZE_TABLE_ST prgAnalyzeTableSt=new PRG_ANALYZE_TABLE_ST();
		ReplaceWithPrAnalyze repAnalyze4=new ReplaceWithPrAnalyze(prgAnalyzeTableSt);
		FunCallAlterer replaceAnalyze4=new FunCallAlterer(prgAnalyzeTableSt, repAnalyze4);
		PRG_ANALYZE_TABLE_DM prgAnalyzeTableDm=new PRG_ANALYZE_TABLE_DM();
		ReplaceWithPrAnalyze repAnalyze5=new ReplaceWithPrAnalyze(prgAnalyzeTableDm);
		FunCallAlterer replaceAnalyze5=new FunCallAlterer(prgAnalyzeTableDm, repAnalyze5);
		PRG_ANALYZE_TABLE_WITH_IDX prgAnalyzeTableWithIdx=new PRG_ANALYZE_TABLE_WITH_IDX();
		ReplaceWithPrAnalyze repAnalyze6=new ReplaceWithPrAnalyze(prgAnalyzeTableWithIdx);
		FunCallAlterer replaceAnalyze6=new FunCallAlterer(prgAnalyzeTableWithIdx, repAnalyze6);
		//
		Decode decode=new Decode();
		ReplaceDecodeWithCase rep=new ReplaceDecodeWithCase();
		FunCallAlterer replaceDecodeWithCase=new FunCallAlterer(decode, rep);
		//
		NVL2 nvl2=new NVL2();
		ReplaceNVL2WithCase replaceNVL2=new ReplaceNVL2WithCase();
		FunCallAlterer replaceNVL2withCase=new FunCallAlterer(nvl2, replaceNVL2);
		//
		SubstrFunction substr=new CustomSubtrFunction();
		ReplaceSubstrWithOracompatSubstr replace=new ReplaceSubstrWithOracompatSubstr(substr);
		FunCallAlterer replaceSubstrWithOracompatSubstr=new FunCallAlterer(substr, replace);
		SubstringFunction substring=new CustomSubstringFunction();
		ReplaceSubstringWithOracompatSubstr replaceSubstring=new ReplaceSubstringWithOracompatSubstr(substring);
		FunCallAlterer replaceSubstringWithOracompatSubstr=new FunCallAlterer(substring, replaceSubstring);
		//
		GreatestGreenplum greatestGreen= new GreatestGreenplum();
		SWF_Greatest sfwGreatest=new SWF_Greatest();
		FunCallAlterer replaceSWF_Greatest=new FunCallAlterer(sfwGreatest, new SimplestNameReplace(sfwGreatest, greatestGreen));
		//
		CCBI_ABEND_MANAGEPackage cci=new CCBI_ABEND_MANAGEPackage();
		WrapInComment commenter=new CommentWithTags("CCBI_ABEND_MANAGE_COMMENT");
		FunCallAlterer commentCallsToCCBI_ABEND_MANAGE=new FunCallAlterer(cci, commenter);
		PR_ABEND_HANDLER pr_hand_fromCCBI_ABEND_MANAGE=new PR_ABEND_HANDLER();
		FunCallAlterer commentPR_ABEND_HANDLER=new FunCallAlterer(pr_hand_fromCCBI_ABEND_MANAGE, commenter);
		Pr_launch_abend_handler rr_launch_abend_handler=new Pr_launch_abend_handler();
		FunCallAlterer commentPr_launch_abend_handler=new FunCallAlterer(rr_launch_abend_handler, commenter);
		//
		PxfLocationFunction pxfLocation=new PxfLocationFunction();
		ReplacePxfLocation replacePxf=new ReplacePxfLocation();
		FunCallAlterer fixPxfCalls=new FunCallAlterer(pxfLocation, replacePxf);
		//
		List<FunCallAlterer> modifications= new ArrayList<FunCallAlterer>();
		String doCommentPrCheckErrLog=Initializer.getProperty("doCommentPrCheckErrLog", true);
		if((doCommentPrCheckErrLog!=null && Boolean.parseBoolean(doCommentPrCheckErrLog)) || doCommentPrCheckErrLog==null) {
			logger.trace("Commenting PR_check_err_log function calls");
			modifications.add(commentPr_check_err_log);
			modifications.add(commentPr_check_err_log_default);
    	}
		String doReplaceNVLwithCOAELESCE=Initializer.getProperty("doReplaceNVLwithCOAELESCE", true);
		if((doReplaceNVLwithCOAELESCE!=null && Boolean.parseBoolean(doReplaceNVLwithCOAELESCE)) || doReplaceNVLwithCOAELESCE==null) {
			logger.trace("Replacing NLV function with COALESCE");
			modifications.add(replaceNVLwithCOAELESCE);
		}
		String doReplaceDecodewithCase=Initializer.getProperty("doReplaceDecodewithCase", true);
		if((doReplaceDecodewithCase!=null && Boolean.parseBoolean(doReplaceDecodewithCase)) || doReplaceDecodewithCase==null) {
			logger.trace("Replacing DECODE function with a case when construct");
			modifications.add(replaceDecodeWithCase);
		}
		String doReplaceTruncateAndAnalyze=Initializer.getProperty("doReplaceTruncateAndAnalyze", true);
		if((doReplaceTruncateAndAnalyze!=null && Boolean.parseBoolean(doReplaceTruncateAndAnalyze)) || doReplaceTruncateAndAnalyze==null) {
			logger.trace("Replacing truncate tables and afterward analyze functions with calls to PR_TRUNCATE and PR_ANALYZE");
			modifications.add(replaceTruncateTable);
			modifications.add(replaceAnalyze1);
			modifications.add(replaceAnalyze2);
			modifications.add(replaceAnalyze3);
			modifications.add(replaceAnalyze4);
			modifications.add(replaceAnalyze5);
			modifications.add(replaceAnalyze6);
		}
		String doCommentPRG_CHANGE_OPTIMIZER=Initializer.getProperty("doCommentPRG_CHANGE_OPTIMIZER", true);
		if((doCommentPRG_CHANGE_OPTIMIZER!=null && Boolean.parseBoolean(doCommentPRG_CHANGE_OPTIMIZER)) || doCommentPRG_CHANGE_OPTIMIZER==null) {
			logger.trace("Commenting PRG_CHANGE_OPTIMIZER function calls");
			modifications.add(commentPRG_CHANGE_OPTIMIZER);
    	}
		String doReplaceNVL2WithCase=Initializer.getProperty("doReplaceNVL2WithCase", true);
		if((doReplaceNVL2WithCase!=null && Boolean.parseBoolean(doReplaceNVL2WithCase)) || doReplaceNVL2WithCase==null) {
			logger.trace("Replacing NVL2 function calls with a case when");
			modifications.add(replaceNVL2withCase);
    	}
		String doReplaceSubstrWithOracompatSubstr=Initializer.getProperty("doReplaceSubstrWithNegativeInput", true);
		if((doReplaceSubstrWithOracompatSubstr!=null && Boolean.parseBoolean(doReplaceSubstrWithOracompatSubstr)) || doReplaceSubstrWithOracompatSubstr==null) {
			logger.trace("Replacing substr(text,index[, length]) and substring(text from index[ for length]) function calls with oracompat version if index is \"-something\"");
			modifications.add(replaceSubstrWithOracompatSubstr);
			modifications.add(replaceSubstringWithOracompatSubstr);
    	}
		String doReplaceSWF_Greatest=Initializer.getProperty("doReplaceSWF_Greatest", true);
		if((doReplaceSWF_Greatest!=null && Boolean.parseBoolean(doReplaceSWF_Greatest)) || doReplaceSWF_Greatest==null) {
			logger.trace("Replacing SWF_Greatest with greatest");
			modifications.add(replaceSWF_Greatest);
    	}
		String doCommentCCBI_ABEND_MANAGE=Initializer.getProperty("doCommentCCBI_ABEND_MANAGE");
		if((doCommentCCBI_ABEND_MANAGE!=null && Boolean.parseBoolean(doCommentCCBI_ABEND_MANAGE)) || doCommentCCBI_ABEND_MANAGE==null) {
			logger.trace("Commenting calls to CCBI_ABEND_MANAGE package");
			modifications.add(commentCallsToCCBI_ABEND_MANAGE);
			modifications.add(commentPR_ABEND_HANDLER);
			modifications.add(commentPr_launch_abend_handler);
    	}
           //		modifications.add(fixPxfCalls);
		//
		Path path=new File(Initializer.getProperty("workingDir")).toPath();
		//
		FilesCyclerFunCallAlterer cycler=new FilesCyclerFunCallAlterer(modifications);
		Files.walkFileTree(path, cycler);
		logger.trace("End FunCallAltererCycle");
		
	}
	
	
}
