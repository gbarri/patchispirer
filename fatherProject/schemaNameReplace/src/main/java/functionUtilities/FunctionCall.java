package functionUtilities;

public interface FunctionCall {
	public String getName();
	public String getSchema();
	public int getNumArgs();
	public boolean checkCall(String call);
//	public String callToString(String[] args);
	public void accept(CallAlteration alterator);
	
}
