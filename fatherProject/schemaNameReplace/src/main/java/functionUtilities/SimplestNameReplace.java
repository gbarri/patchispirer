package functionUtilities;

import org.apache.commons.lang3.StringUtils;

public class SimplestNameReplace implements CallAlteration {
	private FunctionCall fun1;
	private FunctionCall fun2;

	public SimplestNameReplace(FunctionCall fun1, FunctionCall fun2) {
		super();
		this.fun1 = fun1;
		this.fun2 = fun2;
	}

	public FunctionCall getFun1() {
		return fun1;
	}

	public void setFun1(FunctionCall fun1) {
		this.fun1 = fun1;
	}

	public FunctionCall getFun2() {
		return fun2;
	}

	public void setFun2(FunctionCall fun2) {
		this.fun2 = fun2;
	}

	@Override
	public String alterCall(String str, int[] extrema, FunCallAlterer funCallAlterer) {
		int ind1=StringUtils.indexOfIgnoreCase(str, fun1.getName(), extrema[0]);
		String newStr=str.substring(0,  ind1)+fun2.getName()+str.substring(ind1+fun1.getName().length());
		return newStr;
	}

	@Override
	public void visit(FunctionCall functionCall) {
		// TODO Auto-generated method stub
		
	}

}
