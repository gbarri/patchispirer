package functionUtilities;

/**
 * classe in cui aggiungiamo il check di avere il secondo argomento con un "-qualcosa"
 * @author g.barri
 *
 */
public class CustomSubtrFunctionWithINS extends SubstrFunction {

	/**
	 * function that should check if the third argument of the substr is a call to length()-instr()...
	 * @param arg
	 * @return
	 */
	private static boolean argCheck(String arg) {
		FunCallAlterer fun=new FunCallAlterer(new InstrFunction(), null);
		int[] outline= new int[2];
		outline=fun.detectFunctionCallSubString(0);
		if(outline==null) {
			return false;
		}
		fun=new FunCallAlterer(new LengthOrOctectLengthFunction(), null);
		outline=new int[2];
		outline=fun.detectFunctionCallSubString(0);
		if(outline==null) {
			return false;
		}
		
		return true;
	}
	
	
	@Override
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean numArgsCheck=parsedCall.length==getNumArgs()+2 || parsedCall.length==getNumArgs()+1;
		boolean nameCheck=parsedCall[1]!=null && parsedCall[1].trim().toUpperCase().equals(getName().toUpperCase());
		boolean schemaCheck=parsedCall[0]==null;
		boolean checkOnArgument=numArgsCheck && parsedCall[3].trim().matches("\\-.*");
		return numArgsCheck && nameCheck && checkOnArgument && schemaCheck;
	}
}
