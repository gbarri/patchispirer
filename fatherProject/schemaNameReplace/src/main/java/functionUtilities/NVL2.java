package functionUtilities;

public class NVL2 extends AbstractFunctionCall{

	@Override
	public String getName() {
		return "NVL2";
	}

	@Override
	public String getSchema() {
		//I don't have one
		return null;
	}

	@Override
	public int getNumArgs() {
		//irrealistic number
		return 3;
	}
	
	@Override
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean numArgsCheck=parsedCall.length==getNumArgs()+2;
		boolean nameCheck=parsedCall[1]!=null && parsedCall[1].trim().toUpperCase().equals(getName().toUpperCase());
		return numArgsCheck && nameCheck;
	}
	
	@Override
	protected boolean checkNumArgs(String[] args) {
		return args!=null && args.length>0;
	}

	@Override
	public void accept(CallAlteration alterator) {
		alterator.visit(this);
	}

	
}
