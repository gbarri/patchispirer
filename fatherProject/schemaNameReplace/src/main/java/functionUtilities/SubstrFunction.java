package functionUtilities;

/**
 * a particular substr function that resembles SUBSTR(...,-X,Y)
 * @author g.barri
 *
 */
public class SubstrFunction extends AbstractFunctionCall {

	@Override
	public String getName() {
		return "substr";
	}

	@Override
	public String getSchema() {
		return null;
	}

	@Override
	public int getNumArgs() {
		return 3;
	}

	@Override
	public void accept(CallAlteration alterator) {
		// TODO Auto-generated method stub
		alterator.visit(this);
	}
	
	@Override
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean numArgsCheck=parsedCall.length==getNumArgs()+2 || parsedCall.length==getNumArgs()+1;
		boolean nameCheck=parsedCall[1]!=null && parsedCall[1].trim().toUpperCase().equals(getName().toUpperCase());
		boolean schemaCheck=parsedCall[0]==null;
		return numArgsCheck && nameCheck && schemaCheck;
	}
	

}
