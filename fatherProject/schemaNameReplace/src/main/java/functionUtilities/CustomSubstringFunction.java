package functionUtilities;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class CustomSubstringFunction extends SubstringFunction {
	@Override
	public boolean checkCall(String call) {
		boolean check=super.checkCall(call);
		if(check) {
			String[] arg=super.parse(call);
			List<String> args=super.parseArguments(arg[2]);
			check=args.get(1).matches("\\-.*");
		}
		return check;
	}
}
