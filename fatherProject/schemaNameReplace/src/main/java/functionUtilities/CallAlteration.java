package functionUtilities;

public interface CallAlteration {
	String alterCall(String str, int[] extrema, FunCallAlterer funCallAlterer);
	void visit(FunctionCall functionCall);
	String TagPre="--<|DEV_COMMENT_autoreplace-pre|>";
	String TagPost="--<|DEV_COMMENT_autoreplace-post|>";
}
