package functionUtilities;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReplaceSubstringWithOracompatSubstr implements CallAlteration {

	static Logger logger=LoggerFactory.getLogger(ReplaceSubstringWithOracompatSubstr.class);
	private SubstringFunction substr;
	
	public ReplaceSubstringWithOracompatSubstr(SubstringFunction substr) {
		super();
		this.substr = substr;
	}

	@Override
	public String alterCall(String str, int[] extrema, FunCallAlterer funCallAlterer) {
		String caseQuery=createCall(str, extrema, funCallAlterer);
		int lastBefore=str.substring(0, extrema[0]).lastIndexOf("\n");
		String indent=str.substring(lastBefore+1,extrema[0]).replaceAll("[^\\s]", " ");
		String newStr=str.substring(0,extrema[0])+caseQuery.replaceAll("\n", "\n"+indent)+(extrema[1]+1<str.length()?str.substring(extrema[1]+1):"");
		logger.trace("Replacing\t"+str.substring(extrema[0],extrema[1]+1)+"\twith\t"+caseQuery.replaceAll("\n", "\n"+indent));
		return newStr;
	}

	protected String createCall(String str, int[] extrema, FunCallAlterer funCallAlterer) {
		String call=str.substring(extrema[0],extrema[1]+1);
		String[] agrs=this.substr.parse(call);
		List<String> arguments=this.substr.parseArguments(agrs[2]);
		String newCall=new OracompatSubstr().writeCall(arguments);
		return newCall;
	}

	@Override
	public void visit(FunctionCall functionCall) {
		functionCall.accept(this);
	}

}
