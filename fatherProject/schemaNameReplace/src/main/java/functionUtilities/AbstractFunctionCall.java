package functionUtilities;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractFunctionCall implements FunctionCall {

	static Logger logger=LoggerFactory.getLogger(AbstractFunctionCall.class);
	
	@Override
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean numArgsCheck=parsedCall.length==getNumArgs()+2;
		boolean nameCheck=parsedCall[1]!=null && parsedCall[1].trim().toUpperCase().equals(getName().toUpperCase());
		boolean schemaCheck=getSchema()==null || (parsedCall[0]!=null && parsedCall[0].trim().toUpperCase().equals(getSchema().toUpperCase()));
		return numArgsCheck && nameCheck && schemaCheck;
	}
	
	/**
	 * method to decompose the essential elements of a function call.
	 * In case the schema is not indicated, it returns null
	 * @param call
	 * @return
	 */
	public String[] parse(String call) {
		call=call.trim();
		String schema;
		String nameObject;
		String[] args;
		int indParentesi= call.indexOf("(");
		if(indParentesi==-1) {
			throw new IllegalArgumentException("problemi con il parsing: manca la parentesi \"(\". call: "+call); 
		}
		if(call!= null && call.substring(0,indParentesi).indexOf(".")!=-1) {
			schema=call.substring(0, call.indexOf("."));
		}else {
			schema=null;
		}
		if(call!= null && call.indexOf("(")!=-1) {
			nameObject=call.substring(schema==null?0:call.indexOf(".")+1, call.indexOf("(")).trim();
		}else {
			throw new IllegalArgumentException("problemi con il nome oggetto. call: "+call);
		}
		if(call!= null && call.indexOf(")")!=-1) {
			List<String> listArgs=new ArrayList<>();
			int val=1, count=1;
			int startArgument=call.indexOf("("), end=startArgument+1;
			while(count!=0 && end<call.length()) {
				Character c=call.charAt(end);
				if(c=='\'') {
					//assuming val assumes either 0 or 1 values, when encountering \' switch val between 0 and 1.
					val=val==1?0:1;
				}
				count+=(c==')'?-val:(c=='('?val:0));
				if((count==0 || (count==1 && c.equals(',') && val==1)) && startArgument+1<end) {
					listArgs.add(call.substring(startArgument+1, end));
					startArgument=end;
				}
				end++;
			}
			args=new String[listArgs.size()];
			for(int i=0; i<args.length; i++) {
				args[i]=listArgs.get(i);
			}
//			String argument=call.substring(call.indexOf("(")+1,call.indexOf(")"));
//			args=argument.split(",");
		}else {
			throw new IllegalArgumentException("problemi con il nome oggetto. call: "+call);
		}
		String[] out=new String[2+args.length];
		out[0]=schema;
		out[1]=nameObject;
		for(int i=2; i<out.length; i++) {
			out[i]=args[i-2];
		}
		return out;
	}
	
	/**
	 * check if the number of arguments of the calls is coherent with the specific instance.
	 * Needs to be overwritten in case of functions with ambiguous number of parameters
	 * Be careful: it is a change in text and does not add any comment back in!
	 * @param args
	 * @return
	 */
	protected boolean checkNumArgs(String[] args) {
		return args.length==this.getNumArgs();
	}

	/*@Override
	public String callToString(String[] args) {
		if(!checkNumArgs(args)) {
			IllegalArgumentException exc=new IllegalArgumentException("Tried to call function "+this.getSchema()+"."+this.getName()+" with wrong number of arguments");
			logger.error("wrong number of arguments passed to function "+this.getName(), exc);
			throw exc;
		}
		String call="";
		if(this.getSchema()!=null) {
			call+=this.getSchema().toLowerCase()+".";
		}
		call+=this.getName().toUpperCase()+"(";
		if(args.length>0) {
			call+=args[0];
			for(int i=1; i<args.length; i++) {
				call+=","+args[i];
			}
		}
		call+=")";
		return call;
	}
*/
	
}
