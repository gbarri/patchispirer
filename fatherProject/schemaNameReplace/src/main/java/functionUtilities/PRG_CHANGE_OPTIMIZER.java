package functionUtilities;

import org.apache.commons.lang3.StringUtils;

public class PRG_CHANGE_OPTIMIZER extends AbstractFunctionCall {

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "PRG_CHANGE_OPTIMIZER";
	}

	@Override
	public String getSchema() {
		// TODO Auto-generated method stub
		return "dg_utilities";
	}

	@Override
	public int getNumArgs() {
		// TODO Auto-generated method stub
		return 1;
	}
	
	@Override
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean numArgsCheck=parsedCall.length==getNumArgs()+2;
		boolean nameCheck=parsedCall[1]!=null && parsedCall[1].trim().toUpperCase().equals(getName().toUpperCase());
		boolean schemaCheck=parsedCall[0]!=null && ( StringUtils.equalsIgnoreCase(parsedCall[0].trim(), getSchema()) || StringUtils.equalsIgnoreCase(parsedCall[0].trim(), "tl_analytics") );
		return numArgsCheck && nameCheck && schemaCheck;
	}

	@Override
	public void accept(CallAlteration alterator) {
		// TODO Auto-generated method stub
	}

}
