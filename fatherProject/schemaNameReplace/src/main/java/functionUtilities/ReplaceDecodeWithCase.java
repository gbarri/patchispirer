package functionUtilities;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utilities.Decommenter;

public class ReplaceDecodeWithCase implements CallAlteration {
	static Logger logger=LoggerFactory.getLogger(ReplaceDecodeWithCase.class);
	private AbstractFunctionCall decodeFunction=new Decode();
	
	private String buildEqualityInCaseWhen(String baseline, String test, String result) {
		String out="";
		Decommenter decommenter=new Decommenter();
		String cleanedTest=decommenter.initLine(test).trim();
		if(StringUtils.equalsIgnoreCase(cleanedTest,"NULL")) {
			out+=baseline+" IS "+test; 
		}else {
			out+=baseline+" = "+test;
		}
		return out;
	}
	
	private String createCaseQuery(String str, int[] extrema, FunCallAlterer funCallAlterer) {
		String decodeCall=str.substring(extrema[0],extrema[1]+1);
		String[] allArgs=decodeFunction.parse(decodeCall);
		String[] trueArgs=new String[allArgs.length-2];
		for(int i=0; i<trueArgs.length; i++) {
			trueArgs[i]=allArgs[i+2];
		}
		//String defaultArg=;
		String baseline=trueArgs[0];
		String caseWhenQuery="(CASE ";
		int numCycles=(trueArgs.length-1)/2, i;
		for(i=0; i<numCycles; i++) {
			String test=trueArgs[1+2*i];
			String result=trueArgs[1+2*i+1];
			String equal=buildEqualityInCaseWhen(baseline,test,result);
			caseWhenQuery+="\r\nWHEN "+equal+" THEN "+result+" ";
		}
		if(2*i<trueArgs.length-1) {
			caseWhenQuery+="\r\nELSE "+trueArgs[trueArgs.length-1];
		}else {
			caseWhenQuery+="\r\nELSE NULL";
		}
		caseWhenQuery+="\r\nEND)";
		return caseWhenQuery;
	}

	@Override
	public void visit(FunctionCall functionCall) {
		// TODO Auto-generated method stub

	}

	@Override
	public String alterCall(String str, int[] extrema, FunCallAlterer funCallAlterer) {
		String caseQuery=createCaseQuery(str, extrema, funCallAlterer);
		int lastBefore=str.substring(0, extrema[0]).lastIndexOf("\n");
		String indent=str.substring(lastBefore+1,extrema[0]).replaceAll("[^\\s]", " ");
		if(indent.length()>10) {
			indent=indent.substring(0,9);
		}
		String newStr=str.substring(0,extrema[0])+caseQuery.replaceAll("\n", "\n"+indent)+(extrema[1]+1<str.length()?str.substring(extrema[1]+1):"");
		logger.trace("Replacing\t"+str.substring(extrema[0],extrema[1]+1)+"\twith\t"+caseQuery.replaceAll("\n", "\n"+indent));
		return newStr;
	}

}
