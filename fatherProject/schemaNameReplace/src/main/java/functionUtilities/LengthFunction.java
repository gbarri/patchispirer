package functionUtilities;

import org.apache.commons.lang3.StringUtils;

/**
 * The length function of PostgreSQL, it has 1 imput, a string and it counts the string character
 * @author g.barri
 *
 */
public class LengthFunction extends AbstractFunctionCall {

	@Override
	public String getName() {
		return "length";
	}

	@Override
	public String getSchema() {
		return null;
	}

	@Override
	public int getNumArgs() {
		return 1;
	}

	@Override
	public void accept(CallAlteration alterator) {
		// TODO Auto-generated method stub
		alterator.visit(this);
	}
	
	@Override
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean numArgsCheck=parsedCall.length==getNumArgs()+2;
		boolean nameCheck=parsedCall[1]!=null && StringUtils.equalsIgnoreCase(parsedCall[1].trim(), getName());
		boolean schemaCheck=parsedCall[0]==null;
		return numArgsCheck && nameCheck && schemaCheck;
	}
	

}
