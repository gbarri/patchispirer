package functionUtilities;

/**
 * classe in cui aggiungiamo il check di avere il secondo argomento con un "-qualcosa"
 * @author g.barri
 *
 */
public class CustomSubtrFunction extends SubstrFunction {

	@Override
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean numArgsCheck=parsedCall.length==getNumArgs()+2 || parsedCall.length==getNumArgs()+1;
		boolean nameCheck=parsedCall[1]!=null && parsedCall[1].trim().toUpperCase().equals(getName().toUpperCase());
		boolean schemaCheck=parsedCall[0]==null;
//		boolean checkOnArgument=numArgsCheck && parsedCall[3].trim().matches("\\-\\d+.*");
		boolean checkOnArgument=numArgsCheck && parsedCall[3].trim().matches("\\-.*");
		return numArgsCheck && nameCheck && checkOnArgument && schemaCheck;
	}
}
