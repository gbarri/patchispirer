package functionUtilities;

public class Decode extends AbstractFunctionCall {

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "DECODE";
	}

	@Override
	public String getSchema() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getNumArgs() {
		// TODO Auto-generated method stub
		return -1;
	}

	@Override
	public void accept(CallAlteration alterator) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean nameCheck=parsedCall[1]!=null && parsedCall[1].trim().toUpperCase().equals(getName().toUpperCase());
		boolean numArgsCheck=parsedCall.length>=5;
		return nameCheck && numArgsCheck;
	}

}
