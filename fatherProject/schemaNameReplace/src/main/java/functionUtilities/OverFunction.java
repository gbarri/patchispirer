package functionUtilities;

import org.apache.commons.lang3.StringUtils;

public class OverFunction extends AbstractFunctionCall{

	@Override
	public String getName() {
		return "OVER";
	}

	@Override
	public String getSchema() {
		//I don't have one
		return null;
	}

	@Override
	public int getNumArgs() {
		//irrealistic number
		return -1;
	}
	
	@Override
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean nameCheck=parsedCall[1]!=null && parsedCall[1].trim().toUpperCase().equals(getName().toUpperCase());
		boolean isThereNullsLast=true;
		if(nameCheck) {
			if(parsedCall.length>=3) {
				String args=parsedCall[2];
				for(int i=3; i<parsedCall.length; i++) {
					args=args+","+parsedCall[i];
				}
				if(!StringUtils.containsIgnoreCase(args,"NULLS LAST") && StringUtils.containsIgnoreCase(args,"DESC")){
					isThereNullsLast=false;
				}
			}else {
				logger.error("TOO FEW PARSED ARGUMETS TO BE LEGAL: "+call); 
				isThereNullsLast=false;
			}
		}
		return nameCheck&&!isThereNullsLast;
	}
	
	@Override
	protected boolean checkNumArgs(String[] args) {
		return args!=null && args.length>0;
	}

	@Override
	public void accept(CallAlteration alterator) {
		alterator.visit(this);
		
	}

	
}
