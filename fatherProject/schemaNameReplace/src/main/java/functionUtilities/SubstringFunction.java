package functionUtilities;

import java.util.ArrayList;
import java.util.*;

import org.apache.commons.lang3.StringUtils;

public class SubstringFunction extends AbstractFunctionCall {

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "substring";
	}

	@Override
	public String getSchema() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getNumArgs() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public void accept(CallAlteration alterator) {
		// TODO Auto-generated method stub

	}
	
	public List<String> parseArguments(String arg) {
		List<String> args=new ArrayList<>();
		int indexOfFrom=StringUtils.indexOfIgnoreCase(arg, " from ");
		if(indexOfFrom>0) {
			String firstArg=arg.substring(0, indexOfFrom).trim();
			args.add(firstArg);
			int indexOfFor=StringUtils.indexOfIgnoreCase(arg, " for ", indexOfFrom+6);
			if(indexOfFor!=-1) {
				String secondArg=arg.substring(indexOfFrom+6,indexOfFor).trim();
				String thirdArg=arg.substring(indexOfFor+5).trim();
				args.add(secondArg);
				args.add(thirdArg);
			}
		}
		return args;
	}
	
	@Override
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean numArgsCheck=parsedCall.length==getNumArgs()+2;
		boolean nameCheck=parsedCall[1]!=null && StringUtils.equalsIgnoreCase(parsedCall[1].trim(), getName());
		List<String> args=parseArguments(parsedCall[2]);
		boolean checkArgs=numArgsCheck && (args.size()==2 || args.size()==3);
		return numArgsCheck && nameCheck && checkArgs;
	}

}
