package functionUtilities;

import org.apache.commons.lang3.StringUtils;

/**
 * The INSTR function of SQL, it has necessarily 2 imput, a string and something to search in it
 * @author g.barri
 *
 */
public class InstrFunction extends AbstractFunctionCall {

	@Override
	public String getName() {
		return "INSTR";
	}

	@Override
	public String getSchema() {
		return null;
	}

	@Override
	public int getNumArgs() {
		return 2;
	}

	@Override
	public void accept(CallAlteration alterator) {
		// TODO Auto-generated method stub
		alterator.visit(this);
	}
	
	@Override
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean numArgsCheck=parsedCall.length==getNumArgs()+2 || parsedCall.length==getNumArgs()+3;  //sarebbe anche un caso di 4 parametri lecito, ma non è utilizzato
		boolean nameCheck=parsedCall[1]!=null && StringUtils.equalsIgnoreCase(parsedCall[1].trim(), getName());
		boolean schemaCheck=parsedCall[0]==null;
		return numArgsCheck && nameCheck && schemaCheck;
	}
	

}
