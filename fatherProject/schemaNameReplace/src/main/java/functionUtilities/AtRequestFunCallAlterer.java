package functionUtilities;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utilities.Initializer;
import utilities.SQLSearcher;

public class AtRequestFunCallAlterer extends SimpleFileVisitor<Path> {
	
	static Logger logger=LoggerFactory.getLogger(AtRequestFunCallAlterer.class);
	
	public FunCallAlterer alterer;
	
	public AtRequestFunCallAlterer(FunCallAlterer alterer) {
		super();
		this.alterer = alterer;
	}

	@Override
	public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException{
//		logger.trace("Visiting file "+path.toAbsolutePath().toString());
		File file=path.toFile();
		if(checkFile(file)) {
			String textFile=null;
			Scanner scann=null;
			if(this.alterer.alter instanceof SimpleNotifier) {
				SimpleNotifier simpa=(SimpleNotifier)this.alterer.alter;
				simpa.setFileField(file);
			}	
			try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
				Scanner scannFromFile=new Scanner(new BufferedInputStream(fileStream))){
				scann=scannFromFile;
				alterer.setScann(scann);
				textFile=alterer.alterFunctionCalls();
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while reading for\t"+file.getAbsolutePath(), e);
			}
			scann.close();
			
			try(FileOutputStream fileOutStream=new FileOutputStream(file.getAbsolutePath()); 
					PrintWriter print=new PrintWriter(new BufferedOutputStream(fileOutStream))){
				print.print(textFile);
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while writing\n\t"+file.getAbsolutePath(), e);
			}
		}
		return FileVisitResult.CONTINUE;
	}

	private boolean checkFile(File file) {
		boolean cond=file.getName().endsWith(".sql");
		if(cond) {
			String objectName=SQLSearcher.detectName(file);
			if(objectName==null) {
				cond=false;
			}
		}
		return cond;
	}
	
	public static void main(String[] args) throws IOException {
		logger.trace("Starting customized function searcher");
		Path path=new File(Initializer.getProperty("localRepositoryDir")).toPath();
//		Path path=new File("C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_analytics\\views\\tl_analytics_vs_dm_edv_sede_srete_cjd.sql").toPath();
		//
//		ToDateCustom todate=new ToDateCustom();
		OverFunction over=new OverFunction();
		File output=new File("C:\\Users\\g.barri\\Desktop\\tmp\\resultsOfOverNullsLast.txt");
		SimpleNotifier notif=new SimpleNotifier(output);
		FunCallAlterer alterer=new FunCallAlterer(over, notif);
		AtRequestFunCallAlterer cycler=new AtRequestFunCallAlterer(alterer);
		Files.walkFileTree(path, cycler);
//		ReplaceSubstrWithOracompatSubstr replace=new ReplaceSubstrWithOracompatSubstr(substr);
//		AtRequestFunCallAlterer cycler=new AtRequestFunCallAlterer(new FunCallAlterer(substr, replace));
//		Files.walkFileTree(path, cycler);
		logger.trace("End FunCallAltererCycle");
	}

}
