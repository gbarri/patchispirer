package functionUtilities;

public class Pr_check_err_logFunctionCall extends AbstractFunctionCall {

	private static String myName="PR_CHECK_ERR_LOG";
	
	@Override
	public String getName() {
		return myName;
	}

	@Override
	public String getSchema() {
		return "dg_utilities";
	}

	@Override
	public int getNumArgs() {
		return 4;
	}

	@Override
	public void accept(CallAlteration alterator) {
		alterator.visit(this);
		
	}
}
