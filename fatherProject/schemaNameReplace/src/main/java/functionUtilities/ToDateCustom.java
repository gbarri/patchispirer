package functionUtilities;

import org.apache.commons.lang3.StringUtils;

public class ToDateCustom extends AbstractFunctionCall {

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "TO_DATE";
	}

	@Override
	public String getSchema() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getNumArgs() {
		// TODO Auto-generated method stub
		return 1;//da 1 a 3
	}

	@Override
	public void accept(CallAlteration alterator) {
		alterator.visit(this);

	}
	
	@Override
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean numArgsCheck=parsedCall.length>=getNumArgs()+2 && parsedCall.length<=getNumArgs()+4;
		boolean nameCheck=parsedCall[1]!=null && parsedCall[1].trim().toUpperCase().equals(getName().toUpperCase());
		boolean schemaCheck=parsedCall[0]==null || parsedCall[0]=="PUBLIC";
		boolean checkOnArgument=numArgsCheck && parsedCall.length>=4;
		if(checkOnArgument) {
			checkOnArgument=parsedCall[3]!=null && StringUtils.equalsIgnoreCase(parsedCall[3].trim(), "\'YYYY\'");
			if(checkOnArgument) {
				System.out.println(parsedCall);
			}
		}
		return numArgsCheck && nameCheck && checkOnArgument && schemaCheck;
	}

}
