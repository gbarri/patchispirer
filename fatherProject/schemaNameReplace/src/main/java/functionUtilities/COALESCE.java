package functionUtilities;

public class COALESCE extends AbstractFunctionCall{

	@Override
	public String getName() {
		return "COALESCE";
	}

	@Override
	public String getSchema() {
		return null;
	}

	@Override
	public int getNumArgs() {
		return -1;
	}
	
	@Override
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean nameCheck=parsedCall[1]!=null && parsedCall[1].trim().toUpperCase().equals(getName().toUpperCase());
		return nameCheck;
	}
	
	@Override
	protected boolean checkNumArgs(String[] args) {
		return args!=null && args.length>0;
	}

	@Override
	public void accept(CallAlteration alterator) {
		alterator.visit(this);
	}
	

}