package functionUtilities;

import org.apache.commons.lang3.StringUtils;

public class PxfLocationFunction extends AbstractFunctionCall{

	@Override
	public String getName() {
		return "LOCATION";
	}

	@Override
	public String getSchema() {
		return null;
	}

	@Override
	public int getNumArgs() {
		return 1;
	}
	
	@Override
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean schemaCheck=parsedCall[0]==null;
		boolean nameCheck=parsedCall[1]!=null && StringUtils.equalsIgnoreCase(parsedCall[1].trim(),getName());
		boolean argsCheck=parsedCall.length==getNumArgs()+2;
		return nameCheck && schemaCheck && argsCheck;
	}

	@Override
	public void accept(CallAlteration alterator) {
		alterator.visit(this);
	}
	

}