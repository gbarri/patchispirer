package functionUtilities;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jaxen.function.StringFunction;

public class OracompatSubstr extends AbstractFunctionCall {

	@Override
	public String getName() {
		return "SUBSTR";
	}

	@Override
	public String getSchema() {
		return "oracompat";
	}

	@Override
	/**
	 * 3 o 2
	 */
	public int getNumArgs() {
		return 3;
	}

	@Override
	public void accept(CallAlteration alterator) {
		// TODO Auto-generated method stub

	}
	
	@Override
	/**
	 * stessi controlli del metodo substr
	 */
	public boolean checkCall(String call) {
		String[] parsedCall=parse(call);
		boolean numArgsCheck=parsedCall.length==getNumArgs()+2 || parsedCall.length==getNumArgs()+1;
		boolean nameCheck=parsedCall[1]!=null && parsedCall[1].trim().toUpperCase().equals(getName().toUpperCase());
		boolean schemaCheck=parsedCall[0]!=null && StringUtils.equalsIgnoreCase(parsedCall[0].trim(), getSchema());
		return numArgsCheck && nameCheck && schemaCheck;
	}
	
	public String writeCall(List<String> args) {
		if(args.size()<2 || args.size()>3) {
			throw new IllegalArgumentException("The number of arguments in "+args+" does not match the number of arguments necessary for "+this.getSchema()+"."+this.getName().toUpperCase());
		}
		String call=this.getSchema().toLowerCase()+"."+this.getName().toUpperCase()+"( "+args.get(0);
		for(int i=1; i<args.size(); i++) {
			call+=" , "+args.get(i);
		}
		call+=" )";
		return call;
	}

}
