package functionUtilities;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.tinylog.Logger;

/**
 * the class does not change the call but simply notify it presence
 * @author g.barri
 *
 */
public class SimpleNotifier implements CallAlteration {

	protected File outStream;
	
	protected File fileField;
	
	public File getFileField() {
		return fileField;
	}

	public void setFileField(File fileField) {
		this.fileField = fileField;
	}

	public SimpleNotifier(File outStream) {
		super();
		this.outStream = outStream;
	}
	

	@Override
	public String alterCall(String str, int[] extrema, FunCallAlterer funCallAlterer) {
		int lastBefore=str.substring(0, extrema[0]).lastIndexOf("\n");
		if(lastBefore==-1) {
			lastBefore=0;
		}
		String contextLine=str.substring(lastBefore,str.indexOf("\n",extrema[1]+1)).replaceAll("\r", "");
		Logger.trace("Found call "+str.substring(extrema[0],extrema[1]+1)+" in file\t"+this.getFileField().getAbsolutePath()+"\t:\t"+contextLine);
		try(FileWriter print=new FileWriter(outStream,true)){
			print.write(str.substring(extrema[0],extrema[1]+1)+"\t"+contextLine+"\t"+this.getFileField().getAbsolutePath()+"\r\n");
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while writing\n\t"+fileField.getAbsolutePath(), e);
		}
		return str;
	}

	@Override
	public void visit(FunctionCall functionCall) {
		// TODO Auto-generated method stub

	}

}
