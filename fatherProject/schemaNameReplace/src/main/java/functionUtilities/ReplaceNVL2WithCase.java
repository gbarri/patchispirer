package functionUtilities;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utilities.Decommenter;

public class ReplaceNVL2WithCase implements CallAlteration {
	
	static Logger logger=LoggerFactory.getLogger(ReplaceNVL2WithCase.class);
	private AbstractFunctionCall NVL2Function=new NVL2();
	
	private String buildEqualityInCaseWhen(String baseline, String test, String result) {
		String out="";
		Decommenter decommenter=new Decommenter();
		String cleanedTest=decommenter.initLine(test).trim();
		if(StringUtils.equalsIgnoreCase(cleanedTest,"NULL")) {
			out+=baseline+" IS "+test; 
		}else {
			out+=baseline+" = "+test;
		}
		return out;
	}
	
	private String createCaseQuery(String str, int[] extrema, FunCallAlterer funCallAlterer) {
		String decodeCall=str.substring(extrema[0],extrema[1]+1);
		String[] allArgs=NVL2Function.parse(decodeCall);
		String[] trueArgs=new String[allArgs.length-2];
		for(int i=0; i<trueArgs.length; i++) {
			trueArgs[i]=allArgs[i+2];
		}
		String caseWhenQuery="(CASE ";
			caseWhenQuery+="\r\n WHEN "+trueArgs[0]+" IS NOT NULL THEN "+trueArgs[1];		
			caseWhenQuery+="\r\n ELSE "+trueArgs[2];
		caseWhenQuery+="\r\nEND)";
		return caseWhenQuery;
	}

	@Override
	public void visit(FunctionCall functionCall) {
		// TODO Auto-generated method stub

	}

	@Override
	public String alterCall(String str, int[] extrema, FunCallAlterer funCallAlterer) {
		String caseQuery=createCaseQuery(str, extrema, funCallAlterer);
		int lastBefore=str.substring(0, extrema[0]).lastIndexOf("\n");
		String indent=str.substring(lastBefore+1,extrema[0]).replaceAll("[^\\s]", " ");
		String newStr=str.substring(0,extrema[0])+caseQuery.replaceAll("\n", "\n"+indent)+(extrema[1]+1<str.length()?str.substring(extrema[1]+1):"");
		logger.trace("Replacing\t"+str.substring(extrema[0],extrema[1]+1)+"\twith\t"+caseQuery.replaceAll("\n", "\n"+indent));
		return newStr;
	}

}
