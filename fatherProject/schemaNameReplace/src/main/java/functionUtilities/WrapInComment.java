package functionUtilities;

import org.tinylog.Logger;

public class WrapInComment implements CallAlteration {

	@Override
	public void visit(FunctionCall functionCall) {
		// TODO Auto-generated method stub
	}

	@Override
	public String alterCall(String str, int[] extrema, FunCallAlterer funCallAlterer) {
		int lastBefore=str.substring(0, extrema[0]).lastIndexOf("\n");
		String pre="";
		if(lastBefore==-1) {
			pre="--";
			lastBefore=0;
		}
		String newStr=str.substring(0,lastBefore)+pre+str.substring(lastBefore, extrema[1]+1).replace("\n", "\n--")
				+(extrema[1]+1<str.length()-1?str.substring(extrema[1]+1):"");
		Logger.trace("replaced\n"+str.replace("\r\n", "\\r\\n\r\n")+"\nwith\n"+newStr.replace("\r\n", "\\r\\n\r\n")+"\nEND");
		return newStr;
	}
	
//	public static void main(String[] args) {
//		String test="qw\ne\nrtyu\n\niop\n";
//		//           012 34 56789 0 1234
//		WrapInComment wrp=new WrapInComment();
//		int[] extrema= {5,8};
//		System.out.println(test.substring(extrema[0],extrema[1]+1));
//		System.out.println("##");
//		System.out.println(wrp.alterCall(test, extrema));
//	}

}
