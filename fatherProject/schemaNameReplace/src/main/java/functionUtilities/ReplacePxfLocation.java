package functionUtilities;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.SystemOutLogger;
import org.tinylog.Logger;

import loaders.LoaderByColumnIndex;
import loaders.LoaderMultipleSheets;
import utilities.Initializer;

public class ReplacePxfLocation implements CallAlteration {

	Map<String,String[]> newStringMap;
	public ReplacePxfLocation(){
		
		LoaderByColumnIndex loader=new LoaderByColumnIndex(new File("C:\\Users\\g.barri\\Documents\\elenco sistemi per connessione PXF (ABA2).xlsx"),
					    "Loader.useColumnsAt=5\r\n" + 
						"Loader.useSheetAt=0\r\n" + 
						"Loader.usePKAt=2,4");
		try {
			newStringMap=loader.loadMap();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public String alterCall(String str, int[] extrema, FunCallAlterer funCallAlterer) {
		String[] parsedCall=((AbstractFunctionCall)funCallAlterer.function).parse(str.substring(extrema[0], extrema[1]+1));
		String locString=parsedCall[2].replaceAll("'", "").trim();
		if(locString.startsWith("pxf://")) {
			locString=locString.substring(6);
		}
		int puntoDomanda=locString.indexOf("?");
		String object=locString.substring(0, puntoDomanda);
		String nomeOggetto=object.substring(object.indexOf(".")+1);
		String options=locString.substring(puntoDomanda+1);
		String[] couples=options.split("&");
		Map<String,String> optMap=new HashMap<>();
		for(String item:couples) {
			String[] tokens=item.split("=");
			if(tokens.length!=2) {
				throw new IllegalStateException("guai!!");
			}else {
				optMap.put(tokens[0], tokens[1]);
			}
		}
		if(optMap.containsKey("SERVER") && !optMap.containsKey("USER")) {
			System.out.println("File skipped");
			return str;
		}
		String utenza=optMap.get("USER");
		String IP=optMap.get("DB_URL");
		IP=IP.substring(IP.indexOf("@")+1);
		String[] nomeServer=newStringMap.get(utenza+"."+IP);
		if(nomeServer==null) {
			for(String key:newStringMap.keySet()) {
				String IPKey=key.substring(key.indexOf(".")+1);
				if(IPKey.equals(IP)) {
					String nome=newStringMap.get(key)[0];
					String newStr=str.substring(0,extrema[0])+"LOCATION( 'pxf://"+object+"?PROFILE=Jdbc&SERVER="+nome+"')"+str.substring(extrema[1]+1);
					return newStr;
				}
			}
			//throw new IllegalArgumentException("utenza/IP non trovata! IP="+IP+",utenza="+utenza);
			Logger.error("utenza/IP non trovata! IP="+IP+",utenza="+utenza);
			return str;
		}else if(nomeServer.length>1){
			throw new IllegalArgumentException("utenza/IP multiple!");
		}
		String newStr=str.substring(0,extrema[0])+"LOCATION( 'pxf://"+object+"?PROFILE=Jdbc&SERVER="+nomeServer[0]+"')"+str.substring(extrema[1]+1);
		return newStr;
	}

	@Override
	public void visit(FunctionCall functionCall) {
		// TODO Auto-generated method stub
	}

}
