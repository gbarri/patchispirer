package fix_PR_XX;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import schemaMapping.LoaderSchemaMap;

public class PR_ANALYZE extends PR_base {
	static Logger logger=LoggerFactory.getLogger(PR_ANALYZE.class);
	
	public PR_ANALYZE(String field_schema, String field_object) {
		super(field_schema, field_object);
	}

	@Override
	public String getInvocation() {
		String[] newFileds=this.mapFields();
		return schema+"."+"PR_ANALYZE( '"+newFileds[0].toLowerCase()+"' , '"+newFileds[1].toLowerCase()+"' )";
	}
	
}
