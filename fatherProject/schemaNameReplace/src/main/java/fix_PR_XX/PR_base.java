package fix_PR_XX;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import schemaMapping.LoaderSchemaMap;

public abstract class PR_base {	
	
	static Logger logger=LoggerFactory.getLogger(PR_TRUNCATE.class);
	
	protected String field_schema;
	protected String field_object;
	protected static String schema="dg_utilities";

	public PR_base(String field_schema, String field_object) {
		this.field_schema = field_schema.replaceAll("\'", "");
		this.field_object = field_object.replaceAll("\'", "");
	}
	
	protected String[] mapFields() {
		String[] out=new String[2];
		LoaderSchemaMap load=new LoaderSchemaMap();
		Map<String, String> replaceMap=load.loadMap();
		String newObject=replaceMap.get(field_schema.toUpperCase()+"."+field_object.toUpperCase());
		if(newObject==null) {
			logger.warn("Unable to find a mapping for the object "+field_schema.toLowerCase()+"."+field_object.toUpperCase());
			out[0]=field_schema;
			out[1]=field_object;
		}else{
			if(newObject.contains(".")) {
				String newField_schema=newObject.substring(0, newObject.indexOf("."));
				String newField_object=newObject.substring(newObject.indexOf(".")+1);
				out[0]=newField_schema;
				out[1]=newField_object;
				logger.trace("arguments of a call to PR_TRUNCATE ("+field_schema+" , "+field_object+") mapped to ("+newField_schema+" , "+newField_object+")");
			}else {
				logger.warn("The object "+newObject+" obtained through mapping has no \".\" in it. Substituition aborted");
				out[0]=field_schema;
				out[1]=field_object;
			}		
		}
		return out;
	}
	
	public abstract String getInvocation();	
	
}
