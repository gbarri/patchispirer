package fix_PR_XX;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PR_COMP extends PR_base{
	static Logger logger=LoggerFactory.getLogger(PR_COMP.class);
	
	public PR_COMP(String field_schema, String field_object) {
		super(field_schema, field_object);
		logger.warn("The function PR_COMP which is the analog of PR_TRUNCATE and PR_ANALYZE has not been prepared yet!! You should not use this class.");
		throw new UnsupportedOperationException("Not to be used, yet. Check with Roberto if there are news, or if its needed at all");
	}

	@Override
	public String getInvocation() {
		String[] newFileds=this.mapFields();
		return schema+"."+"PR_COMP( '"+newFileds[0].toLowerCase()+"' , '"+newFileds[1].toLowerCase()+"' )";
	}
}
