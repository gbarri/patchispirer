package fix_PR_XX;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.apache.poi.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utilities.Initializer;
import utilities.SQLSearcher;
import utilities.SQLSearcher.SQLTypes;
import utilities.StringUtilities;

/**
 * classe pensata per occuparsi dell'update delle chiamate alle funzioni di truncate, anaylize and compress originalmente
 * note come XXDWH.PR_XXDWH_ANALYZE, XXDWH.PR_XXDWH_COMP, XXDWH.PR_XXDWH_TRUNCATE, XXPM.PR_XXPM_COMP, XXPM.PR_XXPM_TRUNCATE, XXSCH.PR_XXSCH_ANALYZE
 * XXSCH.PR_XXSCH_COMP, XXSCH.PR_XXSCH_TRUNCATE e XXSCHAPP.PR_XXSCHAPP_FT_IRF_GLOBAL.
 * Queste vanno sostituite con le funzioni PR_TRUNCATE e PR_ANAlYZE sotto dg_utilities
 * @author g.barri
 */
public class Fix_PR_XX_YY extends SimpleFileVisitor<Path>{
	
	public static String[] pr_truncate= {"PR_XXDWH_TRUNCATE", "PR_XXPM_TRUNCATE", "PR_XXSCH_TRUNCATE", "pr_xxdwh_truncate", "pr_xxpm_truncate", "pr_xxsch_truncate"};
	public static String[] pr_analyze = {"PR_XXDWH_ANALYZE", "PR_XXPM_ANALYZE", "PR_XXSCH_ANALYZE", "pr_xxdwh_analyze", "pr_xxpm_analyze", "pr_xxsch_analyze"};
	public static String[] pr_compress ={"PR_XXDWH_COMP", "PR_XXPM_COMP", "PR_XXSCH_COMP", "pr_xxdwh_comp", "pr_xxpm_comp", "pr_xxsch_comp"};
	private java.lang.String found;
	private String groupFound;
	static Logger logger=LoggerFactory.getLogger(Fix_PR_XX_YY.class);
	
	/**
	 * only modify files for which checkFile is true
	 * @param file
	 * @return
	 */
	private boolean checkFile(File file) {
		boolean cond=file.getName().endsWith(".sql");
		return cond;
	}
	
	/**
	 * onlt modify lines for which checkLine is true
	 * @param line
	 * @return
	 */
	private boolean checkLine(String line) {
		return !line.trim().startsWith("--");
	}
	
	/**
	 * metodo che verifica il formato della stringa per verificare che sia una chiamata ad una PR_something che è da modificare
	 * @param str
	 * @return
	 * @throws PR_formatException
	 */
	public boolean containsPR_Check(String str) {
		Field[] fields=this.getClass().getDeclaredFields();
		for(Field field:fields) {
			if(field.getType().equals(String[].class)) {
				String[] name=null;
				try {
					if(field.get(null) instanceof String[]) {
						name=(String[]) field.get(null);
					}
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for(String keyFunc:name) {
					if(Pattern.matches(".*"+keyFunc+"\\s?\\(.*\\'.*\\'.*\\'.*\\'\\).*", str)) {
						this.found=keyFunc;
						this.groupFound=field.getName();
						return true;
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * selezione esattamente il blocco della chiamata alla PR_* trovata
	 * @param str
	 * @return
	 */
	private String polishCall(String str) {
		int start=str.indexOf(this.found);
		int end=str.indexOf(")",start+this.found.length());
		if(str.charAt(start-1)=='.') {
			int startSchema=start;
			while(startSchema>=1) {
				if(Character.isWhitespace(str.charAt(startSchema-1))) {
					start=startSchema;
					break;
				}
				startSchema--;
			}
		}
		return str.substring(start, end+1);
	}
		
	@Override
	public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException{
		File file=path.toFile();
//		System.out.println(file.getName());
		if(checkFile(file)) {
			String text="";
			try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
					Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
				while(scann.hasNextLine()) {
					String nextLine=scann.nextLine();
					if(checkLine(nextLine)) {
						this.found=null; //just to be sure
						if(containsPR_Check(nextLine)) {
							//findCall()
							String callToPR=polishCall(nextLine);
							logger.debug(callToPR);
							int firstApex=callToPR.indexOf("'");
							int secondApex=callToPR.indexOf("'",firstApex+1);
							int thirdApex=callToPR.indexOf("'",secondApex+1);
							int fourthApex=callToPR.indexOf("'",thirdApex+1);
							String schema=callToPR.substring(firstApex+1,secondApex);
							String object=callToPR.substring(thirdApex+1,fourthApex);
							String newCallToPR=null;
							switch(groupFound) {
							//qui dovrei modificare la stringa
								case "pr_truncate": 
									newCallToPR=new PR_TRUNCATE(schema, object).getInvocation();
									break;
								case "pr_analyze" : 
									newCallToPR=new PR_ANALYZE(schema, object).getInvocation();
									break;
								case "pr_compress":
									logger.error("Funzionalità ad ora bloccata "+file.getAbsolutePath());
									//newCallToPR=new PR_COMP(schema, object).getInvocation();
									newCallToPR=callToPR;
									break;
								default: 
									logger.warn("group name not found");
									newCallToPR=callToPR;
							}
							String tmpLine=nextLine.replace(callToPR, newCallToPR);
							nextLine=tmpLine;
							this.found=null;
							this.groupFound=null;
						}
					}
					text+=nextLine+"\r\n";
				}
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while reading for\t"+file.getAbsolutePath(), e);
			}
			
			try(FileOutputStream fileOutStream=new FileOutputStream(file.getAbsolutePath()); 
					PrintWriter print=new PrintWriter(new BufferedOutputStream(fileOutStream))){
				print.print(text);
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while writing\n\t"+file.getAbsolutePath(), e);
			}
		}
		return FileVisitResult.CONTINUE;
	}
	
	public static void main(String[] args) throws IOException {
		logger.trace("Starting to replace PR_qualcosa etc.");
		String sql_path=Initializer.getProperty("workingDir");
		logger.trace("Working on the directory "+sql_path);
		logger.trace("start to cycle over files");
		Fix_PR_XX_YY fix=new Fix_PR_XX_YY();
		Files.walkFileTree(new File(sql_path).toPath(), fix);
		logger.trace("Ended the replaces of \"PV_OUT_JOBNUM  NUMERIC DEFAULT FN_JOB_NUM;\"");
	}


}
