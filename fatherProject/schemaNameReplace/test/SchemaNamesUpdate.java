package test;
/**
 * Interface meant for the update of the names of the schemas in the new greenplum DB.
 * The mapping of the schema names is recovered from a config text
 * @author g.barri
 *
 */
public interface SchemaNamesUpdate {
	/**
	 * Metodo per l'update dei nomi degli schema presenti nel file.
	 * @param fileName il nome/path del file da modificare
	 */
	void updateNames(String fileName);
}
