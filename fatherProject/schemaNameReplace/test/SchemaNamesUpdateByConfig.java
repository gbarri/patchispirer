package test;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.StringTokenizer;

public class SchemaNamesUpdateByConfig implements SchemaNamesUpdate{

	private String configFile;
	public Map<String, String> replaceMap; 
	private static String delimiter="::";
	
	public String sql_path;
	public String config_path;
	
	public SchemaNamesUpdateByConfig(String sql_path, String  config_path, String configFile)  {
		this.configFile=configFile;
		this.config_path=config_path;
		this.sql_path=sql_path;
		replaceMap=new HashMap<>();
		loadMap();
	}
	
	public SchemaNamesUpdateByConfig(String sql_path, String  config_path) {
		this(sql_path,config_path,"config.txt");
	}
	
	
	public void updateNames(String fileName) {
		String fullText="";
		
		try(FileInputStream fileStream=new FileInputStream(sql_path+fileName);
		    Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
			Set<String> keys=replaceMap.keySet();
			while(scann.hasNextLine()) {
				String nextLine=scann.nextLine();
				if(!nextLine.startsWith("--")) {
					for(String keyItem:keys) {
						if(nextLine.contains(keyItem+".")) {
							nextLine=nextLine.replaceAll(keyItem+".",replaceMap.get(keyItem)+".");
						}
					}
				}
				fullText+=nextLine+"\n";
			}
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while reading/searching for "+fileName, e);
		}
		
		try(FileOutputStream fileOutStream=new FileOutputStream(sql_path+fileName); 
				PrintWriter print=new PrintWriter(new BufferedOutputStream(fileOutStream))){
			print.print(fullText);
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while writing/searching for "+fileName, e);
		}
		
	}
	
	/**
	 * function  che deve leggere da configFile e caricare la mappa delle sostituzioni
	 * 
	 */
	private void loadMap() {
		try(FileInputStream fileStream=new FileInputStream(config_path+configFile);
			Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
			while(scann.hasNextLine()) {
				String nextLine=scann.nextLine();
				StringTokenizer st=new StringTokenizer(nextLine, delimiter);
				if(st.countTokens()!=2) {
					throw new IllegalArgumentException("Config. file not in desired form");
				}
				replaceMap.put(st.nextToken(), st.nextToken());
			}
		}catch (FileNotFoundException e) {
			throw new RuntimeException("Config file missing");
		}
		catch (IOException e) {
			//TODO pheraps in case something goes wrong, simply the mapp reverts to identity?
			throw new RuntimeException("Exception raised while reading/searching for a config. file", e);
		}
	}
	
	/*
	public static void main(String[] args) throws FileNotFoundException {
		//for testing 
		SchemaNamesUpdateByConfig sc=new SchemaNamesUpdateByConfig();
		//System.out.println(sc.replaceMap);
		sc.updateNames("xxpm_dm_day_idx.sql");
		
	}
	*/

}
