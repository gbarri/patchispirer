package test;

import java.io.File;


public class starter {
	
	public static File[] loadFiles(File folder) {
		File[] listOfFiles = folder.listFiles();
		return listOfFiles;
	}
	
	public static void main(String[] args) {
		String sql_path="C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\EDWP2\\bl_scm_ind\\tables\\ft";
		String config_path="C:\\Users\\g.barri\\eclipse-workspace\\patcher\\ext\\";
		SchemaNamesUpdate schup=new SchemaNamesUpdateByConfig(sql_path+"\\", config_path);
		File folder = new File(sql_path);
		if(!folder.isDirectory()) {
			System.out.println(">>>>ERROR:\n\t"+sql_path+" is not a directory.");
		}
		File[] listOfFiles=loadFiles(folder);

		for (File file : listOfFiles) {
			//System.out.println(file.getName());
		    if (file.isFile() && file.getName().endsWith("sql")) {
		        schup.updateNames(file.getName());
		    }
		}
	}

}
