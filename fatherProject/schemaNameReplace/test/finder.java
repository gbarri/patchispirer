package test;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;


public class finder {
	
	public static String directory="C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\EDWP2\\bl_scm_ind\\tables\\ft";
	public static String key="INDEX";

	
	private static void srchIn(String filename) {
		try(FileInputStream fileStream=new FileInputStream(directory+"\\"+filename);
			Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
			while(scann.hasNextLine()) {
				String nextLine=scann.nextLine();
				if(nextLine.contains(key)) {
					String txt=directory+"\\"+filename+":\n\t"+nextLine;
					System.out.println(txt);
				}
			}
		}catch (FileNotFoundException e) {
			System.out.println("file "+directory+"\\"+filename+" missing.");
		}
		catch (IOException e) {
			//TODO pheraps in case something goes wrong, simply the mapp reverts to identity?
			throw new RuntimeException("Exception raised while reading/searching for "+directory+"\\"+filename+" file", e);
		}
	}
	
	private static void searchKeyInDirectory(){
		File folder = new File(directory);
		File[] listOfFiles=starter.loadFiles(folder);
		
		for (File file : listOfFiles) {
		    if (file.isFile() && file.getName().endsWith("sql")) {
		        srchIn(file.getName());
		    }
		}
		
	}
	
	public static void main(String[] args) {
		File folder = new File(directory);
		if(folder.isFile()) {
			srchIn(directory);
		}else if(folder.isDirectory()) {
			searchKeyInDirectory();
		}
	}

}
