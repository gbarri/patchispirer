package functions;

public class Pr_admin_constraint_check_restore extends InsertableFunction{
	
	/**
	 * schema di riferimento per la funzione
	 */
	public String schemaPackage="dg_utilities";
	/**
	 * nome della funzione
	 */
	public String functionName="pr_admin_constraint_check_restore";
	
	@Override
	public String getFunctionName() {
		return functionName;
	}
	
	public Pr_admin_constraint_check_restore() {
		tag="<|PR_FINALIZE-post|>";
		argnum=3;
	}
	
	@Override
	public String toString() {
		return this.getClass()+": funzione "+functionName+" con tag "+tag;
	}
	
	/*
	 * I tre parametri sono delle stringhe, pertanto sono inseriti tra apici.
	 */
	@Override
	protected String stringToBeInserted(String[] args) throws IllegalArgumentException {
		String outStr="PERFORM "+schemaPackage+"."+this.functionName+"("
						+ "'"+args[0].toLowerCase()+"', '"+args[1].toLowerCase()+"', ID_LOAD, '"+args[2].toLowerCase()+"');\n";
		return outStr;
	}
}
