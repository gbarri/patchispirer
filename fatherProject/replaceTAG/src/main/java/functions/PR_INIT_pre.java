package functions;
/**
 * deve modellizzare una funzione
 * @author g.barri
 *
 */
public class PR_INIT_pre extends InsertableFunction{
	
	public static final String name="pippoFun_pre";
	
	public PR_INIT_pre() {
		tag="<|PR_INITIALIZE-pre|>";
		argnum=3;
	}
	
	@Override
	public String toString() {
		return this.getClass()+": funzione "+name+" con tag "+tag;
	}
	
	@Override
	protected final String stringToBeInserted(String[] args) {
		String outStr="PERFORM "+name+"(";
		for(int i=0; i<args.length; i++) {
			outStr+=args[i];
			if(i<args.length-1) {
				outStr+=",";
			}
		}
		outStr+=");";
		return outStr;
	}

	@Override
	public String getFunctionName() {
		// TODO Auto-generated method stub
		return "Test function - PR_INIT_pre";
	}
	
}
