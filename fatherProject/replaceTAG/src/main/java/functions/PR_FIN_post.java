package functions;
/**
 * deve modellizzare una funzione
 * @author g.barri
 *
 */
public class PR_FIN_post extends InsertableFunction{
	
	public static final String name="pippoFun_POST";
	
	public PR_FIN_post() {
		tag="<|PR_FINALIZE-post|>";
		argnum=3;
	}
	
	@Override
	public String toString() {
		return this.getClass()+": funzione "+name+" con tag "+tag;
	}
	
	@Override
	protected final String stringToBeInserted(String[] args) {
		String outStr="PERFORM "+name+"(";
		for(int i=0; i<args.length; i++) {
			outStr+=args[i];
			if(i<args.length-1) {
				outStr+=",";
			}
		}
		outStr+=");";
		return outStr;
	}

	@Override
	public String getFunctionName() {
		// TODO Auto-generated method stub
		return "Test function - PR_FIN_post";
	}

}
