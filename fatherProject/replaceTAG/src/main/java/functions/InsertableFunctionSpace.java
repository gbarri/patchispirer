package functions;

import java.io.IOException;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.tinylog.Logger;

import loaders.Loader;


public class InsertableFunctionSpace{
	
	private InsertableFunction insFun;
	public Map<String, String[]> mapping;
	
	public Map<String, String[]> getMapping() {
		return mapping;
	}

	public void setMapping(Map<String, String[]> mapping) {
		this.mapping = mapping;
	}

	public InsertableFunctionSpace(InsertableFunction ins, Loader loader) throws EncryptedDocumentException, InvalidFormatException, IOException {
		this.insFun=ins;
		this.mapping=loader.loadMap();
	}
	
	public String stringToBeInserted(String PK) {
		if(PK.contains(".")) {
			String schema=PK.substring(0,PK.indexOf("."));
			String object=PK.substring(PK.indexOf(".")+1);
			PK=schema.toLowerCase()+"."+object.toUpperCase();
		}
		if(mapping.containsKey(PK)) {
			if(mapping.get(PK).length==getArgnum()) {
				Logger.trace("Associated "+PK+" with function "+insFun);
				return insFun.stringToBeInserted(mapping.get(PK));
			}else {
				throw new IllegalArgumentException("--! InsertableFunctionSpace: Number of supposed arguments for "+insFun.getFunctionName()+", "+insFun.getArgnum()+", does not match with the retrieved one, "+mapping.get(PK).length+".\nCheck the configuration file used for this function.");
			}
		}else {
			Logger.trace("PK "+PK+" not found. Swicthing to searching for "+PK+".%, %=anything else");
			boolean found=false;
			String output=null;
			for(String key:mapping.keySet()) {
//				System.out.println(key);
				if(key.startsWith(PK+".")) {
					Logger.trace("Associated "+key+" with function "+insFun);
					found=true;
					output=(output==null?"":output+"\n")+insFun.stringToBeInserted(mapping.get(key));
				}
			}
			if(found) {
				return output;
			}else {
				throw new IllegalArgumentException("--! InsertableFunctionSpace: the package "+PK+" is not a loaded key for function "+insFun.getFunctionName()+".");
			}
		}
		
	}
	
	public String getTag() {
		return insFun.getTag();
	}
	
	public int getArgnum() {
		return insFun.getArgnum();
	}
	
	public String getFunctionName() {
		return insFun.getFunctionName();
	}

	
}
