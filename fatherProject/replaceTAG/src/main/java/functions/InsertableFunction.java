package functions;

public abstract class InsertableFunction {
	protected String tag;
	protected int argnum;
	
	public String getTag() {
		return tag;
	}
	
	public int getArgnum() {
		return argnum;
	}
	
	public abstract String getFunctionName(); 
	
	protected abstract String stringToBeInserted(String[] args) throws IllegalArgumentException;
}
