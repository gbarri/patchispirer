package functions;

public class Pr_admin_format_check extends InsertableFunction {
	
	/**
	 * schema di riferimento per la funzione
	 */
	public String schemaPackage="dg_utilities";
	/**
	 * nome della funzione
	 */
	public String functionName="pr_admin_format_check";
	
	@Override
	public String getFunctionName() {
		return functionName;
	}
	
	public Pr_admin_format_check() {
		tag="<|PR_INITIALIZE-pre|>";
		argnum=3;
	}
	
	@Override
	public String toString() {
		return this.getClass()+": funzione "+functionName+" con tag "+tag;
	}
	
	/*
	 * I tre parametri sono delle stringhe, pertanto sono inseriti tra apici.
	 */
	@Override
	protected String stringToBeInserted(String[] args) throws IllegalArgumentException {
		String outStr="--  Check sui constraint:\n" + 
				"	  PERFORM "+schemaPackage+".pr_admin_format_check("
						+ "'"+args[0]+"', '"+args[1]+"', ID_LOAD, '"+args[2]+"');\n";
		return outStr;
	}

}
