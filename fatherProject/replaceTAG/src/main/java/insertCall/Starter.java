package insertCall;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.swing.text.html.HTMLEditorKit.InsertHTMLTextAction;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import functions.Pr_admin_constraint_check;
import functions.Pr_admin_constraint_check_restore;
import functions.Pr_admin_format_check;
import loaders.LoaderByColumnIndexesDBExcel;
import utilities.Initializer;
import utilities.SQLSearcher;
import utilities.SQLSearcher.SQLTypes;


public class Starter {
	
	private static InsertCaller insertCaller;
	static Logger logger=LoggerFactory.getLogger(Starter.class);
	
//	private static Logger getLogger(String filePath) throws SecurityException, IOException {
//		FileHandler fh=new FileHandler(filePath, true);
//		SimpleFormatter simFormat= new SimpleFormatter();
//		simFormat.format(new LogRecord(Level.ALL, "%1$tc %2$s%n%4$s: %5$s%6$s%n"));
//		fh.setFormatter(simFormat);
//		Logger logger=Logger.getLogger("insertCall.Starter");
//		logger.addHandler(fh);
//		return logger;
//	}
	
	/**
	 * metodo per stabilire se occorre procedere per un dato file o meno.
	 * Utile per stabilire la logica con cui si accede ad un file
	 * @param path
	 * @return se procedere
	 */
	private static boolean checkFile(File file) {
		boolean cond=true;
		cond=cond && file.getName().endsWith(".sql");
		cond=cond && !file.getName().startsWith(".");
		if(SQLTypes.FUNCTION!=SQLSearcher.detectType(file)) {
			cond=false;
		}
		return cond;
	}
	/**
	 * metodo per stabilire se occorre procedere per una data cartella o meno.
	 * Utile per stabilire la logica con cui si accede ad una cartella, o escludere certe cartelle.
	 * @param file
	 * @return
	 */
	private static boolean checkFolder(File folder) {
		boolean cond=true;
		cond=cond && !folder.getName().equals("_Ispirer");
		cond=cond && !folder.getName().startsWith("_");
		return cond;
	}
	
	/**
	 * carica i files di una directory. Per ora in un array di file. Possibili miglioramenti
	 * @param folder
	 * @return File array
	 */
	public static File[] loadFiles(File folder) {
		File[] listOfFiles = folder.listFiles();
		return listOfFiles;
	}
	
	/**
	 * metodo che deve scorrere i files da analizzare e richiama i vari metodi necessari
	 * @param path
	 */
	private static void goThrough(String path) {
		File folder = new File(path);
		if(!folder.isDirectory()) {
			if(folder.isFile() && checkFile(folder)) {
				File file=folder;
				folder=null;
				//metodo di update del file es: metodo di sostituzione
				insertCaller.insert(file);				
//				insertCaller.rollback(file);
			}
		}else if(folder.isDirectory() && checkFolder(folder)){
			File[] listOfFiles=loadFiles(folder);
			for (File file : listOfFiles) {
				goThrough(file.getAbsolutePath());
			}
		}
	}
	
	public static void main(String[] args) throws SecurityException, IOException, EncryptedDocumentException, InvalidFormatException {
		
		/*all*/
		String sql_path=Initializer.getProperty("workingDir");
		
		/*test folder outside repository*/
//		String sql_path="C:\\Users\\g.barri\\Desktop\\tmp";		
		insertCaller=new InsertCaller();
//		insertCaller.logger=getLogger(logPath);
		//registro gli oggetti rappresentativi di una certa tipologia di funzioni da inserire.
		logger.trace("Starting registration fase in TAG replecement");
		String loadPropertiesDBexcelConstraintPath=Initializer.getProperty("loadPropertiesDBexcel_ConstraintCheck");//"C:\\Users\\g.barri\\git\\repository\\replaceTAG\\src\\main\\resources\\LoaderByColmunIndexesDBExcel.properties";
		String loadPropertiesDBexcelConstraintName_format=Initializer.getProperty("loadPropertiesDBexcel_FormatCheck");//"C:\\Users\\g.barri\\git\\repository\\replaceTAG\\src\\main\\resources\\LoaderByColmunIndexesDBExcel_formatCheck.properties";		

		insertCaller.register(new Pr_admin_constraint_check(), new LoaderByColumnIndexesDBExcel(new File(loadPropertiesDBexcelConstraintPath))); 
		insertCaller.register(new Pr_admin_constraint_check_restore(), new LoaderByColumnIndexesDBExcel(new File(loadPropertiesDBexcelConstraintPath)));
		insertCaller.register(new Pr_admin_format_check(), new LoaderByColumnIndexesDBExcel(new File(loadPropertiesDBexcelConstraintName_format)));		
		logger.trace("Ending registration phase in TAG replecement");
		
		//body
		//insertCaller.printFunctionList();
		logger.trace("start TAG replecement");
		goThrough(sql_path);
		logger.trace("end TAG replecement");
		
	}

}
