package
insertCall;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.regex.Pattern;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import functions.InsertableFunction;
import functions.InsertableFunctionSpace;
import loaders.Loader;
import utilities.SQLSearcher;


public class InsertCaller {
	
	private List<InsertableFunctionSpace> functionsList=new ArrayList<>();
	public Map<String, List<InsertableFunctionSpace>> tagToMaps=new HashMap<>();
	public Logger logger=LoggerFactory.getLogger(InsertCaller.class);
	
//	public void printFunctionList() {
//		//TODO improve this shit
////		System.out.println(functionsList);
//	}
	
	/**
	 * Aggiunge all'elenco delle funzioni da inserire un nuovo elemento. 
	 * @param insFun classe rappresentante l'elemento da inserire
	 * @param loader funzione che carica da file gli argomenti dell'elemento da inserire
	 */
	public void register(InsertableFunction insFun, Loader loader) {
		try {
			InsertableFunctionSpace insFunSpace=new InsertableFunctionSpace(insFun, loader);
			functionsList.add(insFunSpace);
			if(tagToMaps.containsKey(insFunSpace.getTag())){
				tagToMaps.get(insFunSpace.getTag()).add(insFunSpace);
			}else {
				List<InsertableFunctionSpace> newList=new ArrayList<>();
				newList.add(insFunSpace);
				tagToMaps.put(insFunSpace.getTag(),newList);
			}
		}catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
			//System.out.println(">>>>The load from "+loader+" failed.\n>>>>Registration of "+insFun+" aborted");
			logger.info("The load from "+loader+" failed.\nRegistration of "+insFun+" aborted");
		}
	}
	
	
	/**
	 * logica di esclusione delle righe.
	 * Aggiungere condizioni che inidichino quali righe vanno salatate. Ad esempio, le righe di commento non sono prese in considerazione.
	 * @param line
	 * @return
	 */
	private static boolean checkLine(String line) {
		boolean cond=true;
		//escludo righe commentate MA non se contengono il tag
		cond=cond && !(line.trim().startsWith("--") && !(line.contains("<|PR_") || line.contains("<|USED_PR")));
		return cond;
	}
	
	/**
	 * Inserisce al posto dei tag registrati le corrispondenti funzioni
	 * @param file
	 */
	public void insert(File file) {
		logger.trace("Controlling file "+file.getName()+" ("+file.getAbsolutePath());
		String fullText="";
		String pckg=SQLSearcher.detectName(file);
		if(pckg==null) {
			logger.warn("we could not find the object name in file "+file.getName()+". ("+file.getAbsolutePath());
		}else {
			try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
					Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
				while(scann.hasNextLine()) {
					String nextLine=scann.nextLine();
					if(checkLine(nextLine)) {
						for(String tagItem:tagToMaps.keySet()) {
							if(nextLine.contains(tagItem)) {
								String toInsert="\n";
								int counter=tagToMaps.get(tagItem).size();
								for(InsertableFunctionSpace item:tagToMaps.get(tagItem)) {
									try {
										toInsert+="--##--\n"+item.stringToBeInserted(pckg)+"\n";
									}catch (IllegalArgumentException e) {
										logger.trace("No association found between\t"+pckg+"\tand function\t"+item.getFunctionName());
										counter--;
									}
								}
								if(counter>=1) {
									//the scanned line containing the tag is commented.
									String space=nextLine.replaceAll(tagItem+".*", "").replaceAll("[^\\s]", " ");
									//at least one insertion for this tag is correct, therefore the tag is commented and a new line inserted
									nextLine="--"+nextLine.replaceFirst("\\<\\|", "\\<\\|USED\\_")+toInsert.replaceAll("\\n", "\\\n"+space)+"--##--";
									logger.info("File "+file.getName()+" has been changed with the following TAG replacement:\n"+nextLine);
								}else {
									//all insertions failed, file is not touched
								}
							}
						}
					}
					fullText+=nextLine+"\n";
				}
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while reading/searching for \n\t"+file.getAbsolutePath(), e);
			}
			try(FileOutputStream fileOutStream=new FileOutputStream(file.getAbsolutePath()); 
					PrintWriter print=new PrintWriter(new BufferedOutputStream(fileOutStream))){
				print.print(fullText);
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while writing/searching for \n\t"+file.getAbsolutePath(), e);
			}
		}
		
	}
	
	/**
	 * Inserisce al posto dei tag registrati le corrispondenti funzioni
	 * @param file
	 */
	public void rollback(File file) {
		logger.trace("Controlling file "+file.getName()+" ("+file.getAbsolutePath());
		String fullText="";
		String pckg=SQLSearcher.detectName(file);
		if(pckg==null) {
			logger.warn("we could not find the object name in file "+file.getName()+". ("+file.getAbsolutePath());
		}else {
			try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
					Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
				while(scann.hasNextLine()) {
					String nextLine=scann.nextLine();
					if(checkLine(nextLine)) {
						for(String tagItem:tagToMaps.keySet()) {
							if(nextLine.replaceAll("\\<\\|USED\\_", "\\<\\|").contains(tagItem)) {
								String toInsert="";
								int counter=tagToMaps.get(tagItem).size();
								for(InsertableFunctionSpace item:tagToMaps.get(tagItem)) {
									try {
										toInsert+=item.stringToBeInserted(pckg);
									}catch (IllegalArgumentException e) {
										counter--;
									}
								}
								if(counter>=1) {
									//the scanned line containing the tag is commented.
									String tmpLine=nextLine+"\n";
									String oldLine=nextLine.replaceFirst("--", "").replaceFirst("\\<\\|USED\\_", "\\<\\|");
									for(int delimiters=0; delimiters<counter+1; ) {
										nextLine=scann.nextLine();
										tmpLine+=nextLine+"\n";
										if(nextLine.contains("--##--")) {
											delimiters++;
										}
									}
									//at least one insertion for this tag is correct, therefore the tag is commented and a new line inserted
									nextLine=oldLine;
									logger.info("File "+file.getName()+" has been changed with the following rollback: from\n"+tmpLine+"to\n"+nextLine);
								}else {
									//all insertions failed, file is not touched
								}
							}
						}
					}
					fullText+=nextLine+"\n";
				}
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while reading/searching for \n\t"+file.getAbsolutePath(), e);
			}
			try(FileOutputStream fileOutStream=new FileOutputStream(file.getAbsolutePath()); 
					PrintWriter print=new PrintWriter(new BufferedOutputStream(fileOutStream))){
				print.print(fullText);
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while writing/searching for \n\t"+file.getAbsolutePath(), e);
			}
		}
		
	}
	
}
