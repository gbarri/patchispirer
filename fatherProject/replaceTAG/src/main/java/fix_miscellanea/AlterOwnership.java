/**
 * 
 */
package fix_miscellanea;

import java.sql.SQLType;

import functionUtilities.CallAlteration;
import functionUtilities.FunCallAlterer;
import functionUtilities.FunctionCall;
import utilities.SQLSearcher;
import utilities.SQLSearcher.SQLTypes;
import utilities.StringUtilities;

/**
 * @author g.barri
 *
 */
public class AlterOwnership implements CallAlteration {
	public SQLTypes typeOfFile=null;  
	public String nameObject;
	public static String targetRole="bigdwh_role";
	/**
	 * 
	 */
	public AlterOwnership(SQLTypes type, String nameObject) {
		this.typeOfFile=type;
		this.nameObject=nameObject;
	}

	public void setTypeOfFile(SQLTypes type) {
		this.typeOfFile=type;
	}
	
	public void setTypeObjectName(String objecectName) {
		this.nameObject=objecectName;
	}
	
	/* (non-Javadoc)
	 * @see functionUtilities.CallAlteration#alterCall(java.lang.String, int[], functionUtilities.FunCallAlterer)
	 */
	@Override
	public String alterCall(String str, int[] extrema, FunCallAlterer funCallAlterer) {
		if(this.typeOfFile==null) {
			throw new IllegalStateException("You called the method alterCall of ALterOwnership without setting the SQLType of the file!");
		}
		String addedClause="";
		switch(typeOfFile) {
			case TABLE:
			case VIEW:
			case SEQUENCE:
				addedClause="ALTER TABLE "+StringUtilities.formatObjectName(this.nameObject)+" OWNER TO "+targetRole;
				break;
				
			case FUNCTION:
				//TODO da sistemare, perchè della fucntion mi serve schema.nome e anche i datatypes
//				addedClause="ALTER FUNCTION "+StringUtilities.formatObjectName(this.nameObject)+"() OWNER TO "+targetRole;
//				throw new UnsupportedOperationException("Still developing");
//				break;
			case CONSTRAINT:
				//do nothing
				;
				break;
			default: 
			throw new IllegalStateException("You called the method alterCall of ALterOwnership without setting a known SQLType of the file!");
		}
		return str+"\r\n"+addedClause;
	}

	/* (non-Javadoc)
	 * @see functionUtilities.CallAlteration#visit(functionUtilities.FunctionCall)
	 */
	@Override
	public void visit(FunctionCall functionCall) {
		// TODO Auto-generated method stub

	}

}
