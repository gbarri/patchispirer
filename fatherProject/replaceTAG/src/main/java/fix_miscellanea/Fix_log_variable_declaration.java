package fix_miscellanea;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import loaders.*;
import schemaMapping.LoaderSchemaMap;
import utilities.Initializer;
import utilities.SQLSearcher;
import utilities.SQLSearcher.SQLTypes;

public class Fix_log_variable_declaration extends SimpleFileVisitor<Path>{

static Logger logger=LoggerFactory.getLogger(Fix_log_variable_declaration.class);
private Map<String, String> procedure2chain;
	
	public Fix_log_variable_declaration() {
//		Loader loader=new LoaderByColumnIndex("C:\\Users\\g.barri\\git\\repository\\replaceTAG\\src\\main\\resources\\Copia di EDWP - SupplyChain & Manufacturing (1).xlsx",
//											   new File("C:\\Users\\g.barri\\git\\repository\\replaceTAG\\src\\main\\resources\\Fix_job_variable_declaration.properties"));
		Loader loader=new LoaderByColumnIndexOrName(Initializer.getProperty("excelMappingWithChains"),new File(Initializer.getProperty("loadPropertiesexcelMappingWithChains")));
		
		try {
			
			Map<String, String[]> tmpMap=loader.loadMap();
			this.procedure2chain=new HashMap<>();
			for(String key:tmpMap.keySet()) {
				if(tmpMap.get(key)[1].trim().equals("YES")) {
					this.procedure2chain.put(key, tmpMap.get(key)[0].trim());
				}
			}
		} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
			logger.error("failed to load from oggetti_catene_associate.xlsx", e);
			e.printStackTrace();
		}
	}
	
	@Override
	public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException{
		String text="";
		File file=path.toFile();
		if(!file.getName().endsWith(".sql") || SQLSearcher.detectType(file)!=SQLTypes.FUNCTION) {
			return FileVisitResult.CONTINUE;
		}
		try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
				Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
			String PV_JOB_CATENA_ID=null;
			String PV_RTA_LOB_NAME=null;
			while(scann.hasNextLine()) {
				String nextLine=scann.nextLine();
//				System.out.println(nextLine);
				if(checkLine(nextLine)) {
					if(nextLine.trim().matches("PV_JOB_CATENA_ID\\s+\\:\\=.*\\'.*\\'.*\\;")) {
						//modifiche a nextLine
						String oldLine=nextLine;
						//recupera l'associazione MP<-->catena dal file excell di Nico
						String tmp=this.procedure2chain.get(SQLSearcher.detectName(file));
						if(tmp!=null) {
							PV_JOB_CATENA_ID=tmp;
						}else {
							logger.warn("Chain of "+SQLSearcher.detectName(file)+" not found!");
							PV_JOB_CATENA_ID="NOT_FOUND";
						}
						//
						int pointsEqual=nextLine.indexOf(":=")+1;
						int firstApex=nextLine.indexOf("'", pointsEqual);
						int secondApex=nextLine.indexOf("'", firstApex+1);
						PV_RTA_LOB_NAME=nextLine.substring(firstApex+1,secondApex);
						nextLine="--"+oldLine+"\r\n"+nextLine.substring(0, nextLine.indexOf(":=")+2)+" '"+PV_JOB_CATENA_ID+"';";
						PV_JOB_CATENA_ID=null;
					}
					if(PV_RTA_LOB_NAME!=null && nextLine.trim().matches("PV_RTA_LOB_NAME\\s+\\:\\=.*\\'.*\\'.*\\;")) {
						String oldLine=nextLine;
						nextLine="--"+oldLine+"\r\n"+nextLine.substring(0, nextLine.indexOf(":=")+2)+" '"+PV_RTA_LOB_NAME+"';";
						PV_RTA_LOB_NAME=null;
					}
				}
				text+=nextLine+"\r\n";
			}
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while reading for\t"+file.getAbsolutePath(), e);
		}
		
		try(FileOutputStream fileOutStream=new FileOutputStream(file.getAbsolutePath()); 
				PrintWriter print=new PrintWriter(new BufferedOutputStream(fileOutStream))){
			print.print(text);
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while writing\n\t"+file.getAbsolutePath(), e);
		}
		return FileVisitResult.CONTINUE;
	}
	
	@Override
	public FileVisitResult visitFileFailed(Path file,
            IOException exc)
              throws IOException{
		logger.error("intercepted exception "+exc.getClass()+": "+exc.getMessage());
		return FileVisitResult.CONTINUE;
	}
	
	private boolean checkLine(String nextLine) {
		boolean cond=!nextLine.trim().startsWith("--");
		return cond;
	}

	public static void main(String[] args) throws IOException {
		logger.trace("Starting to fix the values assigned to PV_JOB_CATENA_ID and PV_RTA_LOB_NAME");
		String sql_path=Initializer.getProperty("localRepositoryDir");//"C:\\Users\\g.barri\\Desktop\\wrk\\XXDWHAPP_MP_ST_CAUSALI_MDR";
		logger.trace("Working on the directory "+sql_path);
		logger.trace("start to cycle over files");
		Fix_log_variable_declaration fix=new Fix_log_variable_declaration();
		Files.walkFileTree(new File(sql_path).toPath(), fix);
		logger.trace("Ended the replaces of the values assigned to PV_JOB_CATENA_ID and PV_RTA_LOB_NAME");
	}

}
