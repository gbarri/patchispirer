package fix_miscellanea;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import functionUtilities.CallAlteration;
import utilities.Initializer;
import utilities.SQLSearcher;
import utilities.SQLSearcher.SQLTypes;
import utilities.StringUtilities;

public class AlterObjectOwnership extends SimpleFileVisitor<Path> {
	static Logger logger=LoggerFactory.getLogger(AlterObjectOwnership.class);
	public AlterOwnership alter;
	public Scanner scann;
	private Stack<Integer> commentIndex;
	private Stack<String>  commentText;
	private String text;

	public AlterObjectOwnership() {
	}
	
	public Scanner getScann() {
		return scann;
	}
	public void setScann(Scanner scann) {
		this.scann = scann;
	}
	
	/**
	 * aggiunge una riga di analisi su richiesta
	 * @return
	 */
	private boolean askForNewLine() {
		if(scann.hasNextLine()) {
			text=initLine(text+scann.nextLine()+"\n");
			return true;
		}else {
			return false;
		}
	}
	
	private String initLine(String nextLine) {
		if(this.commentIndex==null) {
			this.commentIndex=new Stack<>();
		}
		if(this.commentText==null) {
			this.commentText=new Stack<>();
		}
		int indexOfLineComment=0, indexOfBlockComment=0;
		indexOfLineComment=nextLine.indexOf("--");
		indexOfBlockComment=nextLine.indexOf("/*");
		while(indexOfLineComment!=-1 || indexOfBlockComment!=-1) {
			if(indexOfLineComment==-1) {
				indexOfLineComment=nextLine.length()+1;
			}
			if(indexOfBlockComment==-1) {
				indexOfBlockComment=nextLine.length()+1;
			}
			if(indexOfLineComment<indexOfBlockComment) {
				int ind2=nextLine.indexOf("\r\n", indexOfLineComment+1);
				if(ind2==-1) {
//					System.out.println(nextLine.replace("\r\n", "\\r\\n\r\n"));
					ind2=nextLine.indexOf("\n", indexOfLineComment+1);
				}
				if(ind2==-1) {
					ind2=nextLine.length();
				}
//				System.out.println(ind2);
				commentIndex.push(indexOfLineComment);
				commentText.push(nextLine.substring(indexOfLineComment,ind2));
				nextLine=nextLine.replaceFirst("--.*", "");
			}else if(indexOfBlockComment<indexOfLineComment) {
				int ind2=nextLine.indexOf("*/", indexOfBlockComment+2);
				while(ind2==-1) {
					nextLine+=scann.nextLine()+"\n";
					ind2=nextLine.indexOf("*/", indexOfBlockComment+2);
				}
				commentIndex.push(indexOfBlockComment);
				commentText.push(nextLine.substring(indexOfBlockComment,ind2+2));
				nextLine=nextLine.substring(0,indexOfBlockComment)+""+nextLine.substring(ind2+2);
			}
			indexOfLineComment=nextLine.indexOf("--");
			indexOfBlockComment=nextLine.indexOf("/*");
		}
		return nextLine;
	}
	
	private String deinitLine(String text) {
		int[] tmp={};
		return deinitLine(text, tmp);
	}
	
	private String deinitLine(String text, int[] extrema) {
		while(!commentIndex.isEmpty()) {
			Integer index=commentIndex.pop();
			String comment=commentText.pop();
			for(int i=0; i<extrema.length; i++) {
				if(extrema[i]>=index) {
					extrema[i]=extrema[i]+comment.length();
				}
			}
			String firstPart=text.substring(0, index);
			String secondPart=text.substring(index);
			text=firstPart+comment+secondPart;
		}
		return text;
	}
	
	private int firstDiff(String text2, String alteredText) {
		int i;
		for(i=0; i<Math.min(text2.length(), alteredText.length()); i++) {
			if(text2.charAt(i)!=alteredText.charAt(i)) {
				break;
			}
		}
		return i; 
	}
	
	/**
	 * Function that searches for the string CREATE ... (...)
	 * 
	 **/
	protected int[] detectObjectDefSubString(int post) {
		//TODO must be completed
		//NEED: for each type of object, a way to define when a definition ends
		return null;
	}
	
	private String alterLine(String nextLine, String objectName) {
		int indFunction=0;
		//initialize text
		while(indFunction<this.text.length()) {
			int[] extrema=detectObjectDefSubString(indFunction);
			if(extrema==null) {
				//did not found any reference to function
				indFunction=this.text.length();
			}else {
				//found a call to function
				//restore comment & update extrema indexes
				String tmpStr=deinitLine(this.text, extrema);
				//modify text
//				this.alter.setTypeObjectName(objectName);
//				this.alter.setTypeOfFile(SQLSearcher.detectType(currentFile));
				tmpStr=this.alter.alterCall(tmpStr, extrema, null);
				//remove comments
				this.commentIndex=null;
				this.commentText=null;
				tmpStr=initLine(tmpStr);
				//compare 
				indFunction=firstDiff(this.text, tmpStr);
				//update this.text
				this.text=tmpStr;
			}
		}
		//text is now completely replaced
		//System.out.println(this.text);
		nextLine=deinitLine(this.text);
		return nextLine;
	}
	
	public String alterFunctionCalls(String objectName) {
		String fullText="";
		this.text="";
		while(askForNewLine()) {
			if(checkLine(this.text)) {
				text=alterLine(text, objectName);
			}
			fullText+=text;
			this.text="";
		}
		return fullText;
	}
	
	private boolean checkLine(String text2) {
		return true;
	}
	
	@Override
	public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException{
		logger.trace("visiting "+path.toString());
		File file=path.toFile();
		if(checkFile(file)) {
			String objectName=SQLSearcher.detectName(file);
			objectName=StringUtilities.formatObjectName(objectName);
			this.alter=new AlterOwnership(SQLSearcher.detectType(file), objectName);
			String textFile="";
			try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
				Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
				this.scann=scann;
				textFile=alterFunctionCalls(objectName);				
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while reading for\t"+file.getAbsolutePath(), e);
			}
			
			try(FileOutputStream fileOutStream=new FileOutputStream(file.getAbsolutePath()); 
					PrintWriter print=new PrintWriter(new BufferedOutputStream(fileOutStream))){
				print.print(textFile);
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while writing\n\t"+file.getAbsolutePath(), e);
			}
		}
		return FileVisitResult.CONTINUE;
	}

	private boolean checkFile(File file) {
		boolean toDo=file.getName().endsWith(".sql");
		toDo=toDo && SQLTypes.TABLE.equals(SQLSearcher.detectType(file));
		if(toDo) {
			try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
				Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
				boolean WITHfound=false;
				while(scann.hasNextLine() && !WITHfound) {
					this.commentIndex=null;
					this.commentText=null;
					String line=scann.nextLine().replaceFirst("--.*", "").replaceAll("\\/\\*.*\\*\\/", "");
					WITHfound=line.matches(".*WITH\\s*\\(.*") || line.matches(".*with\\s*\\(.*");
				}
				toDo=!WITHfound;				
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while reading for\t"+file.getAbsolutePath(), e);
			}
		}
		this.commentIndex=null;
		this.commentText=null;
		return toDo;
	}
	
	public static void main(String[] args) throws IOException {
		logger.trace("Starting altering objects ownership to "+AlterOwnership.targetRole);
		Path path=new File(Initializer.getProperty("workingDir")).toPath();
		AlterObjectOwnership add=new AlterObjectOwnership();
		Files.walkFileTree(path,add);
		logger.trace("Ending altering objects ownership to "+AlterOwnership.targetRole);
	}
	
}
