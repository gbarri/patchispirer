package fix_miscellanea;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import functionUtilities.CallAlteration;
import functionUtilities.FunCallAlterer;
import functionUtilities.FunctionCall;
import loaders.Loader;
import loaders.LoaderByColumnIndex;
import schemaMapping.LoaderSchemaMap;
import utilities.Initializer;

public class AddStorageParameter implements CallAlteration {
	private String objectName;
	private Scanner scanner;
	static Logger logger=LoggerFactory.getLogger(AddStorageParameter.class);
	private Map<String, List<String>> inverseMap;
	private Map<String, Boolean> tableSizeMap;
	
	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}
	
	public Scanner getScanner() {
		return scanner;
	}

	public void setScanner(Scanner scanner) {
		this.scanner = scanner;
	}

	public AddStorageParameter() {
		objectName=null;
		String sizeFilePath=Initializer.getProperty("QlinkExtractionTableSize");
		if(sizeFilePath==null) {
			logger.error("Unable to load list of append only tables");
			throw new IllegalArgumentException("Unable to load list of append only tables");
		}
		File sizeFile=new File(sizeFilePath);
		Loader load=new LoaderByColumnIndex(sizeFile, "Loader.useColumnsAt=3\r\n" + 
												  	  "Loader.useSheetAt=0\r\n" + 
												  	  "Loader.usePKAt=0,1");
		Map<String, String[]> tmpMap=new HashMap<>();
		try {
			tmpMap=load.loadMap();
		} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
			logger.error("Failed to load properly map "+sizeFile.getAbsolutePath(), e);
			e.printStackTrace();
		}
		tableSizeMap=new HashMap<>();
		Iterator<String> iter=tmpMap.keySet().iterator();
		while(iter.hasNext()) {
			String key=iter.next();
			String val=tmpMap.get(key)[0];
			try {
				Boolean valNum=Boolean.parseBoolean(val);
				tableSizeMap.put(key, valNum);
			}catch (NumberFormatException e) {
				logger.error("entry\t"+key+"\tin excel\t"+sizeFile.getAbsolutePath()+"\tsi not a parsable integer\t"+val+"\tLine is skypped");
			}
		}
		inverseMap=new LoaderSchemaMap().loadInverseMap();
	}
	
	@Override
	public String alterCall(String str, int[] extrema, FunCallAlterer fun) {
		List<String> oracleObjects=inverseMap.get(objectName);
		if(oracleObjects==null || oracleObjects.size()==0) {
			logger.error(objectName+" has not been modified because its former Oracle name was not found");
			return str;
		}
		Boolean appendOnly=false;
		for(String oracleTab:oracleObjects) {
			Boolean appOnlyOracle=this.tableSizeMap.get(oracleTab);
			if(appOnlyOracle!=null) {
				appendOnly=appendOnly||appOnlyOracle;
			}else {
				logger.warn("Tried to check if "+oracleTab+", now "+objectName+" has to be append only, but it is not in the list of table sizes");
			}
		}
		if(appendOnly) {
			String newStr=insertWithClause(str, extrema[1]);
			return newStr;
		}else {
			return str;
		}
	}

	private String insertWithClause(String str, int endindex) {
		String firstHalf=str.substring(0, endindex+1);
		String withClause="WITH ( appendonly=true,blocksize=32768,compresstype=zlib,compresslevel=3,checksum=true,orientation=row )";
		//String secondHalf=str.substring(endindex+1);
		int indDistributed=StringUtils.indexOfIgnoreCase(str, "DISTRIBUTED",endindex+1);
		while(indDistributed==-1 && scanner.hasNextLine()) {
			String nextLine=scanner.nextLine()+"\r\n";
			//manca la corretta gestione dei commenti!!
			//TODO creare classi apposite che fanno rimozione/inserimento dei commenti
			str+=nextLine;
			indDistributed=StringUtils.indexOfIgnoreCase(str, "DISTRIBUTED",endindex+1);
		}
		if(indDistributed!=-1) {
			String secondHalf=str.substring(indDistributed);
			return firstHalf+"\r\n"+withClause+"\r\n"+secondHalf;
		}else {
			IllegalStateException exc= new IllegalStateException("There's no DISTRIBUTED BY (...) or DISTRIBUTED RANDOLMLY clause in this table generation. Make sure to add it!!");
			logger.error("Found error in create table ddl", exc);
			throw exc;
		}
	}

	@Override
	public void visit(FunctionCall functionCall) {
		// TODO Auto-generated method stub
		
	}

}
