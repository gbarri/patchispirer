package fix_miscellanea;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;
import java.util.regex.Pattern;

import org.apache.poi.util.SystemOutLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fix_PR_XX.Fix_PR_XX_YY;
import fix_PR_XX.PR_ANALYZE;
import fix_PR_XX.PR_TRUNCATE;
import fix_PR_XX.PR_formatException;
import net.sf.jsqlparser.parser.TokenMgrException;
import utilities.Initializer;
import utilities.SQLSearcher;
import utilities.StringUtilities;
import utilities.SQLSearcher.SQLTypes;

public class Fix_trim_add_NULLIF extends SimpleFileVisitor<Path>{

	static Logger logger=LoggerFactory.getLogger(Fix_FN_JOB_NUM.class);
	private List<Integer> ignoreTRIM;
	private Scanner scann; //scanner che legge text
	private String text;
	private Stack<Integer> commentIndex;
	private Stack<String>  commentText;
	
	public Fix_trim_add_NULLIF() {
	}
	
	/**
	 * aggiunge una riga di analisi su richiesta
	 * @return
	 */
	private boolean askForNewLine() {
		if(scann.hasNextLine()) {
			text+=initLine(scann.nextLine()+"\n");
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * only modify files for which checkFile is true
	 * @param file
	 * @return
	 */
	private boolean checkFile(File file) {
		boolean cond=file.getName().endsWith(".sql");
		return cond;
	}
	
	/**
	 * only modify lines for which checkLine is true
	 * @param line
	 * @return
	 */
	private boolean checkLine(String line) {
		return !line.trim().startsWith("--");
	}
	
	
	
	/**
	 * aggiungo esternamente a una TRIM già individuata, senza problemi etc. la call a nullif
	 * @param line
	 * @param extrema
	 * @return
	 */
	public String alterTrimCall(String line, int[] extrema) {
		String endLine=line.substring(extrema[1]+1);
		String newLine=line.substring(0,extrema[0])+"NULLIF("+line.substring(extrema[0],extrema[1]+1)+",'')"+endLine;
		logger.trace("replacing:\n"+line+"\n"+newLine);
		List<Integer> updateIgnore=new ArrayList<>();
		for(Integer i:this.ignoreTRIM) {
			int newI=i;
			if(i>extrema[0]) {
				newI+="NULLIF(".length();
			}
			if(i>extrema[1]) {
				newI+=",'')".length();
			}
			updateIgnore.add(newI);
		}
		this.ignoreTRIM=updateIgnore;
		Stack<Integer> newCommentIndex=new Stack<>();
		for(int i=0; i<commentIndex.size(); i++) {
			Integer el=commentIndex.get(i);
			if(el>extrema[0]) {
				el+="NULLIF(".length();
			}
			if(el>extrema[1]) {
				el+=",'')".length();
			}
			newCommentIndex.add(newCommentIndex.size(), el);
		}
		this.commentIndex=newCommentIndex;
		return newLine;
	}

	
	/**
	 * selezione esattamente il blocco della prima chiamata alla function objectFun dopo il carattere numero post
	 * va in errore se non contiene l'oggetto objectFun
	 * @return
	 */
	private int[] detectFunctionCallSubString(String objectFun, int post) {
		int[] outExtrema=new int[2];
		int start=text.indexOf(objectFun, post);
		if(objectFun.equals("TRIM")) {
			//escludo che si tratti di LTRIM o RTRIM
			while(start>0 && (text.charAt(start-1)=='L' || text.charAt(start-1)=='R')) {
				//it is a (L/R)TRIM skip over
				start=text.indexOf(objectFun, start+"TRIM".length());
			}
		}
		if(objectFun.equals("trim")) {
			//escludo che si tratti di LTRIM o RTRIM
			while(start>0 && (text.charAt(start-1)=='l' || text.charAt(start-1)=='r')) {
				//it is a (L/R)TRIM skip over
				start=text.indexOf(objectFun, start+"trim".length());
			}
		}
		if(start==-1) {
			//no objectFunction found
			return null;
		}
		//search for the end
		int end=text.indexOf("(",start+objectFun.length());
		int count=end==-1?0:1;
		while(count==0) {
			if(askForNewLine()) {
				end=text.indexOf("(",start+objectFun.length());
				count=end==-1?0:1;
			}else {
				break;
			}
		}
		if(count==0) {
			//there is a mistake
			//probably a TRIM is not the TRIM function. We did not find any
			return null;
		}
//		System.out.println(text.substring(post));
//		System.out.println(start+objectFun.length()+1+","+ end);
//		String tmp=text.substring(start+objectFun.length()+1, end);
		if(start+objectFun.length()<end) {
//			String tmpStr=text.substring(start+objectFun.length(), end);
			if(!Pattern.matches("\\s*", text.substring(start+objectFun.length(), end))) {
				//it is a mismatch, not a call to trim function! Redo starting from after this point
				return detectFunctionCallSubString(objectFun, start+objectFun.length());
			}
		}

		while(count!=0 ) {
			int val=1;
			if(end<text.length()-1){
				Character c=text.charAt(++end);
				if(c=='\'') {
					//assuming val assumes either 0 or 1 values, when encountering \' switch val between 0 and 1.
					val=val==1?0:1;
				}
				count+=(c==')'?-val:(c=='('?val:0));
			}else {
				if(askForNewLine()) {
					
				}else {
					break;
				}
			}
		}if(count!=0) {
			//there is a mistake
			//probably a TRIM is not the TRIM function. We did not find any
			return null;
		}
		outExtrema[0]=start;
		outExtrema[1]=end;
		return outExtrema;
	}
	
	/**
	 * selezione esattamente il blocco della prima chiamata alla function objectFun dopo il carattere numero post
	 * va in errore se non contiene l'oggetto objectFun
	 * @return
	 */
	private int[] detectFunctionCallSubString(String text, String objectFun, int post) {
		int[] outExtrema=new int[2];
		int start=text.indexOf(objectFun, post);
		if(start==-1) {
			//no objectFunction found
			return null;
		}
		//search for the end
		int end=text.indexOf("(",start+objectFun.length());
		int count=end==-1?0:1;
		if(count==0) {
			//there is a mistake
			//probably a TRIM is not the TRIM function. We did not find any
			return null;
		}
		while(count!=0 ) {
			if(end<text.length()-1){
				Character c=text.charAt(++end);
				count+=(c==')'?-1:(c=='('?1:0));
			}else {
				break;
			}
		}if(count!=0) {
			//there is a mistake
			//probably a TRIM is not the TRIM function. We did not find any
			return null;
		}
		outExtrema[0]=start;
		outExtrema[1]=end;
		return outExtrema;
	}
	
	/**
	 * avendo la stringa esattamente di una chiamata ad una function NULLIF(%,%), controlla se 
	 * si tratta di una chiamata della forma NULLIF(TRIM(%),'')
	 * @param str stringa esattamente di una chiamata ad una function NULLIF(%,%)
	 * @return true se è della forma NULLIF(TRIM(%),'')
	 */
	private boolean checkNullifTrim(String str, String function) {
		int[] extrema=new int[2];
		extrema=detectFunctionCallSubString(str, function, str.indexOf("("));
		if(extrema==null) {
			return false;
		}
		str=str.replace(str.substring(extrema[0],extrema[1]+1), "");
		if(str.matches("NULLIF[\\s\\r\\n]*\\([\\s\\r\\n]*,[\\s\\r\\n]*''[\\s\\r\\n]*\\).*")) {
			return true;
		}else {
			return false;
		}
	}
	
	private String initLine(String nextLine) {
		if(this.commentIndex==null) {
			this.commentIndex=new Stack<>();
		}
		if(this.commentText==null) {
			this.commentText=new Stack<>();
		}
		//remove --comments
		while(nextLine.indexOf("\\-\\-")!=-1) {
			int ind=nextLine.indexOf("\\-\\-");
			int ind2=nextLine.indexOf("\r\n", ind+2);
			commentIndex.push(ind);
			commentText.push(nextLine.substring(ind,ind2+1));
			nextLine=nextLine.replaceFirst("\\-\\-.*", "\r\n");
		}
		//remove /* */ comments
		while(nextLine.indexOf("/*")!=-1) {
			int ind=nextLine.indexOf("/*");
			int ind2=nextLine.indexOf("*/", ind+2);
			while(ind2==-1) {
				nextLine+=scann.nextLine()+"\r\n";
				ind2=nextLine.indexOf("*/", ind+2);
			}
			commentIndex.push(ind);
			commentText.push(nextLine.substring(ind,ind2+2));
//			String f=nextLine.substring(0,ind);
//			String s=nextLine.substring(ind2+2);
			nextLine=nextLine.substring(0,ind)+" "+nextLine.substring(ind2+2);
		}
		return nextLine;
	}
	
	private String deinitLine(String text) {
		while(!commentIndex.isEmpty()) {
			Integer index=commentIndex.pop();
			String comment=commentText.pop();
			String firstPart=text.substring(0, index);
			String secondPart=text.substring(index+1);
			text=firstPart+comment+secondPart;
		}
		return text;
	}
	
private String replaceTRIM(String nextLine, String function) {
	this.text=null;
	int indTRIM=0;
	int indNULLIF=0;
	this.ignoreTRIM=new ArrayList<>();
	this.commentIndex=new Stack<>();
	this.commentText=new Stack<>();
	//initialize text
	this.text=initLine(nextLine);
	while(indTRIM<this.text.length()) {
		int[] extrema=detectFunctionCallSubString(function, indTRIM);
		if(extrema==null) {
			//did not found any TRIM
			indTRIM=this.text.length();
		}else {
			//found a TRIM
			while(extrema[1]>=indNULLIF) {
				int[] ex=detectFunctionCallSubString("NULLIF", indNULLIF);
				if(ex==null) {
					//advance index until end of text
					indNULLIF=this.text.length();
				}else {
					if(checkNullifTrim(this.text.substring(ex[0], ex[1]+1), function)) {
						//add to register index of trim of this nullifTrim sentence
						int[] tmp=detectFunctionCallSubString(this.text.substring(ex[0], ex[1]+1), function, 0);
						this.ignoreTRIM.add(ex[0]+tmp[0]);
					}
					//advance index
					indNULLIF=ex[0]+"NULLIF".length();
				}	
			}
			if(!StringUtilities.contains(ignoreTRIM, extrema[0])) {
				//modify
				this.text=alterTrimCall(this.text, extrema);
				//advance index
				indTRIM=extrema[0]+function.length()+"NULLIF(".length();
			}else {
				//advance index
				indTRIM=extrema[0]+function.length();
			}
		}
	}
	//text is now completely replaced
	this.text=deinitLine(this.text);
	//System.out.println(this.text);
	nextLine=this.text;
	return nextLine;
}
	
	@Override
	public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException{
		logger.trace("visiting "+path.toString());
		File file=path.toFile();
		if(checkFile(file)) {
			String textFile="";
			try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
				Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
				this.scann=scann;
				while(scann.hasNextLine()) {
					String nextLine=scann.nextLine()+"\r\n";
					if(checkLine(nextLine)) {
						nextLine=replaceTRIM(nextLine, "LTRIM");
						nextLine=replaceTRIM(nextLine, "ltrim");
						nextLine=replaceTRIM(nextLine, "RTRIM");
						nextLine=replaceTRIM(nextLine, "rtrim");
						nextLine=replaceTRIM(nextLine, "TRIM");
						nextLine=replaceTRIM(nextLine, "trim");
					}
					textFile+=nextLine;					
				}
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while reading for\t"+file.getAbsolutePath(), e);
			}
			
			try(FileOutputStream fileOutStream=new FileOutputStream(file.getAbsolutePath()); 
					PrintWriter print=new PrintWriter(new BufferedOutputStream(fileOutStream))){
				print.print(textFile);
			}catch (IOException e) {
				throw new RuntimeException("Exception raised while writing\n\t"+file.getAbsolutePath(), e);
			}
		}
		return FileVisitResult.CONTINUE;
	}
	


	/*	public static void main(String[] args) throws IOException {
		logger.trace("Starting to replace PR_qualcosa etc.");
		String sql_path=Initializer.getProperty("workingDir");
		logger.trace("Working on the directory "+sql_path);
		logger.trace("start to cycle over files");
		Fix_PR_XX_YY fix=new Fix_PR_XX_YY();
		Files.walkFileTree(new File(sql_path).toPath(), fix);
		logger.trace("Ended the replaces of \"PV_OUT_JOBNUM  NUMERIC DEFAULT FN_JOB_NUM;\"");
	}
*/
	public static void main(String[] args) throws IOException {
		logger.trace("Starting replacing TRIM calls");
		Path path=new File(Initializer.getProperty("workingDir")).toPath();
		Fix_trim_add_NULLIF fix= new Fix_trim_add_NULLIF();
		Files.walkFileTree(path,fix);
		logger.trace("Ended replacing TRIM calls");
	}

}
