package fix_miscellanea;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.dom4j.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jsqlparser.parser.TokenMgrException;
import utilities.Initializer;
import utilities.SQLSearcher;
import utilities.SQLSearcher.SQLTypes;

/**
 * this class is a standalone that is meant to be a hot fix for the problem caused by sentences:
 *    PV_OUT_JOBNUM  NUMERIC DEFAULT FN_JOB_NUM;
 * that go into errors.
 * They must be substituted with:
 * 
 * @author g.barri
 *    PV_OUT_JOBNUM  NUMERIC DEFAULT <schema>.FN_JOB_NUM();
 *    
 * with <schema> equal to dg_utilities se la riga è presente in un ex-package di XXDWHAPP
 * 	                      bl_scm_ind   se la riga è presente in un ex-package di XXSCHAPP
 */
public class Fix_FN_JOB_NUM extends SimpleFileVisitor<Path>{

	static Logger logger=LoggerFactory.getLogger(Fix_FN_JOB_NUM.class);
	
	private String newDeclare(File file) {
		String qualifiedOracleName=SQLSearcher.detectFormerOracleSchema(file);
		if(qualifiedOracleName==null) {
			logger.error("Unable to detect original Oracle schema for file "+file.getName()+" ("+file.getAbsolutePath());
			throw new IllegalArgumentException("Unable to detect original Oracle schema for file "+file.getName()+" ("+file.getAbsolutePath());
		}
		String oracleSchema=qualifiedOracleName.substring(0, qualifiedOracleName.indexOf("."));
		switch (oracleSchema) {
		case "XXDWHAPP":
			return "PV_OUT_JOBNUM  NUMERIC DEFAULT dg_utilities.FN_JOB_NUM()";

		case "XXSCHAPP":
			return "PV_OUT_JOBNUM  NUMERIC DEFAULT bl_scm_ind.FN_JOB_NUM()";
	
		default:
			throw new IllegalArgumentException("No correct schema found: "+oracleSchema+". Check file "+file.getAbsolutePath());
		}
	}
	
	@Override
	public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException{
		String text="";
		File file=path.toFile();
		if(!file.getName().endsWith(".sql") || SQLSearcher.detectType(file)!=SQLTypes.FUNCTION) {
			return FileVisitResult.CONTINUE;
		}
		try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
				Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
			while(scann.hasNextLine()) {
				String nextLine=scann.nextLine();
//				System.out.println(nextLine);
				if(checkLine(nextLine)) {
					if(nextLine.contains("PV_OUT_JOBNUM  NUMERIC DEFAULT FN_JOB_NUM")) {
						String newDeclarePV_OUT_JOBNUM;
						try {
							newDeclarePV_OUT_JOBNUM=newDeclare(file);
						}catch (Exception e) {
							logger.error("intercepted exception "+e.getMessage());
							return FileVisitResult.CONTINUE;
						}
						
						String newLine=nextLine.replaceFirst("PV_OUT_JOBNUM  NUMERIC DEFAULT FN_JOB_NUM", newDeclarePV_OUT_JOBNUM);
						logger.trace("Substituted in\t"+file.getAbsolutePath()+"\t"+nextLine+"\twith\t"+newLine);
						nextLine=newLine;
					}
				}
					text+=nextLine+"\r\n";
			}
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while reading for\t"+file.getAbsolutePath(), e);
		}
		
		try(FileOutputStream fileOutStream=new FileOutputStream(file.getAbsolutePath()); 
				PrintWriter print=new PrintWriter(new BufferedOutputStream(fileOutStream))){
			print.print(text);
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while writing\n\t"+file.getAbsolutePath(), e);
		}
		return FileVisitResult.CONTINUE;
	}
	
	@Override
	public FileVisitResult visitFileFailed(Path file,
            IOException exc)
              throws IOException{
		logger.error("intercepted excption "+exc.getClass()+": "+exc.getMessage());
		return FileVisitResult.CONTINUE;
	}
	
	private boolean checkLine(String nextLine) {
		boolean cond=!nextLine.trim().startsWith("--");
		return cond;
	}

	public static void main(String[] args) throws IOException {
		logger.trace("Starting to replace \"PV_OUT_JOBNUM  NUMERIC DEFAULT FN_JOB_NUM;\"");
		String sql_path=Initializer.getProperty("workingDir");
		logger.trace("Working on the directory "+sql_path);
		logger.trace("start to cycle over files");
		Fix_FN_JOB_NUM fix=new Fix_FN_JOB_NUM();
		Files.walkFileTree(new File(sql_path).toPath(), fix);
		logger.trace("Ended the replaces of \"PV_OUT_JOBNUM  NUMERIC DEFAULT FN_JOB_NUM;\"");
	}

}
