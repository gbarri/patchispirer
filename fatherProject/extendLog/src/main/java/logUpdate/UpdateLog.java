package logUpdate;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import java.util.Scanner;

import org.apache.poi.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jsqlparser.parser.ParseException;
import net.sf.jsqlparser.parser.TokenMgrException;
import utilities.SQLSearcher;
import utilities.StringUtilities;

public class UpdateLog {
	
	Logger logger = LoggerFactory.getLogger(UpdateLog.class);
	private static boolean ignoreCommentedLines=true; 
	
	/**
	 * nome della function che esegue il logging
	 */
	public static String logFunction="dg_utilities.PR_FUNCTION_LOG_DSX";
	/**
	 * numero di argomenti previsti per logFunction
	 */
	public static int numArgsLogFunction=17;
	
	/**
	 * logica di esclusione delle righe.
	 * Aggiungere condizioni che inidichino quali righe vanno salatate. Ad esempio, le righe di commento non sono prese in considerazione.
	 * @param line
	 * @return
	 */
	private static boolean checkLine(String line) {
		boolean cond=true;
		//escludo righe commentate MA non se contengono la prima parte del TAG
		if(ignoreCommentedLines) {
			cond=cond && !(line.trim().startsWith("--") && !line.contains("<|PR_"));
		}
		return cond;
	}
	
	private void setIgnoreCommentedLines(boolean ignore) {
		ignoreCommentedLines=ignore;
	}
	
	public void update(File file) {
		logger.trace("Updating log calls in "+file.getName()+" ("+file.getAbsolutePath()+")");
		String newFile="";
		try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
				Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
			while(scann.hasNextLine()) {
				String nextLine=scann.nextLine();
				if(checkLine(nextLine)) {
					nextLine=nextLine.replaceAll("PERFORM\\s+PR_FUNCTION_LOG_DSX", "PERFORM "+logFunction);
					Boolean checkForOccurrences=null;
					///
					try {
						List<Integer> occurrences;
						occurrences = StringUtilities.findExactly(nextLine, logFunction);
						checkForOccurrences=occurrences!=null && occurrences.size()>0;
					} catch (TokenMgrException  e) {
						//workaround un poco schifido
						checkForOccurrences=nextLine.contains(logFunction);
					} catch (Exception e) {
						checkForOccurrences=nextLine.contains(logFunction);
					}
					if(checkForOccurrences) {
						String oldLine="--\t"+nextLine+"\n";
						String tmpLine=nextLine+"\n";
						while(nextLine.indexOf(')')==-1) {
							//evito il controllo hasNextLine() dato che di certo deve concludere
							nextLine=scann.nextLine();
							tmpLine+=nextLine+"\n";
							oldLine+="--\t"+nextLine+"\n";
						}
						//at this point tmpLine contains the whole logging function even on multiple levels
						String logArgs=tmpLine.substring(tmpLine.indexOf('(',tmpLine.indexOf(logFunction))+1, tmpLine.indexOf(')'));
						String alteredLine=tmpLine.substring(0,tmpLine.indexOf('(',tmpLine.indexOf(logFunction))+1)+alterLog(logArgs, file)+tmpLine.substring(tmpLine.indexOf(')'));
						//the arguments have been modified
						//return the lines to their position
						if(!alteredLine.equals(tmpLine)) {
							nextLine=oldLine+"--##--\n"+alteredLine+"--##--\n";
							logger.trace("In file "+file.getName()+" replaced\r\n"+oldLine+"\r\nwith\r\n"+alteredLine);
						}else {
							nextLine=alteredLine;
						}
					}
				}
				newFile+=nextLine+"\n";
			}
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while reading for \n\t"+file.getAbsolutePath(), e);
		}
		
		try(FileOutputStream fileOutStream=new FileOutputStream(file.getAbsolutePath()); 
				PrintWriter print=new PrintWriter(new BufferedOutputStream(fileOutStream))){
			print.print(newFile);
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while writing\n\t"+file.getAbsolutePath(), e);
		}
		
	}
	
	
	public void rollBackUpdate(File file) {
		logger.trace("Rollbacking log calls extensions in "+file.getName()+" ("+file.getAbsolutePath()+")");
		boolean previusIgnoreCommentedLInes=ignoreCommentedLines;
		setIgnoreCommentedLines(false);
		String newFile="";
		try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
				Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
			String nextLine=null;
			boolean nextLineAlreadyRead=false;
			while(scann.hasNextLine() || nextLineAlreadyRead) {
				String newLine;
				if(!nextLineAlreadyRead) {
					nextLine=scann.nextLine();
				}{
					nextLineAlreadyRead=false;
				}
				if(checkLine(nextLine)) {
					Boolean checkForOccurrences=null;
					///
					checkForOccurrences=nextLine.contains(logFunction);
					///
					if(checkForOccurrences) {
						String oldLine=nextLine+"\n";
						while(nextLine.indexOf(')')==-1) {
							//evito il controllo hasNextLine() dato che di certo deve concludere
							nextLine=scann.nextLine();
							oldLine+=nextLine+"\n";
						}
						//at this point tmpLine contains the whole logging function even on multiple levels
						//check if immediately after there's another non-commented call to the function.
						if(scann.hasNextLine()) {
							nextLine=scann.nextLine();
							if(nextLine.contains("--##--")) {
								logger.trace("Since "+oldLine+" is followed by a line with tag --##--, it is supposed to be an extended logcal. The content between the tags --##-- is skipped, comments\"--\" in the logcal are removed.");
								boolean closureFound=false;
								while(!closureFound) {
									nextLine=scann.nextLine();
									if(nextLine.contains("--##--")) {
										closureFound=true;
									}
								}
								oldLine=oldLine.replaceAll("--\t","");
								logger.info("File "+file.getName()+", restored call to log function: ("+file.getAbsolutePath()+")\n "+oldLine);
							}else {
								nextLineAlreadyRead=true;
							}
						}
						//return the lines to their position
						newLine=oldLine;
						
					}else {
						
							newLine=nextLine+"\n";
						
					}
				}else {
					
						newLine=nextLine+"\n";
					
				}
				newFile+=newLine;
			}
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while reading from \n\t"+file.getAbsolutePath(), e);
		}
		
		try(FileOutputStream fileOutStream=new FileOutputStream(file.getAbsolutePath()); 
				PrintWriter print=new PrintWriter(new BufferedOutputStream(fileOutStream))){
			print.print(newFile);
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while writing\n\t"+file.getAbsolutePath(), e);
		}
		
		setIgnoreCommentedLines(previusIgnoreCommentedLInes);
		
	}
	
	
	/**
	 * funzione per la modifica degli argomenti della chiamata di log.
	 * In un futuro, potrebbe essere resa dipendente da un file di configurazione
	 * @param logFunction
	 * @return updated string containing the logFunction arguments
	 */
	public String alterLog(String logFunction, File file) {
		int numArgs=StringUtil.countMatches(logFunction, ',')+1;
		if(numArgs<numArgsLogFunction) {
			//then this call to logFunction has not been updated before
			String newArg_in_cd_created_by=null;
			String owner=SQLSearcher.detectName(file);
//			if(owner!=null && owner.contains("MP_")) {
			//BG-06/02/2019: � stato osservato come esistono package irregolari che NON hanno MP_ nel nome. Pertanto tale filtro non � afffidabile
			if(owner!=null) {
				if(owner.contains(".")) {
					newArg_in_cd_created_by=owner.substring(0, owner.indexOf("."));
				}else {
					logger.warn("File "+file+" does not shows a schema in its name!!!");
					newArg_in_cd_created_by=owner;
				}
			}else if(owner!=null && owner.contains("pr_admin_format_check")) {
				throw new UnsupportedOperationException("Non dovrei andare dentro pr_admin_format_check");
			}else if(owner!=null && owner.contains("pr_admin_constraint_check")) {
				throw new UnsupportedOperationException("Non dovrei andare dentro pr_admin_constraint_check");
			}else {
				logger.warn("The altering of the argument of the log function ended up with an unforeseened calling to the log function. ("+file.getAbsolutePath());
				newArg_in_cd_created_by="NULL /* possibly wrong argument */ ";
			}
			String newArgs=", "+"'"+newArg_in_cd_created_by+"'"+", ID_LOAD, NULL";
			logFunction+=newArgs;
		}
		return logFunction;
	}
	
}
