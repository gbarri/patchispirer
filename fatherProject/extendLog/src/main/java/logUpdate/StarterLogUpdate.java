package logUpdate;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utilities.Initializer;
import utilities.SQLSearcher;
import utilities.SQLSearcher.SQLTypes;

public class StarterLogUpdate {
	
	static Logger logger=LoggerFactory.getLogger(StarterLogUpdate.class);
	public UpdateLog updateLogIstance=null;
	
	/**
	 * metodo per stabilire se occorre procedere per un dato file o meno.
	 * Utile per stabilire la logica con cui si accede ad un file
	 * @param path
	 * @return se procedere
	 */
	private static boolean checkFile(File file) {
		logger.trace("Checking file "+file.getName());
		boolean cond=true;
		cond=cond && file.getName().endsWith(".sql");
		cond=cond && !file.getName().startsWith(".");
		if(cond) {
			SQLTypes type=SQLSearcher.detectType(file);	
			cond=cond && type!=null;
			if(!cond) {
				logger.debug("file "+file.getName()+", "+file.getAbsolutePath()+"\n� stato scartato perch� non riconosciuto come def. di un oggetto SQL.");
			}
		}else {
			logger.trace("file "+file.getName()+" has been discarded");
		}
		return cond;
	}
	
	/**
	 * metodo per stabilire se occorre procedere per una data cartella o meno.
	 * Utile per stabilire la logica con cui si accede ad una cartella, o escludere certe cartelle.
	 * @param file
	 * @return
	 */
	private static boolean checkFolder(File folder) {
		logger.trace("checking folder "+folder.getName());
		boolean cond=true;
		cond=cond && !folder.getName().equals("_Ispirer");
		if(!cond) logger.trace("folder "+folder.getName()+" to be skipped");
		return cond;
	}
	
	/**
	 * carica i files di una directory. Per ora in un array di file. Possibili miglioramenti
	 * @param folder
	 * @return File array
	 */
	public static File[] loadFiles(File folder) {
		File[] listOfFiles = folder.listFiles();
		return listOfFiles;
	}
	
	/**
	 * metodo che deve scorrere i files da analizzare e richiama i vari metodi necessari
	 * @param path
	 */
	private void goThrough(String path) {
		File folder = new File(path);
		if(!folder.isDirectory()) {
			if(folder.isFile() && checkFile(folder)) {
				File file=folder;
				folder=null;
				this.updateLogIstance.update(file);
//				this.updateLogIstance.rollBackUpdate(file);
			}
		}else if(folder.isDirectory() && checkFolder(folder)){
			File[] listOfFiles=loadFiles(folder);
			for (File file : listOfFiles) {
				goThrough(file.getAbsolutePath());
			}
		}
	}
	
	public static void main(String[] args) {
		/*real repository*/
//		String sql_path="C:\\Users\\g.barri\\Desktop\\wrk\\XXDWHAPP_MP_ST_CAUSALI_MDR";
		String sql_path=Initializer.getProperty("workingDir");
		
		/*test folder outside repository*/
//		String sql_path="C:\\Users\\g.barri\\Desktop\\test";
		
		logger.trace("Starting main of "+StarterLogUpdate.class);
		StarterLogUpdate start=new StarterLogUpdate();
		start.updateLogIstance=new UpdateLog();
		logger.trace("Going through files in "+sql_path);
		start.goThrough(sql_path);
		logger.trace("Ending main of "+StarterLogUpdate.class);
	}
}
