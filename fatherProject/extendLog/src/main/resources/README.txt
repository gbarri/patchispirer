
Procedura per modificare le chiamate alla funzione di log
----------------------
-- StarterLogUpdate --
----------------------
1-tutti i file da modificare devono essere presenti i una sola directory.
Verrano modificati tutti i file *.sql nella directory indicata E SOTTODIRECTORY che vengono riconosciuti come oggetti SQL.
Controllare il file di log per verificare quali file non sono stati elaborati
2-indicare il path assoluto della directory nella variabie sql_path
3-lanciare il programma (richiede qualche minuto)

----------------------
--    UpdateLog     --
----------------------
Modifiche a come trattare gli argomenti della funzione di log sono effettuate da questa classe.
In particolare si rimanda al metodo alterLog(String).
