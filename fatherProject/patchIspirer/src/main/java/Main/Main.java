package Main;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.util.regex.Pattern;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Configurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tinylog.configuration.Configuration;

import fix_PR_XX.Fix_PR_XX_YY;
import fix_miscellanea.AddStorageSessionToTableDDL;
import fix_miscellanea.Fix_FN_JOB_NUM;
import fix_miscellanea.Fix_log_variable_declaration;
import fix_miscellanea.Fix_trim_add_NULLIF;
import functionUtilities.FilesCyclerFunCallAlterer;
import insertCall.Starter;
import logUpdate.StarterLogUpdate;
import movements.Move;
import schemaMapping.StarterNameReplace;
import utilities.Initializer;

public class Main {
	static Logger logger = LoggerFactory.getLogger(Main.class);
	public static Boolean doStarterNameReplace=true;
	public static Boolean doFix_PR_XX_YY=true;
	public static Boolean doTagReplacement=true;
	public static Boolean doFix_FN_JOB_NUM=true;
	public static Boolean doFix_trim_and_NULLIF=true;
	public static Boolean doFix_log_variable_declaration=false;
	public static Boolean doStarterLogUpdate=true;
	public static Boolean doAddStorageSessionToTableDDL=true;
	public static Boolean doMove=true;
	public static Boolean doReplaceNVLwithCOAELESCE=true;
	public static Boolean doCommentPrCheckErrLog=true;  
	public static Boolean doReplaceDecodewithCase=true;
	public static Boolean doReplaceTruncateAndAnalyze=true;
	public static Boolean doCommentPRG_CHANGE_OPTIMIZER=true;
	public static Boolean doReplaceNVL2WithCase=true;
	public static Boolean doReplaceSubstrWithNegativeInput=true;
	public static Boolean doReplaceSWF_Greatest=true;
	public static Boolean doCommentCCBI_ABEND_MANAGE=true;
	
	public static void setDoCommentCCBI_ABEND_MANAGE(boolean doCommentCCBI_ABEND_MANAGE) {
		Main.doCommentCCBI_ABEND_MANAGE = doCommentCCBI_ABEND_MANAGE;
	}
	
	public static void setDoReplaceSWF_Greatest(boolean doReplaceSWF_Greatest) {
		Main.doReplaceSWF_Greatest = doReplaceSWF_Greatest;
	}
	
	public static void setDoReplaceSubstrWithNegativeInput(boolean doReplaceSubstrWithNegativeInput) {
		Main.doReplaceSubstrWithNegativeInput = doReplaceSubstrWithNegativeInput;
	}
	
	public static void setDoReplaceNVL2WithCase(boolean doReplaceNVL2WithCase) {
		Main.doReplaceNVL2WithCase = doReplaceNVL2WithCase;
	}
	
	public static void setDoCommentPRG_CHANGE_OPTIMIZER(boolean doCommentPRG_CHANGE_OPTIMIZER) {
		Main.doCommentPRG_CHANGE_OPTIMIZER = doCommentPRG_CHANGE_OPTIMIZER;
	}
	
	public static void setDoReplaceTruncateAndAnalyze(boolean doReplaceTruncateAndAnalyze) {
		Main.doReplaceTruncateAndAnalyze = doReplaceTruncateAndAnalyze;
	}
	
	public static void setDoReplaceDecodewithCase(boolean doReplaceDecodewithCase) {
		Main.doReplaceDecodewithCase = doReplaceDecodewithCase;
	}
	
	public static void setDoStarterNameReplace(boolean doStarterNameReplace) {
		Main.doStarterNameReplace = doStarterNameReplace;
	}


	public static void setDoFix_PR_XX_YY(boolean doFix_PR_XX_YY) {
		Main.doFix_PR_XX_YY = doFix_PR_XX_YY;
	}


	public static void setDoTagReplacement(boolean doTagReplacement) {
		Main.doTagReplacement = doTagReplacement;
	}


	public static void setDoFix_FN_JOB_NUM(boolean doFix_FN_JOB_NUM) {
		Main.doFix_FN_JOB_NUM = doFix_FN_JOB_NUM;
	}


	public static void setDoFix_trim_and_NULLIF(boolean doFix_trim_and_NULLIF) {
		Main.doFix_trim_and_NULLIF = doFix_trim_and_NULLIF;
	}



	public static void setDoFix_log_variable_declaration(boolean doFix_log_variable_declaration) {
		Main.doFix_log_variable_declaration = doFix_log_variable_declaration;
	}



	public static void setDoStarterLogUpdate(boolean doStarterLogUpdate) {
		Main.doStarterLogUpdate = doStarterLogUpdate;
	}

	
	public static void setDoAddStorageSessionToTableDDL(boolean doAddStorageSessionToTableDDL) {
		Main.doAddStorageSessionToTableDDL = doAddStorageSessionToTableDDL;
	}

	public static void setDoMove(boolean doMove) {
		Main.doMove = doMove;
	}

	public static void setDoReplaceNVLwithCOAELESCE(boolean doReplaceNVLwithCOAELESCE) {
		Main.doReplaceNVLwithCOAELESCE = doReplaceNVLwithCOAELESCE;
	}


	public static void setDoCommentPrCheckErrLog(boolean doCommentPrCheckErrLog) {
		Main.doCommentPrCheckErrLog = doCommentPrCheckErrLog;
	}


	private static void init() {
		Field[] fields=Main.class.getDeclaredFields();
		for(Field field:fields) {
			if(field.getType().equals(Boolean.class)) {
				String fieldName=field.getName();
				String confSetting=Initializer.getProperty(fieldName, true);
				if(confSetting==null) {
					continue;
				}else {
					try {
						Boolean doModule=Boolean.parseBoolean(confSetting.trim());
						Method setMethod=Main.class.getDeclaredMethod("set"+fieldName.replaceFirst("d","D"), boolean.class);
						setMethod.invoke(null, doModule);
					}catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	public static void main(String[] args) throws InvalidFormatException, IOException, SecurityException, URISyntaxException {	
		init();
		if(doStarterNameReplace) {
			StarterNameReplace.main(null);
		}else {
			logger.info("Replacing schema name skipped by configuration setting");
		}
		if(doReplaceNVLwithCOAELESCE || doCommentPrCheckErrLog || doReplaceDecodewithCase || 
			doReplaceTruncateAndAnalyze || doCommentPRG_CHANGE_OPTIMIZER || doReplaceNVL2WithCase || 
			doReplaceSubstrWithNegativeInput || doReplaceSWF_Greatest || doCommentCCBI_ABEND_MANAGE) {
			FilesCyclerFunCallAlterer.main(null);
		}else {
			logger.info("Generic function call replace module has been skipped by configuration setting");
		}
		if(doFix_PR_XX_YY) {
			Fix_PR_XX_YY.main(null);
		}else {
			logger.info("Updating calls to PR_%schema%_[ANALYZE|TRUNC|COMP] skipped by configuration setting");
		}
		if(doTagReplacement) {
			Starter.main(null);
		}else {
			logger.info("Inserting functions calls in place of TAGs skipped by configuration setting");
		}
		if(doFix_FN_JOB_NUM) {
			Fix_FN_JOB_NUM.main(null);
		}else {
			logger.info("Fixing calls to FN_JOB_NUM function in variables declaration skipped by configuration setting");
		}
		if(doFix_trim_and_NULLIF) {
			Fix_trim_add_NULLIF.main(null);
		}else {
			logger.info("Wrapping every TRIM() function with a NULLIF( ,'') skipped by configuration setting");
		}
		if(doFix_log_variable_declaration) {
			Fix_log_variable_declaration.main(null);
		}else {
			logger.info("Correct variable initialization used in tracing the mapping functions skipped");
		}
		if(doStarterLogUpdate) {
			StarterLogUpdate.main(null);
		}else {
			logger.info("Updating calls to log function skipped by configuration setting.");
		}
		if(doAddStorageSessionToTableDDL) {
			AddStorageSessionToTableDDL.main(null);
		}else {
			logger.info("Insertion of storage parameter in tables ddl skipped by configuration setting.");
		}
		if(doMove) {
			Move.main(null);
		}else {
			logger.info("Move of the files into the repository skipped by configuration setting");
		}
	}

}
