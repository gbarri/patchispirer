package movements;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import schemaMapping.LoaderSchemaMap;
import utilities.Initializer;
import utilities.RepoSearcher;
import utilities.StringUtilities;

public class ClasseDiUtilita {
	static String[] objNames= {"bl_scm_com.WVN_MP_ASSSTOTEL_OUT_UPD",
			"bl_scm_com.WVN_MP_ASSSTOTEL_ALL_INS",
			"bl_scm_com.WVN_MP_ASSSTOTEL_RETE_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_DOG_INS",
			"bl_scm_com.WVN_MP_ASSEVEORD_DOC_INS",
			"bl_scm_com.WVN_MP_ASSEVEORD_ORDC_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_DOG_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_DOC_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_ORDC_INS_SK",
			"bl_scm_com.WVN_MP_ASSEVEORD_ORDC_INS",
			"bl_scm_com.WVN_MP_ASSEVEORD_CCF_MKT_IMP",
			"bl_scm_com.WVN_MP_ASSEVEORD_IMM_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_IMM_MM",
			"bl_scm_com.WVN_MP_ASSEVEORD_IMM_MERGE",
			"bl_scm_com.WVN_MP_ASSEVEORD_IMM",
			"bl_scm_com.WVN_MP_ASSEVEORD_ORDC_UPD_SK",
			"bl_scm_com.WVN_MP_CCFCOMPLETATE_INS",
			"bl_scm_com.WVN_MP_ASSEVEORD_UPD_IMM_EST",
			"bl_scm_com.WVN_MP_ASSEVEORD_INS_IMM_EST",
			"bl_scm_com.WVN_MP_ASSEVEORD_GREALE_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_CCF_CJD",
			"bl_scm_com.WVN_MP_CCFINCOMPLETE_INS",
			"bl_scm_com.WVN_MP_CG_ASEVOR_UPD",
			"bl_scm_com.WVN_MP_CG_ASEVOR_INS",
			"tl_sal.WVN_MP_CG_ASEVOR_INS",
			"bl_scm_com.WVN_MP_CG_ASEVORC_UPD",
			"bl_scm_com.WVN_MP_CCFINCOMPLETE_UPD",
			"bl_scm_com.WVN_MP_CCFCOMPLETATE_UPD",
			"bl_scm_com.WVN_MP_CG_ASEVOR_UPD_AMEER",
			"bl_scm_com.WVN_MP_ASSEVEORD_CANC",
			"bl_scm_com.WVN_MP_DIVSKME_ASEVEOR_UPD",
			"bl_scm_com.WVN_MP_DIVSKME_ASEVEOR_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_CANC",
			"bl_scm_com.WVN_MP_ASSSTOTEL_ALL_INS",
			"bl_scm_com.WVN_MP_ASSSTOTEL_OUT_UPD",
			"bl_scm_com.WVN_MP_ASSSTOTEL_PROP_UPD",
			"bl_scm_com.WVN_MP_ASSSTOTEL_RETE_UPD",
			"bl_scm_com.WVN_MP_ASSSTOCKTEL_UPDINS",
			"bl_scm_com.WVN_MP_ASSEVEORD_DOC_INS",
			"bl_scm_com.WVN_MP_ASSEVEORD_DOG_INS",
			"bl_scm_com.WVN_MP_ASSEVEORD_ORDC_INS",
			"bl_scm_com.WVN_MP_ASSEVEORD_ORDC_INS_SK",
			"bl_scm_com.WVN_MP_ASSEVEORD_DOC_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_DOG_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_ORDC_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_ORDC_UPD_SK",
			"bl_scm_com.WVN_MP_ASSEVEORD_IMM",
			"bl_scm_com.WVN_MP_ASSEVEORD_IMM_MERGE",
			"bl_scm_com.WVN_MP_ASSEVEORD_IMM_MM",
			"bl_scm_com.WVN_MP_ASSEVEORD_IMM_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_CCF_MKT_IMP",
			"bl_scm_com.WVN_MP_ASSEVEORD_CCF_CJD",
			"bl_scm_com.WVN_MP_ASSEVEORD_GREALE_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_INS_IMM_EST",
			"bl_scm_com.WVN_MP_ASSEVEORD_UPD_IMM_EST",
			"bl_scm_com.WVN_MP_CCFCOMPLETATE_INS",
			"bl_scm_com.WVN_MP_CCFCOMPLETATE_UPD",
			"bl_scm_com.WVN_MP_CCFINCOMPLETE_INS",
			"bl_scm_com.WVN_MP_CCFINCOMPLETE_UPD",
			"bl_scm_com.WVN_MP_CG_ASEVORC_UPD",
			"tl_sal.WVN_MP_CG_ASEVOR_INS",
			"bl_scm_com.WVN_MP_CG_ASEVOR_INS",
			"bl_scm_com.WVN_MP_CG_ASEVOR_UPD",
			"bl_scm_com.WVN_MP_CG_ASEVOR_UPD_AMEER"};
	
	static String[] detectMigratedTrue= {
			"tl_analytics.WVN_MP_DM_PACK",
			"tl_analytics.WVN_MP_DM_ORIG_OPT",
			"tl_analytics.WVN_MP_DM_MODALITA_ACQUISTO",
			"tl_sal.WVN_MP_DM_TIPO_CLIENTE",
			"tl_analytics.WVN_MP_DM_TIPO_CARROZZERIA",
			"tl_analytics.WVN_MP_DM_STABILIMENTO",
			"tl_analytics.WVN_MP_DM_PRODOTTO",
			"tl_analytics.WVN_MP_DM_TEMPO_YEAR",
			"tl_analytics.WVN_MP_DM_TEMPO_DAY",
			"tl_sal.WVN_MP_CG_ASEVOR_INS",
			"tl_analytics.WVN_MP_DM_MKT",
			"tl_analytics.WVN_MP_DM_MKT_EDV_SEDE_SRETE",
			"tl_analytics.WVN_MP_DM_MODALITA_ACQUISTO",
			"tl_analytics.WVN_MP_DM_TEMPO_DAY",
			"tl_sal.WVN_MP_CG_ASEVOR_INS"
	};
	
	static String[] detectMigratedFalse= {
			"bl_scm_com.WVN_MP_ASSSTOTEL_OUT_UPD",
			"bl_scm_com.WVN_MP_ASSSTOTEL_ALL_INS",
			"bl_scm_com.WVN_MP_ASSSTOTEL_RETE_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_DOG_INS",
			"bl_scm_com.WVN_MP_ASSEVEORD_DOC_INS",
			"bl_scm_com.WVN_MP_ASSEVEORD_ORDC_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_DOG_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_DOC_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_ORDC_INS_SK",
			"bl_scm_com.WVN_MP_ASSEVEORD_ORDC_INS",
			"bl_scm_com.WVN_MP_ASSEVEORD_CCF_MKT_IMP",
			"bl_scm_com.WVN_MP_ASSEVEORD_IMM_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_IMM_MM",
			"bl_scm_com.WVN_MP_ASSEVEORD_IMM_MERGE",
			"bl_scm_com.WVN_MP_ASSEVEORD_IMM",
			"bl_scm_com.WVN_MP_ASSEVEORD_ORDC_UPD_SK",
			"bl_scm_com.WVN_MP_CCFCOMPLETATE_INS",
			"bl_scm_com.WVN_MP_ASSEVEORD_UPD_IMM_EST",
			"bl_scm_com.WVN_MP_ASSEVEORD_INS_IMM_EST",
			"bl_scm_com.WVN_MP_ASSEVEORD_GREALE_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_CCF_CJD",
			"bl_scm_com.WVN_MP_CCFINCOMPLETE_INS",
			"bl_scm_com.WVN_MP_CG_ASEVOR_UPD",
			"bl_scm_com.WVN_MP_CG_ASEVOR_INS",
			"bl_scm_com.WVN_MP_CG_ASEVORC_UPD",
			"bl_scm_com.WVN_MP_CCFINCOMPLETE_UPD",
			"bl_scm_com.WVN_MP_CCFCOMPLETATE_UPD",
			"bl_scm_com.WVN_MP_CG_ASEVOR_UPD_AMEER",
			"bl_scm_com.WVN_MP_ASSEVEORD_CANC",
			"bl_scm_com.WVN_MP_DIVSKME_ASEVEOR_UPD",
			"bl_scm_com.WVN_MP_DIVSKME_ASEVEOR_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_CANC",
			"bl_scm_com.WVN_MP_ASSSTOTEL_ALL_INS",
			"bl_scm_com.WVN_MP_ASSSTOTEL_OUT_UPD",
			"bl_scm_com.WVN_MP_ASSSTOTEL_PROP_UPD",
			"bl_scm_com.WVN_MP_ASSSTOTEL_RETE_UPD",
			"bl_scm_com.WVN_MP_ASSSTOCKTEL_UPDINS",
			"bl_scm_com.WVN_MP_ASSEVEORD_DOC_INS",
			"bl_scm_com.WVN_MP_ASSEVEORD_DOG_INS",
			"bl_scm_com.WVN_MP_ASSEVEORD_ORDC_INS",
			"bl_scm_com.WVN_MP_ASSEVEORD_ORDC_INS_SK",
			"bl_scm_com.WVN_MP_ASSEVEORD_DOC_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_DOG_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_ORDC_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_ORDC_UPD_SK",
			"bl_scm_com.WVN_MP_ASSEVEORD_IMM",
			"bl_scm_com.WVN_MP_ASSEVEORD_IMM_MERGE",
			"bl_scm_com.WVN_MP_ASSEVEORD_IMM_MM",
			"bl_scm_com.WVN_MP_ASSEVEORD_IMM_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_CCF_MKT_IMP",
			"bl_scm_com.WVN_MP_ASSEVEORD_CCF_CJD",
			"bl_scm_com.WVN_MP_ASSEVEORD_GREALE_UPD",
			"bl_scm_com.WVN_MP_ASSEVEORD_INS_IMM_EST",
			"bl_scm_com.WVN_MP_ASSEVEORD_UPD_IMM_EST",
			"bl_scm_com.WVN_MP_CCFCOMPLETATE_INS",
			"bl_scm_com.WVN_MP_CCFCOMPLETATE_UPD",
			"bl_scm_com.WVN_MP_CCFINCOMPLETE_INS",
			"bl_scm_com.WVN_MP_CCFINCOMPLETE_UPD",
			"bl_scm_com.WVN_MP_CG_ASEVORC_UPD",
			"bl_scm_com.WVN_MP_CG_ASEVOR_INS",
			"bl_scm_com.WVN_MP_CG_ASEVOR_UPD",
			"bl_scm_com.WVN_MP_CG_ASEVOR_UPD_AMEER"};
			
	
	static String[] oracleNames= {
			"ABA21.WVN_MP_DM_PACK",
			"ABA21.WVN_MP_DM_ORIG_OPT",
			"ABA21.WVN_MP_DM_MODALITA_ACQUISTO",
			"ABA21.WVN_MP_DM_TIPO_CLIENTE",
			"ABA21.WVN_MP_DM_TIPO_CARROZZERIA",
			"ABA21.WVN_MP_DM_STABILIMENTO",
			"ABA21.WVN_MP_DM_PRODOTTO",
			"ABA21.WVN_MP_DM_TEMPO_YEAR",
			"ABA21.WVN_MP_DM_TEMPO_DAY",
			"ABA21.WVN_MP_CG_ASEVOR_INS",
			"ABA21.WVN_MP_DM_MKT",
			"ABA21.WVN_MP_DM_MKT_EDV_SEDE_SRETE",
			"ABA21.WVN_MP_DM_MODALITA_ACQUISTO",
			"ABA21.WVN_MP_DM_TEMPO_DAY",
			"ABA21.WVN_MP_CG_ASEVOR_INS"
	};
	
	public static void main(String[] args) {
		Move move=new Move();
		LoaderSchemaMap loder=new LoaderSchemaMap();
		Map<String, List<String>> map=loder.loadInverseMap();
		
		for(String objName:objNames) {
			List<String> tmp=map.get(objName);
			if(tmp!=null && tmp.size()>0) {
				System.out.println(tmp);
			}else {
				System.out.println("NOT FOUND\t"+StringUtilities.detectSchemaInObjectName(objName)+"\t"+StringUtilities.detectNameInObjectName(objName));
			}
			
//			boolean bol=move.detectMigrated(objName);
//			if(bol) {
//				System.out.println("\""+objName+"\",");
//			}else {
////				System.out.println("\""+objName+"\",");
//			}
		}

	}

}
