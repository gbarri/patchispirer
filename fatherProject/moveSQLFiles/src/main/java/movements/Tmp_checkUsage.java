package movements;

import java.io.File;
import java.util.List;

import utilities.RepoSearcher;
import utilities.SQLSearcher;

public class Tmp_checkUsage {

	public static void main(String[] args) {
		RepoSearcher rps=RepoSearcher.getRepoSearcher();
		String[] listFiles= { "C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_sal\\tables\\tl_sal_dw_desc_famiglia_no_vend_tab.sql",
				"C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_sal\\tables\\dmc21_dw_stato_telaio_tab.sql",
				"C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_sal\\tables\\dmc21_dw_famiglia_no_ven_tab.sql",
				"C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_sal\\tables\\tl_sal_dw_desc_stato_telaio_tab.sql",
				"C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_sal\\tables\\tl_sal_st_telai_in_valid_non_vendib_tab.sql",
				"C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_scm_com\\views\\tl_scm_com_vs_desc_motivazioni.sql",
				"C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_scm_com\\views\\tl_scm_com_vs_desc_cluster.sql",
				"C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_analytics\\views\\tl_analytics_vs_delta_fine_serie.sql",
				"C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_sal\\views\\tl_sal_vs_fam_no_vend_mc2.sql",
				"C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_sal\\views\\tl_sal_vs_stato_telaio_mc2.sql",
				"C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_sal\\views\\amc21_vs_telai_non_vend_validati.sql",
				"C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_sal\\functions\\tl_sal_mp_dw_desc_famiglia_no_vend.sql",
				"C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_sal\\functions\\tl_sal_mp_dw_desc_stato_telaio.sql",
				"C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_analytics\\functions\\tl_analytics_mp_dw_fine_serie.sql",
				"C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_analytics\\functions\\tl_analytics_pr_pre_dw_fine_serie.sql",
				"C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_sal\\functions\\tl_sal_mp_dw_telai_in_valid_non_vend.sql",
				"C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_sal\\functions\\tl_sal_pre_dw_telai_valid_non_vendib.sql",
				"C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_scm_com\\functions\\tl_scm_com_wmc_mp_dw_desc_cluster.sql",
				"C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_scm_com\\functions\\tl_scm_com_wmc_mp_dw_desc_motivazioni.sql",
				"C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake\\tl_scm_com\\functions\\tl_scm_com_wmc_mp_dw_telai_squad_fis_cont.sql"
};
		Move move=new Move();
		for(String path:listFiles) {
			File file=new File(path);
			String objectName=SQLSearcher.detectName(file);
			if(!move.detectMigrated(objectName)) {
				String nameFile=file.getName();
				System.out.println("ABSENT:"+nameFile);
			}
		}
	}

}
