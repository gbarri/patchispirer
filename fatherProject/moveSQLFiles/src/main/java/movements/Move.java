package movements;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dirTree.BasicRuledFolder;
import dirTree.CheckPosition;
import dirTree.RuledFolder;
import loaders.*;
import schemaMapping.LoaderSchemaMap;
import utilities.Initializer;
import utilities.RepoSearcher;
import utilities.SQLSearcher;
import utilities.SQLSearcher.SQLTypes;
import utilities.StringUtilities;

public class Move {

static Logger logger=LoggerFactory.getLogger(Move.class);
private BasicRuledFolder rf=null;
private RepoSearcher repositorySearcher=null;
//public String movementsRegisterPath=null;
public String create_all_file=null;
private LoaderMultipleSheetsDBExcel loader=null;
private static Map<String, String[]> tableSizeMap=null;
private static Map<String, List<String>> inverseMap=null; 
private Map<String, String[]> migrated=null;
private SinteticResultPrinter sinteticResultPrinter;
private CompileIstructionPrinter compileIstructionPrinter;

private static String repositoryRootAbsPath;
	
	/**
	 * metodo per stabilire se occorre procedere per un dato file o meno.
	 * Utile per stabilire la logica con cui si accede ad un file
	 * @param path
	 * @return se procedere
	 */
	private static boolean checkFile(File file) {
		boolean cond=true;
		cond=cond && file.getName().endsWith(".sql");
		cond=cond && !file.getName().startsWith(".");
		SQLTypes type=SQLSearcher.detectType(file);
		if(cond && type==null) {
			logger.debug("il file "+file.getName()+", "+file.getAbsolutePath()+"\n� stato scartato perch� non riconosciuto.");
		}
		cond=cond && type!=null;
//		cond=cond && (type==SQLTypes.FUNCTION);
		//NB: per ora spostiamo solo mapping e procedures
		return cond;
	}
	
	/**
	 * metodo per stabilire se occorre procedere per una data cartella o meno.
	 * Utile per stabilire la logica con cui si accede ad una cartella, o escludere certe cartelle.
	 * @param file
	 * @return
	 */
	private static boolean checkFolder(File folder) {
		boolean cond=true;
		cond=cond && !folder.getName().equals("_Ispirer");
		cond=cond && !folder.getName().startsWith("_");
		return cond;
	}
	
	/**
	 * carica i files di una directory. Per ora in un array di file. Possibili miglioramenti
	 * @param folder
	 * @return File array
	 */
	public File[] loadFiles(File folder) {
		File[] listOfFiles = folder.listFiles();
		return listOfFiles;
	}
	
	private void printBatFile(File baseSqlFile) {
		if(baseSqlFile!=null && baseSqlFile.exists()) {
			String nameNoExtension=baseSqlFile.getName().substring(0,baseSqlFile.getName().lastIndexOf('.'));
			String batFileDEV=Initializer.getProperty("localRepositoryDir").replace("\\datalake", "\\tools\\run_psql\\DEV\\")+nameNoExtension+"_DEV.bat";
			String batFilePROD=Initializer.getProperty("localRepositoryDir").replace("\\datalake", "\\tools\\run_psql\\PROD\\")+nameNoExtension+"_PROD.bat";
			File batDEV=new File(batFileDEV);
			if(!batDEV.exists()) {
				try(FileWriter fw=new FileWriter(batDEV);
					BufferedWriter bw = new BufferedWriter(fw);
					PrintWriter print = new PrintWriter(bw)){
					print.println("@ECHO OFF\r\n" + 
							"set psql=C:\\postgresql\\pgsql\\bin\\psql.exe\r\n" + 
							"set PGPASSWORD=gr^&^&n\r\n" + 
							"set workdir=%cd%\r\n" + 
							"set logdir=%cd%\\log\r\n" + 
							"set repo_root_dir=%cd%\\..\\..\\..\r\n" + 
							"set scriptdir=%repo_root_dir%\\datalake\r\n" + 
							"set build_alls_dir=%repo_root_dir%\\tools\\run_psql\\build_alls\r\n" + 
							"\r\n" + 
							"set /P CONFIRM=Do you want to run psql? (Y/N)\r\n" + 
							"if \"%CONFIRM%\"==\"Y\" ( GOTO Confirmed ) else ( GOTO NotConfirmed ) \r\n" + 
							"REM GOTO End\r\n" + 
							"\r\n" + 
							":Confirmed\r\n" + 
							"REM echo StartPoint\r\n" + 
							"REM set /P PGPASSWORD=Enter PROD datalake password\r\n" + 
							"cd %scriptdir%\r\n" + 
							"%psql% -d bdwh -h ora2gp.replycloud.prv -p 5432 -U gpadmin -f %build_alls_dir%\\"+baseSqlFile.getName()+" > %logdir%\\c"+nameNoExtension+".log"+" 2>&1\r\n" + 
							"GOTO End\r\n" + 
							"\r\n" + 
							":NotConfirmed\r\n" + 
							"echo Operation not confirmed\r\n" + 
							"REM exit\r\n" + 
							"\r\n" + 
							":End\r\n" + 
							"REM echo End\r\n" + 
							"cd %workdir%");
						logger.trace("Bat to compile in DEV printed at\t"+batDEV.getAbsolutePath());
				}catch (IOException e) {
					throw new RuntimeException("Exception raised while writing bat file", e);
				}
			}
			File batPROD=new File(batFilePROD);
			if(!batPROD.exists()) {
				try(FileWriter fw=new FileWriter(batPROD);
					BufferedWriter bw = new BufferedWriter(fw);
					PrintWriter print = new PrintWriter(bw)){
					print.println("@ECHO OFF\r\n" + 
							"set psql=C:\\postgresql\\pgsql\\bin\\psql.exe\r\n" + 
							"REM set PGPASSWORD=gr^&^&n\r\n" + 
							"set workdir=%cd%\r\n" + 
							"set logdir=%cd%\\log\r\n" + 
							"set repo_root_dir=%cd%\\..\\..\\..\r\n" + 
							"set scriptdir=%repo_root_dir%\\datalake\r\n" + 
							"set build_alls_dir=%repo_root_dir%\\tools\\run_psql\\build_alls\r\n" + 
							"\r\n" + 
							"set /P CONFIRM=Do you want to run psql? (Y/N)\r\n" + 
							"if \"%CONFIRM%\"==\"Y\" ( GOTO Confirmed ) else ( GOTO NotConfirmed ) \r\n" + 
							"REM GOTO End\r\n" + 
							"\r\n" + 
							":Confirmed\r\n" + 
							"REM echo StartPoint\r\n" + 
							"set /P PGPASSWORD=Enter PROD datalake password\r\n" + 
							"cd %scriptdir%\r\n" + 
							"%psql% -d datalake -h 10.163.114.7 -p 5432 -U a011401 -f %build_alls_dir%\\"+baseSqlFile.getName()+" > %logdir%\\"+nameNoExtension+"_PROD.log"+" 2>&1\r\n" + 
							"GOTO End\r\n" + 
							"\r\n" + 
							":NotConfirmed\r\n" + 
							"echo Operation not confirmed\r\n" + 
							"REM exit\r\n" + 
							"\r\n" + 
							":End\r\n" + 
							"REM echo End\r\n" + 
							"cd %workdir%\r\n" + 
							"\r\n" + 
							"");
						logger.trace("Bat to compile in PROD printed at\t"+batDEV.getAbsolutePath());
				}catch (IOException e) {
					throw new RuntimeException("Exception raised while writing bat file", e);
				}
			}
		}
	}
	
	public void moveFileWrap(File file, String newPath) {
		logger.trace("Moving file\t"+file.getName()+"from\t"+file.getAbsolutePath()+"\nto\t "+newPath);
		Path newFilePath=FileSystems.getDefault().getPath(newPath);
		String fileArleadyExists=null;
		String movementResult=null;
		String objectName=SQLSearcher.detectName(file);
		if(objectName!=null && (SQLSearcher.isTableDef(file) || SQLSearcher.isFunctionDef(file) || SQLSearcher.isViewDef(file)) && !detectMigrated(objectName)) {
			logger.warn("Object\t"+SQLSearcher.detectName(file)+"\tdefined in\t"+file+"\thas not been marked as migrated or has been missed. Pls check "+Initializer.getProperty("excelDocument")+" for more info");
			movementResult="FAIL";
			fileArleadyExists="NOT_MIGRATED/UNRECOGNISED";
		}else { 
			SQLTypes type=SQLSearcher.detectType(file);
			switch(type) {
				case FUNCTION:
				case VIEW:
				case TABLE:
				case SEQUENCE:
					if(repositorySearcher.quickSearchFor(file).size()==0) {
						try {
						Files.copy(file.toPath(), newFilePath);
						fileArleadyExists="NEW";
						movementResult="OK";
						}catch(FileAlreadyExistsException e) {
							fileArleadyExists="ARLEADY_EXISTS";
							movementResult="FAIL";
						}catch (IOException e) {
							logger.error("failed to move the file "+file.getName()+" to "+newPath, e);
							fileArleadyExists="";
							movementResult="FAIL";
						}
					}else {
						logger.trace("A file which defines the same object as\t"+file.getName()+"\talready exists. Original file:\t"+file.getAbsolutePath());
						fileArleadyExists="EQUIVALENT_EXISTS";
						movementResult="FAIL";
					}
					break;
				case INDEX:
				case CONSTRAINT:
					try {
						Files.copy(file.toPath(), newFilePath);
						fileArleadyExists="NEW";
						movementResult="OK";
						}catch(FileAlreadyExistsException e) {
							fileArleadyExists="ARLEADY_EXISTS";
							movementResult="FAIL";
						}catch (IOException e) {
							logger.error("failed to move the file "+file.getName()+" to "+newPath, e);
							fileArleadyExists="";
							movementResult="FAIL";
						}
					break;
				default:
					logger.error("Type of object\t"+SQLSearcher.detectName(file)+"\tdefined in\t"+file+"\thas not been recognised");
					movementResult="FAIL";
					fileArleadyExists="TYPE_OBJECT_UNKNOWN";
					break;
			}
			
		}
		this.compileIstructionPrinter.addResultToPrintList(file, this.sinteticResultPrinter.addResultToPrintList(objectName, file.getAbsolutePath(), newPath, "OK".equals(movementResult), fileArleadyExists));
//		ResultOfMovement result=this.sinteticResultPrinter.addResultToPrintList(objectName, file.getAbsolutePath(), newPath, "OK".equals(movementResult), fileArleadyExists);
//		String writeInAnyCaseString=Initializer.getProperty("CompleteCompileList",true);
//    	Boolean writeInAnyCase=writeInAnyCaseString!=null && Boolean.parseBoolean(writeInAnyCaseString);
//		if(result.isFileHasBeenMoved() || (writeInAnyCase && (fileArleadyExists.equals("ARLEADY_EXISTS") || fileArleadyExists.equals("EQUIVALENT_EXISTS")))) {
//			this.compileIstructionPrinter.addResultToPrintList(file,newPath);
//		}
		
		
	/*	try(FileWriter fw=new FileWriter(movementsRegisterPath, true);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter print = new PrintWriter(bw);
			FileWriter fw2=new FileWriter(this.create_all_file+".appendOnly", true);
				BufferedWriter bw2 = new BufferedWriter(fw2);
				PrintWriter printCompile_appOnly = new PrintWriter(bw2);
			FileWriter fw3=new FileWriter(this.create_all_file+".normal", true);
				BufferedWriter bw3 = new BufferedWriter(fw3);
				PrintWriter printCompile_normal = new PrintWriter(bw3);
			FileWriter fw4=new FileWriter(this.create_all_file+".appendOnly", true);
				BufferedWriter bw4 = new BufferedWriter(fw4);
				PrintWriter printCompile = new PrintWriter(bw4);
			){
			String objName;
			try{
				objName=SQLSearcher.detectName(file);
			}catch(Exception e) {
				logger.warn("intercepted exception "+e);
				e.printStackTrace();
				objName="";
			}
			logger.info("Result of the move operation:\t"+objName+"\t"+file.getAbsolutePath()+"\tmove to\t"+newPath+"\tresult:\t"+movementResult+"\t"+fileArleadyExists);
			print.println(objName+"\t"+file.getAbsolutePath()+"\tmove to\t"+newPath+"\t"+movementResult+"\t"+fileArleadyExists);
			if("OK".equals(movementResult)) {
				String relPath=newPath.replaceFirst(".*datalake", ".").replace("\\", "/");
				SQLTypes sqltype=SQLSearcher.detectType(file);
				switch(sqltype) {
				case TABLE:
					if(tableSizeMap==null) {
						String loadFile=Initializer.getProperty("LoadProperties_move");
						Properties prop=new Properties();
						prop.load(new FileReader(loadFile));
						File sizeFile=new File(prop.getProperty("QlinkExtractionTableSize"));
						Loader load=new LoaderByColumnIndex(sizeFile, "Loader.useColumnsAt=3\r\n" + 
																  	  "Loader.useSheetAt=0\r\n" + 
																  	  "Loader.usePKAt=0,1");
						try {
							tableSizeMap=load.loadMap();
						} catch (EncryptedDocumentException | InvalidFormatException e) {
							logger.error("Failed to load properly map "+sizeFile.getAbsolutePath(), e);
							e.printStackTrace();
						}
					}
					if(inverseMap==null) {
						Map<String, List<String>> invMap=new LoaderSchemaMap().loadInverseMap();
						inverseMap=invMap;
					}
					Boolean appendOnly=Boolean.parseBoolean(tableSizeMap.get(inverseMap.get(objectName).get(0))[0]);
					if(appendOnly) {
						printCompile_appOnly.println("\\echo "+relPath+" :\r\n\\i "+relPath);
					}else {
						printCompile_normal.println("\\echo "+relPath+" :\r\n\\i "+relPath);
					}
					break;
				case CONSTRAINT:
					break;
				case SEQUENCE:
					//TODO complete
					break;
				case INDEX:
					break;
				case FUNCTION:
				case VIEW:
					printCompile.println("\\echo "+relPath+" :\r\n\\i "+relPath);
					break;
				default:
					break;
				}
			
				//generating bat (1 time only)
				//printBatFile(new File(this.create_all_file));
			}
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while writing to a file. Possibly "+movementsRegisterPath+"\\listFileMoved_"+Calendar.getInstance(TimeZone.getDefault()).getTime()+".txt", e);
		}
		*/	
		logger.trace("Moved ended");
	}
	
	/**
	 * questions the excel document to understand if the object is recorded and marked to be migrated
	 * @param objectName
	 * @return
	 */
	public boolean detectMigrated(String objectName) {
		if(this.migrated==null) {
			this.loader=new LoaderMultipleSheetsDBExcel(new File(Initializer.getProperty("LoadProperties_move")));
//			Map<String, String[]> migrated;
			try {
				this.migrated=loader.loadMap();
				Map<String, String[]> tmp=new HashMap<>();
				Iterator<String> iter=this.migrated.keySet().iterator();
				while(iter.hasNext()) {
					String PKey=iter.next();
					String[] val=this.migrated.get(PKey);
					if(val.length>2 && !val[2].trim().equals("")) {
						tmp.put(StringUtilities.formatObjectName(val[0]+"."+val[2]),new String[0]);
					}else {
						tmp.put(StringUtilities.formatObjectName(val[0]+"."+val[1]),new String[0]);
					}
				}
				this.migrated=tmp;
			} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
				logger.error("Intercepted exception while trying to read the excel document. Loaded empty map instead", e);
				e.printStackTrace();
				this.migrated=new HashMap<>();
			}
		}
		int indexPoint=objectName.indexOf(".");
		String checkedObjectName;
		if(indexPoint==-1) {
			logger.warn("object "+objectName+" is not fully qualyfied, we do not modify it. High probability of failure");
			checkedObjectName=objectName;
		}else {
			String schema=objectName.substring(0,indexPoint).toLowerCase();
			String object=objectName.substring(indexPoint+1).toUpperCase();
			checkedObjectName=schema+"."+object;
		}
		return this.migrated.containsKey(checkedObjectName);
	}
	
	/**
	 * metodo che deve scorrere i files da analizzare e richiama i vari metodi necessari
	 * @param path
	 */
	private void goThrough(String path) {
		File folder = new File(path);
		if(!folder.isDirectory()) {
			if(folder.isFile() && checkFile(folder)) {
				File file=folder;
				folder=null;
				//rf.accept(new MoveFile(), file);
				move(file, rf);	
			}
		}else if(folder.isDirectory() && checkFolder(folder)){
			File[] listOfFiles=loadFiles(folder);
			for (File file : listOfFiles) {
				goThrough(file.getAbsolutePath());
			}
		}
	}
	
	boolean move(File file, RuledFolder rf) {
		boolean fileHasBeenMoved=false;
		if(rf.accept(new CheckPosition(file))) {
			//this directory is ok for file
			//prioritize subdirectories:
			for(RuledFolder rfItem:((BasicRuledFolder)rf).subdirectories) {
				//logger.trace("looking into subdirectory "+rfItem.getPath());
				fileHasBeenMoved=move(file, rfItem);
				if(fileHasBeenMoved) {
					break;
				}
			}
			if(!fileHasBeenMoved){
				//file is supposed to be here but there is not a single subdirectory that should contain it.
				String newPath=rf.getPath()+"\\"+file.getName();
				moveFileWrap(file, newPath);
				fileHasBeenMoved=true;
			}
			return fileHasBeenMoved;
		}else{
			//this directory is NOT ok for file
			return false;
		}
		
	}
	
	public static void main(String[] args) throws SecurityException, IOException, URISyntaxException {

		/*test folder outside repository*/
//		String sql_path="C:\\Users\\g.barri\\Desktop\\wrk\\XXDWHAPP_MP_ST_CAUSALI_MDR";
		String sql_path=Initializer.getProperty("workingDir");

//		repositoryRootAbsPath="C:\\Users\\g.barri\\Documents\\FCA_technology\\repository-FCA\\Greenplum\\datalake";
		repositoryRootAbsPath=Initializer.getProperty("localRepositoryDir");
//		String excellPath="C:\\Users\\g.barri\\Desktop\\test\\data";

		logger.info("Starting main of "+Move.class);
		Move move=new Move();
		move.rf=new EDWP2().build(repositoryRootAbsPath);
		move.repositorySearcher=RepoSearcher.getRepoSearcher();
		//TODO da fare anche questo
//		File runningFolder=Initializer.getRunningFolder().toFile();		
		String movementsRegisterPath=Initializer.getProperty("localRepositoryDir").replace("\\datalake", "\\tools\\javaPostProcessing\\log_move")+"\\listFile_"+new SimpleDateFormat("dd-MM-yy_HH-mm-ss").format(new Date())+".csv";
		move.sinteticResultPrinter=new SinteticResultPrinter(movementsRegisterPath);
		String create_all_file=Initializer.getProperty("localRepositoryDir").replace("\\datalake", "\\tools\\run_psql\\build_alls")+"\\create_all_"+new SimpleDateFormat("dd-MM-yy_HH-mm-ss").format(new Date())+".sql";
		move.compileIstructionPrinter=new CompileIstructionPrinter(create_all_file);
		move.goThrough(sql_path);
		//printing results
		move.sinteticResultPrinter.print();
		move.compileIstructionPrinter.print();
		logger.info("Ending main of "+Move.class);
	}
	
	

}
