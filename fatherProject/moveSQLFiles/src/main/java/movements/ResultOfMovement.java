package movements;

public class ResultOfMovement {
	private String objectName;
	private String originalFilePath;
	private String newFilePath;
	private boolean fileHasBeenMoved;
	private String note;
	public ResultOfMovement(String objectName, String originalFilePath, String newFilePath, boolean fileHasBeenMoved, String note) {
		super();
		this.objectName=objectName;
		this.originalFilePath = originalFilePath;
		this.newFilePath = newFilePath;
		this.fileHasBeenMoved = fileHasBeenMoved;
		this.note = note;
	}
	
	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public String getOriginalFilePath() {
		return originalFilePath;
	}
	public void setOriginalFilePath(String originalFilePath) {
		this.originalFilePath = originalFilePath;
	}
	public String getNewFilePath() {
		return newFilePath;
	}
	public void setNewFilePath(String newFilePath) {
		this.newFilePath = newFilePath;
	}
	public boolean isFileHasBeenMoved() {
		return fileHasBeenMoved;
	}
	public void setFileHasBeenMoved(boolean fileHasBeenMoved) {
		this.fileHasBeenMoved = fileHasBeenMoved;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	@Override
	public String toString() {
//		return getObjectName()+"\t"+getOriginalFilePath()+"\t"+getNewFilePath()+"\t"+(isFileHasBeenMoved()?"OK":"FAIL")+"\t"+getNote();
		return getObjectName()+";"+getOriginalFilePath()+";"+getNewFilePath()+";"+(isFileHasBeenMoved()?"OK":"FAIL")+";"+getNote();
	}
	
	public static String getColumns() {
		return "ObjectName"+";"+"FormerPosition"+";"+"NewPosition"+";"+"resultOftheCopy"+";"+"Note";
	}
	
}
