package movements;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.tinylog.Logger;

import loaders.*;
import schemaMapping.LoaderSchemaMap;
import utilities.Initializer;
import utilities.SQLSearcher;
import utilities.SQLSearcher.SQLTypes;

public class SinteticResultPrinter {
	private File output;
	private List<ResultOfMovement> results;
	
	public SinteticResultPrinter(File output) {
		this.output=output;
		results=new ArrayList<>();
	}
	
	public File getOutput() {
		return output;
	}
	
	public SinteticResultPrinter(String output) {
		this(new File(output));
	}
	
    public ResultOfMovement addResultToPrintList(String objectName, String originalFilePath, String newFilePath, boolean fileHasBeenMoved, String note) {
    	ResultOfMovement res=new ResultOfMovement(objectName,  originalFilePath,  newFilePath,  fileHasBeenMoved,  note);
    	results.add(res);
    	return res;
    }
    
    public void print() {
    	String text="";
    	for(int i=0; i<results.size(); i++) {
    		text+=results.get(i).toString()+"\n";
    	}
		try(FileWriter fw=new FileWriter(output);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter print = new PrintWriter(bw);)
		{
			text=ResultOfMovement.getColumns()+"\n"+text;
			print.print(text);
			Logger.info("syntetic results of movements printed at\t"+output.getAbsolutePath());
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while writing to "+output.getAbsolutePath(), e);
		}
    }
}

