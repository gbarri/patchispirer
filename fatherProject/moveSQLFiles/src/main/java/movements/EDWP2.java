package movements;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dirTree.*;
import dirTree.FunctionsFolder;
import dirTree.SchemaFolder;
import dirTree.TablesFolder;
import dirTree.ViewsFolder;
import utilities.Initializer;

public class EDWP2 {
	static Logger logger=LoggerFactory.getLogger(EDWP2.class);
	private void addNewSchema(BasicRuledFolder folder, String relPath, String schema_name) {
		boolean flag=true;
		for(BasicRuledFolder subfold:folder.subdirectories) {
			if(subfold.getNameFolder().equals(relPath)) {
				logger.warn("there is arleady a folder with the same name in folder "+folder.absPath);
				flag=false;
			}
			if(subfold instanceof SchemaFolder) {
				SchemaFolder schemasubsfold=(SchemaFolder) subfold;
				if(schemasubsfold.getSchemaName().equals(schema_name)) {
					logger.warn("there is arleady a folder corresponding to schema "+schema_name+" in the same folder "+folder.absPath);
					flag=false;
				}
			}
		}
		if(flag) {
			SchemaFolder schemaFolder=new SchemaFolder(folder, relPath, schema_name);
			createIfNotExistent(new File(schemaFolder.absPath));
			ViewsFolder view=new ViewsFolder(schemaFolder, "views");
			createIfNotExistent(new File(view.absPath));
			TablesFolder tables=new TablesFolder(schemaFolder, "tables");
			createIfNotExistent(new File(tables.absPath));
			ConstraintsFolder cns=new ConstraintsFolder(tables, "cns");
			createIfNotExistent(new File(cns.absPath));
			IndexesFolder idx=new IndexesFolder(tables, "idx");
			createIfNotExistent(new File(idx.absPath));
			FunctionsFolder functions=new FunctionsFolder(schemaFolder, "functions");
			createIfNotExistent(new File(functions.absPath));
			SequencesFolder sequences=new SequencesFolder(schemaFolder, "sequences");
			createIfNotExistent(new File(sequences.absPath));
		}
	}
	
	private void createIfNotExistent(File folder) {
		if(!folder.exists()) {
			folder.mkdir();
			File infoFile=new File(folder.getAbsoluteFile()+"\\info");
		try(FileWriter fw=new FileWriter(infoFile, true);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter print = new PrintWriter(bw)){
				print.println(folder.getName());
			}catch (IOException e) {
				logger.error("Exception caught while creating the new directory "+folder.getAbsolutePath());
			}
		}
	}
	
	private void addNewSchema(BasicRuledFolder folder, String schema_name) {
		addNewSchema(folder, schema_name, schema_name);
	}
	
	/**
	 * costruisce la struttura di EDWP
	 * @return
	 */
	public BasicRuledFolder build(String RootAbsPath) {
		//root
		BasicRuledFolder EDWP2=new BasicRuledFolder(RootAbsPath);
		//adding schema
//		SchemaFolder tl_scm_ind=new SchemaFolder(EDWP2, "tl_scm_ind", "tl_scm_ind");
//		SchemaFolder tl_pur=new SchemaFolder(EDWP2, "tl_pur", "tl_pur");
//		SchemaFolder tl_mfg=new SchemaFolder(EDWP2, "tl_mfg", "tl_mfg");
//		SchemaFolder tl_analytics=new SchemaFolder(EDWP2, "tl_analytics", "tl_analytics");
//		SchemaFolder oracompat=new SchemaFolder(EDWP2, "oracompat", "oracompat");
//		SchemaFolder lg_mfg=new SchemaFolder(EDWP2, "lg_mfg", "lg_mfg");
//		SchemaFolder ds_mfg=new SchemaFolder(EDWP2, "ds_mfg", "ds_mfg");
//		SchemaFolder dg_utilities=new SchemaFolder(EDWP2, "dg_utilities", "dg_utilities");
//		SchemaFolder bl_scm_ind=new SchemaFolder(EDWP2, "bl_scm_ind", "bl_scm_ind");
//		SchemaFolder bl_mfg=new SchemaFolder(EDWP2, "bl_mfg", "bl_mfg");
//adding views, tables, functions
		//tls_scm_ind
//		ViewsFolder views=new ViewsFolder(tl_scm_ind, "views");
//		TablesFolder tables=new TablesFolder(tl_scm_ind, "tables");
//		FunctionsFolder function=new FunctionsFolder(tl_scm_ind, "functions");
//		//bl_mfg
//		views=new ViewsFolder(bl_mfg, "views");
//		tables=new TablesFolder(bl_mfg, "tables");
//		function=new FunctionsFolder(bl_mfg, "functions");
//		//bl_scm_ind
//		views=new ViewsFolder(bl_scm_ind, "views");
//		tables=new TablesFolder(bl_scm_ind, "tables");
//		function=new FunctionsFolder(bl_scm_ind, "functions");
//		//dg_utilities
//		tables=new TablesFolder(dg_utilities, "tables");
//		function=new FunctionsFolder(dg_utilities, "functions");
//		//ds_mfg
//		tables=new TablesFolder(ds_mfg, "tables");
//		//lg_mfg
//		function=new FunctionsFolder(lg_mfg, "functions");
//		//oracompat
//		function=new FunctionsFolder(oracompat, "functions");
//		//tl_analyitcs
//		views=new ViewsFolder(tl_analytics, "views");
//		tables=new TablesFolder(tl_analytics, "tables");
//		function=new FunctionsFolder(tl_analytics, "function");
//		
//		//tl_mfg
//		views=new ViewsFolder(tl_mfg, "views");
//		tables=new TablesFolder(tl_mfg, "tables");
//		//manca other
//		function=new FunctionsFolder(tl_mfg, "functions");
//		//tl_pur
//		views=new ViewsFolder(tl_pur, "views");
//		tables=new TablesFolder(tl_pur, "tables");
//		function=new FunctionsFolder(tl_pur, "functions");

//		//added new schemas
//		addNewSchema(EDWP2, "tl_scm_com");
//		addNewSchema(EDWP2, "tl_thp");
//		addNewSchema(EDWP2, "tl_sal");
//		addNewSchema(EDWP2, "tl_qlt");
//		addNewSchema(EDWP2, "tl_fin");
//		addNewSchema(EDWP2, "pl_colors");
//		addNewSchema(EDWP2, "bl_sal");
//		addNewSchema(EDWP2, "tl_afs");
//		addNewSchema(EDWP2, "tl_net");
//		addNewSchema(EDWP2, "tl_scm_ind");
//		addNewSchema(EDWP2, "tl_pur");
//		addNewSchema(EDWP2, "tl_mfg");
//		addNewSchema(EDWP2, "tl_analytics");
//		addNewSchema(EDWP2, "oracompat");
////		addNewSchema(EDWP2, "lg_mfg");
//		addNewSchema(EDWP2, "ds_mfg");
//		addNewSchema(EDWP2, "dg_utilities");
//		addNewSchema(EDWP2, "bl_scm_ind");
//		addNewSchema(EDWP2, "bl_scm_com");
//		addNewSchema(EDWP2, "bl_mfg");
//		addNewSchema(EDWP2, "cl_analytics");
		
		String fileName=Initializer.getProperty("ListaSchema");
		File file=new File(fileName);
		try(FileInputStream fileStream=new FileInputStream(file.getAbsolutePath());
				Scanner scann=new Scanner(new BufferedInputStream(fileStream))){
			while(scann.hasNextLine()) {
				String nextSchema=scann.nextLine();
				if(nextSchema.trim().length()>0) {
					addNewSchema(EDWP2, nextSchema);
				}
			}
		}catch(IOException e) {
			throw new RuntimeException("Exception raised while reading for\t"+file.getAbsolutePath(), e);
		}

		return EDWP2;
	}

}
