package movements;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import loaders.*;

import schemaMapping.LoaderSchemaMap;
import utilities.Initializer;
import utilities.SQLSearcher;
import utilities.StringUtilities;
import utilities.SQLSearcher.SQLTypes;

public class CompileIstructionPrinter {
	static Logger logger=LoggerFactory.getLogger(CompileIstructionPrinter.class);
	private File output;
//	private Map<Path, String> appendOnlyTables;
//	private Map<Path, String> heapTables;
//	private Map<Path, String> sequences;
//	private Map<Path, String> functions;
//	private Map<Path, String> views;
//	private Map<Path, String> other;
	private PriorityQueue<CouplePathSchema> appendOnlyTables;
	private PriorityQueue<CouplePathSchema> heapTables;
	private PriorityQueue<CouplePathSchema> sequences;
	private PriorityQueue<CouplePathSchema> functions;
	private PriorityQueue<CouplePathSchema> views;
	private List<Path> other;
	private Map<String, List<String>> inverseMap;
	private Map<String, Boolean> tableSizeMap;
	private Map<String, String[]> externalTablesList;
	
	public CompileIstructionPrinter(File output) {
		this.output=output;
		appendOnlyTables=new PriorityQueue<>();
		heapTables=new PriorityQueue<>();
		sequences=new PriorityQueue<>();
		functions=new PriorityQueue<>();
		views=new PriorityQueue<>();
		other=new ArrayList<>();
		String sizeFilePath=Initializer.getProperty("QlinkExtractionTableSize");
		if(sizeFilePath==null) {
			logger.error("Unable to load list of append only tables");
			throw new IllegalArgumentException("Unable to load list of append only tables");
		}
		File sizeFile=new File(sizeFilePath);
		Loader load=new LoaderByColumnIndex(sizeFile, "Loader.useColumnsAt=3\r\n" + 
												  	  "Loader.useSheetAt=0\r\n" + 
												  	  "Loader.usePKAt=0,1");
		Map<String, String[]> tmpMap=new HashMap<>();
		try {
			tmpMap=load.loadMap();
		} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
			logger.error("Failed to load properly map "+sizeFile.getAbsolutePath(), e);
			e.printStackTrace();
		}
		tableSizeMap=new HashMap<>();
		Iterator<String> iter=tmpMap.keySet().iterator();
		while(iter.hasNext()) {
			String key=iter.next();
			String val=tmpMap.get(key)[0];
			try {
				Boolean valNum=Boolean.parseBoolean(val);
				tableSizeMap.put(key, valNum);
			}catch (NumberFormatException e) {
				logger.error("entry\t"+key+"\tin excel\t"+sizeFile.getAbsolutePath()+"\tsi not a parsable integer\t"+val+"\tLine is skypped");
			}
		}
		inverseMap=new LoaderSchemaMap().loadInverseMap();
		
	}
	
	public CompileIstructionPrinter(String output) {
		this(new File(output));
	}
	
    public void addResultToPrintList(File ddl, String objectName, String newFilePath) {
		SQLTypes sqltype=SQLSearcher.detectType(ddl);
		Path relPath=new File(newFilePath).toPath();
		if(sqltype==null) {
			other.add(relPath);
			logger.error(ddl.getName()+" has been added to other because its type was not found. "+ddl.getAbsolutePath());
			return;
		}
		String objectSchema=StringUtilities.detectSchemaInObjectName(objectName);
		switch(sqltype) {
		case TABLE:
			if(objectName==null) {
				other.add(relPath);
				logger.error(ddl.getName()+(" has been added to other because its name was not found. "+ddl.getAbsolutePath()));
				break;
			}
			int ind=objectName.indexOf(".");
			if(ind>=0) {
				String schema=objectName.substring(0, ind).toLowerCase();
				String obj=objectName.substring(ind+1).toUpperCase();
				objectName=schema+"."+obj;
			}
			if(isExternalTable(objectName)) {
				break;
			}
			List<String> oracleObjects=inverseMap.get(objectName);
			if(oracleObjects==null || oracleObjects.size()==0) {
				other.add(relPath);
				logger.error(ddl.getName()+(" has been added to other because its former Oracle name was not found. "+ddl.getAbsolutePath()));
				break;
			}
			Boolean appendOnly=false;
			for(String oracleTab:oracleObjects) {
				Boolean appOnlyOracle=this.tableSizeMap.get(oracleTab);
				if(appOnlyOracle!=null) {
				appendOnly=appendOnly||appOnlyOracle;
				}else {
					logger.warn("Tried to check if "+oracleTab+", now "+objectName+" has to be append only, but it is not in the list of table sizes");
				}
			}
			if(appendOnly) {
				this.appendOnlyTables.add(new CouplePathSchema(relPath, objectSchema));
			}else {
				this.heapTables.add(new CouplePathSchema(relPath, objectSchema));
			}
			break;
		case CONSTRAINT:
			break;
		case SEQUENCE:
			this.sequences.add(new CouplePathSchema(relPath, objectSchema));
			break;
		case INDEX:
			break;
		case FUNCTION:
			this.functions.add(new CouplePathSchema(relPath, objectSchema));
			break;
		case VIEW:
			this.views.add(new CouplePathSchema(relPath, objectSchema));
			break;
		default:
			this.other.add(relPath);
			logger.error(ddl.getName()+(" has been added to other because its type was not among those classifies. "+ddl.getAbsolutePath()));
			break;
		}	
    }
    
    public void addResultToPrintList(File ddl, ResultOfMovement result) {
//    	String writeInAnyCaseString=Initializer.getProperty("CompleteCompileList",true);
//    	Boolean writeInAnyCase=writeInAnyCaseString!=null && Boolean.parseBoolean(writeInAnyCaseString);
    	if(result.isFileHasBeenMoved()) {
    		addResultToPrintList(ddl, result.getObjectName(),result.getNewFilePath());
    	}
    }
    
    private String PathToString(String[] currentSchema, CouplePathSchema absPathSchema, Path datalake) {
    	String out="";
    	if(!StringUtils.equalsIgnoreCase(currentSchema[0], absPathSchema.getSchema())) {
    		String newSchema=absPathSchema.getSchema().toLowerCase();
    		currentSchema[0]=newSchema;
    		out="\\echo SET search_path TO "+newSchema+", oracompat, dg_utilities;\r\n"
    		   +"      SET search_path TO "+newSchema+", oracompat, dg_utilities;\r\n";
    	}
    	Path absPath=absPathSchema.getPath();
    	Path relNewPath=datalake.relativize(absPath);
    	String relNewPathString=relNewPath.toString().replace("\\", "/");
    	return out
    			  +"\\echo ./"+relNewPathString+":\r\n"
    		      +"\\i    ./"+relNewPathString+" \r\n";
    }
    
    private String PathToString(Path absPath, Path datalake) {
    	String out="";
    	Path relNewPath=datalake.relativize(absPath);
    	String relNewPathString=relNewPath.toString().replace("\\", "/");
    	return out
    			  +"\\echo ./"+relNewPathString+":\r\n"
    		      +"\\i    ./"+relNewPathString+" \r\n";
    }
    
    public void print() {
    	String text="";
    	Path datalake=new File(Initializer.getProperty("localRepositoryDir")).toPath();
    	text+="\\echo set owner to bigdwh_role\r\n"
    			+ "     set session role bigdwh_role;\r\n"  
    			+ "\r\n"
    			+ "--heap tables\r\n";
    	String[] currentSchema= {""};
    	while(!heapTables.isEmpty()) {
    		text+=PathToString(currentSchema, heapTables.poll(), datalake);
    	}
    	text+="--append only tables\r\n";
//    	text+="set gp_default_storage_options = 'appendonly=true,blocksize=32768,compresstype=zlib,compresslevel=3,checksum=true,orientation=row';\r\n" + 
//    			"show gp_default_storage_options;\r\n";
    	while(!appendOnlyTables.isEmpty()) {
    		text+=PathToString(currentSchema, appendOnlyTables.poll(), datalake);
    	}
    	text+="--sequence\r\n";
    	while(!sequences.isEmpty()) {
    		text+=PathToString(currentSchema, sequences.poll(), datalake);
    	}
    	text+="--views\r\n";
    	while(!views.isEmpty()) {
    		text+=PathToString(currentSchema, views.poll(), datalake);
    	}
    	text+="--functions\r\n";
    	while(!functions.isEmpty()) {
    		text+=PathToString(currentSchema, functions.poll(), datalake);
    	}
    	text+="--other (Objects we failed to classify. Must be checked beforehand)\r\n";
    	for(int i=0; i<other.size(); i++) {
    		text+=PathToString(other.get(i), datalake);
    	}
		try(FileWriter fw=new FileWriter(output);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter print = new PrintWriter(bw);)
		{
			print.print(text);
			logger.trace("Compile statements printed at\t"+	output.getAbsolutePath());
		}catch (IOException e) {
			throw new RuntimeException("Exception raised while writing to "+output.getAbsolutePath(), e);
		}
    }

	public File getOutput() {
		return output;
	}
	
	public boolean isExternalTable(String nameObject) {
		if(externalTablesList==null) {
			int sheetNumber;
			String excel=Initializer.getProperty("excelDocument");
			Workbook workbook=null;
			try {
				workbook=WorkbookFactory.create(new File(excel));
				sheetNumber=workbook.getSheetIndex("External Table");
				Loader loader=new LoaderByColumnIndexesDBExcel("Loader.useColumnsAt=5\r\n" + 
					  	  "Loader.useSheetAt="+sheetNumber+"\r\n" + 
					  	  "Loader.usePKAt=4,10");
				externalTablesList=loader.loadMap(workbook);
			} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
				logger.error("An error occored while loading the excel document", e);
				e.printStackTrace();
				
			}
		}
		if(nameObject.contains(".")) {
			String schema=nameObject.substring(0, nameObject.indexOf(".")).toLowerCase();
			String object=nameObject.substring(nameObject.indexOf(".")+1).toUpperCase();
			nameObject=schema+"."+object;
		}
		return externalTablesList.containsKey(nameObject);
	}
	
	protected class CouplePathSchema implements Comparable<CouplePathSchema>{
		private Path path;
		private String schema;
		
		public CouplePathSchema(Path path, String schema) {
			super();
			this.path = path;
			this.schema = schema;
		}

		public Path getPath() {
			return path;
		}

		public String getSchema() {
			return schema;
		}

		@Override
		public int compareTo(CouplePathSchema o) {
			return this.getSchema().compareTo(o.getSchema());
		}
	}
	
	/**
	 * Note: this comparator is not consistent with equals
	 * @author g.barri
	 *
	 */
	protected class OrdererCouple implements Comparator<CouplePathSchema>{

		@Override
		public int compare(CouplePathSchema o1, CouplePathSchema o2) {
			return o1.compareTo(o2);
		}
		
	}
}
