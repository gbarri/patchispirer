package dirTree;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SequencesFolder extends BasicRuledSubfolder{
	
	public SequencesFolder(RuledFolder parent, String relPath) {
		super(parent, relPath);
		// TODO Auto-generated constructor stub
	}
	Logger logger=LoggerFactory.getLogger(FunctionsFolder.class);
	
	@Override
	public boolean accept(Rule rule) {
//		logger.debug("Rule "+rule.getClass()+" accepted by FunctionsFolder "+this.getNameFolder());
		
		return rule.visit(this);
	}
	
	@Override
	public boolean accept(RuleFileDependent rule, File file) {
//		logger.debug("Rule "+rule.getClass()+" accepted by FunctionsFolder "+this.getNameFolder());
		rule.takeThis(file);
		return rule.visit(this);
	}
	
	public String getNameFolder() {
		return nameFolder;
	}

}
