package dirTree;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BasicRuledFolder extends AbstractRuledFolder{
	
	private Logger logger=LoggerFactory.getLogger(BasicRuledFolder.class);
	
	public String nameFolder;
	public List<BasicRuledFolder> subdirectories=new ArrayList<>();
	//public RuledFolder parent;
	
	public BasicRuledFolder(String absPath) {
		this.absPath=absPath;
		String separator=System.getProperty("file.separator");
		this.nameFolder=absPath.substring(absPath.lastIndexOf(separator)+1);
		logger.debug("new folder "+nameFolder+" at \n"+absPath);
	}
	
	@Override
	public boolean accept(Rule rule) {
		return rule.visit(this);
	}
	
	@Override
	public boolean accept(RuleFileDependent rule, File file) {
		rule.takeThis(file);
		return rule.visit(this);
	}
	
	public String getNameFolder() {
		return nameFolder;
	}
	
}
