package dirTree;

import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utilities.SQLSearcher;
import utilities.SQLSearcher.SQLTypes;

/**
 * indica se un file deve stare qui, ovvero in questa directory
 * @author g.barri
 *
 */
public class CheckPosition implements RuleFileDependent {
	
	Logger logger=LoggerFactory.getLogger(CheckPosition.class);
	private File observedFile=null;
	
	public CheckPosition(File file) {
		observedFile=file;
	}
	
	public void takeThis(File file) {
		this.observedFile=file;
	}
	
	@Override
	public boolean visit(ViewsFolder rf) {
		SQLTypes type=SQLSearcher.detectType(observedFile);
		if(type==null||!type.equals(SQLTypes.VIEW)) {
			return false;
		}else {
			return true;
		}
		
	}

	@Override
	public boolean visit(TablesFolder rf) {
		SQLTypes type=SQLSearcher.detectType(observedFile);
		if(type==null||!(type.equals(SQLTypes.TABLE) || type.equals(SQLTypes.INDEX) || type.equals(SQLTypes.CONSTRAINT))) {
			return false;
		}else {
			return true;
		}
	}
	
	@Override
	public boolean visit(SequencesFolder rf) {
		SQLTypes type=SQLSearcher.detectType(observedFile);
		if(type==null||!type.equals(SQLTypes.SEQUENCE)) {
			return false;
		}else {
			return true;
		}
	}
	
	@Override
	public boolean visit(IndexesFolder rf) {
		SQLTypes type=SQLSearcher.detectType(observedFile);
		if(type==null||!type.equals(SQLTypes.INDEX)) {
			return false;
		}else {
			return true;
		}
	}
	
	@Override
	public boolean visit(ConstraintsFolder rf) {
		SQLTypes type=SQLSearcher.detectType(observedFile);
		if(type==null||!type.equals(SQLTypes.CONSTRAINT)) {
			return false;
		}else {
			return true;
		}
	}

	@Override
	public boolean visit(FunctionsFolder rf) {
		SQLTypes type=SQLSearcher.detectType(observedFile);
		if(type==null||!type.equals(SQLTypes.FUNCTION)) {
			return false;
		}else {
			return true;
		}
	}

	@Override
	public boolean visit(BasicRuledFolder rf) {
		//generic folder, no rule applied here. Basically, only the root
		return true;
	}

	@Override
	/**
	 * accetto se contengo il nome della directory + '_'
	 */
	public boolean visit(PrefixFolder rf) {
//		System.out.println(observedFile.getName());
		String name=SQLSearcher.detectName(observedFile);
		if(name==null) {
			logger.warn("il file" +observedFile.getName()+" non � stato riconosciuto come ddl. pls check.\nIl nome viene preso dal nome del file .sql");
			name=observedFile.getName();
		}
//		return name.contains(rf.prefix+"_");
		return StringUtils.containsIgnoreCase(name, rf.prefix+"_") || StringUtils.containsIgnoreCase(name, "_"+rf.prefix);
	}

	@Override
	public boolean visit(SchemaFolder rf) {
		String schema=SQLSearcher.detectSchema(observedFile);
		if(schema!=null &&  StringUtils.equalsIgnoreCase(schema, rf.getSchemaName())) {
			return true;
		}else {
			return false;
		}
	}
	

}
