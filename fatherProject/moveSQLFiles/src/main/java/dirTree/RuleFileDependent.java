package dirTree;

import java.io.File;

public interface RuleFileDependent extends Rule {
	void takeThis(File file);
}
