package dirTree;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractRuledFolder implements RuledFolder{
	
	public String absPath; 
	private Logger logger=LoggerFactory.getLogger(AbstractRuledFolder.class);

	@Override
	public String getPath() {
		return absPath;
	}

}
