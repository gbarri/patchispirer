package dirTree;

import java.io.File;

public class SchemaFolder extends BasicRuledSubfolder{
	
	String schemaName;
	
	public SchemaFolder(RuledFolder parent, String relPath, String schemaName) {
		super(parent, relPath);
		this.schemaName=schemaName;
	}
	
	public SchemaFolder(RuledFolder parent, String schemaName) {
		this(parent, schemaName, schemaName);
	}
	
	public String getSchemaName() {
		return this.schemaName;
	}
	
	@Override
	public boolean accept(Rule rule) {
		//System.out.println(this.getClass());
		
		return rule.visit(this);
	}
	
	@Override
	public boolean accept(RuleFileDependent rule, File file) {
		rule.takeThis(file);
		return rule.visit(this);
	}
	
	public String getNameFolder() {
		return nameFolder;
	}

}
