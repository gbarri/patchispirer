package dirTree;import org.omg.CosNaming.IstringHelper;

public class BasicRuledSubfolder extends BasicRuledFolder {
	
	public RuledFolder parent;

	public BasicRuledSubfolder(RuledFolder parent, String relPath) {
		super(parent.getPath()+"\\"+relPath);
		this.parent=parent;
		if(parent instanceof BasicRuledFolder) {
			((BasicRuledFolder) parent).subdirectories.add(this);
		}
	}

}
