package dirTree;

import java.io.File;

/**
 * a folder in the directory which accepts a certain "rules"
 * @author g.barri
 *
 */
public interface RuledFolder {
	public boolean accept(Rule rule);
	public String getPath();
	boolean accept(RuleFileDependent rule, File file);
}
