package dirTree;

import java.io.File;

/**
 * class destinated to contain only DDL for views
 * @author g.barri
 *
 */
public class ViewsFolder extends BasicRuledSubfolder {

	public ViewsFolder(RuledFolder parent, String relPath) {
		super(parent, relPath);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public boolean accept(Rule rule) {
//		System.out.println(this.getClass());
		return rule.visit(this);
	}
	
	@Override
	public boolean accept(RuleFileDependent rule, File file) {
		rule.takeThis(file);
		return rule.visit(this);
	}
	
	public String getNameFolder() {
		return nameFolder;
	}

}
