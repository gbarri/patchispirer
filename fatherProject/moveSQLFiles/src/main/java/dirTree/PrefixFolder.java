package dirTree;

import java.io.File;

public class PrefixFolder extends BasicRuledSubfolder {
	
	public String prefix=null;
	
	public PrefixFolder(RuledFolder parent, String relPath, String prefix) {
		super(parent, relPath);
		this.prefix=prefix;
	}
	
	public PrefixFolder(RuledFolder parent, String relPath) {
		super(parent, relPath);
		prefix=this.getNameFolder();
	}
	
	@Override
	public boolean accept(Rule rule) {	
		return rule.visit(this);
	}
	
	@Override
	public boolean accept(RuleFileDependent rule, File file) {
		rule.takeThis(file);
		return rule.visit(this);
	}
	
	public String getNameFolder() {
		return nameFolder;
	}

}
