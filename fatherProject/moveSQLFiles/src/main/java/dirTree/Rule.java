package dirTree;
/**
 * represents a rule for a RuledFolder, i.e. indicate which files should be in said folder.
 * Implements pattern visit-visitor
 * @author g.barri
 *
 */
public interface Rule {
	boolean visit(BasicRuledFolder rf);
	boolean visit(ViewsFolder rf);
	boolean visit(TablesFolder rf);
	boolean visit(FunctionsFolder rf);
	boolean visit(PrefixFolder rf);
	boolean visit(SchemaFolder rf);
	boolean visit(IndexesFolder rf);
	boolean visit(SequencesFolder rf);
	boolean visit(ConstraintsFolder rf);

}
